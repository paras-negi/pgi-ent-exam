import React from 'react';
import TestScreen from './pages/TestScreen'
import Instructions from './pages/Instructions';
import Instructions2 from './pages/Instructions2';


class App extends React.Component{
  state={
    route: "instructions"
  }

  onRouteChange = (route) => {
    this.setState({route: route})
  }

  render(){
      return (
        <div>
          {this.state.route === 'instructions' ?
            <Instructions onRouteChange={this.onRouteChange} /> :
            (
                this.state.route === 'instructions2' ?
                  <Instructions2 onRouteChange={this.onRouteChange} />
                  :
                  
                  (this.state.route === 'testpage' ?
                    <TestScreen onRouteChange={this.onRouteChange} />
                    : null)
            )
          }
                 
        </div>
      );
    }

  
}

export default App;
