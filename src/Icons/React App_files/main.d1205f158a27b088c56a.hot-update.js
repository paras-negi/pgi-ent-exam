webpackHotUpdate("main",{

/***/ "./src/pages/TestScreen.js":
/*!*********************************!*\
  !*** ./src/pages/TestScreen.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/Users/prepladder/Desktop/pgi-ent-exam/src/pages/TestScreen.js";


function TestScreen() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "test-page",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "test-page-layout",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "test-page-header",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "header__element",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "header__element header__element--left",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: this
  }, "Static Mock Link for NEET PG Jan 2020 Session"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "header__element header__element--right",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    alt: "img",
    source: "#",
    className: "instruction-icon",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "#",
    className: "instruction-link",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17
    },
    __self: this
  }, "instructions")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "test-page-main-row",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "test-page-col-left",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }, "hjcxgkjasbkj"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "test-page-col-right",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: this
  }, "jklcdhskhckjshysjdjkwndjbskjbsjhh")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "footer-main",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: this
  }, "Version: 2.7.0.0")));
}

/* harmony default export */ __webpack_exports__["default"] = (TestScreen);

/***/ })

})
//# sourceMappingURL=main.d1205f158a27b088c56a.hot-update.js.map