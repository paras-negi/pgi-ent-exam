(this["webpackJsonppgi-ent-exam"] = this["webpackJsonppgi-ent-exam"] || []).push([["main"],{

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./node_modules/resolve-url-loader/index.js?!./node_modules/sass-loader/dist/cjs.js?!./src/styles/main.scss":
/*!**********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-5-1!./node_modules/postcss-loader/src??postcss!./node_modules/resolve-url-loader??ref--6-oneOf-5-3!./node_modules/sass-loader/dist/cjs.js??ref--6-oneOf-5-4!./src/styles/main.scss ***!
  \**********************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "*,\n*:after,\n*:before {\n  margin: 0px;\n  padding: 0px;\n  box-sizing: inherit; }\n\nhtml {\n  font-size: 62.5%; }\n\n.body {\n  box-sizing: border-box;\n  height: 100vh;\n  min-height: 100vh;\n  max-height: 100vh; }\n\n.instr-page-layout {\n  max-width: 136.6rem;\n  margin: 0; }\n  .instr-page-layout .instr-page-header {\n    width: 100%;\n    height: 5rem;\n    background-color: #3b5998;\n    float: left; }\n  .instr-page-layout .main-row {\n    height: calc( 100% - (7.2rem));\n    position: relative; }\n    .instr-page-layout .main-row [class^=\"col\"] {\n      float: left; }\n    .instr-page-layout .main-row .col-left {\n      width: 79%;\n      position: fixed;\n      top: 5rem;\n      left: 0;\n      bottom: 2.2rem;\n      font-size: 3rem; }\n    .instr-page-layout .main-row .col-right {\n      width: 21%;\n      height: calc( 100% - 7.2rem);\n      background: linear-gradient(to bottom, rgba(247, 247, 247, 0) 0%, #e2e1e2 55%, #d4d3d4 91%, #d1cfd1 100%);\n      position: fixed;\n      right: 0;\n      top: 5rem;\n      bottom: 2.2rem;\n      border-left: 1px solid black; }\n  .instr-page-layout .footer-main {\n    position: fixed;\n    bottom: 0;\n    min-width: 100%;\n    font-family: Arial, Helvetica, sans-serif;\n    font-size: 1.1rem;\n    background: #617B8C;\n    color: #FFFFFF;\n    display: block;\n    height: 2.2rem;\n    width: 100%;\n    text-align: center;\n    line-height: 2.2rem; }\n\n.test-page-layout {\n  max-width: 136.6rem;\n  margin: 0; }\n  .test-page-layout .test-page-header {\n    padding: 1rem;\n    width: 100%;\n    height: 3rem;\n    line-height: 3rem; }\n  .test-page-layout .header__element {\n    width: 100%;\n    float: left; }\n    .test-page-layout .header__element--left {\n      float: left;\n      width: 50%; }\n    .test-page-layout .header__element--right {\n      float: right;\n      padding-right: 2rem;\n      width: 50%; }\n  .test-page-layout .footer-main {\n    height: 2.2rem;\n    text-align: center;\n    line-height: 2.2rem;\n    font-family: Arial, Helvetica, sans-serif;\n    font-size: 1.1rem;\n    background: #617B8C;\n    color: #FFFFFF;\n    position: fixed;\n    bottom: 0;\n    min-width: 100%;\n    width: 100%; }\n\n.instructions-heading {\n  height: 4rem;\n  line-height: 4rem;\n  font-size: 1.3rem;\n  font-weight: bold;\n  color: #676568;\n  background: #BCE8F5;\n  padding-left: 1.5rem;\n  font-family: Arial, Helvetica, sans-serif; }\n\n.instruction-row {\n  overflow-y: auto;\n  border-bottom: 1px solid #c8c8c8; }\n  .instruction-row-1 {\n    height: 45rem; }\n  .instruction-row-2 {\n    height: 34rem; }\n  .instruction-row .instruction-paragraph {\n    margin: 0 0 1.6rem 2rem; }\n    .instruction-row .instruction-paragraph u {\n      font-size: 16px;\n      margin: 1.6rem 0; }\n    .instruction-row .instruction-paragraph ol {\n      padding-top: 10px;\n      padding-left: 30px;\n      font-size: 16px; }\n\n.section-rt-main {\n  margin-top: 5rem;\n  height: 32.6rem;\n  width: 100%; }\n  .section-rt-main .candidate-photo {\n    width: auto;\n    height: 100px;\n    margin-bottom: 1rem; }\n\n.main-heading {\n  font-size: 2.5rem;\n  line-height: 40px;\n  font-weight: bold;\n  color: #676568;\n  padding-left: 15px; }\n\nbody {\n  font-family: 'Times New Roman', Times, serif;\n  font-weight: 300; }\n\nh2 {\n  font-size: 1.8rem; }\n\n.candidate-name {\n  font-size: 20px;\n  color: #4f6887;\n  font-weight: bold;\n  text-align: center;\n  font-family: Arial, Helvetica, sans-serif; }\n\n.btn-section {\n  padding: 2rem;\n  color: #252525; }\n  .btn-section .btn {\n    font-family: Arial, Helvetica, sans-serif;\n    border: 1px solid #c8c8c8;\n    border-radius: 2px;\n    cursor: pointer;\n    display: inline-block;\n    font-size: 1.4rem;\n    font-weight: 400;\n    line-height: 39px;\n    padding-left: 20px;\n    padding-right: 20px;\n    text-align: center;\n    vertical-align: middle;\n    white-space: nowrap;\n    margin-bottom: 8px;\n    -webkit-appearance: none; }\n    .btn-section .btn__primary {\n      outline: none;\n      color: #252525; }\n      .btn-section .btn__primary--next {\n        float: right;\n        background-color: #fff; }\n      .btn-section .btn__primary--prev {\n        float: left;\n        background-color: #fff; }\n      .btn-section .btn__primary--blue {\n        background-color: #38aae9;\n        font-size: 1.2rem;\n        border-color: #38aae9;\n        color: #ffffff; }\n    .btn-section .btn:hover {\n      border: 1px solid #333;\n      background-color: #0c7cd5; }\n\n.li-margin-bottom {\n  margin-bottom: 1.6rem; }\n\n.nda {\n  margin: .1rem 0 1.6rem 2rem;\n  border-bottom: 1px solid #c8c8c8;\n  height: 10rem;\n  overflow-y: auto; }\n  .nda-paragraph {\n    font-family: Arial, Helvetica, sans-serif;\n    padding: .4rem 0; }\n    .nda-paragraph-list {\n      font-size: 1.2rem;\n      padding-left: 2rem; }\n  .nda-heading {\n    font-family: Arial, Helvetica, sans-serif;\n    font-size: 1.2rem;\n    padding-left: 10px;\n    float: right; }\n\n.test-page-header {\n  background-color: #363636;\n  font-family: Arial,Helvetica,sans-serif;\n  line-height: 3rem;\n  color: #f7f64e;\n  font-size: 1.2rem; }\n  .test-page-header .header__element {\n    vertical-align: middle; }\n    .test-page-header .header__element .instruction-icon {\n      height: 10px;\n      width: auto; }\n", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles_main_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./styles/main.scss */ "./src/styles/main.scss");
/* harmony import */ var _styles_main_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_styles_main_scss__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _serviceWorker__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./serviceWorker */ "./src/serviceWorker.js");
/* harmony import */ var _pages_TestScreen__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pages/TestScreen */ "./src/pages/TestScreen.js");
var _jsxFileName = "/Users/prepladder/Desktop/pgi-ent-exam/src/index.js";


 // import App from './App';


 // ReactDOM.render(<App />, document.getElementById('root'));

react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.render(react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_pages_TestScreen__WEBPACK_IMPORTED_MODULE_4__["default"], {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 11
  },
  __self: undefined
}), document.getElementById('root')); // If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

_serviceWorker__WEBPACK_IMPORTED_MODULE_3__["unregister"]();

/***/ }),

/***/ "./src/pages/TestScreen.js":
/*!*********************************!*\
  !*** ./src/pages/TestScreen.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/Users/prepladder/Desktop/pgi-ent-exam/src/pages/TestScreen.js";


function TestScreen() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "test-page",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "test-page-layout",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "test-page-header",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "header__element",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "header__element header__element--left",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: this
  }, "Static Mock Link for NEET PG Jan 2020 Session"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "header__element header__element--right",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    alt: "img",
    source: "#",
    className: "instruction-icon",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "#",
    className: "instruction-link",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17
    },
    __self: this
  }, "instructions")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "test-page-main-row",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "test-page-col-left",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }, "hjcxgkjasbkj"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "test-page-col-right",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: this
  }, "jklcdhskhckjshysjdjkwndjbskjbsjhh")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "footer-main",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: this
  }, "Version: 2.7.0.0")));
}

/* harmony default export */ __webpack_exports__["default"] = (TestScreen);

/***/ }),

/***/ "./src/serviceWorker.js":
/*!******************************!*\
  !*** ./src/serviceWorker.js ***!
  \******************************/
/*! exports provided: register, unregister */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "register", function() { return register; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "unregister", function() { return unregister; });
// This optional code is used to register a service worker.
// register() is not called by default.
// This lets the app load faster on subsequent visits in production, and gives
// it offline capabilities. However, it also means that developers (and users)
// will only see deployed updates on subsequent visits to a page, after all the
// existing tabs open on the page have been closed, since previously cached
// resources are updated in the background.
// To learn more about the benefits of this model and instructions on how to
// opt-in, read https://bit.ly/CRA-PWA
const isLocalhost = Boolean(window.location.hostname === 'localhost' || // [::1] is the IPv6 localhost address.
window.location.hostname === '[::1]' || // 127.0.0.0/8 are considered localhost for IPv4.
window.location.hostname.match(/^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/));
function register(config) {
  if (false) {}
}

function registerValidSW(swUrl, config) {
  navigator.serviceWorker.register(swUrl).then(registration => {
    registration.onupdatefound = () => {
      const installingWorker = registration.installing;

      if (installingWorker == null) {
        return;
      }

      installingWorker.onstatechange = () => {
        if (installingWorker.state === 'installed') {
          if (navigator.serviceWorker.controller) {
            // At this point, the updated precached content has been fetched,
            // but the previous service worker will still serve the older
            // content until all client tabs are closed.
            console.log('New content is available and will be used when all ' + 'tabs for this page are closed. See https://bit.ly/CRA-PWA.'); // Execute callback

            if (config && config.onUpdate) {
              config.onUpdate(registration);
            }
          } else {
            // At this point, everything has been precached.
            // It's the perfect time to display a
            // "Content is cached for offline use." message.
            console.log('Content is cached for offline use.'); // Execute callback

            if (config && config.onSuccess) {
              config.onSuccess(registration);
            }
          }
        }
      };
    };
  }).catch(error => {
    console.error('Error during service worker registration:', error);
  });
}

function checkValidServiceWorker(swUrl, config) {
  // Check if the service worker can be found. If it can't reload the page.
  fetch(swUrl, {
    headers: {
      'Service-Worker': 'script'
    }
  }).then(response => {
    // Ensure service worker exists, and that we really are getting a JS file.
    const contentType = response.headers.get('content-type');

    if (response.status === 404 || contentType != null && contentType.indexOf('javascript') === -1) {
      // No service worker found. Probably a different app. Reload the page.
      navigator.serviceWorker.ready.then(registration => {
        registration.unregister().then(() => {
          window.location.reload();
        });
      });
    } else {
      // Service worker found. Proceed as normal.
      registerValidSW(swUrl, config);
    }
  }).catch(() => {
    console.log('No internet connection found. App is running in offline mode.');
  });
}

function unregister() {
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.ready.then(registration => {
      registration.unregister();
    }).catch(error => {
      console.error(error.message);
    });
  }
}

/***/ }),

/***/ "./src/styles/main.scss":
/*!******************************!*\
  !*** ./src/styles/main.scss ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-5-1!../../node_modules/postcss-loader/src??postcss!../../node_modules/resolve-url-loader??ref--6-oneOf-5-3!../../node_modules/sass-loader/dist/cjs.js??ref--6-oneOf-5-4!./main.scss */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./node_modules/resolve-url-loader/index.js?!./node_modules/sass-loader/dist/cjs.js?!./src/styles/main.scss");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-5-1!../../node_modules/postcss-loader/src??postcss!../../node_modules/resolve-url-loader??ref--6-oneOf-5-3!../../node_modules/sass-loader/dist/cjs.js??ref--6-oneOf-5-4!./main.scss */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./node_modules/resolve-url-loader/index.js?!./node_modules/sass-loader/dist/cjs.js?!./src/styles/main.scss", function() {
		var newContent = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-5-1!../../node_modules/postcss-loader/src??postcss!../../node_modules/resolve-url-loader??ref--6-oneOf-5-3!../../node_modules/sass-loader/dist/cjs.js??ref--6-oneOf-5-4!./main.scss */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./node_modules/resolve-url-loader/index.js?!./node_modules/sass-loader/dist/cjs.js?!./src/styles/main.scss");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1:
/*!**************************************************************************************************************!*\
  !*** multi (webpack)/hot/dev-server.js ./node_modules/react-dev-utils/webpackHotDevClient.js ./src/index.js ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /Users/prepladder/Desktop/pgi-ent-exam/node_modules/webpack/hot/dev-server.js */"./node_modules/webpack/hot/dev-server.js");
__webpack_require__(/*! /Users/prepladder/Desktop/pgi-ent-exam/node_modules/react-dev-utils/webpackHotDevClient.js */"./node_modules/react-dev-utils/webpackHotDevClient.js");
module.exports = __webpack_require__(/*! /Users/prepladder/Desktop/pgi-ent-exam/src/index.js */"./src/index.js");


/***/ })

},[[1,"runtime-main",0]]]);
//# sourceMappingURL=main.chunk.js.map