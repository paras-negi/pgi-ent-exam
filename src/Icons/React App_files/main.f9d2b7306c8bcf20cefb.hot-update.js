webpackHotUpdate("main",{

/***/ "./src/pages/Instructions2.js":
/*!************************************!*\
  !*** ./src/pages/Instructions2.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Icons_backwardIcon_png__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Icons/backwardIcon.png */ "./src/Icons/backwardIcon.png");
/* harmony import */ var _Icons_backwardIcon_png__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_Icons_backwardIcon_png__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Icons_defaultIcon_jpg__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Icons/defaultIcon.jpg */ "./src/Icons/defaultIcon.jpg");
/* harmony import */ var _Icons_defaultIcon_jpg__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Icons_defaultIcon_jpg__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "/Users/prepladder/Desktop/pgi-ent-exam/src/pages/Instructions2.js";




function Instructions2() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "instr-page",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "instr-page-layout",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "instr-page-header",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: this
  }, "\xA0"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "main-row",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-left",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "instructions-first",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18
    },
    __self: this
  }, "Other Important Instructions:")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "instruction-row instruction-row-2",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "instruction-paragraph",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }, " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("u", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }, " Instructions for mock test: "), " "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ol", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "li-margin-bottom",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24
    },
    __self: this
  }, " This is a Mock test. The Questions displayed are for practice purpose only. Under no circumstances should this be presumed as sample questions. "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "li-margin-bottom",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    __self: this
  }, " Mock test is meant for creating awareness about the test delivery system. Contents are for sample only and actual content will be different on the day of examination.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("font", {
    size: "4",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28
    },
    __self: this
  }, "Some Important Instructions are as follows:")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ol", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "li-margin-bottom",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: this
  }, "Pen, Pencil, Cell phones, pagers, calculators, Pen Drives, Tablets or any electronic devices are strictly prohibited. Violation will lead to expulsion from the examination. No arrangements have been made at the exam centres for their custody. Candidates are advised not to bring them at the exam centre."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "li-margin-bottom",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32
    },
    __self: this
  }, "Improper conduct will entail expulsion from the examination. Failure to comply with the instructions will entail expulsion/ cancellation of candidature including appropriate legal action."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "li-margin-bottom",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: this
  }, "Follow the instruction given before or after any screen during the computer based exam."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "li-margin-bottom",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34
    },
    __self: this
  }, "Use of keyboard during the exam is strictly prohibited. Wherever use of keyboard is needed a virtual keyboard will appear on screen automatically."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "li-margin-bottom",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35
    },
    __self: this
  }, "Always maintain peace, discipline and silence during the examination process."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "li-margin-bottom",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36
    },
    __self: this
  }, "NBE reserves all rights to verify identity and genuineness of each candidate at the exam centre by taking thumb impression and photograph of the candidate or by any other means."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "li-margin-bottom",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37
    },
    __self: this
  }, "Late entry and/or early exit is not permitted."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "li-margin-bottom",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 38
    },
    __self: this
  }, "Please read the information bulletin carefully.")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "nda",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "nda-paragraph",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    style: {
      display: "inline-block"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "checkbox",
    style: {
      marginTop: "1px",
      float: "left"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "nda-heading",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46
    },
    __self: this
  }, "NON DISCLOSURE AGREEMENT (NDA)- NEET-PG"))), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "nda-paragraph-list",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ol", {
    style: {
      listStyle: "none"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "li-margin-bottom",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51
    },
    __self: this
  }, "NEET- PG is a proprietary examination and is conducted by National Board of Examinations. The contents of this exam are confidential, proprietary and are owned by National Board of Examinations. NBE explicitly prohibits the candidate from publishing, reproducing or transmitting any or some contents of this test, in whole or in part, in any form or by any means verbal or written, electronic or mechanical or for any purpose. "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "li-margin-bottom",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56
    },
    __self: this
  }, "No content of this exam must be shared with friends, acquaintances or third parties including sharing through online means or via social media. Social media includes but not limited to ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58
    },
    __self: this
  }, "SMS, Whatsapp, Facebook, Twitter, Hangouts, Blogs "), "etc using either one\u2019s own account or proxy account(s)."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "li-margin-bottom",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 61
    },
    __self: this
  }, "By registering for and /or appearing in NEET-PG the candidate explicitly agrees to the above Non Disclosure Agreement and general terms of use for NEET- PG as contained in this Information Bulletin & NEET- PG website ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 64
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "http://www.nbe.edu.in/",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 64
    },
    __self: this
  }, "www.nbe.edu.in")), "."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "li-margin-bottom",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 68
    },
    __self: this
  }, "Violation of any act or breach of the same shall be liable for penal action and cancellation of the candidature at the bare threshold."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "li-margin-bottom",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72
    },
    __self: this
  }, "I have read and understood the instructions. All computer hardware allotted to me are in proper working condition. I declare that I am not in possession of / not wearing / not carrying any prohibited gadget like mobile phone, bluetooth devices etc. /any prohibited material with me into the Examination Hall.I agree that in case of not adhering to the instructions, I shall be liable to be debarred from this Test and/or to disciplinary action, which may include ban from future Tests / Examinations."))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "btn-section",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 85
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "btn btn__primary btn__primary--prev",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 86
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 87
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    alt: "forward-icon",
    className: "forward-icon",
    src: _Icons_backwardIcon_png__WEBPACK_IMPORTED_MODULE_1___default.a,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 88
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 89
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 90
    },
    __self: this
  }, "Previous"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("center", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 93
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "btn btn__primary btn__primary--blue",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 93
    },
    __self: this
  }, "I am ready to begin")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-right",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 96
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "section-rt-main",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 97
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 98
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("center", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 99
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    alt: "candidate-photo",
    src: _Icons_defaultIcon_jpg__WEBPACK_IMPORTED_MODULE_2___default.a,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 100
    },
    __self: this
  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "candidate-name",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 103
    },
    __self: this
  }, " John Smith ")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "footer-main",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 107
    },
    __self: this
  }, " Version : 2.4.0.0 ")));
}

/* harmony default export */ __webpack_exports__["default"] = (Instructions2);

/***/ })

})
//# sourceMappingURL=main.f9d2b7306c8bcf20cefb.hot-update.js.map