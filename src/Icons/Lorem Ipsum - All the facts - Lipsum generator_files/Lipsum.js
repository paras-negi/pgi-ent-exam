!function(t) {
    var e = {};
    function n(r) {
        if (e[r])
            return e[r].exports;
        var i = e[r] = {
            i: r,
            l: !1,
            exports: {}
        };
        return t[r].call(i.exports, i, i.exports, n),
        i.l = !0,
        i.exports
    }
    n.m = t,
    n.c = e,
    n.d = function(t, e, r) {
        n.o(t, e) || Object.defineProperty(t, e, {
            enumerable: !0,
            get: r
        })
    }
    ,
    n.r = function(t) {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
            value: "Module"
        }),
        Object.defineProperty(t, "__esModule", {
            value: !0
        })
    }
    ,
    n.t = function(t, e) {
        if (1 & e && (t = n(t)),
        8 & e)
            return t;
        if (4 & e && "object" == typeof t && t && t.__esModule)
            return t;
        var r = Object.create(null);
        if (n.r(r),
        Object.defineProperty(r, "default", {
            enumerable: !0,
            value: t
        }),
        2 & e && "string" != typeof t)
            for (var i in t)
                n.d(r, i, function(e) {
                    return t[e]
                }
                .bind(null, i));
        return r
    }
    ,
    n.n = function(t) {
        var e = t && t.__esModule ? function() {
            return t.default
        }
        : function() {
            return t
        }
        ;
        return n.d(e, "a", e),
        e
    }
    ,
    n.o = function(t, e) {
        return Object.prototype.hasOwnProperty.call(t, e)
    }
    ,
    n.p = "",
    n(n.s = 149)
}([function(t, e, n) {
    var r = n(3)
      , i = n(21)
      , o = n(12)
      , u = n(13)
      , a = n(22)
      , s = function(t, e, n) {
        var c, f, l, h, p = t & s.F, d = t & s.G, v = t & s.S, g = t & s.P, y = t & s.B, m = d ? r : v ? r[e] || (r[e] = {}) : (r[e] || {}).prototype, b = d ? i : i[e] || (i[e] = {}), w = b.prototype || (b.prototype = {});
        for (c in d && (n = e),
        n)
            l = ((f = !p && m && void 0 !== m[c]) ? m : n)[c],
            h = y && f ? a(l, r) : g && "function" == typeof l ? a(Function.call, l) : l,
            m && u(m, c, l, t & s.U),
            b[c] != l && o(b, c, h),
            g && w[c] != l && (w[c] = l)
    };
    r.core = i,
    s.F = 1,
    s.G = 2,
    s.S = 4,
    s.P = 8,
    s.B = 16,
    s.W = 32,
    s.U = 64,
    s.R = 128,
    t.exports = s
}
, function(t, e, n) {
    "use strict";
    n.d(e, "b", (function() {
        return u
    }
    )),
    n.d(e, "a", (function() {
        return a
    }
    ));
    var r = n(148)
      , i = n.n(r);
    function o(t, e) {
        for (var n = 0; n < e.length; n++) {
            var r = e[n];
            r.enumerable = r.enumerable || !1,
            r.configurable = !0,
            "value"in r && (r.writable = !0),
            Object.defineProperty(t, r.key, r)
        }
    }
    var u = function() {
        function t() {
            !function(t, e) {
                if (!(t instanceof e))
                    throw new TypeError("Cannot call a class as a function")
            }(this, t)
        }
        var e, n, r;
        return e = t,
        r = [{
            key: "getDeviceType",
            value: function() {
                return /bot|googlebot|crawler|spider|robot|crawling|google|baidu|bing|msn|duckduckbot|teoma|slurp|yandex/i.test(navigator.userAgent.toLowerCase()) ? "bot" : /(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(navigator.userAgent) ? "tablet" : /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4)) ? "mobile" : "desktop"
            }
        }, {
            key: "getBrowserLanguage",
            value: function() {
                try {
                    return [].concat(navigator.languages, navigator.language, navigator.userLanguage, navigator.browserLanguage, navigator.systemLanguage).filter(Boolean)[0].split("-")[0]
                } catch (t) {
                    return "unknown"
                }
            }
        }],
        (n = null) && o(e.prototype, n),
        r && o(e, r),
        t
    }()
      , a = i()({
        name: "Leya"
    })
}
, function(t, e, n) {
    var r = n(5);
    t.exports = function(t) {
        if (!r(t))
            throw TypeError(t + " is not an object!");
        return t
    }
}
, function(t, e) {
    var n = t.exports = "undefined" != typeof window && window.Math == Math ? window : "undefined" != typeof self && self.Math == Math ? self : Function("return this")();
    "number" == typeof __g && (__g = n)
}
, function(t, e) {
    t.exports = function(t) {
        try {
            return !!t()
        } catch (t) {
            return !0
        }
    }
}
, function(t, e) {
    t.exports = function(t) {
        return "object" == typeof t ? null !== t : "function" == typeof t
    }
}
, function(t, e, n) {
    var r = n(50)("wks")
      , i = n(36)
      , o = n(3).Symbol
      , u = "function" == typeof o;
    (t.exports = function(t) {
        return r[t] || (r[t] = u && o[t] || (u ? o : i)("Symbol." + t))
    }
    ).store = r
}
, function(t, e, n) {
    var r = n(24)
      , i = Math.min;
    t.exports = function(t) {
        return t > 0 ? i(r(t), 9007199254740991) : 0
    }
}
, function(t, e, n) {
    t.exports = !n(4)((function() {
        return 7 != Object.defineProperty({}, "a", {
            get: function() {
                return 7
            }
        }).a
    }
    ))
}
, function(t, e, n) {
    var r = n(2)
      , i = n(102)
      , o = n(26)
      , u = Object.defineProperty;
    e.f = n(8) ? Object.defineProperty : function(t, e, n) {
        if (r(t),
        e = o(e, !0),
        r(n),
        i)
            try {
                return u(t, e, n)
            } catch (t) {}
        if ("get"in n || "set"in n)
            throw TypeError("Accessors not supported!");
        return "value"in n && (t[e] = n.value),
        t
    }
}
, function(t, e, n) {
    var r = n(27);
    t.exports = function(t) {
        return Object(r(t))
    }
}
, function(t, e) {
    t.exports = function(t) {
        if ("function" != typeof t)
            throw TypeError(t + " is not a function!");
        return t
    }
}
, function(t, e, n) {
    var r = n(9)
      , i = n(35);
    t.exports = n(8) ? function(t, e, n) {
        return r.f(t, e, i(1, n))
    }
    : function(t, e, n) {
        return t[e] = n,
        t
    }
}
, function(t, e, n) {
    var r = n(3)
      , i = n(12)
      , o = n(16)
      , u = n(36)("src")
      , a = n(153)
      , s = ("" + a).split("toString");
    n(21).inspectSource = function(t) {
        return a.call(t)
    }
    ,
    (t.exports = function(t, e, n, a) {
        var c = "function" == typeof n;
        c && (o(n, "name") || i(n, "name", e)),
        t[e] !== n && (c && (o(n, u) || i(n, u, t[e] ? "" + t[e] : s.join(String(e)))),
        t === r ? t[e] = n : a ? t[e] ? t[e] = n : i(t, e, n) : (delete t[e],
        i(t, e, n)))
    }
    )(Function.prototype, "toString", (function() {
        return "function" == typeof this && this[u] || a.call(this)
    }
    ))
}
, function(t, e, n) {
    var r = n(0)
      , i = n(4)
      , o = n(27)
      , u = /"/g
      , a = function(t, e, n, r) {
        var i = String(o(t))
          , a = "<" + e;
        return "" !== n && (a += " " + n + '="' + String(r).replace(u, "&quot;") + '"'),
        a + ">" + i + "</" + e + ">"
    };
    t.exports = function(t, e) {
        var n = {};
        n[t] = e(a),
        r(r.P + r.F * i((function() {
            var e = ""[t]('"');
            return e !== e.toLowerCase() || e.split('"').length > 3
        }
        )), "String", n)
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(140)
      , i = Object.prototype.toString;
    function o(t) {
        return "[object Array]" === i.call(t)
    }
    function u(t) {
        return void 0 === t
    }
    function a(t) {
        return null !== t && "object" == typeof t
    }
    function s(t) {
        return "[object Function]" === i.call(t)
    }
    function c(t, e) {
        if (null != t)
            if ("object" != typeof t && (t = [t]),
            o(t))
                for (var n = 0, r = t.length; n < r; n++)
                    e.call(null, t[n], n, t);
            else
                for (var i in t)
                    Object.prototype.hasOwnProperty.call(t, i) && e.call(null, t[i], i, t)
    }
    t.exports = {
        isArray: o,
        isArrayBuffer: function(t) {
            return "[object ArrayBuffer]" === i.call(t)
        },
        isBuffer: function(t) {
            return null !== t && !u(t) && null !== t.constructor && !u(t.constructor) && "function" == typeof t.constructor.isBuffer && t.constructor.isBuffer(t)
        },
        isFormData: function(t) {
            return "undefined" != typeof FormData && t instanceof FormData
        },
        isArrayBufferView: function(t) {
            return "undefined" != typeof ArrayBuffer && ArrayBuffer.isView ? ArrayBuffer.isView(t) : t && t.buffer && t.buffer instanceof ArrayBuffer
        },
        isString: function(t) {
            return "string" == typeof t
        },
        isNumber: function(t) {
            return "number" == typeof t
        },
        isObject: a,
        isUndefined: u,
        isDate: function(t) {
            return "[object Date]" === i.call(t)
        },
        isFile: function(t) {
            return "[object File]" === i.call(t)
        },
        isBlob: function(t) {
            return "[object Blob]" === i.call(t)
        },
        isFunction: s,
        isStream: function(t) {
            return a(t) && s(t.pipe)
        },
        isURLSearchParams: function(t) {
            return "undefined" != typeof URLSearchParams && t instanceof URLSearchParams
        },
        isStandardBrowserEnv: function() {
            return ("undefined" == typeof navigator || "ReactNative" !== navigator.product && "NativeScript" !== navigator.product && "NS" !== navigator.product) && ("undefined" != typeof window && "undefined" != typeof document)
        },
        forEach: c,
        merge: function t() {
            var e = {};
            function n(n, r) {
                "object" == typeof e[r] && "object" == typeof n ? e[r] = t(e[r], n) : e[r] = n
            }
            for (var r = 0, i = arguments.length; r < i; r++)
                c(arguments[r], n);
            return e
        },
        deepMerge: function t() {
            var e = {};
            function n(n, r) {
                "object" == typeof e[r] && "object" == typeof n ? e[r] = t(e[r], n) : e[r] = "object" == typeof n ? t({}, n) : n
            }
            for (var r = 0, i = arguments.length; r < i; r++)
                c(arguments[r], n);
            return e
        },
        extend: function(t, e, n) {
            return c(e, (function(e, i) {
                t[i] = n && "function" == typeof e ? r(e, n) : e
            }
            )),
            t
        },
        trim: function(t) {
            return t.replace(/^\s*/, "").replace(/\s*$/, "")
        }
    }
}
, function(t, e) {
    var n = {}.hasOwnProperty;
    t.exports = function(t, e) {
        return n.call(t, e)
    }
}
, function(t, e, n) {
    var r = n(51)
      , i = n(27);
    t.exports = function(t) {
        return r(i(t))
    }
}
, function(t, e, n) {
    var r = n(52)
      , i = n(35)
      , o = n(17)
      , u = n(26)
      , a = n(16)
      , s = n(102)
      , c = Object.getOwnPropertyDescriptor;
    e.f = n(8) ? c : function(t, e) {
        if (t = o(t),
        e = u(e, !0),
        s)
            try {
                return c(t, e)
            } catch (t) {}
        if (a(t, e))
            return i(!r.f.call(t, e), t[e])
    }
}
, function(t, e, n) {
    var r = n(16)
      , i = n(10)
      , o = n(73)("IE_PROTO")
      , u = Object.prototype;
    t.exports = Object.getPrototypeOf || function(t) {
        return t = i(t),
        r(t, o) ? t[o] : "function" == typeof t.constructor && t instanceof t.constructor ? t.constructor.prototype : t instanceof Object ? u : null
    }
}
, function(t, e, n) {
    "use strict";
    function r(t) {
        return (r = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
            return typeof t
        }
        : function(t) {
            return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
        }
        )(t)
    }
    function i(t, e) {
        if (!(t instanceof e))
            throw new TypeError("Cannot call a class as a function")
    }
    function o(t, e) {
        return !e || "object" !== r(e) && "function" != typeof e ? function(t) {
            if (void 0 === t)
                throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return t
        }(t) : e
    }
    function u(t) {
        var e = "function" == typeof Map ? new Map : void 0;
        return (u = function(t) {
            if (null === t || (n = t,
            -1 === Function.toString.call(n).indexOf("[native code]")))
                return t;
            var n;
            if ("function" != typeof t)
                throw new TypeError("Super expression must either be null or a function");
            if (void 0 !== e) {
                if (e.has(t))
                    return e.get(t);
                e.set(t, r)
            }
            function r() {
                return s(t, arguments, f(this).constructor)
            }
            return r.prototype = Object.create(t.prototype, {
                constructor: {
                    value: r,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }),
            c(r, t)
        }
        )(t)
    }
    function a() {
        if ("undefined" == typeof Reflect || !Reflect.construct)
            return !1;
        if (Reflect.construct.sham)
            return !1;
        if ("function" == typeof Proxy)
            return !0;
        try {
            return Date.prototype.toString.call(Reflect.construct(Date, [], (function() {}
            ))),
            !0
        } catch (t) {
            return !1
        }
    }
    function s(t, e, n) {
        return (s = a() ? Reflect.construct : function(t, e, n) {
            var r = [null];
            r.push.apply(r, e);
            var i = new (Function.bind.apply(t, r));
            return n && c(i, n.prototype),
            i
        }
        ).apply(null, arguments)
    }
    function c(t, e) {
        return (c = Object.setPrototypeOf || function(t, e) {
            return t.__proto__ = e,
            t
        }
        )(t, e)
    }
    function f(t) {
        return (f = Object.setPrototypeOf ? Object.getPrototypeOf : function(t) {
            return t.__proto__ || Object.getPrototypeOf(t)
        }
        )(t)
    }
    n.d(e, "a", (function() {
        return l
    }
    ));
    var l = function(t) {
        function e() {
            return i(this, e),
            o(this, f(e).apply(this, arguments))
        }
        return function(t, e) {
            if ("function" != typeof e && null !== e)
                throw new TypeError("Super expression must either be null or a function");
            t.prototype = Object.create(e && e.prototype, {
                constructor: {
                    value: t,
                    writable: !0,
                    configurable: !0
                }
            }),
            e && c(t, e)
        }(e, t),
        e
    }(u(Error))
}
, function(t, e) {
    var n = t.exports = {
        version: "2.6.11"
    };
    "number" == typeof __e && (__e = n)
}
, function(t, e, n) {
    var r = n(11);
    t.exports = function(t, e, n) {
        if (r(t),
        void 0 === e)
            return t;
        switch (n) {
        case 1:
            return function(n) {
                return t.call(e, n)
            }
            ;
        case 2:
            return function(n, r) {
                return t.call(e, n, r)
            }
            ;
        case 3:
            return function(n, r, i) {
                return t.call(e, n, r, i)
            }
        }
        return function() {
            return t.apply(e, arguments)
        }
    }
}
, function(t, e) {
    var n = {}.toString;
    t.exports = function(t) {
        return n.call(t).slice(8, -1)
    }
}
, function(t, e) {
    var n = Math.ceil
      , r = Math.floor;
    t.exports = function(t) {
        return isNaN(t = +t) ? 0 : (t > 0 ? r : n)(t)
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(4);
    t.exports = function(t, e) {
        return !!t && r((function() {
            e ? t.call(null, (function() {}
            ), 1) : t.call(null)
        }
        ))
    }
}
, function(t, e, n) {
    var r = n(5);
    t.exports = function(t, e) {
        if (!r(t))
            return t;
        var n, i;
        if (e && "function" == typeof (n = t.toString) && !r(i = n.call(t)))
            return i;
        if ("function" == typeof (n = t.valueOf) && !r(i = n.call(t)))
            return i;
        if (!e && "function" == typeof (n = t.toString) && !r(i = n.call(t)))
            return i;
        throw TypeError("Can't convert object to primitive value")
    }
}
, function(t, e) {
    t.exports = function(t) {
        if (null == t)
            throw TypeError("Can't call method on  " + t);
        return t
    }
}
, function(t, e, n) {
    var r = n(0)
      , i = n(21)
      , o = n(4);
    t.exports = function(t, e) {
        var n = (i.Object || {})[t] || Object[t]
          , u = {};
        u[t] = e(n),
        r(r.S + r.F * o((function() {
            n(1)
        }
        )), "Object", u)
    }
}
, function(t, e, n) {
    var r = n(22)
      , i = n(51)
      , o = n(10)
      , u = n(7)
      , a = n(89);
    t.exports = function(t, e) {
        var n = 1 == t
          , s = 2 == t
          , c = 3 == t
          , f = 4 == t
          , l = 6 == t
          , h = 5 == t || l
          , p = e || a;
        return function(e, a, d) {
            for (var v, g, y = o(e), m = i(y), b = r(a, d, 3), w = u(m.length), _ = 0, x = n ? p(e, w) : s ? p(e, 0) : void 0; w > _; _++)
                if ((h || _ in m) && (g = b(v = m[_], _, y),
                t))
                    if (n)
                        x[_] = g;
                    else if (g)
                        switch (t) {
                        case 3:
                            return !0;
                        case 5:
                            return v;
                        case 6:
                            return _;
                        case 2:
                            x.push(v)
                        }
                    else if (f)
                        return !1;
            return l ? -1 : c || f ? f : x
        }
    }
}
, function(t, e, n) {
    "use strict";
    if (n(8)) {
        var r = n(32)
          , i = n(3)
          , o = n(4)
          , u = n(0)
          , a = n(66)
          , s = n(97)
          , c = n(22)
          , f = n(42)
          , l = n(35)
          , h = n(12)
          , p = n(44)
          , d = n(24)
          , v = n(7)
          , g = n(130)
          , y = n(38)
          , m = n(26)
          , b = n(16)
          , w = n(47)
          , _ = n(5)
          , x = n(10)
          , S = n(86)
          , k = n(39)
          , O = n(19)
          , E = n(40).f
          , j = n(88)
          , M = n(36)
          , P = n(6)
          , R = n(29)
          , T = n(56)
          , A = n(54)
          , L = n(91)
          , F = n(49)
          , I = n(61)
          , N = n(41)
          , C = n(90)
          , z = n(119)
          , D = n(9)
          , U = n(18)
          , B = D.f
          , V = U.f
          , W = i.RangeError
          , G = i.TypeError
          , q = i.Uint8Array
          , $ = Array.prototype
          , J = s.ArrayBuffer
          , K = s.DataView
          , H = R(0)
          , Y = R(2)
          , X = R(3)
          , Q = R(4)
          , Z = R(5)
          , tt = R(6)
          , et = T(!0)
          , nt = T(!1)
          , rt = L.values
          , it = L.keys
          , ot = L.entries
          , ut = $.lastIndexOf
          , at = $.reduce
          , st = $.reduceRight
          , ct = $.join
          , ft = $.sort
          , lt = $.slice
          , ht = $.toString
          , pt = $.toLocaleString
          , dt = P("iterator")
          , vt = P("toStringTag")
          , gt = M("typed_constructor")
          , yt = M("def_constructor")
          , mt = a.CONSTR
          , bt = a.TYPED
          , wt = a.VIEW
          , _t = R(1, (function(t, e) {
            return Et(A(t, t[yt]), e)
        }
        ))
          , xt = o((function() {
            return 1 === new q(new Uint16Array([1]).buffer)[0]
        }
        ))
          , St = !!q && !!q.prototype.set && o((function() {
            new q(1).set({})
        }
        ))
          , kt = function(t, e) {
            var n = d(t);
            if (n < 0 || n % e)
                throw W("Wrong offset!");
            return n
        }
          , Ot = function(t) {
            if (_(t) && bt in t)
                return t;
            throw G(t + " is not a typed array!")
        }
          , Et = function(t, e) {
            if (!(_(t) && gt in t))
                throw G("It is not a typed array constructor!");
            return new t(e)
        }
          , jt = function(t, e) {
            return Mt(A(t, t[yt]), e)
        }
          , Mt = function(t, e) {
            for (var n = 0, r = e.length, i = Et(t, r); r > n; )
                i[n] = e[n++];
            return i
        }
          , Pt = function(t, e, n) {
            B(t, e, {
                get: function() {
                    return this._d[n]
                }
            })
        }
          , Rt = function(t) {
            var e, n, r, i, o, u, a = x(t), s = arguments.length, f = s > 1 ? arguments[1] : void 0, l = void 0 !== f, h = j(a);
            if (null != h && !S(h)) {
                for (u = h.call(a),
                r = [],
                e = 0; !(o = u.next()).done; e++)
                    r.push(o.value);
                a = r
            }
            for (l && s > 2 && (f = c(f, arguments[2], 2)),
            e = 0,
            n = v(a.length),
            i = Et(this, n); n > e; e++)
                i[e] = l ? f(a[e], e) : a[e];
            return i
        }
          , Tt = function() {
            for (var t = 0, e = arguments.length, n = Et(this, e); e > t; )
                n[t] = arguments[t++];
            return n
        }
          , At = !!q && o((function() {
            pt.call(new q(1))
        }
        ))
          , Lt = function() {
            return pt.apply(At ? lt.call(Ot(this)) : Ot(this), arguments)
        }
          , Ft = {
            copyWithin: function(t, e) {
                return z.call(Ot(this), t, e, arguments.length > 2 ? arguments[2] : void 0)
            },
            every: function(t) {
                return Q(Ot(this), t, arguments.length > 1 ? arguments[1] : void 0)
            },
            fill: function(t) {
                return C.apply(Ot(this), arguments)
            },
            filter: function(t) {
                return jt(this, Y(Ot(this), t, arguments.length > 1 ? arguments[1] : void 0))
            },
            find: function(t) {
                return Z(Ot(this), t, arguments.length > 1 ? arguments[1] : void 0)
            },
            findIndex: function(t) {
                return tt(Ot(this), t, arguments.length > 1 ? arguments[1] : void 0)
            },
            forEach: function(t) {
                H(Ot(this), t, arguments.length > 1 ? arguments[1] : void 0)
            },
            indexOf: function(t) {
                return nt(Ot(this), t, arguments.length > 1 ? arguments[1] : void 0)
            },
            includes: function(t) {
                return et(Ot(this), t, arguments.length > 1 ? arguments[1] : void 0)
            },
            join: function(t) {
                return ct.apply(Ot(this), arguments)
            },
            lastIndexOf: function(t) {
                return ut.apply(Ot(this), arguments)
            },
            map: function(t) {
                return _t(Ot(this), t, arguments.length > 1 ? arguments[1] : void 0)
            },
            reduce: function(t) {
                return at.apply(Ot(this), arguments)
            },
            reduceRight: function(t) {
                return st.apply(Ot(this), arguments)
            },
            reverse: function() {
                for (var t, e = Ot(this).length, n = Math.floor(e / 2), r = 0; r < n; )
                    t = this[r],
                    this[r++] = this[--e],
                    this[e] = t;
                return this
            },
            some: function(t) {
                return X(Ot(this), t, arguments.length > 1 ? arguments[1] : void 0)
            },
            sort: function(t) {
                return ft.call(Ot(this), t)
            },
            subarray: function(t, e) {
                var n = Ot(this)
                  , r = n.length
                  , i = y(t, r);
                return new (A(n, n[yt]))(n.buffer,n.byteOffset + i * n.BYTES_PER_ELEMENT,v((void 0 === e ? r : y(e, r)) - i))
            }
        }
          , It = function(t, e) {
            return jt(this, lt.call(Ot(this), t, e))
        }
          , Nt = function(t) {
            Ot(this);
            var e = kt(arguments[1], 1)
              , n = this.length
              , r = x(t)
              , i = v(r.length)
              , o = 0;
            if (i + e > n)
                throw W("Wrong length!");
            for (; o < i; )
                this[e + o] = r[o++]
        }
          , Ct = {
            entries: function() {
                return ot.call(Ot(this))
            },
            keys: function() {
                return it.call(Ot(this))
            },
            values: function() {
                return rt.call(Ot(this))
            }
        }
          , zt = function(t, e) {
            return _(t) && t[bt] && "symbol" != typeof e && e in t && String(+e) == String(e)
        }
          , Dt = function(t, e) {
            return zt(t, e = m(e, !0)) ? l(2, t[e]) : V(t, e)
        }
          , Ut = function(t, e, n) {
            return !(zt(t, e = m(e, !0)) && _(n) && b(n, "value")) || b(n, "get") || b(n, "set") || n.configurable || b(n, "writable") && !n.writable || b(n, "enumerable") && !n.enumerable ? B(t, e, n) : (t[e] = n.value,
            t)
        };
        mt || (U.f = Dt,
        D.f = Ut),
        u(u.S + u.F * !mt, "Object", {
            getOwnPropertyDescriptor: Dt,
            defineProperty: Ut
        }),
        o((function() {
            ht.call({})
        }
        )) && (ht = pt = function() {
            return ct.call(this)
        }
        );
        var Bt = p({}, Ft);
        p(Bt, Ct),
        h(Bt, dt, Ct.values),
        p(Bt, {
            slice: It,
            set: Nt,
            constructor: function() {},
            toString: ht,
            toLocaleString: Lt
        }),
        Pt(Bt, "buffer", "b"),
        Pt(Bt, "byteOffset", "o"),
        Pt(Bt, "byteLength", "l"),
        Pt(Bt, "length", "e"),
        B(Bt, vt, {
            get: function() {
                return this[bt]
            }
        }),
        t.exports = function(t, e, n, s) {
            var c = t + ((s = !!s) ? "Clamped" : "") + "Array"
              , l = "get" + t
              , p = "set" + t
              , d = i[c]
              , y = d || {}
              , m = d && O(d)
              , b = !d || !a.ABV
              , x = {}
              , S = d && d.prototype
              , j = function(t, n) {
                B(t, n, {
                    get: function() {
                        return function(t, n) {
                            var r = t._d;
                            return r.v[l](n * e + r.o, xt)
                        }(this, n)
                    },
                    set: function(t) {
                        return function(t, n, r) {
                            var i = t._d;
                            s && (r = (r = Math.round(r)) < 0 ? 0 : r > 255 ? 255 : 255 & r),
                            i.v[p](n * e + i.o, r, xt)
                        }(this, n, t)
                    },
                    enumerable: !0
                })
            };
            b ? (d = n((function(t, n, r, i) {
                f(t, d, c, "_d");
                var o, u, a, s, l = 0, p = 0;
                if (_(n)) {
                    if (!(n instanceof J || "ArrayBuffer" == (s = w(n)) || "SharedArrayBuffer" == s))
                        return bt in n ? Mt(d, n) : Rt.call(d, n);
                    o = n,
                    p = kt(r, e);
                    var y = n.byteLength;
                    if (void 0 === i) {
                        if (y % e)
                            throw W("Wrong length!");
                        if ((u = y - p) < 0)
                            throw W("Wrong length!")
                    } else if ((u = v(i) * e) + p > y)
                        throw W("Wrong length!");
                    a = u / e
                } else
                    a = g(n),
                    o = new J(u = a * e);
                for (h(t, "_d", {
                    b: o,
                    o: p,
                    l: u,
                    e: a,
                    v: new K(o)
                }); l < a; )
                    j(t, l++)
            }
            )),
            S = d.prototype = k(Bt),
            h(S, "constructor", d)) : o((function() {
                d(1)
            }
            )) && o((function() {
                new d(-1)
            }
            )) && I((function(t) {
                new d,
                new d(null),
                new d(1.5),
                new d(t)
            }
            ), !0) || (d = n((function(t, n, r, i) {
                var o;
                return f(t, d, c),
                _(n) ? n instanceof J || "ArrayBuffer" == (o = w(n)) || "SharedArrayBuffer" == o ? void 0 !== i ? new y(n,kt(r, e),i) : void 0 !== r ? new y(n,kt(r, e)) : new y(n) : bt in n ? Mt(d, n) : Rt.call(d, n) : new y(g(n))
            }
            )),
            H(m !== Function.prototype ? E(y).concat(E(m)) : E(y), (function(t) {
                t in d || h(d, t, y[t])
            }
            )),
            d.prototype = S,
            r || (S.constructor = d));
            var M = S[dt]
              , P = !!M && ("values" == M.name || null == M.name)
              , R = Ct.values;
            h(d, gt, !0),
            h(S, bt, c),
            h(S, wt, !0),
            h(S, yt, d),
            (s ? new d(1)[vt] == c : vt in S) || B(S, vt, {
                get: function() {
                    return c
                }
            }),
            x[c] = d,
            u(u.G + u.W + u.F * (d != y), x),
            u(u.S, c, {
                BYTES_PER_ELEMENT: e
            }),
            u(u.S + u.F * o((function() {
                y.of.call(d, 1)
            }
            )), c, {
                from: Rt,
                of: Tt
            }),
            "BYTES_PER_ELEMENT"in S || h(S, "BYTES_PER_ELEMENT", e),
            u(u.P, c, Ft),
            N(c),
            u(u.P + u.F * St, c, {
                set: Nt
            }),
            u(u.P + u.F * !P, c, Ct),
            r || S.toString == ht || (S.toString = ht),
            u(u.P + u.F * o((function() {
                new d(1).slice()
            }
            )), c, {
                slice: It
            }),
            u(u.P + u.F * (o((function() {
                return [1, 2].toLocaleString() != new d([1, 2]).toLocaleString()
            }
            )) || !o((function() {
                S.toLocaleString.call([1, 2])
            }
            ))), c, {
                toLocaleString: Lt
            }),
            F[c] = P ? M : R,
            r || P || h(S, dt, R)
        }
    } else
        t.exports = function() {}
}
, function(t, e, n) {
    var r = n(125)
      , i = n(0)
      , o = n(50)("metadata")
      , u = o.store || (o.store = new (n(128)))
      , a = function(t, e, n) {
        var i = u.get(t);
        if (!i) {
            if (!n)
                return;
            u.set(t, i = new r)
        }
        var o = i.get(e);
        if (!o) {
            if (!n)
                return;
            i.set(e, o = new r)
        }
        return o
    };
    t.exports = {
        store: u,
        map: a,
        has: function(t, e, n) {
            var r = a(e, n, !1);
            return void 0 !== r && r.has(t)
        },
        get: function(t, e, n) {
            var r = a(e, n, !1);
            return void 0 === r ? void 0 : r.get(t)
        },
        set: function(t, e, n, r) {
            a(n, r, !0).set(t, e)
        },
        keys: function(t, e) {
            var n = a(t, e, !1)
              , r = [];
            return n && n.forEach((function(t, e) {
                r.push(e)
            }
            )),
            r
        },
        key: function(t) {
            return void 0 === t || "symbol" == typeof t ? t : String(t)
        },
        exp: function(t) {
            i(i.S, "Reflect", t)
        }
    }
}
, function(t, e) {
    t.exports = !1
}
, function(t, e, n) {
    var r = n(36)("meta")
      , i = n(5)
      , o = n(16)
      , u = n(9).f
      , a = 0
      , s = Object.isExtensible || function() {
        return !0
    }
      , c = !n(4)((function() {
        return s(Object.preventExtensions({}))
    }
    ))
      , f = function(t) {
        u(t, r, {
            value: {
                i: "O" + ++a,
                w: {}
            }
        })
    }
      , l = t.exports = {
        KEY: r,
        NEED: !1,
        fastKey: function(t, e) {
            if (!i(t))
                return "symbol" == typeof t ? t : ("string" == typeof t ? "S" : "P") + t;
            if (!o(t, r)) {
                if (!s(t))
                    return "F";
                if (!e)
                    return "E";
                f(t)
            }
            return t[r].i
        },
        getWeak: function(t, e) {
            if (!o(t, r)) {
                if (!s(t))
                    return !0;
                if (!e)
                    return !1;
                f(t)
            }
            return t[r].w
        },
        onFreeze: function(t) {
            return c && l.NEED && s(t) && !o(t, r) && f(t),
            t
        }
    }
}
, function(t, e, n) {
    var r = n(6)("unscopables")
      , i = Array.prototype;
    null == i[r] && n(12)(i, r, {}),
    t.exports = function(t) {
        i[r][t] = !0
    }
}
, function(t, e) {
    t.exports = function(t, e) {
        return {
            enumerable: !(1 & t),
            configurable: !(2 & t),
            writable: !(4 & t),
            value: e
        }
    }
}
, function(t, e) {
    var n = 0
      , r = Math.random();
    t.exports = function(t) {
        return "Symbol(".concat(void 0 === t ? "" : t, ")_", (++n + r).toString(36))
    }
}
, function(t, e, n) {
    var r = n(104)
      , i = n(74);
    t.exports = Object.keys || function(t) {
        return r(t, i)
    }
}
, function(t, e, n) {
    var r = n(24)
      , i = Math.max
      , o = Math.min;
    t.exports = function(t, e) {
        return (t = r(t)) < 0 ? i(t + e, 0) : o(t, e)
    }
}
, function(t, e, n) {
    var r = n(2)
      , i = n(105)
      , o = n(74)
      , u = n(73)("IE_PROTO")
      , a = function() {}
      , s = function() {
        var t, e = n(71)("iframe"), r = o.length;
        for (e.style.display = "none",
        n(75).appendChild(e),
        e.src = "javascript:",
        (t = e.contentWindow.document).open(),
        t.write("<script>document.F=Object<\/script>"),
        t.close(),
        s = t.F; r--; )
            delete s.prototype[o[r]];
        return s()
    };
    t.exports = Object.create || function(t, e) {
        var n;
        return null !== t ? (a.prototype = r(t),
        n = new a,
        a.prototype = null,
        n[u] = t) : n = s(),
        void 0 === e ? n : i(n, e)
    }
}
, function(t, e, n) {
    var r = n(104)
      , i = n(74).concat("length", "prototype");
    e.f = Object.getOwnPropertyNames || function(t) {
        return r(t, i)
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(3)
      , i = n(9)
      , o = n(8)
      , u = n(6)("species");
    t.exports = function(t) {
        var e = r[t];
        o && e && !e[u] && i.f(e, u, {
            configurable: !0,
            get: function() {
                return this
            }
        })
    }
}
, function(t, e) {
    t.exports = function(t, e, n, r) {
        if (!(t instanceof e) || void 0 !== r && r in t)
            throw TypeError(n + ": incorrect invocation!");
        return t
    }
}
, function(t, e, n) {
    var r = n(22)
      , i = n(117)
      , o = n(86)
      , u = n(2)
      , a = n(7)
      , s = n(88)
      , c = {}
      , f = {};
    (e = t.exports = function(t, e, n, l, h) {
        var p, d, v, g, y = h ? function() {
            return t
        }
        : s(t), m = r(n, l, e ? 2 : 1), b = 0;
        if ("function" != typeof y)
            throw TypeError(t + " is not iterable!");
        if (o(y)) {
            for (p = a(t.length); p > b; b++)
                if ((g = e ? m(u(d = t[b])[0], d[1]) : m(t[b])) === c || g === f)
                    return g
        } else
            for (v = y.call(t); !(d = v.next()).done; )
                if ((g = i(v, m, d.value, e)) === c || g === f)
                    return g
    }
    ).BREAK = c,
    e.RETURN = f
}
, function(t, e, n) {
    var r = n(13);
    t.exports = function(t, e, n) {
        for (var i in e)
            r(t, i, e[i], n);
        return t
    }
}
, function(t, e, n) {
    var r = n(5);
    t.exports = function(t, e) {
        if (!r(t) || t._t !== e)
            throw TypeError("Incompatible receiver, " + e + " required!");
        return t
    }
}
, function(t, e, n) {
    var r = n(9).f
      , i = n(16)
      , o = n(6)("toStringTag");
    t.exports = function(t, e, n) {
        t && !i(t = n ? t : t.prototype, o) && r(t, o, {
            configurable: !0,
            value: e
        })
    }
}
, function(t, e, n) {
    var r = n(23)
      , i = n(6)("toStringTag")
      , o = "Arguments" == r(function() {
        return arguments
    }());
    t.exports = function(t) {
        var e, n, u;
        return void 0 === t ? "Undefined" : null === t ? "Null" : "string" == typeof (n = function(t, e) {
            try {
                return t[e]
            } catch (t) {}
        }(e = Object(t), i)) ? n : o ? r(e) : "Object" == (u = r(e)) && "function" == typeof e.callee ? "Arguments" : u
    }
}
, function(t, e, n) {
    var r = n(0)
      , i = n(27)
      , o = n(4)
      , u = n(77)
      , a = "[" + u + "]"
      , s = RegExp("^" + a + a + "*")
      , c = RegExp(a + a + "*$")
      , f = function(t, e, n) {
        var i = {}
          , a = o((function() {
            return !!u[t]() || "​" != "​"[t]()
        }
        ))
          , s = i[t] = a ? e(l) : u[t];
        n && (i[n] = s),
        r(r.P + r.F * a, "String", i)
    }
      , l = f.trim = function(t, e) {
        return t = String(i(t)),
        1 & e && (t = t.replace(s, "")),
        2 & e && (t = t.replace(c, "")),
        t
    }
    ;
    t.exports = f
}
, function(t, e) {
    t.exports = {}
}
, function(t, e, n) {
    var r = n(21)
      , i = n(3)
      , o = i["__core-js_shared__"] || (i["__core-js_shared__"] = {});
    (t.exports = function(t, e) {
        return o[t] || (o[t] = void 0 !== e ? e : {})
    }
    )("versions", []).push({
        version: r.version,
        mode: n(32) ? "pure" : "global",
        copyright: "© 2019 Denis Pushkarev (zloirock.ru)"
    })
}
, function(t, e, n) {
    var r = n(23);
    t.exports = Object("z").propertyIsEnumerable(0) ? Object : function(t) {
        return "String" == r(t) ? t.split("") : Object(t)
    }
}
, function(t, e) {
    e.f = {}.propertyIsEnumerable
}
, function(t, e, n) {
    "use strict";
    var r = n(2);
    t.exports = function() {
        var t = r(this)
          , e = "";
        return t.global && (e += "g"),
        t.ignoreCase && (e += "i"),
        t.multiline && (e += "m"),
        t.unicode && (e += "u"),
        t.sticky && (e += "y"),
        e
    }
}
, function(t, e, n) {
    var r = n(2)
      , i = n(11)
      , o = n(6)("species");
    t.exports = function(t, e) {
        var n, u = r(t).constructor;
        return void 0 === u || null == (n = r(u)[o]) ? e : i(n)
    }
}
, function(t, e, n) {
    var r = n(364)
      , i = n(365);
    t.exports = function(t, e, n) {
        var o = e && n || 0;
        "string" == typeof t && (e = "binary" === t ? new Array(16) : null,
        t = null);
        var u = (t = t || {}).random || (t.rng || r)();
        if (u[6] = 15 & u[6] | 64,
        u[8] = 63 & u[8] | 128,
        e)
            for (var a = 0; a < 16; ++a)
                e[o + a] = u[a];
        return e || i(u)
    }
}
, function(t, e, n) {
    var r = n(17)
      , i = n(7)
      , o = n(38);
    t.exports = function(t) {
        return function(e, n, u) {
            var a, s = r(e), c = i(s.length), f = o(u, c);
            if (t && n != n) {
                for (; c > f; )
                    if ((a = s[f++]) != a)
                        return !0
            } else
                for (; c > f; f++)
                    if ((t || f in s) && s[f] === n)
                        return t || f || 0;
            return !t && -1
        }
    }
}
, function(t, e) {
    e.f = Object.getOwnPropertySymbols
}
, function(t, e, n) {
    var r = n(23);
    t.exports = Array.isArray || function(t) {
        return "Array" == r(t)
    }
}
, function(t, e, n) {
    var r = n(24)
      , i = n(27);
    t.exports = function(t) {
        return function(e, n) {
            var o, u, a = String(i(e)), s = r(n), c = a.length;
            return s < 0 || s >= c ? t ? "" : void 0 : (o = a.charCodeAt(s)) < 55296 || o > 56319 || s + 1 === c || (u = a.charCodeAt(s + 1)) < 56320 || u > 57343 ? t ? a.charAt(s) : o : t ? a.slice(s, s + 2) : u - 56320 + (o - 55296 << 10) + 65536
        }
    }
}
, function(t, e, n) {
    var r = n(5)
      , i = n(23)
      , o = n(6)("match");
    t.exports = function(t) {
        var e;
        return r(t) && (void 0 !== (e = t[o]) ? !!e : "RegExp" == i(t))
    }
}
, function(t, e, n) {
    var r = n(6)("iterator")
      , i = !1;
    try {
        var o = [7][r]();
        o.return = function() {
            i = !0
        }
        ,
        Array.from(o, (function() {
            throw 2
        }
        ))
    } catch (t) {}
    t.exports = function(t, e) {
        if (!e && !i)
            return !1;
        var n = !1;
        try {
            var o = [7]
              , u = o[r]();
            u.next = function() {
                return {
                    done: n = !0
                }
            }
            ,
            o[r] = function() {
                return u
            }
            ,
            t(o)
        } catch (t) {}
        return n
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(47)
      , i = RegExp.prototype.exec;
    t.exports = function(t, e) {
        var n = t.exec;
        if ("function" == typeof n) {
            var o = n.call(t, e);
            if ("object" != typeof o)
                throw new TypeError("RegExp exec method returned something other than an Object or null");
            return o
        }
        if ("RegExp" !== r(t))
            throw new TypeError("RegExp#exec called on incompatible receiver");
        return i.call(t, e)
    }
}
, function(t, e, n) {
    "use strict";
    n(121);
    var r = n(13)
      , i = n(12)
      , o = n(4)
      , u = n(27)
      , a = n(6)
      , s = n(92)
      , c = a("species")
      , f = !o((function() {
        var t = /./;
        return t.exec = function() {
            var t = [];
            return t.groups = {
                a: "7"
            },
            t
        }
        ,
        "7" !== "".replace(t, "$<a>")
    }
    ))
      , l = function() {
        var t = /(?:)/
          , e = t.exec;
        t.exec = function() {
            return e.apply(this, arguments)
        }
        ;
        var n = "ab".split(t);
        return 2 === n.length && "a" === n[0] && "b" === n[1]
    }();
    t.exports = function(t, e, n) {
        var h = a(t)
          , p = !o((function() {
            var e = {};
            return e[h] = function() {
                return 7
            }
            ,
            7 != ""[t](e)
        }
        ))
          , d = p ? !o((function() {
            var e = !1
              , n = /a/;
            return n.exec = function() {
                return e = !0,
                null
            }
            ,
            "split" === t && (n.constructor = {},
            n.constructor[c] = function() {
                return n
            }
            ),
            n[h](""),
            !e
        }
        )) : void 0;
        if (!p || !d || "replace" === t && !f || "split" === t && !l) {
            var v = /./[h]
              , g = n(u, h, ""[t], (function(t, e, n, r, i) {
                return e.exec === s ? p && !i ? {
                    done: !0,
                    value: v.call(e, n, r)
                } : {
                    done: !0,
                    value: t.call(n, e, r)
                } : {
                    done: !1
                }
            }
            ))
              , y = g[0]
              , m = g[1];
            r(String.prototype, t, y),
            i(RegExp.prototype, h, 2 == e ? function(t, e) {
                return m.call(t, this, e)
            }
            : function(t) {
                return m.call(t, this)
            }
            )
        }
    }
}
, function(t, e, n) {
    var r = n(3).navigator;
    t.exports = r && r.userAgent || ""
}
, function(t, e, n) {
    "use strict";
    var r = n(3)
      , i = n(0)
      , o = n(13)
      , u = n(44)
      , a = n(33)
      , s = n(43)
      , c = n(42)
      , f = n(5)
      , l = n(4)
      , h = n(61)
      , p = n(46)
      , d = n(78);
    t.exports = function(t, e, n, v, g, y) {
        var m = r[t]
          , b = m
          , w = g ? "set" : "add"
          , _ = b && b.prototype
          , x = {}
          , S = function(t) {
            var e = _[t];
            o(_, t, "delete" == t ? function(t) {
                return !(y && !f(t)) && e.call(this, 0 === t ? 0 : t)
            }
            : "has" == t ? function(t) {
                return !(y && !f(t)) && e.call(this, 0 === t ? 0 : t)
            }
            : "get" == t ? function(t) {
                return y && !f(t) ? void 0 : e.call(this, 0 === t ? 0 : t)
            }
            : "add" == t ? function(t) {
                return e.call(this, 0 === t ? 0 : t),
                this
            }
            : function(t, n) {
                return e.call(this, 0 === t ? 0 : t, n),
                this
            }
            )
        };
        if ("function" == typeof b && (y || _.forEach && !l((function() {
            (new b).entries().next()
        }
        )))) {
            var k = new b
              , O = k[w](y ? {} : -0, 1) != k
              , E = l((function() {
                k.has(1)
            }
            ))
              , j = h((function(t) {
                new b(t)
            }
            ))
              , M = !y && l((function() {
                for (var t = new b, e = 5; e--; )
                    t[w](e, e);
                return !t.has(-0)
            }
            ));
            j || ((b = e((function(e, n) {
                c(e, b, t);
                var r = d(new m, e, b);
                return null != n && s(n, g, r[w], r),
                r
            }
            ))).prototype = _,
            _.constructor = b),
            (E || M) && (S("delete"),
            S("has"),
            g && S("get")),
            (M || O) && S(w),
            y && _.clear && delete _.clear
        } else
            b = v.getConstructor(e, t, g, w),
            u(b.prototype, n),
            a.NEED = !0;
        return p(b, t),
        x[t] = b,
        i(i.G + i.W + i.F * (b != m), x),
        y || v.setStrong(b, t, g),
        b
    }
}
, function(t, e, n) {
    for (var r, i = n(3), o = n(12), u = n(36), a = u("typed_array"), s = u("view"), c = !(!i.ArrayBuffer || !i.DataView), f = c, l = 0, h = "Int8Array,Uint8Array,Uint8ClampedArray,Int16Array,Uint16Array,Int32Array,Uint32Array,Float32Array,Float64Array".split(","); l < 9; )
        (r = i[h[l++]]) ? (o(r.prototype, a, !0),
        o(r.prototype, s, !0)) : f = !1;
    t.exports = {
        ABV: c,
        CONSTR: f,
        TYPED: a,
        VIEW: s
    }
}
, function(t, e, n) {
    "use strict";
    t.exports = n(32) || !n(4)((function() {
        var t = Math.random();
        __defineSetter__.call(null, t, (function() {}
        )),
        delete n(3)[t]
    }
    ))
}
, function(t, e, n) {
    "use strict";
    var r = n(0);
    t.exports = function(t) {
        r(r.S, t, {
            of: function() {
                for (var t = arguments.length, e = new Array(t); t--; )
                    e[t] = arguments[t];
                return new this(e)
            }
        })
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(11)
      , o = n(22)
      , u = n(43);
    t.exports = function(t) {
        r(r.S, t, {
            from: function(t) {
                var e, n, r, a, s = arguments[1];
                return i(this),
                (e = void 0 !== s) && i(s),
                null == t ? new this : (n = [],
                e ? (r = 0,
                a = o(s, arguments[2], 2),
                u(t, !1, (function(t) {
                    n.push(a(t, r++))
                }
                ))) : u(t, !1, n.push, n),
                new this(n))
            }
        })
    }
}
, function(t, e, n) {
    "use strict";
    function r(t) {
        return (r = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
            return typeof t
        }
        : function(t) {
            return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
        }
        )(t)
    }
    function i(t, e) {
        if (!(t instanceof e))
            throw new TypeError("Cannot call a class as a function")
    }
    function o(t, e) {
        return !e || "object" !== r(e) && "function" != typeof e ? function(t) {
            if (void 0 === t)
                throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return t
        }(t) : e
    }
    function u(t) {
        var e = "function" == typeof Map ? new Map : void 0;
        return (u = function(t) {
            if (null === t || (n = t,
            -1 === Function.toString.call(n).indexOf("[native code]")))
                return t;
            var n;
            if ("function" != typeof t)
                throw new TypeError("Super expression must either be null or a function");
            if (void 0 !== e) {
                if (e.has(t))
                    return e.get(t);
                e.set(t, r)
            }
            function r() {
                return s(t, arguments, f(this).constructor)
            }
            return r.prototype = Object.create(t.prototype, {
                constructor: {
                    value: r,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }),
            c(r, t)
        }
        )(t)
    }
    function a() {
        if ("undefined" == typeof Reflect || !Reflect.construct)
            return !1;
        if (Reflect.construct.sham)
            return !1;
        if ("function" == typeof Proxy)
            return !0;
        try {
            return Date.prototype.toString.call(Reflect.construct(Date, [], (function() {}
            ))),
            !0
        } catch (t) {
            return !1
        }
    }
    function s(t, e, n) {
        return (s = a() ? Reflect.construct : function(t, e, n) {
            var r = [null];
            r.push.apply(r, e);
            var i = new (Function.bind.apply(t, r));
            return n && c(i, n.prototype),
            i
        }
        ).apply(null, arguments)
    }
    function c(t, e) {
        return (c = Object.setPrototypeOf || function(t, e) {
            return t.__proto__ = e,
            t
        }
        )(t, e)
    }
    function f(t) {
        return (f = Object.setPrototypeOf ? Object.getPrototypeOf : function(t) {
            return t.__proto__ || Object.getPrototypeOf(t)
        }
        )(t)
    }
    n.d(e, "a", (function() {
        return l
    }
    ));
    var l = function(t) {
        function e() {
            return i(this, e),
            o(this, f(e).apply(this, arguments))
        }
        return function(t, e) {
            if ("function" != typeof e && null !== e)
                throw new TypeError("Super expression must either be null or a function");
            t.prototype = Object.create(e && e.prototype, {
                constructor: {
                    value: t,
                    writable: !0,
                    configurable: !0
                }
            }),
            e && c(t, e)
        }(e, t),
        e
    }(u(Error))
}
, function(t, e, n) {
    var r = n(5)
      , i = n(3).document
      , o = r(i) && r(i.createElement);
    t.exports = function(t) {
        return o ? i.createElement(t) : {}
    }
}
, function(t, e, n) {
    var r = n(3)
      , i = n(21)
      , o = n(32)
      , u = n(103)
      , a = n(9).f;
    t.exports = function(t) {
        var e = i.Symbol || (i.Symbol = o ? {} : r.Symbol || {});
        "_" == t.charAt(0) || t in e || a(e, t, {
            value: u.f(t)
        })
    }
}
, function(t, e, n) {
    var r = n(50)("keys")
      , i = n(36);
    t.exports = function(t) {
        return r[t] || (r[t] = i(t))
    }
}
, function(t, e) {
    t.exports = "constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",")
}
, function(t, e, n) {
    var r = n(3).document;
    t.exports = r && r.documentElement
}
, function(t, e, n) {
    var r = n(5)
      , i = n(2)
      , o = function(t, e) {
        if (i(t),
        !r(e) && null !== e)
            throw TypeError(e + ": can't set as prototype!")
    };
    t.exports = {
        set: Object.setPrototypeOf || ("__proto__"in {} ? function(t, e, r) {
            try {
                (r = n(22)(Function.call, n(18).f(Object.prototype, "__proto__").set, 2))(t, []),
                e = !(t instanceof Array)
            } catch (t) {
                e = !0
            }
            return function(t, n) {
                return o(t, n),
                e ? t.__proto__ = n : r(t, n),
                t
            }
        }({}, !1) : void 0),
        check: o
    }
}
, function(t, e) {
    t.exports = "\t\n\v\f\r   ᠎             　\u2028\u2029\ufeff"
}
, function(t, e, n) {
    var r = n(5)
      , i = n(76).set;
    t.exports = function(t, e, n) {
        var o, u = e.constructor;
        return u !== n && "function" == typeof u && (o = u.prototype) !== n.prototype && r(o) && i && i(t, o),
        t
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(24)
      , i = n(27);
    t.exports = function(t) {
        var e = String(i(this))
          , n = ""
          , o = r(t);
        if (o < 0 || o == 1 / 0)
            throw RangeError("Count can't be negative");
        for (; o > 0; (o >>>= 1) && (e += e))
            1 & o && (n += e);
        return n
    }
}
, function(t, e) {
    t.exports = Math.sign || function(t) {
        return 0 == (t = +t) || t != t ? t : t < 0 ? -1 : 1
    }
}
, function(t, e) {
    var n = Math.expm1;
    t.exports = !n || n(10) > 22025.465794806718 || n(10) < 22025.465794806718 || -2e-17 != n(-2e-17) ? function(t) {
        return 0 == (t = +t) ? t : t > -1e-6 && t < 1e-6 ? t + t * t / 2 : Math.exp(t) - 1
    }
    : n
}
, function(t, e, n) {
    "use strict";
    var r = n(32)
      , i = n(0)
      , o = n(13)
      , u = n(12)
      , a = n(49)
      , s = n(83)
      , c = n(46)
      , f = n(19)
      , l = n(6)("iterator")
      , h = !([].keys && "next"in [].keys())
      , p = function() {
        return this
    };
    t.exports = function(t, e, n, d, v, g, y) {
        s(n, e, d);
        var m, b, w, _ = function(t) {
            if (!h && t in O)
                return O[t];
            switch (t) {
            case "keys":
            case "values":
                return function() {
                    return new n(this,t)
                }
            }
            return function() {
                return new n(this,t)
            }
        }, x = e + " Iterator", S = "values" == v, k = !1, O = t.prototype, E = O[l] || O["@@iterator"] || v && O[v], j = E || _(v), M = v ? S ? _("entries") : j : void 0, P = "Array" == e && O.entries || E;
        if (P && (w = f(P.call(new t))) !== Object.prototype && w.next && (c(w, x, !0),
        r || "function" == typeof w[l] || u(w, l, p)),
        S && E && "values" !== E.name && (k = !0,
        j = function() {
            return E.call(this)
        }
        ),
        r && !y || !h && !k && O[l] || u(O, l, j),
        a[e] = j,
        a[x] = p,
        v)
            if (m = {
                values: S ? j : _("values"),
                keys: g ? j : _("keys"),
                entries: M
            },
            y)
                for (b in m)
                    b in O || o(O, b, m[b]);
            else
                i(i.P + i.F * (h || k), e, m);
        return m
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(39)
      , i = n(35)
      , o = n(46)
      , u = {};
    n(12)(u, n(6)("iterator"), (function() {
        return this
    }
    )),
    t.exports = function(t, e, n) {
        t.prototype = r(u, {
            next: i(1, n)
        }),
        o(t, e + " Iterator")
    }
}
, function(t, e, n) {
    var r = n(60)
      , i = n(27);
    t.exports = function(t, e, n) {
        if (r(e))
            throw TypeError("String#" + n + " doesn't accept regex!");
        return String(i(t))
    }
}
, function(t, e, n) {
    var r = n(6)("match");
    t.exports = function(t) {
        var e = /./;
        try {
            "/./"[t](e)
        } catch (n) {
            try {
                return e[r] = !1,
                !"/./"[t](e)
            } catch (t) {}
        }
        return !0
    }
}
, function(t, e, n) {
    var r = n(49)
      , i = n(6)("iterator")
      , o = Array.prototype;
    t.exports = function(t) {
        return void 0 !== t && (r.Array === t || o[i] === t)
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(9)
      , i = n(35);
    t.exports = function(t, e, n) {
        e in t ? r.f(t, e, i(0, n)) : t[e] = n
    }
}
, function(t, e, n) {
    var r = n(47)
      , i = n(6)("iterator")
      , o = n(49);
    t.exports = n(21).getIteratorMethod = function(t) {
        if (null != t)
            return t[i] || t["@@iterator"] || o[r(t)]
    }
}
, function(t, e, n) {
    var r = n(242);
    t.exports = function(t, e) {
        return new (r(t))(e)
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(10)
      , i = n(38)
      , o = n(7);
    t.exports = function(t) {
        for (var e = r(this), n = o(e.length), u = arguments.length, a = i(u > 1 ? arguments[1] : void 0, n), s = u > 2 ? arguments[2] : void 0, c = void 0 === s ? n : i(s, n); c > a; )
            e[a++] = t;
        return e
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(34)
      , i = n(120)
      , o = n(49)
      , u = n(17);
    t.exports = n(82)(Array, "Array", (function(t, e) {
        this._t = u(t),
        this._i = 0,
        this._k = e
    }
    ), (function() {
        var t = this._t
          , e = this._k
          , n = this._i++;
        return !t || n >= t.length ? (this._t = void 0,
        i(1)) : i(0, "keys" == e ? n : "values" == e ? t[n] : [n, t[n]])
    }
    ), "values"),
    o.Arguments = o.Array,
    r("keys"),
    r("values"),
    r("entries")
}
, function(t, e, n) {
    "use strict";
    var r, i, o = n(53), u = RegExp.prototype.exec, a = String.prototype.replace, s = u, c = (r = /a/,
    i = /b*/g,
    u.call(r, "a"),
    u.call(i, "a"),
    0 !== r.lastIndex || 0 !== i.lastIndex), f = void 0 !== /()??/.exec("")[1];
    (c || f) && (s = function(t) {
        var e, n, r, i, s = this;
        return f && (n = new RegExp("^" + s.source + "$(?!\\s)",o.call(s))),
        c && (e = s.lastIndex),
        r = u.call(s, t),
        c && r && (s.lastIndex = s.global ? r.index + r[0].length : e),
        f && r && r.length > 1 && a.call(r[0], n, (function() {
            for (i = 1; i < arguments.length - 2; i++)
                void 0 === arguments[i] && (r[i] = void 0)
        }
        )),
        r
    }
    ),
    t.exports = s
}
, function(t, e, n) {
    "use strict";
    var r = n(59)(!0);
    t.exports = function(t, e, n) {
        return e + (n ? r(t, e).length : 1)
    }
}
, function(t, e, n) {
    var r, i, o, u = n(22), a = n(110), s = n(75), c = n(71), f = n(3), l = f.process, h = f.setImmediate, p = f.clearImmediate, d = f.MessageChannel, v = f.Dispatch, g = 0, y = {}, m = function() {
        var t = +this;
        if (y.hasOwnProperty(t)) {
            var e = y[t];
            delete y[t],
            e()
        }
    }, b = function(t) {
        m.call(t.data)
    };
    h && p || (h = function(t) {
        for (var e = [], n = 1; arguments.length > n; )
            e.push(arguments[n++]);
        return y[++g] = function() {
            a("function" == typeof t ? t : Function(t), e)
        }
        ,
        r(g),
        g
    }
    ,
    p = function(t) {
        delete y[t]
    }
    ,
    "process" == n(23)(l) ? r = function(t) {
        l.nextTick(u(m, t, 1))
    }
    : v && v.now ? r = function(t) {
        v.now(u(m, t, 1))
    }
    : d ? (o = (i = new d).port2,
    i.port1.onmessage = b,
    r = u(o.postMessage, o, 1)) : f.addEventListener && "function" == typeof postMessage && !f.importScripts ? (r = function(t) {
        f.postMessage(t + "", "*")
    }
    ,
    f.addEventListener("message", b, !1)) : r = "onreadystatechange"in c("script") ? function(t) {
        s.appendChild(c("script")).onreadystatechange = function() {
            s.removeChild(this),
            m.call(t)
        }
    }
    : function(t) {
        setTimeout(u(m, t, 1), 0)
    }
    ),
    t.exports = {
        set: h,
        clear: p
    }
}
, function(t, e, n) {
    var r = n(3)
      , i = n(94).set
      , o = r.MutationObserver || r.WebKitMutationObserver
      , u = r.process
      , a = r.Promise
      , s = "process" == n(23)(u);
    t.exports = function() {
        var t, e, n, c = function() {
            var r, i;
            for (s && (r = u.domain) && r.exit(); t; ) {
                i = t.fn,
                t = t.next;
                try {
                    i()
                } catch (r) {
                    throw t ? n() : e = void 0,
                    r
                }
            }
            e = void 0,
            r && r.enter()
        };
        if (s)
            n = function() {
                u.nextTick(c)
            }
            ;
        else if (!o || r.navigator && r.navigator.standalone)
            if (a && a.resolve) {
                var f = a.resolve(void 0);
                n = function() {
                    f.then(c)
                }
            } else
                n = function() {
                    i.call(r, c)
                }
                ;
        else {
            var l = !0
              , h = document.createTextNode("");
            new o(c).observe(h, {
                characterData: !0
            }),
            n = function() {
                h.data = l = !l
            }
        }
        return function(r) {
            var i = {
                fn: r,
                next: void 0
            };
            e && (e.next = i),
            t || (t = i,
            n()),
            e = i
        }
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(11);
    function i(t) {
        var e, n;
        this.promise = new t((function(t, r) {
            if (void 0 !== e || void 0 !== n)
                throw TypeError("Bad Promise constructor");
            e = t,
            n = r
        }
        )),
        this.resolve = r(e),
        this.reject = r(n)
    }
    t.exports.f = function(t) {
        return new i(t)
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(3)
      , i = n(8)
      , o = n(32)
      , u = n(66)
      , a = n(12)
      , s = n(44)
      , c = n(4)
      , f = n(42)
      , l = n(24)
      , h = n(7)
      , p = n(130)
      , d = n(40).f
      , v = n(9).f
      , g = n(90)
      , y = n(46)
      , m = r.ArrayBuffer
      , b = r.DataView
      , w = r.Math
      , _ = r.RangeError
      , x = r.Infinity
      , S = m
      , k = w.abs
      , O = w.pow
      , E = w.floor
      , j = w.log
      , M = w.LN2
      , P = i ? "_b" : "buffer"
      , R = i ? "_l" : "byteLength"
      , T = i ? "_o" : "byteOffset";
    function A(t, e, n) {
        var r, i, o, u = new Array(n), a = 8 * n - e - 1, s = (1 << a) - 1, c = s >> 1, f = 23 === e ? O(2, -24) - O(2, -77) : 0, l = 0, h = t < 0 || 0 === t && 1 / t < 0 ? 1 : 0;
        for ((t = k(t)) != t || t === x ? (i = t != t ? 1 : 0,
        r = s) : (r = E(j(t) / M),
        t * (o = O(2, -r)) < 1 && (r--,
        o *= 2),
        (t += r + c >= 1 ? f / o : f * O(2, 1 - c)) * o >= 2 && (r++,
        o /= 2),
        r + c >= s ? (i = 0,
        r = s) : r + c >= 1 ? (i = (t * o - 1) * O(2, e),
        r += c) : (i = t * O(2, c - 1) * O(2, e),
        r = 0)); e >= 8; u[l++] = 255 & i,
        i /= 256,
        e -= 8)
            ;
        for (r = r << e | i,
        a += e; a > 0; u[l++] = 255 & r,
        r /= 256,
        a -= 8)
            ;
        return u[--l] |= 128 * h,
        u
    }
    function L(t, e, n) {
        var r, i = 8 * n - e - 1, o = (1 << i) - 1, u = o >> 1, a = i - 7, s = n - 1, c = t[s--], f = 127 & c;
        for (c >>= 7; a > 0; f = 256 * f + t[s],
        s--,
        a -= 8)
            ;
        for (r = f & (1 << -a) - 1,
        f >>= -a,
        a += e; a > 0; r = 256 * r + t[s],
        s--,
        a -= 8)
            ;
        if (0 === f)
            f = 1 - u;
        else {
            if (f === o)
                return r ? NaN : c ? -x : x;
            r += O(2, e),
            f -= u
        }
        return (c ? -1 : 1) * r * O(2, f - e)
    }
    function F(t) {
        return t[3] << 24 | t[2] << 16 | t[1] << 8 | t[0]
    }
    function I(t) {
        return [255 & t]
    }
    function N(t) {
        return [255 & t, t >> 8 & 255]
    }
    function C(t) {
        return [255 & t, t >> 8 & 255, t >> 16 & 255, t >> 24 & 255]
    }
    function z(t) {
        return A(t, 52, 8)
    }
    function D(t) {
        return A(t, 23, 4)
    }
    function U(t, e, n) {
        v(t.prototype, e, {
            get: function() {
                return this[n]
            }
        })
    }
    function B(t, e, n, r) {
        var i = p(+n);
        if (i + e > t[R])
            throw _("Wrong index!");
        var o = t[P]._b
          , u = i + t[T]
          , a = o.slice(u, u + e);
        return r ? a : a.reverse()
    }
    function V(t, e, n, r, i, o) {
        var u = p(+n);
        if (u + e > t[R])
            throw _("Wrong index!");
        for (var a = t[P]._b, s = u + t[T], c = r(+i), f = 0; f < e; f++)
            a[s + f] = c[o ? f : e - f - 1]
    }
    if (u.ABV) {
        if (!c((function() {
            m(1)
        }
        )) || !c((function() {
            new m(-1)
        }
        )) || c((function() {
            return new m,
            new m(1.5),
            new m(NaN),
            "ArrayBuffer" != m.name
        }
        ))) {
            for (var W, G = (m = function(t) {
                return f(this, m),
                new S(p(t))
            }
            ).prototype = S.prototype, q = d(S), $ = 0; q.length > $; )
                (W = q[$++])in m || a(m, W, S[W]);
            o || (G.constructor = m)
        }
        var J = new b(new m(2))
          , K = b.prototype.setInt8;
        J.setInt8(0, 2147483648),
        J.setInt8(1, 2147483649),
        !J.getInt8(0) && J.getInt8(1) || s(b.prototype, {
            setInt8: function(t, e) {
                K.call(this, t, e << 24 >> 24)
            },
            setUint8: function(t, e) {
                K.call(this, t, e << 24 >> 24)
            }
        }, !0)
    } else
        m = function(t) {
            f(this, m, "ArrayBuffer");
            var e = p(t);
            this._b = g.call(new Array(e), 0),
            this[R] = e
        }
        ,
        b = function(t, e, n) {
            f(this, b, "DataView"),
            f(t, m, "DataView");
            var r = t[R]
              , i = l(e);
            if (i < 0 || i > r)
                throw _("Wrong offset!");
            if (i + (n = void 0 === n ? r - i : h(n)) > r)
                throw _("Wrong length!");
            this[P] = t,
            this[T] = i,
            this[R] = n
        }
        ,
        i && (U(m, "byteLength", "_l"),
        U(b, "buffer", "_b"),
        U(b, "byteLength", "_l"),
        U(b, "byteOffset", "_o")),
        s(b.prototype, {
            getInt8: function(t) {
                return B(this, 1, t)[0] << 24 >> 24
            },
            getUint8: function(t) {
                return B(this, 1, t)[0]
            },
            getInt16: function(t) {
                var e = B(this, 2, t, arguments[1]);
                return (e[1] << 8 | e[0]) << 16 >> 16
            },
            getUint16: function(t) {
                var e = B(this, 2, t, arguments[1]);
                return e[1] << 8 | e[0]
            },
            getInt32: function(t) {
                return F(B(this, 4, t, arguments[1]))
            },
            getUint32: function(t) {
                return F(B(this, 4, t, arguments[1])) >>> 0
            },
            getFloat32: function(t) {
                return L(B(this, 4, t, arguments[1]), 23, 4)
            },
            getFloat64: function(t) {
                return L(B(this, 8, t, arguments[1]), 52, 8)
            },
            setInt8: function(t, e) {
                V(this, 1, t, I, e)
            },
            setUint8: function(t, e) {
                V(this, 1, t, I, e)
            },
            setInt16: function(t, e) {
                V(this, 2, t, N, e, arguments[2])
            },
            setUint16: function(t, e) {
                V(this, 2, t, N, e, arguments[2])
            },
            setInt32: function(t, e) {
                V(this, 4, t, C, e, arguments[2])
            },
            setUint32: function(t, e) {
                V(this, 4, t, C, e, arguments[2])
            },
            setFloat32: function(t, e) {
                V(this, 4, t, D, e, arguments[2])
            },
            setFloat64: function(t, e) {
                V(this, 8, t, z, e, arguments[2])
            }
        });
    y(m, "ArrayBuffer"),
    y(b, "DataView"),
    a(b.prototype, u.VIEW, !0),
    e.ArrayBuffer = m,
    e.DataView = b
}
, function(t, e) {
    var n, r, i = t.exports = {};
    function o() {
        throw new Error("setTimeout has not been defined")
    }
    function u() {
        throw new Error("clearTimeout has not been defined")
    }
    function a(t) {
        if (n === setTimeout)
            return setTimeout(t, 0);
        if ((n === o || !n) && setTimeout)
            return n = setTimeout,
            setTimeout(t, 0);
        try {
            return n(t, 0)
        } catch (e) {
            try {
                return n.call(null, t, 0)
            } catch (e) {
                return n.call(this, t, 0)
            }
        }
    }
    !function() {
        try {
            n = "function" == typeof setTimeout ? setTimeout : o
        } catch (t) {
            n = o
        }
        try {
            r = "function" == typeof clearTimeout ? clearTimeout : u
        } catch (t) {
            r = u
        }
    }();
    var s, c = [], f = !1, l = -1;
    function h() {
        f && s && (f = !1,
        s.length ? c = s.concat(c) : l = -1,
        c.length && p())
    }
    function p() {
        if (!f) {
            var t = a(h);
            f = !0;
            for (var e = c.length; e; ) {
                for (s = c,
                c = []; ++l < e; )
                    s && s[l].run();
                l = -1,
                e = c.length
            }
            s = null,
            f = !1,
            function(t) {
                if (r === clearTimeout)
                    return clearTimeout(t);
                if ((r === u || !r) && clearTimeout)
                    return r = clearTimeout,
                    clearTimeout(t);
                try {
                    r(t)
                } catch (e) {
                    try {
                        return r.call(null, t)
                    } catch (e) {
                        return r.call(this, t)
                    }
                }
            }(t)
        }
    }
    function d(t, e) {
        this.fun = t,
        this.array = e
    }
    function v() {}
    i.nextTick = function(t) {
        var e = new Array(arguments.length - 1);
        if (arguments.length > 1)
            for (var n = 1; n < arguments.length; n++)
                e[n - 1] = arguments[n];
        c.push(new d(t,e)),
        1 !== c.length || f || a(p)
    }
    ,
    d.prototype.run = function() {
        this.fun.apply(null, this.array)
    }
    ,
    i.title = "browser",
    i.browser = !0,
    i.env = {},
    i.argv = [],
    i.version = "",
    i.versions = {},
    i.on = v,
    i.addListener = v,
    i.once = v,
    i.off = v,
    i.removeListener = v,
    i.removeAllListeners = v,
    i.emit = v,
    i.prependListener = v,
    i.prependOnceListener = v,
    i.listeners = function(t) {
        return []
    }
    ,
    i.binding = function(t) {
        throw new Error("process.binding is not supported")
    }
    ,
    i.cwd = function() {
        return "/"
    }
    ,
    i.chdir = function(t) {
        throw new Error("process.chdir is not supported")
    }
    ,
    i.umask = function() {
        return 0
    }
}
, function(t, e) {
    const n = ()=>{}
      , r = Symbol("log-levels")
      , i = Symbol("log-instance");
    t.exports = class {
        constructor(t) {
            this[i] = t,
            this[r] = {
                TRACE: 0,
                DEBUG: 1,
                INFO: 2,
                WARN: 3,
                ERROR: 4,
                SILENT: 5
            }
        }
        get levels() {
            return this[r]
        }
        get logger() {
            return this[i]
        }
        set logger(t) {
            this[i] = t
        }
        get methods() {
            return Object.keys(this.levels).map(t=>t.toLowerCase()).filter(t=>"silent" !== t)
        }
        bindMethod(t, e) {
            const n = t[e];
            if ("function" == typeof n.bind)
                return n.bind(t);
            try {
                return Function.prototype.bind.call(n, t)
            } catch (e) {
                return function() {
                    return Function.prototype.apply.apply(n, [t, arguments])
                }
            }
        }
        distillLevel(t) {
            let e = t;
            return "string" == typeof e && void 0 !== this.levels[e.toUpperCase()] && (e = this.levels[e.toUpperCase()]),
            !!this.levelValid(e) && e
        }
        levelValid(t) {
            return "number" == typeof t && t >= 0 && t <= this.levels.SILENT
        }
        make(t) {
            return "debug" === t && (t = "log"),
            void 0 !== console[t] ? this.bindMethod(console, t) : void 0 !== console.log ? this.bindMethod(console, "log") : n
        }
        replaceMethods(t) {
            const e = this.distillLevel(t);
            if (null == e)
                throw new Error(`loglevelnext: replaceMethods() called with invalid level: ${t}`);
            if (!this.logger || "LogLevel" !== this.logger.type)
                throw new TypeError("loglevelnext: Logger is undefined or invalid. Please specify a valid Logger instance.");
            this.methods.forEach(t=>{
                const {[t.toUpperCase()]: r} = this.levels;
                this.logger[t] = r < e ? n : this.make(t)
            }
            ),
            this.logger.log = this.logger.debug
        }
    }
}
, function(t, e, n) {
    t.exports = n(366)
}
, function(t, e) {
    var n;
    n = function() {
        return this
    }();
    try {
        n = n || new Function("return this")()
    } catch (t) {
        "object" == typeof window && (n = window)
    }
    t.exports = n
}
, function(t, e, n) {
    t.exports = !n(8) && !n(4)((function() {
        return 7 != Object.defineProperty(n(71)("div"), "a", {
            get: function() {
                return 7
            }
        }).a
    }
    ))
}
, function(t, e, n) {
    e.f = n(6)
}
, function(t, e, n) {
    var r = n(16)
      , i = n(17)
      , o = n(56)(!1)
      , u = n(73)("IE_PROTO");
    t.exports = function(t, e) {
        var n, a = i(t), s = 0, c = [];
        for (n in a)
            n != u && r(a, n) && c.push(n);
        for (; e.length > s; )
            r(a, n = e[s++]) && (~o(c, n) || c.push(n));
        return c
    }
}
, function(t, e, n) {
    var r = n(9)
      , i = n(2)
      , o = n(37);
    t.exports = n(8) ? Object.defineProperties : function(t, e) {
        i(t);
        for (var n, u = o(e), a = u.length, s = 0; a > s; )
            r.f(t, n = u[s++], e[n]);
        return t
    }
}
, function(t, e, n) {
    var r = n(17)
      , i = n(40).f
      , o = {}.toString
      , u = "object" == typeof window && window && Object.getOwnPropertyNames ? Object.getOwnPropertyNames(window) : [];
    t.exports.f = function(t) {
        return u && "[object Window]" == o.call(t) ? function(t) {
            try {
                return i(t)
            } catch (t) {
                return u.slice()
            }
        }(t) : i(r(t))
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(8)
      , i = n(37)
      , o = n(57)
      , u = n(52)
      , a = n(10)
      , s = n(51)
      , c = Object.assign;
    t.exports = !c || n(4)((function() {
        var t = {}
          , e = {}
          , n = Symbol()
          , r = "abcdefghijklmnopqrst";
        return t[n] = 7,
        r.split("").forEach((function(t) {
            e[t] = t
        }
        )),
        7 != c({}, t)[n] || Object.keys(c({}, e)).join("") != r
    }
    )) ? function(t, e) {
        for (var n = a(t), c = arguments.length, f = 1, l = o.f, h = u.f; c > f; )
            for (var p, d = s(arguments[f++]), v = l ? i(d).concat(l(d)) : i(d), g = v.length, y = 0; g > y; )
                p = v[y++],
                r && !h.call(d, p) || (n[p] = d[p]);
        return n
    }
    : c
}
, function(t, e) {
    t.exports = Object.is || function(t, e) {
        return t === e ? 0 !== t || 1 / t == 1 / e : t != t && e != e
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(11)
      , i = n(5)
      , o = n(110)
      , u = [].slice
      , a = {}
      , s = function(t, e, n) {
        if (!(e in a)) {
            for (var r = [], i = 0; i < e; i++)
                r[i] = "a[" + i + "]";
            a[e] = Function("F,a", "return new F(" + r.join(",") + ")")
        }
        return a[e](t, n)
    };
    t.exports = Function.bind || function(t) {
        var e = r(this)
          , n = u.call(arguments, 1)
          , a = function() {
            var r = n.concat(u.call(arguments));
            return this instanceof a ? s(e, r.length, r) : o(e, r, t)
        };
        return i(e.prototype) && (a.prototype = e.prototype),
        a
    }
}
, function(t, e) {
    t.exports = function(t, e, n) {
        var r = void 0 === n;
        switch (e.length) {
        case 0:
            return r ? t() : t.call(n);
        case 1:
            return r ? t(e[0]) : t.call(n, e[0]);
        case 2:
            return r ? t(e[0], e[1]) : t.call(n, e[0], e[1]);
        case 3:
            return r ? t(e[0], e[1], e[2]) : t.call(n, e[0], e[1], e[2]);
        case 4:
            return r ? t(e[0], e[1], e[2], e[3]) : t.call(n, e[0], e[1], e[2], e[3])
        }
        return t.apply(n, e)
    }
}
, function(t, e, n) {
    var r = n(3).parseInt
      , i = n(48).trim
      , o = n(77)
      , u = /^[-+]?0[xX]/;
    t.exports = 8 !== r(o + "08") || 22 !== r(o + "0x16") ? function(t, e) {
        var n = i(String(t), 3);
        return r(n, e >>> 0 || (u.test(n) ? 16 : 10))
    }
    : r
}
, function(t, e, n) {
    var r = n(3).parseFloat
      , i = n(48).trim;
    t.exports = 1 / r(n(77) + "-0") != -1 / 0 ? function(t) {
        var e = i(String(t), 3)
          , n = r(e);
        return 0 === n && "-" == e.charAt(0) ? -0 : n
    }
    : r
}
, function(t, e, n) {
    var r = n(23);
    t.exports = function(t, e) {
        if ("number" != typeof t && "Number" != r(t))
            throw TypeError(e);
        return +t
    }
}
, function(t, e, n) {
    var r = n(5)
      , i = Math.floor;
    t.exports = function(t) {
        return !r(t) && isFinite(t) && i(t) === t
    }
}
, function(t, e) {
    t.exports = Math.log1p || function(t) {
        return (t = +t) > -1e-8 && t < 1e-8 ? t - t * t / 2 : Math.log(1 + t)
    }
}
, function(t, e, n) {
    var r = n(80)
      , i = Math.pow
      , o = i(2, -52)
      , u = i(2, -23)
      , a = i(2, 127) * (2 - u)
      , s = i(2, -126);
    t.exports = Math.fround || function(t) {
        var e, n, i = Math.abs(t), c = r(t);
        return i < s ? c * (i / s / u + 1 / o - 1 / o) * s * u : (n = (e = (1 + u / o) * i) - (e - i)) > a || n != n ? c * (1 / 0) : c * n
    }
}
, function(t, e, n) {
    var r = n(2);
    t.exports = function(t, e, n, i) {
        try {
            return i ? e(r(n)[0], n[1]) : e(n)
        } catch (e) {
            var o = t.return;
            throw void 0 !== o && r(o.call(t)),
            e
        }
    }
}
, function(t, e, n) {
    var r = n(11)
      , i = n(10)
      , o = n(51)
      , u = n(7);
    t.exports = function(t, e, n, a, s) {
        r(e);
        var c = i(t)
          , f = o(c)
          , l = u(c.length)
          , h = s ? l - 1 : 0
          , p = s ? -1 : 1;
        if (n < 2)
            for (; ; ) {
                if (h in f) {
                    a = f[h],
                    h += p;
                    break
                }
                if (h += p,
                s ? h < 0 : l <= h)
                    throw TypeError("Reduce of empty array with no initial value")
            }
        for (; s ? h >= 0 : l > h; h += p)
            h in f && (a = e(a, f[h], h, c));
        return a
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(10)
      , i = n(38)
      , o = n(7);
    t.exports = [].copyWithin || function(t, e) {
        var n = r(this)
          , u = o(n.length)
          , a = i(t, u)
          , s = i(e, u)
          , c = arguments.length > 2 ? arguments[2] : void 0
          , f = Math.min((void 0 === c ? u : i(c, u)) - s, u - a)
          , l = 1;
        for (s < a && a < s + f && (l = -1,
        s += f - 1,
        a += f - 1); f-- > 0; )
            s in n ? n[a] = n[s] : delete n[a],
            a += l,
            s += l;
        return n
    }
}
, function(t, e) {
    t.exports = function(t, e) {
        return {
            value: e,
            done: !!t
        }
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(92);
    n(0)({
        target: "RegExp",
        proto: !0,
        forced: r !== /./.exec
    }, {
        exec: r
    })
}
, function(t, e, n) {
    n(8) && "g" != /./g.flags && n(9).f(RegExp.prototype, "flags", {
        configurable: !0,
        get: n(53)
    })
}
, function(t, e) {
    t.exports = function(t) {
        try {
            return {
                e: !1,
                v: t()
            }
        } catch (t) {
            return {
                e: !0,
                v: t
            }
        }
    }
}
, function(t, e, n) {
    var r = n(2)
      , i = n(5)
      , o = n(96);
    t.exports = function(t, e) {
        if (r(t),
        i(e) && e.constructor === t)
            return e;
        var n = o.f(t);
        return (0,
        n.resolve)(e),
        n.promise
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(126)
      , i = n(45);
    t.exports = n(65)("Map", (function(t) {
        return function() {
            return t(this, arguments.length > 0 ? arguments[0] : void 0)
        }
    }
    ), {
        get: function(t) {
            var e = r.getEntry(i(this, "Map"), t);
            return e && e.v
        },
        set: function(t, e) {
            return r.def(i(this, "Map"), 0 === t ? 0 : t, e)
        }
    }, r, !0)
}
, function(t, e, n) {
    "use strict";
    var r = n(9).f
      , i = n(39)
      , o = n(44)
      , u = n(22)
      , a = n(42)
      , s = n(43)
      , c = n(82)
      , f = n(120)
      , l = n(41)
      , h = n(8)
      , p = n(33).fastKey
      , d = n(45)
      , v = h ? "_s" : "size"
      , g = function(t, e) {
        var n, r = p(e);
        if ("F" !== r)
            return t._i[r];
        for (n = t._f; n; n = n.n)
            if (n.k == e)
                return n
    };
    t.exports = {
        getConstructor: function(t, e, n, c) {
            var f = t((function(t, r) {
                a(t, f, e, "_i"),
                t._t = e,
                t._i = i(null),
                t._f = void 0,
                t._l = void 0,
                t[v] = 0,
                null != r && s(r, n, t[c], t)
            }
            ));
            return o(f.prototype, {
                clear: function() {
                    for (var t = d(this, e), n = t._i, r = t._f; r; r = r.n)
                        r.r = !0,
                        r.p && (r.p = r.p.n = void 0),
                        delete n[r.i];
                    t._f = t._l = void 0,
                    t[v] = 0
                },
                delete: function(t) {
                    var n = d(this, e)
                      , r = g(n, t);
                    if (r) {
                        var i = r.n
                          , o = r.p;
                        delete n._i[r.i],
                        r.r = !0,
                        o && (o.n = i),
                        i && (i.p = o),
                        n._f == r && (n._f = i),
                        n._l == r && (n._l = o),
                        n[v]--
                    }
                    return !!r
                },
                forEach: function(t) {
                    d(this, e);
                    for (var n, r = u(t, arguments.length > 1 ? arguments[1] : void 0, 3); n = n ? n.n : this._f; )
                        for (r(n.v, n.k, this); n && n.r; )
                            n = n.p
                },
                has: function(t) {
                    return !!g(d(this, e), t)
                }
            }),
            h && r(f.prototype, "size", {
                get: function() {
                    return d(this, e)[v]
                }
            }),
            f
        },
        def: function(t, e, n) {
            var r, i, o = g(t, e);
            return o ? o.v = n : (t._l = o = {
                i: i = p(e, !0),
                k: e,
                v: n,
                p: r = t._l,
                n: void 0,
                r: !1
            },
            t._f || (t._f = o),
            r && (r.n = o),
            t[v]++,
            "F" !== i && (t._i[i] = o)),
            t
        },
        getEntry: g,
        setStrong: function(t, e, n) {
            c(t, e, (function(t, n) {
                this._t = d(t, e),
                this._k = n,
                this._l = void 0
            }
            ), (function() {
                for (var t = this._k, e = this._l; e && e.r; )
                    e = e.p;
                return this._t && (this._l = e = e ? e.n : this._t._f) ? f(0, "keys" == t ? e.k : "values" == t ? e.v : [e.k, e.v]) : (this._t = void 0,
                f(1))
            }
            ), n ? "entries" : "values", !n, !0),
            l(e)
        }
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(126)
      , i = n(45);
    t.exports = n(65)("Set", (function(t) {
        return function() {
            return t(this, arguments.length > 0 ? arguments[0] : void 0)
        }
    }
    ), {
        add: function(t) {
            return r.def(i(this, "Set"), t = 0 === t ? 0 : t, t)
        }
    }, r)
}
, function(t, e, n) {
    "use strict";
    var r, i = n(3), o = n(29)(0), u = n(13), a = n(33), s = n(107), c = n(129), f = n(5), l = n(45), h = n(45), p = !i.ActiveXObject && "ActiveXObject"in i, d = a.getWeak, v = Object.isExtensible, g = c.ufstore, y = function(t) {
        return function() {
            return t(this, arguments.length > 0 ? arguments[0] : void 0)
        }
    }, m = {
        get: function(t) {
            if (f(t)) {
                var e = d(t);
                return !0 === e ? g(l(this, "WeakMap")).get(t) : e ? e[this._i] : void 0
            }
        },
        set: function(t, e) {
            return c.def(l(this, "WeakMap"), t, e)
        }
    }, b = t.exports = n(65)("WeakMap", y, m, c, !0, !0);
    h && p && (s((r = c.getConstructor(y, "WeakMap")).prototype, m),
    a.NEED = !0,
    o(["delete", "has", "get", "set"], (function(t) {
        var e = b.prototype
          , n = e[t];
        u(e, t, (function(e, i) {
            if (f(e) && !v(e)) {
                this._f || (this._f = new r);
                var o = this._f[t](e, i);
                return "set" == t ? this : o
            }
            return n.call(this, e, i)
        }
        ))
    }
    )))
}
, function(t, e, n) {
    "use strict";
    var r = n(44)
      , i = n(33).getWeak
      , o = n(2)
      , u = n(5)
      , a = n(42)
      , s = n(43)
      , c = n(29)
      , f = n(16)
      , l = n(45)
      , h = c(5)
      , p = c(6)
      , d = 0
      , v = function(t) {
        return t._l || (t._l = new g)
    }
      , g = function() {
        this.a = []
    }
      , y = function(t, e) {
        return h(t.a, (function(t) {
            return t[0] === e
        }
        ))
    };
    g.prototype = {
        get: function(t) {
            var e = y(this, t);
            if (e)
                return e[1]
        },
        has: function(t) {
            return !!y(this, t)
        },
        set: function(t, e) {
            var n = y(this, t);
            n ? n[1] = e : this.a.push([t, e])
        },
        delete: function(t) {
            var e = p(this.a, (function(e) {
                return e[0] === t
            }
            ));
            return ~e && this.a.splice(e, 1),
            !!~e
        }
    },
    t.exports = {
        getConstructor: function(t, e, n, o) {
            var c = t((function(t, r) {
                a(t, c, e, "_i"),
                t._t = e,
                t._i = d++,
                t._l = void 0,
                null != r && s(r, n, t[o], t)
            }
            ));
            return r(c.prototype, {
                delete: function(t) {
                    if (!u(t))
                        return !1;
                    var n = i(t);
                    return !0 === n ? v(l(this, e)).delete(t) : n && f(n, this._i) && delete n[this._i]
                },
                has: function(t) {
                    if (!u(t))
                        return !1;
                    var n = i(t);
                    return !0 === n ? v(l(this, e)).has(t) : n && f(n, this._i)
                }
            }),
            c
        },
        def: function(t, e, n) {
            var r = i(o(e), !0);
            return !0 === r ? v(t).set(e, n) : r[t._i] = n,
            t
        },
        ufstore: v
    }
}
, function(t, e, n) {
    var r = n(24)
      , i = n(7);
    t.exports = function(t) {
        if (void 0 === t)
            return 0;
        var e = r(t)
          , n = i(e);
        if (e !== n)
            throw RangeError("Wrong length!");
        return n
    }
}
, function(t, e, n) {
    var r = n(40)
      , i = n(57)
      , o = n(2)
      , u = n(3).Reflect;
    t.exports = u && u.ownKeys || function(t) {
        var e = r.f(o(t))
          , n = i.f;
        return n ? e.concat(n(t)) : e
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(58)
      , i = n(5)
      , o = n(7)
      , u = n(22)
      , a = n(6)("isConcatSpreadable");
    t.exports = function t(e, n, s, c, f, l, h, p) {
        for (var d, v, g = f, y = 0, m = !!h && u(h, p, 3); y < c; ) {
            if (y in s) {
                if (d = m ? m(s[y], y, n) : s[y],
                v = !1,
                i(d) && (v = void 0 !== (v = d[a]) ? !!v : r(d)),
                v && l > 0)
                    g = t(e, n, d, o(d.length), g, l - 1) - 1;
                else {
                    if (g >= 9007199254740991)
                        throw TypeError();
                    e[g] = d
                }
                g++
            }
            y++
        }
        return g
    }
}
, function(t, e, n) {
    var r = n(7)
      , i = n(79)
      , o = n(27);
    t.exports = function(t, e, n, u) {
        var a = String(o(t))
          , s = a.length
          , c = void 0 === n ? " " : String(n)
          , f = r(e);
        if (f <= s || "" == c)
            return a;
        var l = f - s
          , h = i.call(c, Math.ceil(l / c.length));
        return h.length > l && (h = h.slice(0, l)),
        u ? h + a : a + h
    }
}
, function(t, e, n) {
    var r = n(8)
      , i = n(37)
      , o = n(17)
      , u = n(52).f;
    t.exports = function(t) {
        return function(e) {
            for (var n, a = o(e), s = i(a), c = s.length, f = 0, l = []; c > f; )
                n = s[f++],
                r && !u.call(a, n) || l.push(t ? [n, a[n]] : a[n]);
            return l
        }
    }
}
, function(t, e, n) {
    var r = n(47)
      , i = n(136);
    t.exports = function(t) {
        return function() {
            if (r(this) != t)
                throw TypeError(t + "#toJSON isn't generic");
            return i(this)
        }
    }
}
, function(t, e, n) {
    var r = n(43);
    t.exports = function(t, e) {
        var n = [];
        return r(t, !1, n.push, n, e),
        n
    }
}
, function(t, e) {
    t.exports = Math.scale || function(t, e, n, r, i) {
        return 0 === arguments.length || t != t || e != e || n != n || r != r || i != i ? NaN : t === 1 / 0 || t === -1 / 0 ? t : (t - e) * (i - r) / (n - e) + r
    }
}
, function(t, e, n) {
    var r = n(357)
      , i = {};
    for (var o in r)
        r.hasOwnProperty(o) && (i[r[o]] = o);
    var u = t.exports = {
        rgb: {
            channels: 3,
            labels: "rgb"
        },
        hsl: {
            channels: 3,
            labels: "hsl"
        },
        hsv: {
            channels: 3,
            labels: "hsv"
        },
        hwb: {
            channels: 3,
            labels: "hwb"
        },
        cmyk: {
            channels: 4,
            labels: "cmyk"
        },
        xyz: {
            channels: 3,
            labels: "xyz"
        },
        lab: {
            channels: 3,
            labels: "lab"
        },
        lch: {
            channels: 3,
            labels: "lch"
        },
        hex: {
            channels: 1,
            labels: ["hex"]
        },
        keyword: {
            channels: 1,
            labels: ["keyword"]
        },
        ansi16: {
            channels: 1,
            labels: ["ansi16"]
        },
        ansi256: {
            channels: 1,
            labels: ["ansi256"]
        },
        hcg: {
            channels: 3,
            labels: ["h", "c", "g"]
        },
        apple: {
            channels: 3,
            labels: ["r16", "g16", "b16"]
        },
        gray: {
            channels: 1,
            labels: ["gray"]
        }
    };
    for (var a in u)
        if (u.hasOwnProperty(a)) {
            if (!("channels"in u[a]))
                throw new Error("missing channels property: " + a);
            if (!("labels"in u[a]))
                throw new Error("missing channel labels property: " + a);
            if (u[a].labels.length !== u[a].channels)
                throw new Error("channel and label counts mismatch: " + a);
            var s = u[a].channels
              , c = u[a].labels;
            delete u[a].channels,
            delete u[a].labels,
            Object.defineProperty(u[a], "channels", {
                value: s
            }),
            Object.defineProperty(u[a], "labels", {
                value: c
            })
        }
    u.rgb.hsl = function(t) {
        var e, n, r = t[0] / 255, i = t[1] / 255, o = t[2] / 255, u = Math.min(r, i, o), a = Math.max(r, i, o), s = a - u;
        return a === u ? e = 0 : r === a ? e = (i - o) / s : i === a ? e = 2 + (o - r) / s : o === a && (e = 4 + (r - i) / s),
        (e = Math.min(60 * e, 360)) < 0 && (e += 360),
        n = (u + a) / 2,
        [e, 100 * (a === u ? 0 : n <= .5 ? s / (a + u) : s / (2 - a - u)), 100 * n]
    }
    ,
    u.rgb.hsv = function(t) {
        var e, n, r, i, o, u = t[0] / 255, a = t[1] / 255, s = t[2] / 255, c = Math.max(u, a, s), f = c - Math.min(u, a, s), l = function(t) {
            return (c - t) / 6 / f + .5
        };
        return 0 === f ? i = o = 0 : (o = f / c,
        e = l(u),
        n = l(a),
        r = l(s),
        u === c ? i = r - n : a === c ? i = 1 / 3 + e - r : s === c && (i = 2 / 3 + n - e),
        i < 0 ? i += 1 : i > 1 && (i -= 1)),
        [360 * i, 100 * o, 100 * c]
    }
    ,
    u.rgb.hwb = function(t) {
        var e = t[0]
          , n = t[1]
          , r = t[2];
        return [u.rgb.hsl(t)[0], 100 * (1 / 255 * Math.min(e, Math.min(n, r))), 100 * (r = 1 - 1 / 255 * Math.max(e, Math.max(n, r)))]
    }
    ,
    u.rgb.cmyk = function(t) {
        var e, n = t[0] / 255, r = t[1] / 255, i = t[2] / 255;
        return [100 * ((1 - n - (e = Math.min(1 - n, 1 - r, 1 - i))) / (1 - e) || 0), 100 * ((1 - r - e) / (1 - e) || 0), 100 * ((1 - i - e) / (1 - e) || 0), 100 * e]
    }
    ,
    u.rgb.keyword = function(t) {
        var e = i[t];
        if (e)
            return e;
        var n, o, u, a = 1 / 0;
        for (var s in r)
            if (r.hasOwnProperty(s)) {
                var c = r[s]
                  , f = (o = t,
                u = c,
                Math.pow(o[0] - u[0], 2) + Math.pow(o[1] - u[1], 2) + Math.pow(o[2] - u[2], 2));
                f < a && (a = f,
                n = s)
            }
        return n
    }
    ,
    u.keyword.rgb = function(t) {
        return r[t]
    }
    ,
    u.rgb.xyz = function(t) {
        var e = t[0] / 255
          , n = t[1] / 255
          , r = t[2] / 255;
        return [100 * (.4124 * (e = e > .04045 ? Math.pow((e + .055) / 1.055, 2.4) : e / 12.92) + .3576 * (n = n > .04045 ? Math.pow((n + .055) / 1.055, 2.4) : n / 12.92) + .1805 * (r = r > .04045 ? Math.pow((r + .055) / 1.055, 2.4) : r / 12.92)), 100 * (.2126 * e + .7152 * n + .0722 * r), 100 * (.0193 * e + .1192 * n + .9505 * r)]
    }
    ,
    u.rgb.lab = function(t) {
        var e = u.rgb.xyz(t)
          , n = e[0]
          , r = e[1]
          , i = e[2];
        return r /= 100,
        i /= 108.883,
        n = (n /= 95.047) > .008856 ? Math.pow(n, 1 / 3) : 7.787 * n + 16 / 116,
        [116 * (r = r > .008856 ? Math.pow(r, 1 / 3) : 7.787 * r + 16 / 116) - 16, 500 * (n - r), 200 * (r - (i = i > .008856 ? Math.pow(i, 1 / 3) : 7.787 * i + 16 / 116))]
    }
    ,
    u.hsl.rgb = function(t) {
        var e, n, r, i, o, u = t[0] / 360, a = t[1] / 100, s = t[2] / 100;
        if (0 === a)
            return [o = 255 * s, o, o];
        e = 2 * s - (n = s < .5 ? s * (1 + a) : s + a - s * a),
        i = [0, 0, 0];
        for (var c = 0; c < 3; c++)
            (r = u + 1 / 3 * -(c - 1)) < 0 && r++,
            r > 1 && r--,
            o = 6 * r < 1 ? e + 6 * (n - e) * r : 2 * r < 1 ? n : 3 * r < 2 ? e + (n - e) * (2 / 3 - r) * 6 : e,
            i[c] = 255 * o;
        return i
    }
    ,
    u.hsl.hsv = function(t) {
        var e = t[0]
          , n = t[1] / 100
          , r = t[2] / 100
          , i = n
          , o = Math.max(r, .01);
        return n *= (r *= 2) <= 1 ? r : 2 - r,
        i *= o <= 1 ? o : 2 - o,
        [e, 100 * (0 === r ? 2 * i / (o + i) : 2 * n / (r + n)), 100 * ((r + n) / 2)]
    }
    ,
    u.hsv.rgb = function(t) {
        var e = t[0] / 60
          , n = t[1] / 100
          , r = t[2] / 100
          , i = Math.floor(e) % 6
          , o = e - Math.floor(e)
          , u = 255 * r * (1 - n)
          , a = 255 * r * (1 - n * o)
          , s = 255 * r * (1 - n * (1 - o));
        switch (r *= 255,
        i) {
        case 0:
            return [r, s, u];
        case 1:
            return [a, r, u];
        case 2:
            return [u, r, s];
        case 3:
            return [u, a, r];
        case 4:
            return [s, u, r];
        case 5:
            return [r, u, a]
        }
    }
    ,
    u.hsv.hsl = function(t) {
        var e, n, r, i = t[0], o = t[1] / 100, u = t[2] / 100, a = Math.max(u, .01);
        return r = (2 - o) * u,
        n = o * a,
        [i, 100 * (n = (n /= (e = (2 - o) * a) <= 1 ? e : 2 - e) || 0), 100 * (r /= 2)]
    }
    ,
    u.hwb.rgb = function(t) {
        var e, n, r, i, o, u, a, s = t[0] / 360, c = t[1] / 100, f = t[2] / 100, l = c + f;
        switch (l > 1 && (c /= l,
        f /= l),
        r = 6 * s - (e = Math.floor(6 * s)),
        0 != (1 & e) && (r = 1 - r),
        i = c + r * ((n = 1 - f) - c),
        e) {
        default:
        case 6:
        case 0:
            o = n,
            u = i,
            a = c;
            break;
        case 1:
            o = i,
            u = n,
            a = c;
            break;
        case 2:
            o = c,
            u = n,
            a = i;
            break;
        case 3:
            o = c,
            u = i,
            a = n;
            break;
        case 4:
            o = i,
            u = c,
            a = n;
            break;
        case 5:
            o = n,
            u = c,
            a = i
        }
        return [255 * o, 255 * u, 255 * a]
    }
    ,
    u.cmyk.rgb = function(t) {
        var e = t[0] / 100
          , n = t[1] / 100
          , r = t[2] / 100
          , i = t[3] / 100;
        return [255 * (1 - Math.min(1, e * (1 - i) + i)), 255 * (1 - Math.min(1, n * (1 - i) + i)), 255 * (1 - Math.min(1, r * (1 - i) + i))]
    }
    ,
    u.xyz.rgb = function(t) {
        var e, n, r, i = t[0] / 100, o = t[1] / 100, u = t[2] / 100;
        return n = -.9689 * i + 1.8758 * o + .0415 * u,
        r = .0557 * i + -.204 * o + 1.057 * u,
        e = (e = 3.2406 * i + -1.5372 * o + -.4986 * u) > .0031308 ? 1.055 * Math.pow(e, 1 / 2.4) - .055 : 12.92 * e,
        n = n > .0031308 ? 1.055 * Math.pow(n, 1 / 2.4) - .055 : 12.92 * n,
        r = r > .0031308 ? 1.055 * Math.pow(r, 1 / 2.4) - .055 : 12.92 * r,
        [255 * (e = Math.min(Math.max(0, e), 1)), 255 * (n = Math.min(Math.max(0, n), 1)), 255 * (r = Math.min(Math.max(0, r), 1))]
    }
    ,
    u.xyz.lab = function(t) {
        var e = t[0]
          , n = t[1]
          , r = t[2];
        return n /= 100,
        r /= 108.883,
        e = (e /= 95.047) > .008856 ? Math.pow(e, 1 / 3) : 7.787 * e + 16 / 116,
        [116 * (n = n > .008856 ? Math.pow(n, 1 / 3) : 7.787 * n + 16 / 116) - 16, 500 * (e - n), 200 * (n - (r = r > .008856 ? Math.pow(r, 1 / 3) : 7.787 * r + 16 / 116))]
    }
    ,
    u.lab.xyz = function(t) {
        var e, n, r, i = t[0];
        e = t[1] / 500 + (n = (i + 16) / 116),
        r = n - t[2] / 200;
        var o = Math.pow(n, 3)
          , u = Math.pow(e, 3)
          , a = Math.pow(r, 3);
        return n = o > .008856 ? o : (n - 16 / 116) / 7.787,
        e = u > .008856 ? u : (e - 16 / 116) / 7.787,
        r = a > .008856 ? a : (r - 16 / 116) / 7.787,
        [e *= 95.047, n *= 100, r *= 108.883]
    }
    ,
    u.lab.lch = function(t) {
        var e, n = t[0], r = t[1], i = t[2];
        return (e = 360 * Math.atan2(i, r) / 2 / Math.PI) < 0 && (e += 360),
        [n, Math.sqrt(r * r + i * i), e]
    }
    ,
    u.lch.lab = function(t) {
        var e, n = t[0], r = t[1];
        return e = t[2] / 360 * 2 * Math.PI,
        [n, r * Math.cos(e), r * Math.sin(e)]
    }
    ,
    u.rgb.ansi16 = function(t) {
        var e = t[0]
          , n = t[1]
          , r = t[2]
          , i = 1 in arguments ? arguments[1] : u.rgb.hsv(t)[2];
        if (0 === (i = Math.round(i / 50)))
            return 30;
        var o = 30 + (Math.round(r / 255) << 2 | Math.round(n / 255) << 1 | Math.round(e / 255));
        return 2 === i && (o += 60),
        o
    }
    ,
    u.hsv.ansi16 = function(t) {
        return u.rgb.ansi16(u.hsv.rgb(t), t[2])
    }
    ,
    u.rgb.ansi256 = function(t) {
        var e = t[0]
          , n = t[1]
          , r = t[2];
        return e === n && n === r ? e < 8 ? 16 : e > 248 ? 231 : Math.round((e - 8) / 247 * 24) + 232 : 16 + 36 * Math.round(e / 255 * 5) + 6 * Math.round(n / 255 * 5) + Math.round(r / 255 * 5)
    }
    ,
    u.ansi16.rgb = function(t) {
        var e = t % 10;
        if (0 === e || 7 === e)
            return t > 50 && (e += 3.5),
            [e = e / 10.5 * 255, e, e];
        var n = .5 * (1 + ~~(t > 50));
        return [(1 & e) * n * 255, (e >> 1 & 1) * n * 255, (e >> 2 & 1) * n * 255]
    }
    ,
    u.ansi256.rgb = function(t) {
        if (t >= 232) {
            var e = 10 * (t - 232) + 8;
            return [e, e, e]
        }
        var n;
        return t -= 16,
        [Math.floor(t / 36) / 5 * 255, Math.floor((n = t % 36) / 6) / 5 * 255, n % 6 / 5 * 255]
    }
    ,
    u.rgb.hex = function(t) {
        var e = (((255 & Math.round(t[0])) << 16) + ((255 & Math.round(t[1])) << 8) + (255 & Math.round(t[2]))).toString(16).toUpperCase();
        return "000000".substring(e.length) + e
    }
    ,
    u.hex.rgb = function(t) {
        var e = t.toString(16).match(/[a-f0-9]{6}|[a-f0-9]{3}/i);
        if (!e)
            return [0, 0, 0];
        var n = e[0];
        3 === e[0].length && (n = n.split("").map((function(t) {
            return t + t
        }
        )).join(""));
        var r = parseInt(n, 16);
        return [r >> 16 & 255, r >> 8 & 255, 255 & r]
    }
    ,
    u.rgb.hcg = function(t) {
        var e, n = t[0] / 255, r = t[1] / 255, i = t[2] / 255, o = Math.max(Math.max(n, r), i), u = Math.min(Math.min(n, r), i), a = o - u;
        return e = a <= 0 ? 0 : o === n ? (r - i) / a % 6 : o === r ? 2 + (i - n) / a : 4 + (n - r) / a + 4,
        e /= 6,
        [360 * (e %= 1), 100 * a, 100 * (a < 1 ? u / (1 - a) : 0)]
    }
    ,
    u.hsl.hcg = function(t) {
        var e = t[1] / 100
          , n = t[2] / 100
          , r = 1
          , i = 0;
        return (r = n < .5 ? 2 * e * n : 2 * e * (1 - n)) < 1 && (i = (n - .5 * r) / (1 - r)),
        [t[0], 100 * r, 100 * i]
    }
    ,
    u.hsv.hcg = function(t) {
        var e = t[1] / 100
          , n = t[2] / 100
          , r = e * n
          , i = 0;
        return r < 1 && (i = (n - r) / (1 - r)),
        [t[0], 100 * r, 100 * i]
    }
    ,
    u.hcg.rgb = function(t) {
        var e = t[0] / 360
          , n = t[1] / 100
          , r = t[2] / 100;
        if (0 === n)
            return [255 * r, 255 * r, 255 * r];
        var i, o = [0, 0, 0], u = e % 1 * 6, a = u % 1, s = 1 - a;
        switch (Math.floor(u)) {
        case 0:
            o[0] = 1,
            o[1] = a,
            o[2] = 0;
            break;
        case 1:
            o[0] = s,
            o[1] = 1,
            o[2] = 0;
            break;
        case 2:
            o[0] = 0,
            o[1] = 1,
            o[2] = a;
            break;
        case 3:
            o[0] = 0,
            o[1] = s,
            o[2] = 1;
            break;
        case 4:
            o[0] = a,
            o[1] = 0,
            o[2] = 1;
            break;
        default:
            o[0] = 1,
            o[1] = 0,
            o[2] = s
        }
        return i = (1 - n) * r,
        [255 * (n * o[0] + i), 255 * (n * o[1] + i), 255 * (n * o[2] + i)]
    }
    ,
    u.hcg.hsv = function(t) {
        var e = t[1] / 100
          , n = e + t[2] / 100 * (1 - e)
          , r = 0;
        return n > 0 && (r = e / n),
        [t[0], 100 * r, 100 * n]
    }
    ,
    u.hcg.hsl = function(t) {
        var e = t[1] / 100
          , n = t[2] / 100 * (1 - e) + .5 * e
          , r = 0;
        return n > 0 && n < .5 ? r = e / (2 * n) : n >= .5 && n < 1 && (r = e / (2 * (1 - n))),
        [t[0], 100 * r, 100 * n]
    }
    ,
    u.hcg.hwb = function(t) {
        var e = t[1] / 100
          , n = e + t[2] / 100 * (1 - e);
        return [t[0], 100 * (n - e), 100 * (1 - n)]
    }
    ,
    u.hwb.hcg = function(t) {
        var e = t[1] / 100
          , n = 1 - t[2] / 100
          , r = n - e
          , i = 0;
        return r < 1 && (i = (n - r) / (1 - r)),
        [t[0], 100 * r, 100 * i]
    }
    ,
    u.apple.rgb = function(t) {
        return [t[0] / 65535 * 255, t[1] / 65535 * 255, t[2] / 65535 * 255]
    }
    ,
    u.rgb.apple = function(t) {
        return [t[0] / 255 * 65535, t[1] / 255 * 65535, t[2] / 255 * 65535]
    }
    ,
    u.gray.rgb = function(t) {
        return [t[0] / 100 * 255, t[0] / 100 * 255, t[0] / 100 * 255]
    }
    ,
    u.gray.hsl = u.gray.hsv = function(t) {
        return [0, 0, t[0]]
    }
    ,
    u.gray.hwb = function(t) {
        return [0, 100, t[0]]
    }
    ,
    u.gray.cmyk = function(t) {
        return [0, 0, 0, t[0]]
    }
    ,
    u.gray.lab = function(t) {
        return [t[0], 0, 0]
    }
    ,
    u.gray.hex = function(t) {
        var e = 255 & Math.round(t[0] / 100 * 255)
          , n = ((e << 16) + (e << 8) + e).toString(16).toUpperCase();
        return "000000".substring(n.length) + n
    }
    ,
    u.rgb.gray = function(t) {
        return [(t[0] + t[1] + t[2]) / 3 / 255 * 100]
    }
}
, function(t, e, n) {
    const r = n(99)
      , i = {
        level: t=>`[${t.level}]`,
        name: t=>t.logger.name,
        template: "{{time}} {{level}} ",
        time: ()=>(new Date).toTimeString().split(" ")[0]
    };
    t.exports = class extends r {
        constructor(t, e) {
            super(t),
            this.options = Object.assign({}, i, e)
        }
        interpolate(t) {
            return this.options.template.replace(/{{([^{}]*)}}/g, (e,n)=>{
                const r = this.options[n];
                return r ? r({
                    level: t,
                    logger: this.logger
                }) : e
            }
            )
        }
        make(t) {
            const e = super.make(t);
            return (...n)=>{
                const r = this.interpolate(t)
                  , [i] = n;
                "string" == typeof i ? n[0] = r + i : n.unshift(r),
                e(...n)
            }
        }
    }
}
, function(t, e, n) {
    "use strict";
    t.exports = function(t, e) {
        return function() {
            for (var n = new Array(arguments.length), r = 0; r < n.length; r++)
                n[r] = arguments[r];
            return t.apply(e, n)
        }
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(15);
    function i(t) {
        return encodeURIComponent(t).replace(/%40/gi, "@").replace(/%3A/gi, ":").replace(/%24/g, "$").replace(/%2C/gi, ",").replace(/%20/g, "+").replace(/%5B/gi, "[").replace(/%5D/gi, "]")
    }
    t.exports = function(t, e, n) {
        if (!e)
            return t;
        var o;
        if (n)
            o = n(e);
        else if (r.isURLSearchParams(e))
            o = e.toString();
        else {
            var u = [];
            r.forEach(e, (function(t, e) {
                null != t && (r.isArray(t) ? e += "[]" : t = [t],
                r.forEach(t, (function(t) {
                    r.isDate(t) ? t = t.toISOString() : r.isObject(t) && (t = JSON.stringify(t)),
                    u.push(i(e) + "=" + i(t))
                }
                )))
            }
            )),
            o = u.join("&")
        }
        if (o) {
            var a = t.indexOf("#");
            -1 !== a && (t = t.slice(0, a)),
            t += (-1 === t.indexOf("?") ? "?" : "&") + o
        }
        return t
    }
}
, function(t, e, n) {
    "use strict";
    t.exports = function(t) {
        return !(!t || !t.__CANCEL__)
    }
}
, function(t, e, n) {
    "use strict";
    (function(e) {
        var r = n(15)
          , i = n(371)
          , o = {
            "Content-Type": "application/x-www-form-urlencoded"
        };
        function u(t, e) {
            !r.isUndefined(t) && r.isUndefined(t["Content-Type"]) && (t["Content-Type"] = e)
        }
        var a, s = {
            adapter: ("undefined" != typeof XMLHttpRequest ? a = n(144) : void 0 !== e && "[object process]" === Object.prototype.toString.call(e) && (a = n(144)),
            a),
            transformRequest: [function(t, e) {
                return i(e, "Accept"),
                i(e, "Content-Type"),
                r.isFormData(t) || r.isArrayBuffer(t) || r.isBuffer(t) || r.isStream(t) || r.isFile(t) || r.isBlob(t) ? t : r.isArrayBufferView(t) ? t.buffer : r.isURLSearchParams(t) ? (u(e, "application/x-www-form-urlencoded;charset=utf-8"),
                t.toString()) : r.isObject(t) ? (u(e, "application/json;charset=utf-8"),
                JSON.stringify(t)) : t
            }
            ],
            transformResponse: [function(t) {
                if ("string" == typeof t)
                    try {
                        t = JSON.parse(t)
                    } catch (t) {}
                return t
            }
            ],
            timeout: 0,
            xsrfCookieName: "XSRF-TOKEN",
            xsrfHeaderName: "X-XSRF-TOKEN",
            maxContentLength: -1,
            validateStatus: function(t) {
                return t >= 200 && t < 300
            }
        };
        s.headers = {
            common: {
                Accept: "application/json, text/plain, */*"
            }
        },
        r.forEach(["delete", "get", "head"], (function(t) {
            s.headers[t] = {}
        }
        )),
        r.forEach(["post", "put", "patch"], (function(t) {
            s.headers[t] = r.merge(o)
        }
        )),
        t.exports = s
    }
    ).call(this, n(98))
}
, function(t, e, n) {
    "use strict";
    var r = n(15)
      , i = n(372)
      , o = n(141)
      , u = n(374)
      , a = n(377)
      , s = n(378)
      , c = n(145);
    t.exports = function(t) {
        return new Promise((function(e, f) {
            var l = t.data
              , h = t.headers;
            r.isFormData(l) && delete h["Content-Type"];
            var p = new XMLHttpRequest;
            if (t.auth) {
                var d = t.auth.username || ""
                  , v = t.auth.password || "";
                h.Authorization = "Basic " + btoa(d + ":" + v)
            }
            var g = u(t.baseURL, t.url);
            if (p.open(t.method.toUpperCase(), o(g, t.params, t.paramsSerializer), !0),
            p.timeout = t.timeout,
            p.onreadystatechange = function() {
                if (p && 4 === p.readyState && (0 !== p.status || p.responseURL && 0 === p.responseURL.indexOf("file:"))) {
                    var n = "getAllResponseHeaders"in p ? a(p.getAllResponseHeaders()) : null
                      , r = {
                        data: t.responseType && "text" !== t.responseType ? p.response : p.responseText,
                        status: p.status,
                        statusText: p.statusText,
                        headers: n,
                        config: t,
                        request: p
                    };
                    i(e, f, r),
                    p = null
                }
            }
            ,
            p.onabort = function() {
                p && (f(c("Request aborted", t, "ECONNABORTED", p)),
                p = null)
            }
            ,
            p.onerror = function() {
                f(c("Network Error", t, null, p)),
                p = null
            }
            ,
            p.ontimeout = function() {
                var e = "timeout of " + t.timeout + "ms exceeded";
                t.timeoutErrorMessage && (e = t.timeoutErrorMessage),
                f(c(e, t, "ECONNABORTED", p)),
                p = null
            }
            ,
            r.isStandardBrowserEnv()) {
                var y = n(379)
                  , m = (t.withCredentials || s(g)) && t.xsrfCookieName ? y.read(t.xsrfCookieName) : void 0;
                m && (h[t.xsrfHeaderName] = m)
            }
            if ("setRequestHeader"in p && r.forEach(h, (function(t, e) {
                void 0 === l && "content-type" === e.toLowerCase() ? delete h[e] : p.setRequestHeader(e, t)
            }
            )),
            r.isUndefined(t.withCredentials) || (p.withCredentials = !!t.withCredentials),
            t.responseType)
                try {
                    p.responseType = t.responseType
                } catch (e) {
                    if ("json" !== t.responseType)
                        throw e
                }
            "function" == typeof t.onDownloadProgress && p.addEventListener("progress", t.onDownloadProgress),
            "function" == typeof t.onUploadProgress && p.upload && p.upload.addEventListener("progress", t.onUploadProgress),
            t.cancelToken && t.cancelToken.promise.then((function(t) {
                p && (p.abort(),
                f(t),
                p = null)
            }
            )),
            void 0 === l && (l = null),
            p.send(l)
        }
        ))
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(373);
    t.exports = function(t, e, n, i, o) {
        var u = new Error(t);
        return r(u, e, n, i, o)
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(15);
    t.exports = function(t, e) {
        e = e || {};
        var n = {}
          , i = ["url", "method", "params", "data"]
          , o = ["headers", "auth", "proxy"]
          , u = ["baseURL", "url", "transformRequest", "transformResponse", "paramsSerializer", "timeout", "withCredentials", "adapter", "responseType", "xsrfCookieName", "xsrfHeaderName", "onUploadProgress", "onDownloadProgress", "maxContentLength", "validateStatus", "maxRedirects", "httpAgent", "httpsAgent", "cancelToken", "socketPath"];
        r.forEach(i, (function(t) {
            void 0 !== e[t] && (n[t] = e[t])
        }
        )),
        r.forEach(o, (function(i) {
            r.isObject(e[i]) ? n[i] = r.deepMerge(t[i], e[i]) : void 0 !== e[i] ? n[i] = e[i] : r.isObject(t[i]) ? n[i] = r.deepMerge(t[i]) : void 0 !== t[i] && (n[i] = t[i])
        }
        )),
        r.forEach(u, (function(r) {
            void 0 !== e[r] ? n[r] = e[r] : void 0 !== t[r] && (n[r] = t[r])
        }
        ));
        var a = i.concat(o).concat(u)
          , s = Object.keys(e).filter((function(t) {
            return -1 === a.indexOf(t)
        }
        ));
        return r.forEach(s, (function(r) {
            void 0 !== e[r] ? n[r] = e[r] : void 0 !== t[r] && (n[r] = t[r])
        }
        )),
        n
    }
}
, function(t, e, n) {
    "use strict";
    function r(t) {
        this.message = t
    }
    r.prototype.toString = function() {
        return "Cancel" + (this.message ? ": " + this.message : "")
    }
    ,
    r.prototype.__CANCEL__ = !0,
    t.exports = r
}
, function(t, e, n) {
    (function(e) {
        const r = n(352)
          , i = n(361)
          , o = n(363)
          , u = {
            level: "info",
            name: "<webpack-log>",
            timestamp: !1,
            unique: !0
        }
          , a = "⬡"
          , s = "⬢"
          , c = {
            trace: "cyan",
            debug: "magenta",
            info: "blue",
            warn: "yellow",
            error: "red"
        }
          , f = t=>{
            const e = Object.assign({}, u, t)
              , n = {
                level: ({level: t})=>{
                    const n = c[t]
                      , i = ["error", "warn"].includes(t) ? s : a;
                    return r[n](`${i} ${e.name}: `)
                }
                ,
                template: "{{level}}"
            };
            return e.timestamp && (n.template = `[{{time}}] ${n.template}`),
            e.id = e.name + (e.unique ? o() : ""),
            e.prefix = n,
            i.create(e)
        }
        ;
        Object.defineProperty(f, "colors", {
            get: ()=>(e.emitWarning("The colors property in webpack-log exports is deprecated. Please install and use `chalk` or a similar ANSI color module.", {
                type: "DeprecationWarning"
            }),
            r)
        }),
        f.delete = t=>{
            delete i.loggers[t]
        }
        ,
        t.exports = f
    }
    ).call(this, n(98))
}
, function(t, e, n) {
    n(150),
    n(384),
    n(383),
    t.exports = n(382)
}
, function(t, e, n) {
    "use strict";
    (function(t) {
        if (n(151),
        n(348),
        n(349),
        t._babelPolyfill)
            throw new Error("only one instance of babel-polyfill is allowed");
        t._babelPolyfill = !0;
        function e(t, e, n) {
            t[e] || Object.defineProperty(t, e, {
                writable: !0,
                configurable: !0,
                value: n
            })
        }
        e(String.prototype, "padLeft", "".padStart),
        e(String.prototype, "padRight", "".padEnd),
        "pop,reverse,shift,keys,values,entries,indexOf,every,some,forEach,map,filter,find,findIndex,includes,join,slice,concat,push,splice,unshift,sort,lastIndexOf,reduce,reduceRight,copyWithin,fill".split(",").forEach((function(t) {
            [][t] && e(Array, t, Function.call.bind([][t]))
        }
        ))
    }
    ).call(this, n(101))
}
, function(t, e, n) {
    n(152),
    n(155),
    n(156),
    n(157),
    n(158),
    n(159),
    n(160),
    n(161),
    n(162),
    n(163),
    n(164),
    n(165),
    n(166),
    n(167),
    n(168),
    n(169),
    n(170),
    n(171),
    n(172),
    n(173),
    n(174),
    n(175),
    n(176),
    n(177),
    n(178),
    n(179),
    n(180),
    n(181),
    n(182),
    n(183),
    n(184),
    n(185),
    n(186),
    n(187),
    n(188),
    n(189),
    n(190),
    n(191),
    n(192),
    n(193),
    n(194),
    n(195),
    n(196),
    n(197),
    n(198),
    n(199),
    n(200),
    n(201),
    n(202),
    n(203),
    n(204),
    n(205),
    n(206),
    n(207),
    n(208),
    n(209),
    n(210),
    n(211),
    n(212),
    n(213),
    n(214),
    n(215),
    n(216),
    n(217),
    n(218),
    n(219),
    n(220),
    n(221),
    n(222),
    n(223),
    n(224),
    n(225),
    n(226),
    n(227),
    n(228),
    n(229),
    n(230),
    n(232),
    n(233),
    n(235),
    n(236),
    n(237),
    n(238),
    n(239),
    n(240),
    n(241),
    n(243),
    n(244),
    n(245),
    n(246),
    n(247),
    n(248),
    n(249),
    n(250),
    n(251),
    n(252),
    n(253),
    n(254),
    n(255),
    n(91),
    n(256),
    n(121),
    n(257),
    n(122),
    n(258),
    n(259),
    n(260),
    n(261),
    n(262),
    n(125),
    n(127),
    n(128),
    n(263),
    n(264),
    n(265),
    n(266),
    n(267),
    n(268),
    n(269),
    n(270),
    n(271),
    n(272),
    n(273),
    n(274),
    n(275),
    n(276),
    n(277),
    n(278),
    n(279),
    n(280),
    n(281),
    n(282),
    n(283),
    n(284),
    n(285),
    n(286),
    n(287),
    n(288),
    n(289),
    n(290),
    n(291),
    n(292),
    n(293),
    n(294),
    n(295),
    n(296),
    n(297),
    n(298),
    n(299),
    n(300),
    n(301),
    n(302),
    n(303),
    n(304),
    n(305),
    n(306),
    n(307),
    n(308),
    n(309),
    n(310),
    n(311),
    n(312),
    n(313),
    n(314),
    n(315),
    n(316),
    n(317),
    n(318),
    n(319),
    n(320),
    n(321),
    n(322),
    n(323),
    n(324),
    n(325),
    n(326),
    n(327),
    n(328),
    n(329),
    n(330),
    n(331),
    n(332),
    n(333),
    n(334),
    n(335),
    n(336),
    n(337),
    n(338),
    n(339),
    n(340),
    n(341),
    n(342),
    n(343),
    n(344),
    n(345),
    n(346),
    n(347),
    t.exports = n(21)
}
, function(t, e, n) {
    "use strict";
    var r = n(3)
      , i = n(16)
      , o = n(8)
      , u = n(0)
      , a = n(13)
      , s = n(33).KEY
      , c = n(4)
      , f = n(50)
      , l = n(46)
      , h = n(36)
      , p = n(6)
      , d = n(103)
      , v = n(72)
      , g = n(154)
      , y = n(58)
      , m = n(2)
      , b = n(5)
      , w = n(10)
      , _ = n(17)
      , x = n(26)
      , S = n(35)
      , k = n(39)
      , O = n(106)
      , E = n(18)
      , j = n(57)
      , M = n(9)
      , P = n(37)
      , R = E.f
      , T = M.f
      , A = O.f
      , L = r.Symbol
      , F = r.JSON
      , I = F && F.stringify
      , N = p("_hidden")
      , C = p("toPrimitive")
      , z = {}.propertyIsEnumerable
      , D = f("symbol-registry")
      , U = f("symbols")
      , B = f("op-symbols")
      , V = Object.prototype
      , W = "function" == typeof L && !!j.f
      , G = r.QObject
      , q = !G || !G.prototype || !G.prototype.findChild
      , $ = o && c((function() {
        return 7 != k(T({}, "a", {
            get: function() {
                return T(this, "a", {
                    value: 7
                }).a
            }
        })).a
    }
    )) ? function(t, e, n) {
        var r = R(V, e);
        r && delete V[e],
        T(t, e, n),
        r && t !== V && T(V, e, r)
    }
    : T
      , J = function(t) {
        var e = U[t] = k(L.prototype);
        return e._k = t,
        e
    }
      , K = W && "symbol" == typeof L.iterator ? function(t) {
        return "symbol" == typeof t
    }
    : function(t) {
        return t instanceof L
    }
      , H = function(t, e, n) {
        return t === V && H(B, e, n),
        m(t),
        e = x(e, !0),
        m(n),
        i(U, e) ? (n.enumerable ? (i(t, N) && t[N][e] && (t[N][e] = !1),
        n = k(n, {
            enumerable: S(0, !1)
        })) : (i(t, N) || T(t, N, S(1, {})),
        t[N][e] = !0),
        $(t, e, n)) : T(t, e, n)
    }
      , Y = function(t, e) {
        m(t);
        for (var n, r = g(e = _(e)), i = 0, o = r.length; o > i; )
            H(t, n = r[i++], e[n]);
        return t
    }
      , X = function(t) {
        var e = z.call(this, t = x(t, !0));
        return !(this === V && i(U, t) && !i(B, t)) && (!(e || !i(this, t) || !i(U, t) || i(this, N) && this[N][t]) || e)
    }
      , Q = function(t, e) {
        if (t = _(t),
        e = x(e, !0),
        t !== V || !i(U, e) || i(B, e)) {
            var n = R(t, e);
            return !n || !i(U, e) || i(t, N) && t[N][e] || (n.enumerable = !0),
            n
        }
    }
      , Z = function(t) {
        for (var e, n = A(_(t)), r = [], o = 0; n.length > o; )
            i(U, e = n[o++]) || e == N || e == s || r.push(e);
        return r
    }
      , tt = function(t) {
        for (var e, n = t === V, r = A(n ? B : _(t)), o = [], u = 0; r.length > u; )
            !i(U, e = r[u++]) || n && !i(V, e) || o.push(U[e]);
        return o
    };
    W || (a((L = function() {
        if (this instanceof L)
            throw TypeError("Symbol is not a constructor!");
        var t = h(arguments.length > 0 ? arguments[0] : void 0)
          , e = function(n) {
            this === V && e.call(B, n),
            i(this, N) && i(this[N], t) && (this[N][t] = !1),
            $(this, t, S(1, n))
        };
        return o && q && $(V, t, {
            configurable: !0,
            set: e
        }),
        J(t)
    }
    ).prototype, "toString", (function() {
        return this._k
    }
    )),
    E.f = Q,
    M.f = H,
    n(40).f = O.f = Z,
    n(52).f = X,
    j.f = tt,
    o && !n(32) && a(V, "propertyIsEnumerable", X, !0),
    d.f = function(t) {
        return J(p(t))
    }
    ),
    u(u.G + u.W + u.F * !W, {
        Symbol: L
    });
    for (var et = "hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables".split(","), nt = 0; et.length > nt; )
        p(et[nt++]);
    for (var rt = P(p.store), it = 0; rt.length > it; )
        v(rt[it++]);
    u(u.S + u.F * !W, "Symbol", {
        for: function(t) {
            return i(D, t += "") ? D[t] : D[t] = L(t)
        },
        keyFor: function(t) {
            if (!K(t))
                throw TypeError(t + " is not a symbol!");
            for (var e in D)
                if (D[e] === t)
                    return e
        },
        useSetter: function() {
            q = !0
        },
        useSimple: function() {
            q = !1
        }
    }),
    u(u.S + u.F * !W, "Object", {
        create: function(t, e) {
            return void 0 === e ? k(t) : Y(k(t), e)
        },
        defineProperty: H,
        defineProperties: Y,
        getOwnPropertyDescriptor: Q,
        getOwnPropertyNames: Z,
        getOwnPropertySymbols: tt
    });
    var ot = c((function() {
        j.f(1)
    }
    ));
    u(u.S + u.F * ot, "Object", {
        getOwnPropertySymbols: function(t) {
            return j.f(w(t))
        }
    }),
    F && u(u.S + u.F * (!W || c((function() {
        var t = L();
        return "[null]" != I([t]) || "{}" != I({
            a: t
        }) || "{}" != I(Object(t))
    }
    ))), "JSON", {
        stringify: function(t) {
            for (var e, n, r = [t], i = 1; arguments.length > i; )
                r.push(arguments[i++]);
            if (n = e = r[1],
            (b(e) || void 0 !== t) && !K(t))
                return y(e) || (e = function(t, e) {
                    if ("function" == typeof n && (e = n.call(this, t, e)),
                    !K(e))
                        return e
                }
                ),
                r[1] = e,
                I.apply(F, r)
        }
    }),
    L.prototype[C] || n(12)(L.prototype, C, L.prototype.valueOf),
    l(L, "Symbol"),
    l(Math, "Math", !0),
    l(r.JSON, "JSON", !0)
}
, function(t, e, n) {
    t.exports = n(50)("native-function-to-string", Function.toString)
}
, function(t, e, n) {
    var r = n(37)
      , i = n(57)
      , o = n(52);
    t.exports = function(t) {
        var e = r(t)
          , n = i.f;
        if (n)
            for (var u, a = n(t), s = o.f, c = 0; a.length > c; )
                s.call(t, u = a[c++]) && e.push(u);
        return e
    }
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Object", {
        create: n(39)
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S + r.F * !n(8), "Object", {
        defineProperty: n(9).f
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S + r.F * !n(8), "Object", {
        defineProperties: n(105)
    })
}
, function(t, e, n) {
    var r = n(17)
      , i = n(18).f;
    n(28)("getOwnPropertyDescriptor", (function() {
        return function(t, e) {
            return i(r(t), e)
        }
    }
    ))
}
, function(t, e, n) {
    var r = n(10)
      , i = n(19);
    n(28)("getPrototypeOf", (function() {
        return function(t) {
            return i(r(t))
        }
    }
    ))
}
, function(t, e, n) {
    var r = n(10)
      , i = n(37);
    n(28)("keys", (function() {
        return function(t) {
            return i(r(t))
        }
    }
    ))
}
, function(t, e, n) {
    n(28)("getOwnPropertyNames", (function() {
        return n(106).f
    }
    ))
}
, function(t, e, n) {
    var r = n(5)
      , i = n(33).onFreeze;
    n(28)("freeze", (function(t) {
        return function(e) {
            return t && r(e) ? t(i(e)) : e
        }
    }
    ))
}
, function(t, e, n) {
    var r = n(5)
      , i = n(33).onFreeze;
    n(28)("seal", (function(t) {
        return function(e) {
            return t && r(e) ? t(i(e)) : e
        }
    }
    ))
}
, function(t, e, n) {
    var r = n(5)
      , i = n(33).onFreeze;
    n(28)("preventExtensions", (function(t) {
        return function(e) {
            return t && r(e) ? t(i(e)) : e
        }
    }
    ))
}
, function(t, e, n) {
    var r = n(5);
    n(28)("isFrozen", (function(t) {
        return function(e) {
            return !r(e) || !!t && t(e)
        }
    }
    ))
}
, function(t, e, n) {
    var r = n(5);
    n(28)("isSealed", (function(t) {
        return function(e) {
            return !r(e) || !!t && t(e)
        }
    }
    ))
}
, function(t, e, n) {
    var r = n(5);
    n(28)("isExtensible", (function(t) {
        return function(e) {
            return !!r(e) && (!t || t(e))
        }
    }
    ))
}
, function(t, e, n) {
    var r = n(0);
    r(r.S + r.F, "Object", {
        assign: n(107)
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Object", {
        is: n(108)
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Object", {
        setPrototypeOf: n(76).set
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(47)
      , i = {};
    i[n(6)("toStringTag")] = "z",
    i + "" != "[object z]" && n(13)(Object.prototype, "toString", (function() {
        return "[object " + r(this) + "]"
    }
    ), !0)
}
, function(t, e, n) {
    var r = n(0);
    r(r.P, "Function", {
        bind: n(109)
    })
}
, function(t, e, n) {
    var r = n(9).f
      , i = Function.prototype
      , o = /^\s*function ([^ (]*)/;
    "name"in i || n(8) && r(i, "name", {
        configurable: !0,
        get: function() {
            try {
                return ("" + this).match(o)[1]
            } catch (t) {
                return ""
            }
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(5)
      , i = n(19)
      , o = n(6)("hasInstance")
      , u = Function.prototype;
    o in u || n(9).f(u, o, {
        value: function(t) {
            if ("function" != typeof this || !r(t))
                return !1;
            if (!r(this.prototype))
                return t instanceof this;
            for (; t = i(t); )
                if (this.prototype === t)
                    return !0;
            return !1
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(111);
    r(r.G + r.F * (parseInt != i), {
        parseInt: i
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(112);
    r(r.G + r.F * (parseFloat != i), {
        parseFloat: i
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(3)
      , i = n(16)
      , o = n(23)
      , u = n(78)
      , a = n(26)
      , s = n(4)
      , c = n(40).f
      , f = n(18).f
      , l = n(9).f
      , h = n(48).trim
      , p = r.Number
      , d = p
      , v = p.prototype
      , g = "Number" == o(n(39)(v))
      , y = "trim"in String.prototype
      , m = function(t) {
        var e = a(t, !1);
        if ("string" == typeof e && e.length > 2) {
            var n, r, i, o = (e = y ? e.trim() : h(e, 3)).charCodeAt(0);
            if (43 === o || 45 === o) {
                if (88 === (n = e.charCodeAt(2)) || 120 === n)
                    return NaN
            } else if (48 === o) {
                switch (e.charCodeAt(1)) {
                case 66:
                case 98:
                    r = 2,
                    i = 49;
                    break;
                case 79:
                case 111:
                    r = 8,
                    i = 55;
                    break;
                default:
                    return +e
                }
                for (var u, s = e.slice(2), c = 0, f = s.length; c < f; c++)
                    if ((u = s.charCodeAt(c)) < 48 || u > i)
                        return NaN;
                return parseInt(s, r)
            }
        }
        return +e
    };
    if (!p(" 0o1") || !p("0b1") || p("+0x1")) {
        p = function(t) {
            var e = arguments.length < 1 ? 0 : t
              , n = this;
            return n instanceof p && (g ? s((function() {
                v.valueOf.call(n)
            }
            )) : "Number" != o(n)) ? u(new d(m(e)), n, p) : m(e)
        }
        ;
        for (var b, w = n(8) ? c(d) : "MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger".split(","), _ = 0; w.length > _; _++)
            i(d, b = w[_]) && !i(p, b) && l(p, b, f(d, b));
        p.prototype = v,
        v.constructor = p,
        n(13)(r, "Number", p)
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(24)
      , o = n(113)
      , u = n(79)
      , a = 1..toFixed
      , s = Math.floor
      , c = [0, 0, 0, 0, 0, 0]
      , f = "Number.toFixed: incorrect invocation!"
      , l = function(t, e) {
        for (var n = -1, r = e; ++n < 6; )
            r += t * c[n],
            c[n] = r % 1e7,
            r = s(r / 1e7)
    }
      , h = function(t) {
        for (var e = 6, n = 0; --e >= 0; )
            n += c[e],
            c[e] = s(n / t),
            n = n % t * 1e7
    }
      , p = function() {
        for (var t = 6, e = ""; --t >= 0; )
            if ("" !== e || 0 === t || 0 !== c[t]) {
                var n = String(c[t]);
                e = "" === e ? n : e + u.call("0", 7 - n.length) + n
            }
        return e
    }
      , d = function(t, e, n) {
        return 0 === e ? n : e % 2 == 1 ? d(t, e - 1, n * t) : d(t * t, e / 2, n)
    };
    r(r.P + r.F * (!!a && ("0.000" !== 8e-5.toFixed(3) || "1" !== .9.toFixed(0) || "1.25" !== 1.255.toFixed(2) || "1000000000000000128" !== (0xde0b6b3a7640080).toFixed(0)) || !n(4)((function() {
        a.call({})
    }
    ))), "Number", {
        toFixed: function(t) {
            var e, n, r, a, s = o(this, f), c = i(t), v = "", g = "0";
            if (c < 0 || c > 20)
                throw RangeError(f);
            if (s != s)
                return "NaN";
            if (s <= -1e21 || s >= 1e21)
                return String(s);
            if (s < 0 && (v = "-",
            s = -s),
            s > 1e-21)
                if (n = (e = function(t) {
                    for (var e = 0, n = t; n >= 4096; )
                        e += 12,
                        n /= 4096;
                    for (; n >= 2; )
                        e += 1,
                        n /= 2;
                    return e
                }(s * d(2, 69, 1)) - 69) < 0 ? s * d(2, -e, 1) : s / d(2, e, 1),
                n *= 4503599627370496,
                (e = 52 - e) > 0) {
                    for (l(0, n),
                    r = c; r >= 7; )
                        l(1e7, 0),
                        r -= 7;
                    for (l(d(10, r, 1), 0),
                    r = e - 1; r >= 23; )
                        h(1 << 23),
                        r -= 23;
                    h(1 << r),
                    l(1, 1),
                    h(2),
                    g = p()
                } else
                    l(0, n),
                    l(1 << -e, 0),
                    g = p() + u.call("0", c);
            return g = c > 0 ? v + ((a = g.length) <= c ? "0." + u.call("0", c - a) + g : g.slice(0, a - c) + "." + g.slice(a - c)) : v + g
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(4)
      , o = n(113)
      , u = 1..toPrecision;
    r(r.P + r.F * (i((function() {
        return "1" !== u.call(1, void 0)
    }
    )) || !i((function() {
        u.call({})
    }
    ))), "Number", {
        toPrecision: function(t) {
            var e = o(this, "Number#toPrecision: incorrect invocation!");
            return void 0 === t ? u.call(e) : u.call(e, t)
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Number", {
        EPSILON: Math.pow(2, -52)
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(3).isFinite;
    r(r.S, "Number", {
        isFinite: function(t) {
            return "number" == typeof t && i(t)
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Number", {
        isInteger: n(114)
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Number", {
        isNaN: function(t) {
            return t != t
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(114)
      , o = Math.abs;
    r(r.S, "Number", {
        isSafeInteger: function(t) {
            return i(t) && o(t) <= 9007199254740991
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Number", {
        MAX_SAFE_INTEGER: 9007199254740991
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Number", {
        MIN_SAFE_INTEGER: -9007199254740991
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(112);
    r(r.S + r.F * (Number.parseFloat != i), "Number", {
        parseFloat: i
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(111);
    r(r.S + r.F * (Number.parseInt != i), "Number", {
        parseInt: i
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(115)
      , o = Math.sqrt
      , u = Math.acosh;
    r(r.S + r.F * !(u && 710 == Math.floor(u(Number.MAX_VALUE)) && u(1 / 0) == 1 / 0), "Math", {
        acosh: function(t) {
            return (t = +t) < 1 ? NaN : t > 94906265.62425156 ? Math.log(t) + Math.LN2 : i(t - 1 + o(t - 1) * o(t + 1))
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = Math.asinh;
    r(r.S + r.F * !(i && 1 / i(0) > 0), "Math", {
        asinh: function t(e) {
            return isFinite(e = +e) && 0 != e ? e < 0 ? -t(-e) : Math.log(e + Math.sqrt(e * e + 1)) : e
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = Math.atanh;
    r(r.S + r.F * !(i && 1 / i(-0) < 0), "Math", {
        atanh: function(t) {
            return 0 == (t = +t) ? t : Math.log((1 + t) / (1 - t)) / 2
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(80);
    r(r.S, "Math", {
        cbrt: function(t) {
            return i(t = +t) * Math.pow(Math.abs(t), 1 / 3)
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Math", {
        clz32: function(t) {
            return (t >>>= 0) ? 31 - Math.floor(Math.log(t + .5) * Math.LOG2E) : 32
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = Math.exp;
    r(r.S, "Math", {
        cosh: function(t) {
            return (i(t = +t) + i(-t)) / 2
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(81);
    r(r.S + r.F * (i != Math.expm1), "Math", {
        expm1: i
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Math", {
        fround: n(116)
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = Math.abs;
    r(r.S, "Math", {
        hypot: function(t, e) {
            for (var n, r, o = 0, u = 0, a = arguments.length, s = 0; u < a; )
                s < (n = i(arguments[u++])) ? (o = o * (r = s / n) * r + 1,
                s = n) : o += n > 0 ? (r = n / s) * r : n;
            return s === 1 / 0 ? 1 / 0 : s * Math.sqrt(o)
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = Math.imul;
    r(r.S + r.F * n(4)((function() {
        return -5 != i(4294967295, 5) || 2 != i.length
    }
    )), "Math", {
        imul: function(t, e) {
            var n = +t
              , r = +e
              , i = 65535 & n
              , o = 65535 & r;
            return 0 | i * o + ((65535 & n >>> 16) * o + i * (65535 & r >>> 16) << 16 >>> 0)
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Math", {
        log10: function(t) {
            return Math.log(t) * Math.LOG10E
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Math", {
        log1p: n(115)
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Math", {
        log2: function(t) {
            return Math.log(t) / Math.LN2
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Math", {
        sign: n(80)
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(81)
      , o = Math.exp;
    r(r.S + r.F * n(4)((function() {
        return -2e-17 != !Math.sinh(-2e-17)
    }
    )), "Math", {
        sinh: function(t) {
            return Math.abs(t = +t) < 1 ? (i(t) - i(-t)) / 2 : (o(t - 1) - o(-t - 1)) * (Math.E / 2)
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(81)
      , o = Math.exp;
    r(r.S, "Math", {
        tanh: function(t) {
            var e = i(t = +t)
              , n = i(-t);
            return e == 1 / 0 ? 1 : n == 1 / 0 ? -1 : (e - n) / (o(t) + o(-t))
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Math", {
        trunc: function(t) {
            return (t > 0 ? Math.floor : Math.ceil)(t)
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(38)
      , o = String.fromCharCode
      , u = String.fromCodePoint;
    r(r.S + r.F * (!!u && 1 != u.length), "String", {
        fromCodePoint: function(t) {
            for (var e, n = [], r = arguments.length, u = 0; r > u; ) {
                if (e = +arguments[u++],
                i(e, 1114111) !== e)
                    throw RangeError(e + " is not a valid code point");
                n.push(e < 65536 ? o(e) : o(55296 + ((e -= 65536) >> 10), e % 1024 + 56320))
            }
            return n.join("")
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(17)
      , o = n(7);
    r(r.S, "String", {
        raw: function(t) {
            for (var e = i(t.raw), n = o(e.length), r = arguments.length, u = [], a = 0; n > a; )
                u.push(String(e[a++])),
                a < r && u.push(String(arguments[a]));
            return u.join("")
        }
    })
}
, function(t, e, n) {
    "use strict";
    n(48)("trim", (function(t) {
        return function() {
            return t(this, 3)
        }
    }
    ))
}
, function(t, e, n) {
    "use strict";
    var r = n(59)(!0);
    n(82)(String, "String", (function(t) {
        this._t = String(t),
        this._i = 0
    }
    ), (function() {
        var t, e = this._t, n = this._i;
        return n >= e.length ? {
            value: void 0,
            done: !0
        } : (t = r(e, n),
        this._i += t.length,
        {
            value: t,
            done: !1
        })
    }
    ))
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(59)(!1);
    r(r.P, "String", {
        codePointAt: function(t) {
            return i(this, t)
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(7)
      , o = n(84)
      , u = "".endsWith;
    r(r.P + r.F * n(85)("endsWith"), "String", {
        endsWith: function(t) {
            var e = o(this, t, "endsWith")
              , n = arguments.length > 1 ? arguments[1] : void 0
              , r = i(e.length)
              , a = void 0 === n ? r : Math.min(i(n), r)
              , s = String(t);
            return u ? u.call(e, s, a) : e.slice(a - s.length, a) === s
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(84);
    r(r.P + r.F * n(85)("includes"), "String", {
        includes: function(t) {
            return !!~i(this, t, "includes").indexOf(t, arguments.length > 1 ? arguments[1] : void 0)
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.P, "String", {
        repeat: n(79)
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(7)
      , o = n(84)
      , u = "".startsWith;
    r(r.P + r.F * n(85)("startsWith"), "String", {
        startsWith: function(t) {
            var e = o(this, t, "startsWith")
              , n = i(Math.min(arguments.length > 1 ? arguments[1] : void 0, e.length))
              , r = String(t);
            return u ? u.call(e, r, n) : e.slice(n, n + r.length) === r
        }
    })
}
, function(t, e, n) {
    "use strict";
    n(14)("anchor", (function(t) {
        return function(e) {
            return t(this, "a", "name", e)
        }
    }
    ))
}
, function(t, e, n) {
    "use strict";
    n(14)("big", (function(t) {
        return function() {
            return t(this, "big", "", "")
        }
    }
    ))
}
, function(t, e, n) {
    "use strict";
    n(14)("blink", (function(t) {
        return function() {
            return t(this, "blink", "", "")
        }
    }
    ))
}
, function(t, e, n) {
    "use strict";
    n(14)("bold", (function(t) {
        return function() {
            return t(this, "b", "", "")
        }
    }
    ))
}
, function(t, e, n) {
    "use strict";
    n(14)("fixed", (function(t) {
        return function() {
            return t(this, "tt", "", "")
        }
    }
    ))
}
, function(t, e, n) {
    "use strict";
    n(14)("fontcolor", (function(t) {
        return function(e) {
            return t(this, "font", "color", e)
        }
    }
    ))
}
, function(t, e, n) {
    "use strict";
    n(14)("fontsize", (function(t) {
        return function(e) {
            return t(this, "font", "size", e)
        }
    }
    ))
}
, function(t, e, n) {
    "use strict";
    n(14)("italics", (function(t) {
        return function() {
            return t(this, "i", "", "")
        }
    }
    ))
}
, function(t, e, n) {
    "use strict";
    n(14)("link", (function(t) {
        return function(e) {
            return t(this, "a", "href", e)
        }
    }
    ))
}
, function(t, e, n) {
    "use strict";
    n(14)("small", (function(t) {
        return function() {
            return t(this, "small", "", "")
        }
    }
    ))
}
, function(t, e, n) {
    "use strict";
    n(14)("strike", (function(t) {
        return function() {
            return t(this, "strike", "", "")
        }
    }
    ))
}
, function(t, e, n) {
    "use strict";
    n(14)("sub", (function(t) {
        return function() {
            return t(this, "sub", "", "")
        }
    }
    ))
}
, function(t, e, n) {
    "use strict";
    n(14)("sup", (function(t) {
        return function() {
            return t(this, "sup", "", "")
        }
    }
    ))
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Date", {
        now: function() {
            return (new Date).getTime()
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(10)
      , o = n(26);
    r(r.P + r.F * n(4)((function() {
        return null !== new Date(NaN).toJSON() || 1 !== Date.prototype.toJSON.call({
            toISOString: function() {
                return 1
            }
        })
    }
    )), "Date", {
        toJSON: function(t) {
            var e = i(this)
              , n = o(e);
            return "number" != typeof n || isFinite(n) ? e.toISOString() : null
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(231);
    r(r.P + r.F * (Date.prototype.toISOString !== i), "Date", {
        toISOString: i
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(4)
      , i = Date.prototype.getTime
      , o = Date.prototype.toISOString
      , u = function(t) {
        return t > 9 ? t : "0" + t
    };
    t.exports = r((function() {
        return "0385-07-25T07:06:39.999Z" != o.call(new Date(-5e13 - 1))
    }
    )) || !r((function() {
        o.call(new Date(NaN))
    }
    )) ? function() {
        if (!isFinite(i.call(this)))
            throw RangeError("Invalid time value");
        var t = this
          , e = t.getUTCFullYear()
          , n = t.getUTCMilliseconds()
          , r = e < 0 ? "-" : e > 9999 ? "+" : "";
        return r + ("00000" + Math.abs(e)).slice(r ? -6 : -4) + "-" + u(t.getUTCMonth() + 1) + "-" + u(t.getUTCDate()) + "T" + u(t.getUTCHours()) + ":" + u(t.getUTCMinutes()) + ":" + u(t.getUTCSeconds()) + "." + (n > 99 ? n : "0" + u(n)) + "Z"
    }
    : o
}
, function(t, e, n) {
    var r = Date.prototype
      , i = r.toString
      , o = r.getTime;
    new Date(NaN) + "" != "Invalid Date" && n(13)(r, "toString", (function() {
        var t = o.call(this);
        return t == t ? i.call(this) : "Invalid Date"
    }
    ))
}
, function(t, e, n) {
    var r = n(6)("toPrimitive")
      , i = Date.prototype;
    r in i || n(12)(i, r, n(234))
}
, function(t, e, n) {
    "use strict";
    var r = n(2)
      , i = n(26);
    t.exports = function(t) {
        if ("string" !== t && "number" !== t && "default" !== t)
            throw TypeError("Incorrect hint");
        return i(r(this), "number" != t)
    }
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Array", {
        isArray: n(58)
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(22)
      , i = n(0)
      , o = n(10)
      , u = n(117)
      , a = n(86)
      , s = n(7)
      , c = n(87)
      , f = n(88);
    i(i.S + i.F * !n(61)((function(t) {
        Array.from(t)
    }
    )), "Array", {
        from: function(t) {
            var e, n, i, l, h = o(t), p = "function" == typeof this ? this : Array, d = arguments.length, v = d > 1 ? arguments[1] : void 0, g = void 0 !== v, y = 0, m = f(h);
            if (g && (v = r(v, d > 2 ? arguments[2] : void 0, 2)),
            null == m || p == Array && a(m))
                for (n = new p(e = s(h.length)); e > y; y++)
                    c(n, y, g ? v(h[y], y) : h[y]);
            else
                for (l = m.call(h),
                n = new p; !(i = l.next()).done; y++)
                    c(n, y, g ? u(l, v, [i.value, y], !0) : i.value);
            return n.length = y,
            n
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(87);
    r(r.S + r.F * n(4)((function() {
        function t() {}
        return !(Array.of.call(t)instanceof t)
    }
    )), "Array", {
        of: function() {
            for (var t = 0, e = arguments.length, n = new ("function" == typeof this ? this : Array)(e); e > t; )
                i(n, t, arguments[t++]);
            return n.length = e,
            n
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(17)
      , o = [].join;
    r(r.P + r.F * (n(51) != Object || !n(25)(o)), "Array", {
        join: function(t) {
            return o.call(i(this), void 0 === t ? "," : t)
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(75)
      , o = n(23)
      , u = n(38)
      , a = n(7)
      , s = [].slice;
    r(r.P + r.F * n(4)((function() {
        i && s.call(i)
    }
    )), "Array", {
        slice: function(t, e) {
            var n = a(this.length)
              , r = o(this);
            if (e = void 0 === e ? n : e,
            "Array" == r)
                return s.call(this, t, e);
            for (var i = u(t, n), c = u(e, n), f = a(c - i), l = new Array(f), h = 0; h < f; h++)
                l[h] = "String" == r ? this.charAt(i + h) : this[i + h];
            return l
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(11)
      , o = n(10)
      , u = n(4)
      , a = [].sort
      , s = [1, 2, 3];
    r(r.P + r.F * (u((function() {
        s.sort(void 0)
    }
    )) || !u((function() {
        s.sort(null)
    }
    )) || !n(25)(a)), "Array", {
        sort: function(t) {
            return void 0 === t ? a.call(o(this)) : a.call(o(this), i(t))
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(29)(0)
      , o = n(25)([].forEach, !0);
    r(r.P + r.F * !o, "Array", {
        forEach: function(t) {
            return i(this, t, arguments[1])
        }
    })
}
, function(t, e, n) {
    var r = n(5)
      , i = n(58)
      , o = n(6)("species");
    t.exports = function(t) {
        var e;
        return i(t) && ("function" != typeof (e = t.constructor) || e !== Array && !i(e.prototype) || (e = void 0),
        r(e) && null === (e = e[o]) && (e = void 0)),
        void 0 === e ? Array : e
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(29)(1);
    r(r.P + r.F * !n(25)([].map, !0), "Array", {
        map: function(t) {
            return i(this, t, arguments[1])
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(29)(2);
    r(r.P + r.F * !n(25)([].filter, !0), "Array", {
        filter: function(t) {
            return i(this, t, arguments[1])
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(29)(3);
    r(r.P + r.F * !n(25)([].some, !0), "Array", {
        some: function(t) {
            return i(this, t, arguments[1])
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(29)(4);
    r(r.P + r.F * !n(25)([].every, !0), "Array", {
        every: function(t) {
            return i(this, t, arguments[1])
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(118);
    r(r.P + r.F * !n(25)([].reduce, !0), "Array", {
        reduce: function(t) {
            return i(this, t, arguments.length, arguments[1], !1)
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(118);
    r(r.P + r.F * !n(25)([].reduceRight, !0), "Array", {
        reduceRight: function(t) {
            return i(this, t, arguments.length, arguments[1], !0)
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(56)(!1)
      , o = [].indexOf
      , u = !!o && 1 / [1].indexOf(1, -0) < 0;
    r(r.P + r.F * (u || !n(25)(o)), "Array", {
        indexOf: function(t) {
            return u ? o.apply(this, arguments) || 0 : i(this, t, arguments[1])
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(17)
      , o = n(24)
      , u = n(7)
      , a = [].lastIndexOf
      , s = !!a && 1 / [1].lastIndexOf(1, -0) < 0;
    r(r.P + r.F * (s || !n(25)(a)), "Array", {
        lastIndexOf: function(t) {
            if (s)
                return a.apply(this, arguments) || 0;
            var e = i(this)
              , n = u(e.length)
              , r = n - 1;
            for (arguments.length > 1 && (r = Math.min(r, o(arguments[1]))),
            r < 0 && (r = n + r); r >= 0; r--)
                if (r in e && e[r] === t)
                    return r || 0;
            return -1
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.P, "Array", {
        copyWithin: n(119)
    }),
    n(34)("copyWithin")
}
, function(t, e, n) {
    var r = n(0);
    r(r.P, "Array", {
        fill: n(90)
    }),
    n(34)("fill")
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(29)(5)
      , o = !0;
    "find"in [] && Array(1).find((function() {
        o = !1
    }
    )),
    r(r.P + r.F * o, "Array", {
        find: function(t) {
            return i(this, t, arguments.length > 1 ? arguments[1] : void 0)
        }
    }),
    n(34)("find")
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(29)(6)
      , o = "findIndex"
      , u = !0;
    o in [] && Array(1)[o]((function() {
        u = !1
    }
    )),
    r(r.P + r.F * u, "Array", {
        findIndex: function(t) {
            return i(this, t, arguments.length > 1 ? arguments[1] : void 0)
        }
    }),
    n(34)(o)
}
, function(t, e, n) {
    n(41)("Array")
}
, function(t, e, n) {
    var r = n(3)
      , i = n(78)
      , o = n(9).f
      , u = n(40).f
      , a = n(60)
      , s = n(53)
      , c = r.RegExp
      , f = c
      , l = c.prototype
      , h = /a/g
      , p = /a/g
      , d = new c(h) !== h;
    if (n(8) && (!d || n(4)((function() {
        return p[n(6)("match")] = !1,
        c(h) != h || c(p) == p || "/a/i" != c(h, "i")
    }
    )))) {
        c = function(t, e) {
            var n = this instanceof c
              , r = a(t)
              , o = void 0 === e;
            return !n && r && t.constructor === c && o ? t : i(d ? new f(r && !o ? t.source : t,e) : f((r = t instanceof c) ? t.source : t, r && o ? s.call(t) : e), n ? this : l, c)
        }
        ;
        for (var v = function(t) {
            t in c || o(c, t, {
                configurable: !0,
                get: function() {
                    return f[t]
                },
                set: function(e) {
                    f[t] = e
                }
            })
        }, g = u(f), y = 0; g.length > y; )
            v(g[y++]);
        l.constructor = c,
        c.prototype = l,
        n(13)(r, "RegExp", c)
    }
    n(41)("RegExp")
}
, function(t, e, n) {
    "use strict";
    n(122);
    var r = n(2)
      , i = n(53)
      , o = n(8)
      , u = /./.toString
      , a = function(t) {
        n(13)(RegExp.prototype, "toString", t, !0)
    };
    n(4)((function() {
        return "/a/b" != u.call({
            source: "a",
            flags: "b"
        })
    }
    )) ? a((function() {
        var t = r(this);
        return "/".concat(t.source, "/", "flags"in t ? t.flags : !o && t instanceof RegExp ? i.call(t) : void 0)
    }
    )) : "toString" != u.name && a((function() {
        return u.call(this)
    }
    ))
}
, function(t, e, n) {
    "use strict";
    var r = n(2)
      , i = n(7)
      , o = n(93)
      , u = n(62);
    n(63)("match", 1, (function(t, e, n, a) {
        return [function(n) {
            var r = t(this)
              , i = null == n ? void 0 : n[e];
            return void 0 !== i ? i.call(n, r) : new RegExp(n)[e](String(r))
        }
        , function(t) {
            var e = a(n, t, this);
            if (e.done)
                return e.value;
            var s = r(t)
              , c = String(this);
            if (!s.global)
                return u(s, c);
            var f = s.unicode;
            s.lastIndex = 0;
            for (var l, h = [], p = 0; null !== (l = u(s, c)); ) {
                var d = String(l[0]);
                h[p] = d,
                "" === d && (s.lastIndex = o(c, i(s.lastIndex), f)),
                p++
            }
            return 0 === p ? null : h
        }
        ]
    }
    ))
}
, function(t, e, n) {
    "use strict";
    var r = n(2)
      , i = n(10)
      , o = n(7)
      , u = n(24)
      , a = n(93)
      , s = n(62)
      , c = Math.max
      , f = Math.min
      , l = Math.floor
      , h = /\$([$&`']|\d\d?|<[^>]*>)/g
      , p = /\$([$&`']|\d\d?)/g;
    n(63)("replace", 2, (function(t, e, n, d) {
        return [function(r, i) {
            var o = t(this)
              , u = null == r ? void 0 : r[e];
            return void 0 !== u ? u.call(r, o, i) : n.call(String(o), r, i)
        }
        , function(t, e) {
            var i = d(n, t, this, e);
            if (i.done)
                return i.value;
            var l = r(t)
              , h = String(this)
              , p = "function" == typeof e;
            p || (e = String(e));
            var g = l.global;
            if (g) {
                var y = l.unicode;
                l.lastIndex = 0
            }
            for (var m = []; ; ) {
                var b = s(l, h);
                if (null === b)
                    break;
                if (m.push(b),
                !g)
                    break;
                "" === String(b[0]) && (l.lastIndex = a(h, o(l.lastIndex), y))
            }
            for (var w, _ = "", x = 0, S = 0; S < m.length; S++) {
                b = m[S];
                for (var k = String(b[0]), O = c(f(u(b.index), h.length), 0), E = [], j = 1; j < b.length; j++)
                    E.push(void 0 === (w = b[j]) ? w : String(w));
                var M = b.groups;
                if (p) {
                    var P = [k].concat(E, O, h);
                    void 0 !== M && P.push(M);
                    var R = String(e.apply(void 0, P))
                } else
                    R = v(k, h, O, E, M, e);
                O >= x && (_ += h.slice(x, O) + R,
                x = O + k.length)
            }
            return _ + h.slice(x)
        }
        ];
        function v(t, e, r, o, u, a) {
            var s = r + t.length
              , c = o.length
              , f = p;
            return void 0 !== u && (u = i(u),
            f = h),
            n.call(a, f, (function(n, i) {
                var a;
                switch (i.charAt(0)) {
                case "$":
                    return "$";
                case "&":
                    return t;
                case "`":
                    return e.slice(0, r);
                case "'":
                    return e.slice(s);
                case "<":
                    a = u[i.slice(1, -1)];
                    break;
                default:
                    var f = +i;
                    if (0 === f)
                        return n;
                    if (f > c) {
                        var h = l(f / 10);
                        return 0 === h ? n : h <= c ? void 0 === o[h - 1] ? i.charAt(1) : o[h - 1] + i.charAt(1) : n
                    }
                    a = o[f - 1]
                }
                return void 0 === a ? "" : a
            }
            ))
        }
    }
    ))
}
, function(t, e, n) {
    "use strict";
    var r = n(2)
      , i = n(108)
      , o = n(62);
    n(63)("search", 1, (function(t, e, n, u) {
        return [function(n) {
            var r = t(this)
              , i = null == n ? void 0 : n[e];
            return void 0 !== i ? i.call(n, r) : new RegExp(n)[e](String(r))
        }
        , function(t) {
            var e = u(n, t, this);
            if (e.done)
                return e.value;
            var a = r(t)
              , s = String(this)
              , c = a.lastIndex;
            i(c, 0) || (a.lastIndex = 0);
            var f = o(a, s);
            return i(a.lastIndex, c) || (a.lastIndex = c),
            null === f ? -1 : f.index
        }
        ]
    }
    ))
}
, function(t, e, n) {
    "use strict";
    var r = n(60)
      , i = n(2)
      , o = n(54)
      , u = n(93)
      , a = n(7)
      , s = n(62)
      , c = n(92)
      , f = n(4)
      , l = Math.min
      , h = [].push
      , p = !f((function() {
        RegExp(4294967295, "y")
    }
    ));
    n(63)("split", 2, (function(t, e, n, f) {
        var d;
        return d = "c" == "abbc".split(/(b)*/)[1] || 4 != "test".split(/(?:)/, -1).length || 2 != "ab".split(/(?:ab)*/).length || 4 != ".".split(/(.?)(.?)/).length || ".".split(/()()/).length > 1 || "".split(/.?/).length ? function(t, e) {
            var i = String(this);
            if (void 0 === t && 0 === e)
                return [];
            if (!r(t))
                return n.call(i, t, e);
            for (var o, u, a, s = [], f = (t.ignoreCase ? "i" : "") + (t.multiline ? "m" : "") + (t.unicode ? "u" : "") + (t.sticky ? "y" : ""), l = 0, p = void 0 === e ? 4294967295 : e >>> 0, d = new RegExp(t.source,f + "g"); (o = c.call(d, i)) && !((u = d.lastIndex) > l && (s.push(i.slice(l, o.index)),
            o.length > 1 && o.index < i.length && h.apply(s, o.slice(1)),
            a = o[0].length,
            l = u,
            s.length >= p)); )
                d.lastIndex === o.index && d.lastIndex++;
            return l === i.length ? !a && d.test("") || s.push("") : s.push(i.slice(l)),
            s.length > p ? s.slice(0, p) : s
        }
        : "0".split(void 0, 0).length ? function(t, e) {
            return void 0 === t && 0 === e ? [] : n.call(this, t, e)
        }
        : n,
        [function(n, r) {
            var i = t(this)
              , o = null == n ? void 0 : n[e];
            return void 0 !== o ? o.call(n, i, r) : d.call(String(i), n, r)
        }
        , function(t, e) {
            var r = f(d, t, this, e, d !== n);
            if (r.done)
                return r.value;
            var c = i(t)
              , h = String(this)
              , v = o(c, RegExp)
              , g = c.unicode
              , y = (c.ignoreCase ? "i" : "") + (c.multiline ? "m" : "") + (c.unicode ? "u" : "") + (p ? "y" : "g")
              , m = new v(p ? c : "^(?:" + c.source + ")",y)
              , b = void 0 === e ? 4294967295 : e >>> 0;
            if (0 === b)
                return [];
            if (0 === h.length)
                return null === s(m, h) ? [h] : [];
            for (var w = 0, _ = 0, x = []; _ < h.length; ) {
                m.lastIndex = p ? _ : 0;
                var S, k = s(m, p ? h : h.slice(_));
                if (null === k || (S = l(a(m.lastIndex + (p ? 0 : _)), h.length)) === w)
                    _ = u(h, _, g);
                else {
                    if (x.push(h.slice(w, _)),
                    x.length === b)
                        return x;
                    for (var O = 1; O <= k.length - 1; O++)
                        if (x.push(k[O]),
                        x.length === b)
                            return x;
                    _ = w = S
                }
            }
            return x.push(h.slice(w)),
            x
        }
        ]
    }
    ))
}
, function(t, e, n) {
    "use strict";
    var r, i, o, u, a = n(32), s = n(3), c = n(22), f = n(47), l = n(0), h = n(5), p = n(11), d = n(42), v = n(43), g = n(54), y = n(94).set, m = n(95)(), b = n(96), w = n(123), _ = n(64), x = n(124), S = s.TypeError, k = s.process, O = k && k.versions, E = O && O.v8 || "", j = s.Promise, M = "process" == f(k), P = function() {}, R = i = b.f, T = !!function() {
        try {
            var t = j.resolve(1)
              , e = (t.constructor = {})[n(6)("species")] = function(t) {
                t(P, P)
            }
            ;
            return (M || "function" == typeof PromiseRejectionEvent) && t.then(P)instanceof e && 0 !== E.indexOf("6.6") && -1 === _.indexOf("Chrome/66")
        } catch (t) {}
    }(), A = function(t) {
        var e;
        return !(!h(t) || "function" != typeof (e = t.then)) && e
    }, L = function(t, e) {
        if (!t._n) {
            t._n = !0;
            var n = t._c;
            m((function() {
                for (var r = t._v, i = 1 == t._s, o = 0, u = function(e) {
                    var n, o, u, a = i ? e.ok : e.fail, s = e.resolve, c = e.reject, f = e.domain;
                    try {
                        a ? (i || (2 == t._h && N(t),
                        t._h = 1),
                        !0 === a ? n = r : (f && f.enter(),
                        n = a(r),
                        f && (f.exit(),
                        u = !0)),
                        n === e.promise ? c(S("Promise-chain cycle")) : (o = A(n)) ? o.call(n, s, c) : s(n)) : c(r)
                    } catch (t) {
                        f && !u && f.exit(),
                        c(t)
                    }
                }; n.length > o; )
                    u(n[o++]);
                t._c = [],
                t._n = !1,
                e && !t._h && F(t)
            }
            ))
        }
    }, F = function(t) {
        y.call(s, (function() {
            var e, n, r, i = t._v, o = I(t);
            if (o && (e = w((function() {
                M ? k.emit("unhandledRejection", i, t) : (n = s.onunhandledrejection) ? n({
                    promise: t,
                    reason: i
                }) : (r = s.console) && r.error && r.error("Unhandled promise rejection", i)
            }
            )),
            t._h = M || I(t) ? 2 : 1),
            t._a = void 0,
            o && e.e)
                throw e.v
        }
        ))
    }, I = function(t) {
        return 1 !== t._h && 0 === (t._a || t._c).length
    }, N = function(t) {
        y.call(s, (function() {
            var e;
            M ? k.emit("rejectionHandled", t) : (e = s.onrejectionhandled) && e({
                promise: t,
                reason: t._v
            })
        }
        ))
    }, C = function(t) {
        var e = this;
        e._d || (e._d = !0,
        (e = e._w || e)._v = t,
        e._s = 2,
        e._a || (e._a = e._c.slice()),
        L(e, !0))
    }, z = function(t) {
        var e, n = this;
        if (!n._d) {
            n._d = !0,
            n = n._w || n;
            try {
                if (n === t)
                    throw S("Promise can't be resolved itself");
                (e = A(t)) ? m((function() {
                    var r = {
                        _w: n,
                        _d: !1
                    };
                    try {
                        e.call(t, c(z, r, 1), c(C, r, 1))
                    } catch (t) {
                        C.call(r, t)
                    }
                }
                )) : (n._v = t,
                n._s = 1,
                L(n, !1))
            } catch (t) {
                C.call({
                    _w: n,
                    _d: !1
                }, t)
            }
        }
    };
    T || (j = function(t) {
        d(this, j, "Promise", "_h"),
        p(t),
        r.call(this);
        try {
            t(c(z, this, 1), c(C, this, 1))
        } catch (t) {
            C.call(this, t)
        }
    }
    ,
    (r = function(t) {
        this._c = [],
        this._a = void 0,
        this._s = 0,
        this._d = !1,
        this._v = void 0,
        this._h = 0,
        this._n = !1
    }
    ).prototype = n(44)(j.prototype, {
        then: function(t, e) {
            var n = R(g(this, j));
            return n.ok = "function" != typeof t || t,
            n.fail = "function" == typeof e && e,
            n.domain = M ? k.domain : void 0,
            this._c.push(n),
            this._a && this._a.push(n),
            this._s && L(this, !1),
            n.promise
        },
        catch: function(t) {
            return this.then(void 0, t)
        }
    }),
    o = function() {
        var t = new r;
        this.promise = t,
        this.resolve = c(z, t, 1),
        this.reject = c(C, t, 1)
    }
    ,
    b.f = R = function(t) {
        return t === j || t === u ? new o(t) : i(t)
    }
    ),
    l(l.G + l.W + l.F * !T, {
        Promise: j
    }),
    n(46)(j, "Promise"),
    n(41)("Promise"),
    u = n(21).Promise,
    l(l.S + l.F * !T, "Promise", {
        reject: function(t) {
            var e = R(this);
            return (0,
            e.reject)(t),
            e.promise
        }
    }),
    l(l.S + l.F * (a || !T), "Promise", {
        resolve: function(t) {
            return x(a && this === u ? j : this, t)
        }
    }),
    l(l.S + l.F * !(T && n(61)((function(t) {
        j.all(t).catch(P)
    }
    ))), "Promise", {
        all: function(t) {
            var e = this
              , n = R(e)
              , r = n.resolve
              , i = n.reject
              , o = w((function() {
                var n = []
                  , o = 0
                  , u = 1;
                v(t, !1, (function(t) {
                    var a = o++
                      , s = !1;
                    n.push(void 0),
                    u++,
                    e.resolve(t).then((function(t) {
                        s || (s = !0,
                        n[a] = t,
                        --u || r(n))
                    }
                    ), i)
                }
                )),
                --u || r(n)
            }
            ));
            return o.e && i(o.v),
            n.promise
        },
        race: function(t) {
            var e = this
              , n = R(e)
              , r = n.reject
              , i = w((function() {
                v(t, !1, (function(t) {
                    e.resolve(t).then(n.resolve, r)
                }
                ))
            }
            ));
            return i.e && r(i.v),
            n.promise
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(129)
      , i = n(45);
    n(65)("WeakSet", (function(t) {
        return function() {
            return t(this, arguments.length > 0 ? arguments[0] : void 0)
        }
    }
    ), {
        add: function(t) {
            return r.def(i(this, "WeakSet"), t, !0)
        }
    }, r, !1, !0)
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(66)
      , o = n(97)
      , u = n(2)
      , a = n(38)
      , s = n(7)
      , c = n(5)
      , f = n(3).ArrayBuffer
      , l = n(54)
      , h = o.ArrayBuffer
      , p = o.DataView
      , d = i.ABV && f.isView
      , v = h.prototype.slice
      , g = i.VIEW;
    r(r.G + r.W + r.F * (f !== h), {
        ArrayBuffer: h
    }),
    r(r.S + r.F * !i.CONSTR, "ArrayBuffer", {
        isView: function(t) {
            return d && d(t) || c(t) && g in t
        }
    }),
    r(r.P + r.U + r.F * n(4)((function() {
        return !new h(2).slice(1, void 0).byteLength
    }
    )), "ArrayBuffer", {
        slice: function(t, e) {
            if (void 0 !== v && void 0 === e)
                return v.call(u(this), t);
            for (var n = u(this).byteLength, r = a(t, n), i = a(void 0 === e ? n : e, n), o = new (l(this, h))(s(i - r)), c = new p(this), f = new p(o), d = 0; r < i; )
                f.setUint8(d++, c.getUint8(r++));
            return o
        }
    }),
    n(41)("ArrayBuffer")
}
, function(t, e, n) {
    var r = n(0);
    r(r.G + r.W + r.F * !n(66).ABV, {
        DataView: n(97).DataView
    })
}
, function(t, e, n) {
    n(30)("Int8", 1, (function(t) {
        return function(e, n, r) {
            return t(this, e, n, r)
        }
    }
    ))
}
, function(t, e, n) {
    n(30)("Uint8", 1, (function(t) {
        return function(e, n, r) {
            return t(this, e, n, r)
        }
    }
    ))
}
, function(t, e, n) {
    n(30)("Uint8", 1, (function(t) {
        return function(e, n, r) {
            return t(this, e, n, r)
        }
    }
    ), !0)
}
, function(t, e, n) {
    n(30)("Int16", 2, (function(t) {
        return function(e, n, r) {
            return t(this, e, n, r)
        }
    }
    ))
}
, function(t, e, n) {
    n(30)("Uint16", 2, (function(t) {
        return function(e, n, r) {
            return t(this, e, n, r)
        }
    }
    ))
}
, function(t, e, n) {
    n(30)("Int32", 4, (function(t) {
        return function(e, n, r) {
            return t(this, e, n, r)
        }
    }
    ))
}
, function(t, e, n) {
    n(30)("Uint32", 4, (function(t) {
        return function(e, n, r) {
            return t(this, e, n, r)
        }
    }
    ))
}
, function(t, e, n) {
    n(30)("Float32", 4, (function(t) {
        return function(e, n, r) {
            return t(this, e, n, r)
        }
    }
    ))
}
, function(t, e, n) {
    n(30)("Float64", 8, (function(t) {
        return function(e, n, r) {
            return t(this, e, n, r)
        }
    }
    ))
}
, function(t, e, n) {
    var r = n(0)
      , i = n(11)
      , o = n(2)
      , u = (n(3).Reflect || {}).apply
      , a = Function.apply;
    r(r.S + r.F * !n(4)((function() {
        u((function() {}
        ))
    }
    )), "Reflect", {
        apply: function(t, e, n) {
            var r = i(t)
              , s = o(n);
            return u ? u(r, e, s) : a.call(r, e, s)
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(39)
      , o = n(11)
      , u = n(2)
      , a = n(5)
      , s = n(4)
      , c = n(109)
      , f = (n(3).Reflect || {}).construct
      , l = s((function() {
        function t() {}
        return !(f((function() {}
        ), [], t)instanceof t)
    }
    ))
      , h = !s((function() {
        f((function() {}
        ))
    }
    ));
    r(r.S + r.F * (l || h), "Reflect", {
        construct: function(t, e) {
            o(t),
            u(e);
            var n = arguments.length < 3 ? t : o(arguments[2]);
            if (h && !l)
                return f(t, e, n);
            if (t == n) {
                switch (e.length) {
                case 0:
                    return new t;
                case 1:
                    return new t(e[0]);
                case 2:
                    return new t(e[0],e[1]);
                case 3:
                    return new t(e[0],e[1],e[2]);
                case 4:
                    return new t(e[0],e[1],e[2],e[3])
                }
                var r = [null];
                return r.push.apply(r, e),
                new (c.apply(t, r))
            }
            var s = n.prototype
              , p = i(a(s) ? s : Object.prototype)
              , d = Function.apply.call(t, p, e);
            return a(d) ? d : p
        }
    })
}
, function(t, e, n) {
    var r = n(9)
      , i = n(0)
      , o = n(2)
      , u = n(26);
    i(i.S + i.F * n(4)((function() {
        Reflect.defineProperty(r.f({}, 1, {
            value: 1
        }), 1, {
            value: 2
        })
    }
    )), "Reflect", {
        defineProperty: function(t, e, n) {
            o(t),
            e = u(e, !0),
            o(n);
            try {
                return r.f(t, e, n),
                !0
            } catch (t) {
                return !1
            }
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(18).f
      , o = n(2);
    r(r.S, "Reflect", {
        deleteProperty: function(t, e) {
            var n = i(o(t), e);
            return !(n && !n.configurable) && delete t[e]
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(2)
      , o = function(t) {
        this._t = i(t),
        this._i = 0;
        var e, n = this._k = [];
        for (e in t)
            n.push(e)
    };
    n(83)(o, "Object", (function() {
        var t, e = this._k;
        do {
            if (this._i >= e.length)
                return {
                    value: void 0,
                    done: !0
                }
        } while (!((t = e[this._i++])in this._t));return {
            value: t,
            done: !1
        }
    }
    )),
    r(r.S, "Reflect", {
        enumerate: function(t) {
            return new o(t)
        }
    })
}
, function(t, e, n) {
    var r = n(18)
      , i = n(19)
      , o = n(16)
      , u = n(0)
      , a = n(5)
      , s = n(2);
    u(u.S, "Reflect", {
        get: function t(e, n) {
            var u, c, f = arguments.length < 3 ? e : arguments[2];
            return s(e) === f ? e[n] : (u = r.f(e, n)) ? o(u, "value") ? u.value : void 0 !== u.get ? u.get.call(f) : void 0 : a(c = i(e)) ? t(c, n, f) : void 0
        }
    })
}
, function(t, e, n) {
    var r = n(18)
      , i = n(0)
      , o = n(2);
    i(i.S, "Reflect", {
        getOwnPropertyDescriptor: function(t, e) {
            return r.f(o(t), e)
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(19)
      , o = n(2);
    r(r.S, "Reflect", {
        getPrototypeOf: function(t) {
            return i(o(t))
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Reflect", {
        has: function(t, e) {
            return e in t
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(2)
      , o = Object.isExtensible;
    r(r.S, "Reflect", {
        isExtensible: function(t) {
            return i(t),
            !o || o(t)
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Reflect", {
        ownKeys: n(131)
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(2)
      , o = Object.preventExtensions;
    r(r.S, "Reflect", {
        preventExtensions: function(t) {
            i(t);
            try {
                return o && o(t),
                !0
            } catch (t) {
                return !1
            }
        }
    })
}
, function(t, e, n) {
    var r = n(9)
      , i = n(18)
      , o = n(19)
      , u = n(16)
      , a = n(0)
      , s = n(35)
      , c = n(2)
      , f = n(5);
    a(a.S, "Reflect", {
        set: function t(e, n, a) {
            var l, h, p = arguments.length < 4 ? e : arguments[3], d = i.f(c(e), n);
            if (!d) {
                if (f(h = o(e)))
                    return t(h, n, a, p);
                d = s(0)
            }
            if (u(d, "value")) {
                if (!1 === d.writable || !f(p))
                    return !1;
                if (l = i.f(p, n)) {
                    if (l.get || l.set || !1 === l.writable)
                        return !1;
                    l.value = a,
                    r.f(p, n, l)
                } else
                    r.f(p, n, s(0, a));
                return !0
            }
            return void 0 !== d.set && (d.set.call(p, a),
            !0)
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(76);
    i && r(r.S, "Reflect", {
        setPrototypeOf: function(t, e) {
            i.check(t, e);
            try {
                return i.set(t, e),
                !0
            } catch (t) {
                return !1
            }
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(56)(!0);
    r(r.P, "Array", {
        includes: function(t) {
            return i(this, t, arguments.length > 1 ? arguments[1] : void 0)
        }
    }),
    n(34)("includes")
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(132)
      , o = n(10)
      , u = n(7)
      , a = n(11)
      , s = n(89);
    r(r.P, "Array", {
        flatMap: function(t) {
            var e, n, r = o(this);
            return a(t),
            e = u(r.length),
            n = s(r, 0),
            i(n, r, r, e, 0, 1, t, arguments[1]),
            n
        }
    }),
    n(34)("flatMap")
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(132)
      , o = n(10)
      , u = n(7)
      , a = n(24)
      , s = n(89);
    r(r.P, "Array", {
        flatten: function() {
            var t = arguments[0]
              , e = o(this)
              , n = u(e.length)
              , r = s(e, 0);
            return i(r, e, e, n, 0, void 0 === t ? 1 : a(t)),
            r
        }
    }),
    n(34)("flatten")
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(59)(!0);
    r(r.P, "String", {
        at: function(t) {
            return i(this, t)
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(133)
      , o = n(64)
      , u = /Version\/10\.\d+(\.\d+)?( Mobile\/\w+)? Safari\//.test(o);
    r(r.P + r.F * u, "String", {
        padStart: function(t) {
            return i(this, t, arguments.length > 1 ? arguments[1] : void 0, !0)
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(133)
      , o = n(64)
      , u = /Version\/10\.\d+(\.\d+)?( Mobile\/\w+)? Safari\//.test(o);
    r(r.P + r.F * u, "String", {
        padEnd: function(t) {
            return i(this, t, arguments.length > 1 ? arguments[1] : void 0, !1)
        }
    })
}
, function(t, e, n) {
    "use strict";
    n(48)("trimLeft", (function(t) {
        return function() {
            return t(this, 1)
        }
    }
    ), "trimStart")
}
, function(t, e, n) {
    "use strict";
    n(48)("trimRight", (function(t) {
        return function() {
            return t(this, 2)
        }
    }
    ), "trimEnd")
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(27)
      , o = n(7)
      , u = n(60)
      , a = n(53)
      , s = RegExp.prototype
      , c = function(t, e) {
        this._r = t,
        this._s = e
    };
    n(83)(c, "RegExp String", (function() {
        var t = this._r.exec(this._s);
        return {
            value: t,
            done: null === t
        }
    }
    )),
    r(r.P, "String", {
        matchAll: function(t) {
            if (i(this),
            !u(t))
                throw TypeError(t + " is not a regexp!");
            var e = String(this)
              , n = "flags"in s ? String(t.flags) : a.call(t)
              , r = new RegExp(t.source,~n.indexOf("g") ? n : "g" + n);
            return r.lastIndex = o(t.lastIndex),
            new c(r,e)
        }
    })
}
, function(t, e, n) {
    n(72)("asyncIterator")
}
, function(t, e, n) {
    n(72)("observable")
}
, function(t, e, n) {
    var r = n(0)
      , i = n(131)
      , o = n(17)
      , u = n(18)
      , a = n(87);
    r(r.S, "Object", {
        getOwnPropertyDescriptors: function(t) {
            for (var e, n, r = o(t), s = u.f, c = i(r), f = {}, l = 0; c.length > l; )
                void 0 !== (n = s(r, e = c[l++])) && a(f, e, n);
            return f
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(134)(!1);
    r(r.S, "Object", {
        values: function(t) {
            return i(t)
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(134)(!0);
    r(r.S, "Object", {
        entries: function(t) {
            return i(t)
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(10)
      , o = n(11)
      , u = n(9);
    n(8) && r(r.P + n(67), "Object", {
        __defineGetter__: function(t, e) {
            u.f(i(this), t, {
                get: o(e),
                enumerable: !0,
                configurable: !0
            })
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(10)
      , o = n(11)
      , u = n(9);
    n(8) && r(r.P + n(67), "Object", {
        __defineSetter__: function(t, e) {
            u.f(i(this), t, {
                set: o(e),
                enumerable: !0,
                configurable: !0
            })
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(10)
      , o = n(26)
      , u = n(19)
      , a = n(18).f;
    n(8) && r(r.P + n(67), "Object", {
        __lookupGetter__: function(t) {
            var e, n = i(this), r = o(t, !0);
            do {
                if (e = a(n, r))
                    return e.get
            } while (n = u(n))
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(10)
      , o = n(26)
      , u = n(19)
      , a = n(18).f;
    n(8) && r(r.P + n(67), "Object", {
        __lookupSetter__: function(t) {
            var e, n = i(this), r = o(t, !0);
            do {
                if (e = a(n, r))
                    return e.set
            } while (n = u(n))
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.P + r.R, "Map", {
        toJSON: n(135)("Map")
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.P + r.R, "Set", {
        toJSON: n(135)("Set")
    })
}
, function(t, e, n) {
    n(68)("Map")
}
, function(t, e, n) {
    n(68)("Set")
}
, function(t, e, n) {
    n(68)("WeakMap")
}
, function(t, e, n) {
    n(68)("WeakSet")
}
, function(t, e, n) {
    n(69)("Map")
}
, function(t, e, n) {
    n(69)("Set")
}
, function(t, e, n) {
    n(69)("WeakMap")
}
, function(t, e, n) {
    n(69)("WeakSet")
}
, function(t, e, n) {
    var r = n(0);
    r(r.G, {
        global: n(3)
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "System", {
        global: n(3)
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(23);
    r(r.S, "Error", {
        isError: function(t) {
            return "Error" === i(t)
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Math", {
        clamp: function(t, e, n) {
            return Math.min(n, Math.max(e, t))
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Math", {
        DEG_PER_RAD: Math.PI / 180
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = 180 / Math.PI;
    r(r.S, "Math", {
        degrees: function(t) {
            return t * i
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(137)
      , o = n(116);
    r(r.S, "Math", {
        fscale: function(t, e, n, r, u) {
            return o(i(t, e, n, r, u))
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Math", {
        iaddh: function(t, e, n, r) {
            var i = t >>> 0
              , o = n >>> 0;
            return (e >>> 0) + (r >>> 0) + ((i & o | (i | o) & ~(i + o >>> 0)) >>> 31) | 0
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Math", {
        isubh: function(t, e, n, r) {
            var i = t >>> 0
              , o = n >>> 0;
            return (e >>> 0) - (r >>> 0) - ((~i & o | ~(i ^ o) & i - o >>> 0) >>> 31) | 0
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Math", {
        imulh: function(t, e) {
            var n = +t
              , r = +e
              , i = 65535 & n
              , o = 65535 & r
              , u = n >> 16
              , a = r >> 16
              , s = (u * o >>> 0) + (i * o >>> 16);
            return u * a + (s >> 16) + ((i * a >>> 0) + (65535 & s) >> 16)
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Math", {
        RAD_PER_DEG: 180 / Math.PI
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = Math.PI / 180;
    r(r.S, "Math", {
        radians: function(t) {
            return t * i
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Math", {
        scale: n(137)
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Math", {
        umulh: function(t, e) {
            var n = +t
              , r = +e
              , i = 65535 & n
              , o = 65535 & r
              , u = n >>> 16
              , a = r >>> 16
              , s = (u * o >>> 0) + (i * o >>> 16);
            return u * a + (s >>> 16) + ((i * a >>> 0) + (65535 & s) >>> 16)
        }
    })
}
, function(t, e, n) {
    var r = n(0);
    r(r.S, "Math", {
        signbit: function(t) {
            return (t = +t) != t ? t : 0 == t ? 1 / t == 1 / 0 : t > 0
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(21)
      , o = n(3)
      , u = n(54)
      , a = n(124);
    r(r.P + r.R, "Promise", {
        finally: function(t) {
            var e = u(this, i.Promise || o.Promise)
              , n = "function" == typeof t;
            return this.then(n ? function(n) {
                return a(e, t()).then((function() {
                    return n
                }
                ))
            }
            : t, n ? function(n) {
                return a(e, t()).then((function() {
                    throw n
                }
                ))
            }
            : t)
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(96)
      , o = n(123);
    r(r.S, "Promise", {
        try: function(t) {
            var e = i.f(this)
              , n = o(t);
            return (n.e ? e.reject : e.resolve)(n.v),
            e.promise
        }
    })
}
, function(t, e, n) {
    var r = n(31)
      , i = n(2)
      , o = r.key
      , u = r.set;
    r.exp({
        defineMetadata: function(t, e, n, r) {
            u(t, e, i(n), o(r))
        }
    })
}
, function(t, e, n) {
    var r = n(31)
      , i = n(2)
      , o = r.key
      , u = r.map
      , a = r.store;
    r.exp({
        deleteMetadata: function(t, e) {
            var n = arguments.length < 3 ? void 0 : o(arguments[2])
              , r = u(i(e), n, !1);
            if (void 0 === r || !r.delete(t))
                return !1;
            if (r.size)
                return !0;
            var s = a.get(e);
            return s.delete(n),
            !!s.size || a.delete(e)
        }
    })
}
, function(t, e, n) {
    var r = n(31)
      , i = n(2)
      , o = n(19)
      , u = r.has
      , a = r.get
      , s = r.key
      , c = function(t, e, n) {
        if (u(t, e, n))
            return a(t, e, n);
        var r = o(e);
        return null !== r ? c(t, r, n) : void 0
    };
    r.exp({
        getMetadata: function(t, e) {
            return c(t, i(e), arguments.length < 3 ? void 0 : s(arguments[2]))
        }
    })
}
, function(t, e, n) {
    var r = n(127)
      , i = n(136)
      , o = n(31)
      , u = n(2)
      , a = n(19)
      , s = o.keys
      , c = o.key
      , f = function(t, e) {
        var n = s(t, e)
          , o = a(t);
        if (null === o)
            return n;
        var u = f(o, e);
        return u.length ? n.length ? i(new r(n.concat(u))) : u : n
    };
    o.exp({
        getMetadataKeys: function(t) {
            return f(u(t), arguments.length < 2 ? void 0 : c(arguments[1]))
        }
    })
}
, function(t, e, n) {
    var r = n(31)
      , i = n(2)
      , o = r.get
      , u = r.key;
    r.exp({
        getOwnMetadata: function(t, e) {
            return o(t, i(e), arguments.length < 3 ? void 0 : u(arguments[2]))
        }
    })
}
, function(t, e, n) {
    var r = n(31)
      , i = n(2)
      , o = r.keys
      , u = r.key;
    r.exp({
        getOwnMetadataKeys: function(t) {
            return o(i(t), arguments.length < 2 ? void 0 : u(arguments[1]))
        }
    })
}
, function(t, e, n) {
    var r = n(31)
      , i = n(2)
      , o = n(19)
      , u = r.has
      , a = r.key
      , s = function(t, e, n) {
        if (u(t, e, n))
            return !0;
        var r = o(e);
        return null !== r && s(t, r, n)
    };
    r.exp({
        hasMetadata: function(t, e) {
            return s(t, i(e), arguments.length < 3 ? void 0 : a(arguments[2]))
        }
    })
}
, function(t, e, n) {
    var r = n(31)
      , i = n(2)
      , o = r.has
      , u = r.key;
    r.exp({
        hasOwnMetadata: function(t, e) {
            return o(t, i(e), arguments.length < 3 ? void 0 : u(arguments[2]))
        }
    })
}
, function(t, e, n) {
    var r = n(31)
      , i = n(2)
      , o = n(11)
      , u = r.key
      , a = r.set;
    r.exp({
        metadata: function(t, e) {
            return function(n, r) {
                a(t, e, (void 0 !== r ? i : o)(n), u(r))
            }
        }
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(95)()
      , o = n(3).process
      , u = "process" == n(23)(o);
    r(r.G, {
        asap: function(t) {
            var e = u && o.domain;
            i(e ? e.bind(t) : t)
        }
    })
}
, function(t, e, n) {
    "use strict";
    var r = n(0)
      , i = n(3)
      , o = n(21)
      , u = n(95)()
      , a = n(6)("observable")
      , s = n(11)
      , c = n(2)
      , f = n(42)
      , l = n(44)
      , h = n(12)
      , p = n(43)
      , d = p.RETURN
      , v = function(t) {
        return null == t ? void 0 : s(t)
    }
      , g = function(t) {
        var e = t._c;
        e && (t._c = void 0,
        e())
    }
      , y = function(t) {
        return void 0 === t._o
    }
      , m = function(t) {
        y(t) || (t._o = void 0,
        g(t))
    }
      , b = function(t, e) {
        c(t),
        this._c = void 0,
        this._o = t,
        t = new w(this);
        try {
            var n = e(t)
              , r = n;
            null != n && ("function" == typeof n.unsubscribe ? n = function() {
                r.unsubscribe()
            }
            : s(n),
            this._c = n)
        } catch (e) {
            return void t.error(e)
        }
        y(this) && g(this)
    };
    b.prototype = l({}, {
        unsubscribe: function() {
            m(this)
        }
    });
    var w = function(t) {
        this._s = t
    };
    w.prototype = l({}, {
        next: function(t) {
            var e = this._s;
            if (!y(e)) {
                var n = e._o;
                try {
                    var r = v(n.next);
                    if (r)
                        return r.call(n, t)
                } catch (t) {
                    try {
                        m(e)
                    } finally {
                        throw t
                    }
                }
            }
        },
        error: function(t) {
            var e = this._s;
            if (y(e))
                throw t;
            var n = e._o;
            e._o = void 0;
            try {
                var r = v(n.error);
                if (!r)
                    throw t;
                t = r.call(n, t)
            } catch (t) {
                try {
                    g(e)
                } finally {
                    throw t
                }
            }
            return g(e),
            t
        },
        complete: function(t) {
            var e = this._s;
            if (!y(e)) {
                var n = e._o;
                e._o = void 0;
                try {
                    var r = v(n.complete);
                    t = r ? r.call(n, t) : void 0
                } catch (t) {
                    try {
                        g(e)
                    } finally {
                        throw t
                    }
                }
                return g(e),
                t
            }
        }
    });
    var _ = function(t) {
        f(this, _, "Observable", "_f")._f = s(t)
    };
    l(_.prototype, {
        subscribe: function(t) {
            return new b(t,this._f)
        },
        forEach: function(t) {
            var e = this;
            return new (o.Promise || i.Promise)((function(n, r) {
                s(t);
                var i = e.subscribe({
                    next: function(e) {
                        try {
                            return t(e)
                        } catch (t) {
                            r(t),
                            i.unsubscribe()
                        }
                    },
                    error: r,
                    complete: n
                })
            }
            ))
        }
    }),
    l(_, {
        from: function(t) {
            var e = "function" == typeof this ? this : _
              , n = v(c(t)[a]);
            if (n) {
                var r = c(n.call(t));
                return r.constructor === e ? r : new e((function(t) {
                    return r.subscribe(t)
                }
                ))
            }
            return new e((function(e) {
                var n = !1;
                return u((function() {
                    if (!n) {
                        try {
                            if (p(t, !1, (function(t) {
                                if (e.next(t),
                                n)
                                    return d
                            }
                            )) === d)
                                return
                        } catch (t) {
                            if (n)
                                throw t;
                            return void e.error(t)
                        }
                        e.complete()
                    }
                }
                )),
                function() {
                    n = !0
                }
            }
            ))
        },
        of: function() {
            for (var t = 0, e = arguments.length, n = new Array(e); t < e; )
                n[t] = arguments[t++];
            return new ("function" == typeof this ? this : _)((function(t) {
                var e = !1;
                return u((function() {
                    if (!e) {
                        for (var r = 0; r < n.length; ++r)
                            if (t.next(n[r]),
                            e)
                                return;
                        t.complete()
                    }
                }
                )),
                function() {
                    e = !0
                }
            }
            ))
        }
    }),
    h(_.prototype, a, (function() {
        return this
    }
    )),
    r(r.G, {
        Observable: _
    }),
    n(41)("Observable")
}
, function(t, e, n) {
    var r = n(3)
      , i = n(0)
      , o = n(64)
      , u = [].slice
      , a = /MSIE .\./.test(o)
      , s = function(t) {
        return function(e, n) {
            var r = arguments.length > 2
              , i = !!r && u.call(arguments, 2);
            return t(r ? function() {
                ("function" == typeof e ? e : Function(e)).apply(this, i)
            }
            : e, n)
        }
    };
    i(i.G + i.B + i.F * a, {
        setTimeout: s(r.setTimeout),
        setInterval: s(r.setInterval)
    })
}
, function(t, e, n) {
    var r = n(0)
      , i = n(94);
    r(r.G + r.B, {
        setImmediate: i.set,
        clearImmediate: i.clear
    })
}
, function(t, e, n) {
    for (var r = n(91), i = n(37), o = n(13), u = n(3), a = n(12), s = n(49), c = n(6), f = c("iterator"), l = c("toStringTag"), h = s.Array, p = {
        CSSRuleList: !0,
        CSSStyleDeclaration: !1,
        CSSValueList: !1,
        ClientRectList: !1,
        DOMRectList: !1,
        DOMStringList: !1,
        DOMTokenList: !0,
        DataTransferItemList: !1,
        FileList: !1,
        HTMLAllCollection: !1,
        HTMLCollection: !1,
        HTMLFormElement: !1,
        HTMLSelectElement: !1,
        MediaList: !0,
        MimeTypeArray: !1,
        NamedNodeMap: !1,
        NodeList: !0,
        PaintRequestList: !1,
        Plugin: !1,
        PluginArray: !1,
        SVGLengthList: !1,
        SVGNumberList: !1,
        SVGPathSegList: !1,
        SVGPointList: !1,
        SVGStringList: !1,
        SVGTransformList: !1,
        SourceBufferList: !1,
        StyleSheetList: !0,
        TextTrackCueList: !1,
        TextTrackList: !1,
        TouchList: !1
    }, d = i(p), v = 0; v < d.length; v++) {
        var g, y = d[v], m = p[y], b = u[y], w = b && b.prototype;
        if (w && (w[f] || a(w, f, h),
        w[l] || a(w, l, y),
        s[y] = h,
        m))
            for (g in r)
                w[g] || o(w, g, r[g], !0)
    }
}
, function(t, e, n) {
    (function(e) {
        !function(e) {
            "use strict";
            var n = Object.prototype
              , r = n.hasOwnProperty
              , i = "function" == typeof Symbol ? Symbol : {}
              , o = i.iterator || "@@iterator"
              , u = i.asyncIterator || "@@asyncIterator"
              , a = i.toStringTag || "@@toStringTag"
              , s = "object" == typeof t
              , c = e.regeneratorRuntime;
            if (c)
                s && (t.exports = c);
            else {
                (c = e.regeneratorRuntime = s ? t.exports : {}).wrap = v;
                var f = {}
                  , l = {};
                l[o] = function() {
                    return this
                }
                ;
                var h = Object.getPrototypeOf
                  , p = h && h(h(E([])));
                p && p !== n && r.call(p, o) && (l = p);
                var d = b.prototype = y.prototype = Object.create(l);
                m.prototype = d.constructor = b,
                b.constructor = m,
                b[a] = m.displayName = "GeneratorFunction",
                c.isGeneratorFunction = function(t) {
                    var e = "function" == typeof t && t.constructor;
                    return !!e && (e === m || "GeneratorFunction" === (e.displayName || e.name))
                }
                ,
                c.mark = function(t) {
                    return Object.setPrototypeOf ? Object.setPrototypeOf(t, b) : (t.__proto__ = b,
                    a in t || (t[a] = "GeneratorFunction")),
                    t.prototype = Object.create(d),
                    t
                }
                ,
                c.awrap = function(t) {
                    return {
                        __await: t
                    }
                }
                ,
                w(_.prototype),
                _.prototype[u] = function() {
                    return this
                }
                ,
                c.AsyncIterator = _,
                c.async = function(t, e, n, r) {
                    var i = new _(v(t, e, n, r));
                    return c.isGeneratorFunction(e) ? i : i.next().then((function(t) {
                        return t.done ? t.value : i.next()
                    }
                    ))
                }
                ,
                w(d),
                d[a] = "Generator",
                d[o] = function() {
                    return this
                }
                ,
                d.toString = function() {
                    return "[object Generator]"
                }
                ,
                c.keys = function(t) {
                    var e = [];
                    for (var n in t)
                        e.push(n);
                    return e.reverse(),
                    function n() {
                        for (; e.length; ) {
                            var r = e.pop();
                            if (r in t)
                                return n.value = r,
                                n.done = !1,
                                n
                        }
                        return n.done = !0,
                        n
                    }
                }
                ,
                c.values = E,
                O.prototype = {
                    constructor: O,
                    reset: function(t) {
                        if (this.prev = 0,
                        this.next = 0,
                        this.sent = this._sent = void 0,
                        this.done = !1,
                        this.delegate = null,
                        this.method = "next",
                        this.arg = void 0,
                        this.tryEntries.forEach(k),
                        !t)
                            for (var e in this)
                                "t" === e.charAt(0) && r.call(this, e) && !isNaN(+e.slice(1)) && (this[e] = void 0)
                    },
                    stop: function() {
                        this.done = !0;
                        var t = this.tryEntries[0].completion;
                        if ("throw" === t.type)
                            throw t.arg;
                        return this.rval
                    },
                    dispatchException: function(t) {
                        if (this.done)
                            throw t;
                        var e = this;
                        function n(n, r) {
                            return u.type = "throw",
                            u.arg = t,
                            e.next = n,
                            r && (e.method = "next",
                            e.arg = void 0),
                            !!r
                        }
                        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
                            var o = this.tryEntries[i]
                              , u = o.completion;
                            if ("root" === o.tryLoc)
                                return n("end");
                            if (o.tryLoc <= this.prev) {
                                var a = r.call(o, "catchLoc")
                                  , s = r.call(o, "finallyLoc");
                                if (a && s) {
                                    if (this.prev < o.catchLoc)
                                        return n(o.catchLoc, !0);
                                    if (this.prev < o.finallyLoc)
                                        return n(o.finallyLoc)
                                } else if (a) {
                                    if (this.prev < o.catchLoc)
                                        return n(o.catchLoc, !0)
                                } else {
                                    if (!s)
                                        throw new Error("try statement without catch or finally");
                                    if (this.prev < o.finallyLoc)
                                        return n(o.finallyLoc)
                                }
                            }
                        }
                    },
                    abrupt: function(t, e) {
                        for (var n = this.tryEntries.length - 1; n >= 0; --n) {
                            var i = this.tryEntries[n];
                            if (i.tryLoc <= this.prev && r.call(i, "finallyLoc") && this.prev < i.finallyLoc) {
                                var o = i;
                                break
                            }
                        }
                        o && ("break" === t || "continue" === t) && o.tryLoc <= e && e <= o.finallyLoc && (o = null);
                        var u = o ? o.completion : {};
                        return u.type = t,
                        u.arg = e,
                        o ? (this.method = "next",
                        this.next = o.finallyLoc,
                        f) : this.complete(u)
                    },
                    complete: function(t, e) {
                        if ("throw" === t.type)
                            throw t.arg;
                        return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg,
                        this.method = "return",
                        this.next = "end") : "normal" === t.type && e && (this.next = e),
                        f
                    },
                    finish: function(t) {
                        for (var e = this.tryEntries.length - 1; e >= 0; --e) {
                            var n = this.tryEntries[e];
                            if (n.finallyLoc === t)
                                return this.complete(n.completion, n.afterLoc),
                                k(n),
                                f
                        }
                    },
                    catch: function(t) {
                        for (var e = this.tryEntries.length - 1; e >= 0; --e) {
                            var n = this.tryEntries[e];
                            if (n.tryLoc === t) {
                                var r = n.completion;
                                if ("throw" === r.type) {
                                    var i = r.arg;
                                    k(n)
                                }
                                return i
                            }
                        }
                        throw new Error("illegal catch attempt")
                    },
                    delegateYield: function(t, e, n) {
                        return this.delegate = {
                            iterator: E(t),
                            resultName: e,
                            nextLoc: n
                        },
                        "next" === this.method && (this.arg = void 0),
                        f
                    }
                }
            }
            function v(t, e, n, r) {
                var i = e && e.prototype instanceof y ? e : y
                  , o = Object.create(i.prototype)
                  , u = new O(r || []);
                return o._invoke = function(t, e, n) {
                    var r = "suspendedStart";
                    return function(i, o) {
                        if ("executing" === r)
                            throw new Error("Generator is already running");
                        if ("completed" === r) {
                            if ("throw" === i)
                                throw o;
                            return j()
                        }
                        for (n.method = i,
                        n.arg = o; ; ) {
                            var u = n.delegate;
                            if (u) {
                                var a = x(u, n);
                                if (a) {
                                    if (a === f)
                                        continue;
                                    return a
                                }
                            }
                            if ("next" === n.method)
                                n.sent = n._sent = n.arg;
                            else if ("throw" === n.method) {
                                if ("suspendedStart" === r)
                                    throw r = "completed",
                                    n.arg;
                                n.dispatchException(n.arg)
                            } else
                                "return" === n.method && n.abrupt("return", n.arg);
                            r = "executing";
                            var s = g(t, e, n);
                            if ("normal" === s.type) {
                                if (r = n.done ? "completed" : "suspendedYield",
                                s.arg === f)
                                    continue;
                                return {
                                    value: s.arg,
                                    done: n.done
                                }
                            }
                            "throw" === s.type && (r = "completed",
                            n.method = "throw",
                            n.arg = s.arg)
                        }
                    }
                }(t, n, u),
                o
            }
            function g(t, e, n) {
                try {
                    return {
                        type: "normal",
                        arg: t.call(e, n)
                    }
                } catch (t) {
                    return {
                        type: "throw",
                        arg: t
                    }
                }
            }
            function y() {}
            function m() {}
            function b() {}
            function w(t) {
                ["next", "throw", "return"].forEach((function(e) {
                    t[e] = function(t) {
                        return this._invoke(e, t)
                    }
                }
                ))
            }
            function _(t) {
                function n(e, i, o, u) {
                    var a = g(t[e], t, i);
                    if ("throw" !== a.type) {
                        var s = a.arg
                          , c = s.value;
                        return c && "object" == typeof c && r.call(c, "__await") ? Promise.resolve(c.__await).then((function(t) {
                            n("next", t, o, u)
                        }
                        ), (function(t) {
                            n("throw", t, o, u)
                        }
                        )) : Promise.resolve(c).then((function(t) {
                            s.value = t,
                            o(s)
                        }
                        ), u)
                    }
                    u(a.arg)
                }
                var i;
                "object" == typeof e.process && e.process.domain && (n = e.process.domain.bind(n)),
                this._invoke = function(t, e) {
                    function r() {
                        return new Promise((function(r, i) {
                            n(t, e, r, i)
                        }
                        ))
                    }
                    return i = i ? i.then(r, r) : r()
                }
            }
            function x(t, e) {
                var n = t.iterator[e.method];
                if (void 0 === n) {
                    if (e.delegate = null,
                    "throw" === e.method) {
                        if (t.iterator.return && (e.method = "return",
                        e.arg = void 0,
                        x(t, e),
                        "throw" === e.method))
                            return f;
                        e.method = "throw",
                        e.arg = new TypeError("The iterator does not provide a 'throw' method")
                    }
                    return f
                }
                var r = g(n, t.iterator, e.arg);
                if ("throw" === r.type)
                    return e.method = "throw",
                    e.arg = r.arg,
                    e.delegate = null,
                    f;
                var i = r.arg;
                return i ? i.done ? (e[t.resultName] = i.value,
                e.next = t.nextLoc,
                "return" !== e.method && (e.method = "next",
                e.arg = void 0),
                e.delegate = null,
                f) : i : (e.method = "throw",
                e.arg = new TypeError("iterator result is not an object"),
                e.delegate = null,
                f)
            }
            function S(t) {
                var e = {
                    tryLoc: t[0]
                };
                1 in t && (e.catchLoc = t[1]),
                2 in t && (e.finallyLoc = t[2],
                e.afterLoc = t[3]),
                this.tryEntries.push(e)
            }
            function k(t) {
                var e = t.completion || {};
                e.type = "normal",
                delete e.arg,
                t.completion = e
            }
            function O(t) {
                this.tryEntries = [{
                    tryLoc: "root"
                }],
                t.forEach(S, this),
                this.reset(!0)
            }
            function E(t) {
                if (t) {
                    var e = t[o];
                    if (e)
                        return e.call(t);
                    if ("function" == typeof t.next)
                        return t;
                    if (!isNaN(t.length)) {
                        var n = -1
                          , i = function e() {
                            for (; ++n < t.length; )
                                if (r.call(t, n))
                                    return e.value = t[n],
                                    e.done = !1,
                                    e;
                            return e.value = void 0,
                            e.done = !0,
                            e
                        };
                        return i.next = i
                    }
                }
                return {
                    next: j
                }
            }
            function j() {
                return {
                    value: void 0,
                    done: !0
                }
            }
        }("object" == typeof e ? e : "object" == typeof window ? window : "object" == typeof self ? self : this)
    }
    ).call(this, n(101))
}
, function(t, e, n) {
    n(350),
    t.exports = n(21).RegExp.escape
}
, function(t, e, n) {
    var r = n(0)
      , i = n(351)(/[\\^$*+?.()|[\]{}]/g, "\\$&");
    r(r.S, "RegExp", {
        escape: function(t) {
            return i(t)
        }
    })
}
, function(t, e) {
    t.exports = function(t, e) {
        var n = e === Object(e) ? function(t) {
            return e[t]
        }
        : e;
        return function(e) {
            return String(e).replace(t, n)
        }
    }
}
, function(t, e, n) {
    "use strict";
    (function(e) {
        const r = n(353)
          , i = n(354)
          , o = n(359).stdout
          , u = n(360)
          , a = "win32" === e.platform && !(e.env.TERM || "").toLowerCase().startsWith("xterm")
          , s = ["ansi", "ansi", "ansi256", "ansi16m"]
          , c = new Set(["gray"])
          , f = Object.create(null);
        function l(t, e) {
            e = e || {};
            const n = o ? o.level : 0;
            t.level = void 0 === e.level ? n : e.level,
            t.enabled = "enabled"in e ? e.enabled : t.level > 0
        }
        function h(t) {
            if (!this || !(this instanceof h) || this.template) {
                const e = {};
                return l(e, t),
                e.template = function() {
                    const t = [].slice.call(arguments);
                    return g.apply(null, [e.template].concat(t))
                }
                ,
                Object.setPrototypeOf(e, h.prototype),
                Object.setPrototypeOf(e.template, e),
                e.template.constructor = h,
                e.template
            }
            l(this, t)
        }
        a && (i.blue.open = "[94m");
        for (const t of Object.keys(i))
            i[t].closeRe = new RegExp(r(i[t].close),"g"),
            f[t] = {
                get() {
                    const e = i[t];
                    return d.call(this, this._styles ? this._styles.concat(e) : [e], this._empty, t)
                }
            };
        f.visible = {
            get() {
                return d.call(this, this._styles || [], !0, "visible")
            }
        },
        i.color.closeRe = new RegExp(r(i.color.close),"g");
        for (const t of Object.keys(i.color.ansi))
            c.has(t) || (f[t] = {
                get() {
                    const e = this.level;
                    return function() {
                        const n = i.color[s[e]][t].apply(null, arguments)
                          , r = {
                            open: n,
                            close: i.color.close,
                            closeRe: i.color.closeRe
                        };
                        return d.call(this, this._styles ? this._styles.concat(r) : [r], this._empty, t)
                    }
                }
            });
        i.bgColor.closeRe = new RegExp(r(i.bgColor.close),"g");
        for (const t of Object.keys(i.bgColor.ansi)) {
            if (c.has(t))
                continue;
            f["bg" + t[0].toUpperCase() + t.slice(1)] = {
                get() {
                    const e = this.level;
                    return function() {
                        const n = i.bgColor[s[e]][t].apply(null, arguments)
                          , r = {
                            open: n,
                            close: i.bgColor.close,
                            closeRe: i.bgColor.closeRe
                        };
                        return d.call(this, this._styles ? this._styles.concat(r) : [r], this._empty, t)
                    }
                }
            }
        }
        const p = Object.defineProperties(()=>{}
        , f);
        function d(t, e, n) {
            const r = function() {
                return v.apply(r, arguments)
            };
            r._styles = t,
            r._empty = e;
            const i = this;
            return Object.defineProperty(r, "level", {
                enumerable: !0,
                get: ()=>i.level,
                set(t) {
                    i.level = t
                }
            }),
            Object.defineProperty(r, "enabled", {
                enumerable: !0,
                get: ()=>i.enabled,
                set(t) {
                    i.enabled = t
                }
            }),
            r.hasGrey = this.hasGrey || "gray" === n || "grey" === n,
            r.__proto__ = p,
            r
        }
        function v() {
            const t = arguments
              , e = t.length;
            let n = String(arguments[0]);
            if (0 === e)
                return "";
            if (e > 1)
                for (let r = 1; r < e; r++)
                    n += " " + t[r];
            if (!this.enabled || this.level <= 0 || !n)
                return this._empty ? "" : n;
            const r = i.dim.open;
            a && this.hasGrey && (i.dim.open = "");
            for (const t of this._styles.slice().reverse())
                n = t.open + n.replace(t.closeRe, t.open) + t.close,
                n = n.replace(/\r?\n/g, `${t.close}$&${t.open}`);
            return i.dim.open = r,
            n
        }
        function g(t, e) {
            if (!Array.isArray(e))
                return [].slice.call(arguments, 1).join(" ");
            const n = [].slice.call(arguments, 2)
              , r = [e.raw[0]];
            for (let t = 1; t < e.length; t++)
                r.push(String(n[t - 1]).replace(/[{}\\]/g, "\\$&")),
                r.push(String(e.raw[t]));
            return u(t, r.join(""))
        }
        Object.defineProperties(h.prototype, f),
        t.exports = h(),
        t.exports.supportsColor = o,
        t.exports.default = t.exports
    }
    ).call(this, n(98))
}
, function(t, e, n) {
    "use strict";
    var r = /[|\\{}()[\]^$+*?.]/g;
    t.exports = function(t) {
        if ("string" != typeof t)
            throw new TypeError("Expected a string");
        return t.replace(r, "\\$&")
    }
}
, function(t, e, n) {
    "use strict";
    (function(t) {
        const e = n(356)
          , r = (t,n)=>function() {
            const r = t.apply(e, arguments);
            return `[${r + n}m`
        }
          , i = (t,n)=>function() {
            const r = t.apply(e, arguments);
            return `[${38 + n};5;${r}m`
        }
          , o = (t,n)=>function() {
            const r = t.apply(e, arguments);
            return `[${38 + n};2;${r[0]};${r[1]};${r[2]}m`
        }
        ;
        Object.defineProperty(t, "exports", {
            enumerable: !0,
            get: function() {
                const t = new Map
                  , n = {
                    modifier: {
                        reset: [0, 0],
                        bold: [1, 22],
                        dim: [2, 22],
                        italic: [3, 23],
                        underline: [4, 24],
                        inverse: [7, 27],
                        hidden: [8, 28],
                        strikethrough: [9, 29]
                    },
                    color: {
                        black: [30, 39],
                        red: [31, 39],
                        green: [32, 39],
                        yellow: [33, 39],
                        blue: [34, 39],
                        magenta: [35, 39],
                        cyan: [36, 39],
                        white: [37, 39],
                        gray: [90, 39],
                        redBright: [91, 39],
                        greenBright: [92, 39],
                        yellowBright: [93, 39],
                        blueBright: [94, 39],
                        magentaBright: [95, 39],
                        cyanBright: [96, 39],
                        whiteBright: [97, 39]
                    },
                    bgColor: {
                        bgBlack: [40, 49],
                        bgRed: [41, 49],
                        bgGreen: [42, 49],
                        bgYellow: [43, 49],
                        bgBlue: [44, 49],
                        bgMagenta: [45, 49],
                        bgCyan: [46, 49],
                        bgWhite: [47, 49],
                        bgBlackBright: [100, 49],
                        bgRedBright: [101, 49],
                        bgGreenBright: [102, 49],
                        bgYellowBright: [103, 49],
                        bgBlueBright: [104, 49],
                        bgMagentaBright: [105, 49],
                        bgCyanBright: [106, 49],
                        bgWhiteBright: [107, 49]
                    }
                };
                n.color.grey = n.color.gray;
                for (const e of Object.keys(n)) {
                    const r = n[e];
                    for (const e of Object.keys(r)) {
                        const i = r[e];
                        n[e] = {
                            open: `[${i[0]}m`,
                            close: `[${i[1]}m`
                        },
                        r[e] = n[e],
                        t.set(i[0], i[1])
                    }
                    Object.defineProperty(n, e, {
                        value: r,
                        enumerable: !1
                    }),
                    Object.defineProperty(n, "codes", {
                        value: t,
                        enumerable: !1
                    })
                }
                const u = t=>t
                  , a = (t,e,n)=>[t, e, n];
                n.color.close = "[39m",
                n.bgColor.close = "[49m",
                n.color.ansi = {
                    ansi: r(u, 0)
                },
                n.color.ansi256 = {
                    ansi256: i(u, 0)
                },
                n.color.ansi16m = {
                    rgb: o(a, 0)
                },
                n.bgColor.ansi = {
                    ansi: r(u, 10)
                },
                n.bgColor.ansi256 = {
                    ansi256: i(u, 10)
                },
                n.bgColor.ansi16m = {
                    rgb: o(a, 10)
                };
                for (let t of Object.keys(e)) {
                    if ("object" != typeof e[t])
                        continue;
                    const u = e[t];
                    "ansi16" === t && (t = "ansi"),
                    "ansi16"in u && (n.color.ansi[t] = r(u.ansi16, 0),
                    n.bgColor.ansi[t] = r(u.ansi16, 10)),
                    "ansi256"in u && (n.color.ansi256[t] = i(u.ansi256, 0),
                    n.bgColor.ansi256[t] = i(u.ansi256, 10)),
                    "rgb"in u && (n.color.ansi16m[t] = o(u.rgb, 0),
                    n.bgColor.ansi16m[t] = o(u.rgb, 10))
                }
                return n
            }
        })
    }
    ).call(this, n(355)(t))
}
, function(t, e) {
    t.exports = function(t) {
        return t.webpackPolyfill || (t.deprecate = function() {}
        ,
        t.paths = [],
        t.children || (t.children = []),
        Object.defineProperty(t, "loaded", {
            enumerable: !0,
            get: function() {
                return t.l
            }
        }),
        Object.defineProperty(t, "id", {
            enumerable: !0,
            get: function() {
                return t.i
            }
        }),
        t.webpackPolyfill = 1),
        t
    }
}
, function(t, e, n) {
    var r = n(138)
      , i = n(358)
      , o = {};
    Object.keys(r).forEach((function(t) {
        o[t] = {},
        Object.defineProperty(o[t], "channels", {
            value: r[t].channels
        }),
        Object.defineProperty(o[t], "labels", {
            value: r[t].labels
        });
        var e = i(t);
        Object.keys(e).forEach((function(n) {
            var r = e[n];
            o[t][n] = function(t) {
                var e = function(e) {
                    if (null == e)
                        return e;
                    arguments.length > 1 && (e = Array.prototype.slice.call(arguments));
                    var n = t(e);
                    if ("object" == typeof n)
                        for (var r = n.length, i = 0; i < r; i++)
                            n[i] = Math.round(n[i]);
                    return n
                };
                return "conversion"in t && (e.conversion = t.conversion),
                e
            }(r),
            o[t][n].raw = function(t) {
                var e = function(e) {
                    return null == e ? e : (arguments.length > 1 && (e = Array.prototype.slice.call(arguments)),
                    t(e))
                };
                return "conversion"in t && (e.conversion = t.conversion),
                e
            }(r)
        }
        ))
    }
    )),
    t.exports = o
}
, function(t, e, n) {
    "use strict";
    t.exports = {
        aliceblue: [240, 248, 255],
        antiquewhite: [250, 235, 215],
        aqua: [0, 255, 255],
        aquamarine: [127, 255, 212],
        azure: [240, 255, 255],
        beige: [245, 245, 220],
        bisque: [255, 228, 196],
        black: [0, 0, 0],
        blanchedalmond: [255, 235, 205],
        blue: [0, 0, 255],
        blueviolet: [138, 43, 226],
        brown: [165, 42, 42],
        burlywood: [222, 184, 135],
        cadetblue: [95, 158, 160],
        chartreuse: [127, 255, 0],
        chocolate: [210, 105, 30],
        coral: [255, 127, 80],
        cornflowerblue: [100, 149, 237],
        cornsilk: [255, 248, 220],
        crimson: [220, 20, 60],
        cyan: [0, 255, 255],
        darkblue: [0, 0, 139],
        darkcyan: [0, 139, 139],
        darkgoldenrod: [184, 134, 11],
        darkgray: [169, 169, 169],
        darkgreen: [0, 100, 0],
        darkgrey: [169, 169, 169],
        darkkhaki: [189, 183, 107],
        darkmagenta: [139, 0, 139],
        darkolivegreen: [85, 107, 47],
        darkorange: [255, 140, 0],
        darkorchid: [153, 50, 204],
        darkred: [139, 0, 0],
        darksalmon: [233, 150, 122],
        darkseagreen: [143, 188, 143],
        darkslateblue: [72, 61, 139],
        darkslategray: [47, 79, 79],
        darkslategrey: [47, 79, 79],
        darkturquoise: [0, 206, 209],
        darkviolet: [148, 0, 211],
        deeppink: [255, 20, 147],
        deepskyblue: [0, 191, 255],
        dimgray: [105, 105, 105],
        dimgrey: [105, 105, 105],
        dodgerblue: [30, 144, 255],
        firebrick: [178, 34, 34],
        floralwhite: [255, 250, 240],
        forestgreen: [34, 139, 34],
        fuchsia: [255, 0, 255],
        gainsboro: [220, 220, 220],
        ghostwhite: [248, 248, 255],
        gold: [255, 215, 0],
        goldenrod: [218, 165, 32],
        gray: [128, 128, 128],
        green: [0, 128, 0],
        greenyellow: [173, 255, 47],
        grey: [128, 128, 128],
        honeydew: [240, 255, 240],
        hotpink: [255, 105, 180],
        indianred: [205, 92, 92],
        indigo: [75, 0, 130],
        ivory: [255, 255, 240],
        khaki: [240, 230, 140],
        lavender: [230, 230, 250],
        lavenderblush: [255, 240, 245],
        lawngreen: [124, 252, 0],
        lemonchiffon: [255, 250, 205],
        lightblue: [173, 216, 230],
        lightcoral: [240, 128, 128],
        lightcyan: [224, 255, 255],
        lightgoldenrodyellow: [250, 250, 210],
        lightgray: [211, 211, 211],
        lightgreen: [144, 238, 144],
        lightgrey: [211, 211, 211],
        lightpink: [255, 182, 193],
        lightsalmon: [255, 160, 122],
        lightseagreen: [32, 178, 170],
        lightskyblue: [135, 206, 250],
        lightslategray: [119, 136, 153],
        lightslategrey: [119, 136, 153],
        lightsteelblue: [176, 196, 222],
        lightyellow: [255, 255, 224],
        lime: [0, 255, 0],
        limegreen: [50, 205, 50],
        linen: [250, 240, 230],
        magenta: [255, 0, 255],
        maroon: [128, 0, 0],
        mediumaquamarine: [102, 205, 170],
        mediumblue: [0, 0, 205],
        mediumorchid: [186, 85, 211],
        mediumpurple: [147, 112, 219],
        mediumseagreen: [60, 179, 113],
        mediumslateblue: [123, 104, 238],
        mediumspringgreen: [0, 250, 154],
        mediumturquoise: [72, 209, 204],
        mediumvioletred: [199, 21, 133],
        midnightblue: [25, 25, 112],
        mintcream: [245, 255, 250],
        mistyrose: [255, 228, 225],
        moccasin: [255, 228, 181],
        navajowhite: [255, 222, 173],
        navy: [0, 0, 128],
        oldlace: [253, 245, 230],
        olive: [128, 128, 0],
        olivedrab: [107, 142, 35],
        orange: [255, 165, 0],
        orangered: [255, 69, 0],
        orchid: [218, 112, 214],
        palegoldenrod: [238, 232, 170],
        palegreen: [152, 251, 152],
        paleturquoise: [175, 238, 238],
        palevioletred: [219, 112, 147],
        papayawhip: [255, 239, 213],
        peachpuff: [255, 218, 185],
        peru: [205, 133, 63],
        pink: [255, 192, 203],
        plum: [221, 160, 221],
        powderblue: [176, 224, 230],
        purple: [128, 0, 128],
        rebeccapurple: [102, 51, 153],
        red: [255, 0, 0],
        rosybrown: [188, 143, 143],
        royalblue: [65, 105, 225],
        saddlebrown: [139, 69, 19],
        salmon: [250, 128, 114],
        sandybrown: [244, 164, 96],
        seagreen: [46, 139, 87],
        seashell: [255, 245, 238],
        sienna: [160, 82, 45],
        silver: [192, 192, 192],
        skyblue: [135, 206, 235],
        slateblue: [106, 90, 205],
        slategray: [112, 128, 144],
        slategrey: [112, 128, 144],
        snow: [255, 250, 250],
        springgreen: [0, 255, 127],
        steelblue: [70, 130, 180],
        tan: [210, 180, 140],
        teal: [0, 128, 128],
        thistle: [216, 191, 216],
        tomato: [255, 99, 71],
        turquoise: [64, 224, 208],
        violet: [238, 130, 238],
        wheat: [245, 222, 179],
        white: [255, 255, 255],
        whitesmoke: [245, 245, 245],
        yellow: [255, 255, 0],
        yellowgreen: [154, 205, 50]
    }
}
, function(t, e, n) {
    var r = n(138);
    function i(t) {
        var e = function() {
            for (var t = {}, e = Object.keys(r), n = e.length, i = 0; i < n; i++)
                t[e[i]] = {
                    distance: -1,
                    parent: null
                };
            return t
        }()
          , n = [t];
        for (e[t].distance = 0; n.length; )
            for (var i = n.pop(), o = Object.keys(r[i]), u = o.length, a = 0; a < u; a++) {
                var s = o[a]
                  , c = e[s];
                -1 === c.distance && (c.distance = e[i].distance + 1,
                c.parent = i,
                n.unshift(s))
            }
        return e
    }
    function o(t, e) {
        return function(n) {
            return e(t(n))
        }
    }
    function u(t, e) {
        for (var n = [e[t].parent, t], i = r[e[t].parent][t], u = e[t].parent; e[u].parent; )
            n.unshift(e[u].parent),
            i = o(r[e[u].parent][u], i),
            u = e[u].parent;
        return i.conversion = n,
        i
    }
    t.exports = function(t) {
        for (var e = i(t), n = {}, r = Object.keys(e), o = r.length, a = 0; a < o; a++) {
            var s = r[a];
            null !== e[s].parent && (n[s] = u(s, e))
        }
        return n
    }
}
, function(t, e, n) {
    "use strict";
    t.exports = {
        stdout: !1,
        stderr: !1
    }
}
, function(t, e, n) {
    "use strict";
    const r = /(?:\\(u[a-f\d]{4}|x[a-f\d]{2}|.))|(?:\{(~)?(\w+(?:\([^)]*\))?(?:\.\w+(?:\([^)]*\))?)*)(?:[ \t]|(?=\r?\n)))|(\})|((?:.|[\r\n\f])+?)/gi
      , i = /(?:^|\.)(\w+)(?:\(([^)]*)\))?/g
      , o = /^(['"])((?:\\.|(?!\1)[^\\])*)\1$/
      , u = /\\(u[a-f\d]{4}|x[a-f\d]{2}|.)|([^\\])/gi
      , a = new Map([["n", "\n"], ["r", "\r"], ["t", "\t"], ["b", "\b"], ["f", "\f"], ["v", "\v"], ["0", "\0"], ["\\", "\\"], ["e", ""], ["a", ""]]);
    function s(t) {
        return "u" === t[0] && 5 === t.length || "x" === t[0] && 3 === t.length ? String.fromCharCode(parseInt(t.slice(1), 16)) : a.get(t) || t
    }
    function c(t, e) {
        const n = []
          , r = e.trim().split(/\s*,\s*/g);
        let i;
        for (const e of r)
            if (isNaN(e)) {
                if (!(i = e.match(o)))
                    throw new Error(`Invalid Chalk template style argument: ${e} (in style '${t}')`);
                n.push(i[2].replace(u, (t,e,n)=>e ? s(e) : n))
            } else
                n.push(Number(e));
        return n
    }
    function f(t) {
        i.lastIndex = 0;
        const e = [];
        let n;
        for (; null !== (n = i.exec(t)); ) {
            const t = n[1];
            if (n[2]) {
                const r = c(t, n[2]);
                e.push([t].concat(r))
            } else
                e.push([t])
        }
        return e
    }
    function l(t, e) {
        const n = {};
        for (const t of e)
            for (const e of t.styles)
                n[e[0]] = t.inverse ? null : e.slice(1);
        let r = t;
        for (const t of Object.keys(n))
            if (Array.isArray(n[t])) {
                if (!(t in r))
                    throw new Error(`Unknown Chalk style: ${t}`);
                r = n[t].length > 0 ? r[t].apply(r, n[t]) : r[t]
            }
        return r
    }
    t.exports = (t,e)=>{
        const n = []
          , i = [];
        let o = [];
        if (e.replace(r, (e,r,u,a,c,h)=>{
            if (r)
                o.push(s(r));
            else if (a) {
                const e = o.join("");
                o = [],
                i.push(0 === n.length ? e : l(t, n)(e)),
                n.push({
                    inverse: u,
                    styles: f(a)
                })
            } else if (c) {
                if (0 === n.length)
                    throw new Error("Found extraneous } in Chalk template literal");
                i.push(l(t, n)(o.join(""))),
                o = [],
                n.pop()
            } else
                o.push(h)
        }
        ),
        i.push(o.join("")),
        n.length > 0) {
            const t = `Chalk template literal is missing ${n.length} closing bracket${1 === n.length ? "" : "s"} (\`}\`)`;
            throw new Error(t)
        }
        return i.join("")
    }
}
, function(t, e, n) {
    const r = n(362)
      , i = n(99)
      , o = n(139)
      , u = Symbol("log-factories");
    t.exports = new class extends r {
        constructor() {
            super({
                name: "default"
            }),
            this.cache = {
                default: this
            },
            this[u] = {
                MethodFactory: i,
                PrefixFactory: o
            }
        }
        get factories() {
            return this[u]
        }
        get loggers() {
            return this.cache
        }
        create(t) {
            let e;
            e = "string" == typeof t ? {
                name: t
            } : Object.assign({}, t),
            e.id || (e.id = e.name);
            const {name: n, id: i} = e
              , o = {
                level: this.level
            };
            if ("string" != typeof n || !n || !n.length)
                throw new TypeError("You must supply a name when creating a logger.");
            let u = this.cache[i];
            return u || (u = new r(Object.assign({}, o, e)),
            this.cache[i] = u),
            u
        }
    }
    ,
    t.exports.default = t.exports
}
, function(t, e, n) {
    const r = n(139)
      , i = n(99)
      , o = {
        factory: null,
        level: "warn",
        name: +new Date,
        prefix: null
    };
    t.exports = class {
        constructor(t) {
            if (this.type = "LogLevel",
            this.options = Object.assign({}, o, t),
            this.methodFactory = t.factory,
            !this.methodFactory) {
                const e = t.prefix ? new r(this,t.prefix) : new i(this);
                this.methodFactory = e
            }
            this.methodFactory.logger || (this.methodFactory.logger = this),
            this.name = t.name || "<unknown>",
            this.level = this.options.level
        }
        get factory() {
            return this.methodFactory
        }
        set factory(t) {
            t.logger = this,
            this.methodFactory = t,
            this.methodFactory.replaceMethods(this.level)
        }
        disable() {
            this.level = this.levels.SILENT
        }
        enable() {
            this.level = this.levels.TRACE
        }
        get level() {
            return this.currentLevel
        }
        set level(t) {
            const e = this.methodFactory.distillLevel(t);
            if (!1 === e || null == e)
                throw new RangeError(`loglevelnext: setLevel() called with invalid level: ${t}`);
            this.currentLevel = e,
            this.methodFactory.replaceMethods(e),
            "undefined" == typeof console && e < this.levels.SILENT && console.warn("loglevelnext: console is undefined. The log will produce no output.")
        }
        get levels() {
            return this.methodFactory.levels
        }
    }
}
, function(t, e, n) {
    for (var r = self.crypto || self.msCrypto, i = "-_", o = 36; o--; )
        i += o.toString(36);
    for (o = 36; o-- - 10; )
        i += o.toString(36).toUpperCase();
    t.exports = function(t) {
        var e = ""
          , n = r.getRandomValues(new Uint8Array(t || 21));
        for (o = t || 21; o--; )
            e += i[63 & n[o]];
        return e
    }
}
, function(t, e) {
    var n = "undefined" != typeof crypto && crypto.getRandomValues && crypto.getRandomValues.bind(crypto) || "undefined" != typeof msCrypto && "function" == typeof window.msCrypto.getRandomValues && msCrypto.getRandomValues.bind(msCrypto);
    if (n) {
        var r = new Uint8Array(16);
        t.exports = function() {
            return n(r),
            r
        }
    } else {
        var i = new Array(16);
        t.exports = function() {
            for (var t, e = 0; e < 16; e++)
                0 == (3 & e) && (t = 4294967296 * Math.random()),
                i[e] = t >>> ((3 & e) << 3) & 255;
            return i
        }
    }
}
, function(t, e) {
    for (var n = [], r = 0; r < 256; ++r)
        n[r] = (r + 256).toString(16).substr(1);
    t.exports = function(t, e) {
        var r = e || 0
          , i = n;
        return [i[t[r++]], i[t[r++]], i[t[r++]], i[t[r++]], "-", i[t[r++]], i[t[r++]], "-", i[t[r++]], i[t[r++]], "-", i[t[r++]], i[t[r++]], "-", i[t[r++]], i[t[r++]], i[t[r++]], i[t[r++]], i[t[r++]], i[t[r++]]].join("")
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(15)
      , i = n(140)
      , o = n(367)
      , u = n(146);
    function a(t) {
        var e = new o(t)
          , n = i(o.prototype.request, e);
        return r.extend(n, o.prototype, e),
        r.extend(n, e),
        n
    }
    var s = a(n(143));
    s.Axios = o,
    s.create = function(t) {
        return a(u(s.defaults, t))
    }
    ,
    s.Cancel = n(147),
    s.CancelToken = n(380),
    s.isCancel = n(142),
    s.all = function(t) {
        return Promise.all(t)
    }
    ,
    s.spread = n(381),
    t.exports = s,
    t.exports.default = s
}
, function(t, e, n) {
    "use strict";
    var r = n(15)
      , i = n(141)
      , o = n(368)
      , u = n(369)
      , a = n(146);
    function s(t) {
        this.defaults = t,
        this.interceptors = {
            request: new o,
            response: new o
        }
    }
    s.prototype.request = function(t) {
        "string" == typeof t ? (t = arguments[1] || {}).url = arguments[0] : t = t || {},
        (t = a(this.defaults, t)).method ? t.method = t.method.toLowerCase() : this.defaults.method ? t.method = this.defaults.method.toLowerCase() : t.method = "get";
        var e = [u, void 0]
          , n = Promise.resolve(t);
        for (this.interceptors.request.forEach((function(t) {
            e.unshift(t.fulfilled, t.rejected)
        }
        )),
        this.interceptors.response.forEach((function(t) {
            e.push(t.fulfilled, t.rejected)
        }
        )); e.length; )
            n = n.then(e.shift(), e.shift());
        return n
    }
    ,
    s.prototype.getUri = function(t) {
        return t = a(this.defaults, t),
        i(t.url, t.params, t.paramsSerializer).replace(/^\?/, "")
    }
    ,
    r.forEach(["delete", "get", "head", "options"], (function(t) {
        s.prototype[t] = function(e, n) {
            return this.request(r.merge(n || {}, {
                method: t,
                url: e
            }))
        }
    }
    )),
    r.forEach(["post", "put", "patch"], (function(t) {
        s.prototype[t] = function(e, n, i) {
            return this.request(r.merge(i || {}, {
                method: t,
                url: e,
                data: n
            }))
        }
    }
    )),
    t.exports = s
}
, function(t, e, n) {
    "use strict";
    var r = n(15);
    function i() {
        this.handlers = []
    }
    i.prototype.use = function(t, e) {
        return this.handlers.push({
            fulfilled: t,
            rejected: e
        }),
        this.handlers.length - 1
    }
    ,
    i.prototype.eject = function(t) {
        this.handlers[t] && (this.handlers[t] = null)
    }
    ,
    i.prototype.forEach = function(t) {
        r.forEach(this.handlers, (function(e) {
            null !== e && t(e)
        }
        ))
    }
    ,
    t.exports = i
}
, function(t, e, n) {
    "use strict";
    var r = n(15)
      , i = n(370)
      , o = n(142)
      , u = n(143);
    function a(t) {
        t.cancelToken && t.cancelToken.throwIfRequested()
    }
    t.exports = function(t) {
        return a(t),
        t.headers = t.headers || {},
        t.data = i(t.data, t.headers, t.transformRequest),
        t.headers = r.merge(t.headers.common || {}, t.headers[t.method] || {}, t.headers),
        r.forEach(["delete", "get", "head", "post", "put", "patch", "common"], (function(e) {
            delete t.headers[e]
        }
        )),
        (t.adapter || u.adapter)(t).then((function(e) {
            return a(t),
            e.data = i(e.data, e.headers, t.transformResponse),
            e
        }
        ), (function(e) {
            return o(e) || (a(t),
            e && e.response && (e.response.data = i(e.response.data, e.response.headers, t.transformResponse))),
            Promise.reject(e)
        }
        ))
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(15);
    t.exports = function(t, e, n) {
        return r.forEach(n, (function(n) {
            t = n(t, e)
        }
        )),
        t
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(15);
    t.exports = function(t, e) {
        r.forEach(t, (function(n, r) {
            r !== e && r.toUpperCase() === e.toUpperCase() && (t[e] = n,
            delete t[r])
        }
        ))
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(145);
    t.exports = function(t, e, n) {
        var i = n.config.validateStatus;
        !i || i(n.status) ? t(n) : e(r("Request failed with status code " + n.status, n.config, null, n.request, n))
    }
}
, function(t, e, n) {
    "use strict";
    t.exports = function(t, e, n, r, i) {
        return t.config = e,
        n && (t.code = n),
        t.request = r,
        t.response = i,
        t.isAxiosError = !0,
        t.toJSON = function() {
            return {
                message: this.message,
                name: this.name,
                description: this.description,
                number: this.number,
                fileName: this.fileName,
                lineNumber: this.lineNumber,
                columnNumber: this.columnNumber,
                stack: this.stack,
                config: this.config,
                code: this.code
            }
        }
        ,
        t
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(375)
      , i = n(376);
    t.exports = function(t, e) {
        return t && !r(e) ? i(t, e) : e
    }
}
, function(t, e, n) {
    "use strict";
    t.exports = function(t) {
        return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(t)
    }
}
, function(t, e, n) {
    "use strict";
    t.exports = function(t, e) {
        return e ? t.replace(/\/+$/, "") + "/" + e.replace(/^\/+/, "") : t
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(15)
      , i = ["age", "authorization", "content-length", "content-type", "etag", "expires", "from", "host", "if-modified-since", "if-unmodified-since", "last-modified", "location", "max-forwards", "proxy-authorization", "referer", "retry-after", "user-agent"];
    t.exports = function(t) {
        var e, n, o, u = {};
        return t ? (r.forEach(t.split("\n"), (function(t) {
            if (o = t.indexOf(":"),
            e = r.trim(t.substr(0, o)).toLowerCase(),
            n = r.trim(t.substr(o + 1)),
            e) {
                if (u[e] && i.indexOf(e) >= 0)
                    return;
                u[e] = "set-cookie" === e ? (u[e] ? u[e] : []).concat([n]) : u[e] ? u[e] + ", " + n : n
            }
        }
        )),
        u) : u
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(15);
    t.exports = r.isStandardBrowserEnv() ? function() {
        var t, e = /(msie|trident)/i.test(navigator.userAgent), n = document.createElement("a");
        function i(t) {
            var r = t;
            return e && (n.setAttribute("href", r),
            r = n.href),
            n.setAttribute("href", r),
            {
                href: n.href,
                protocol: n.protocol ? n.protocol.replace(/:$/, "") : "",
                host: n.host,
                search: n.search ? n.search.replace(/^\?/, "") : "",
                hash: n.hash ? n.hash.replace(/^#/, "") : "",
                hostname: n.hostname,
                port: n.port,
                pathname: "/" === n.pathname.charAt(0) ? n.pathname : "/" + n.pathname
            }
        }
        return t = i(window.location.href),
        function(e) {
            var n = r.isString(e) ? i(e) : e;
            return n.protocol === t.protocol && n.host === t.host
        }
    }() : function() {
        return !0
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(15);
    t.exports = r.isStandardBrowserEnv() ? {
        write: function(t, e, n, i, o, u) {
            var a = [];
            a.push(t + "=" + encodeURIComponent(e)),
            r.isNumber(n) && a.push("expires=" + new Date(n).toGMTString()),
            r.isString(i) && a.push("path=" + i),
            r.isString(o) && a.push("domain=" + o),
            !0 === u && a.push("secure"),
            document.cookie = a.join("; ")
        },
        read: function(t) {
            var e = document.cookie.match(new RegExp("(^|;\\s*)(" + t + ")=([^;]*)"));
            return e ? decodeURIComponent(e[3]) : null
        },
        remove: function(t) {
            this.write(t, "", Date.now() - 864e5)
        }
    } : {
        write: function() {},
        read: function() {
            return null
        },
        remove: function() {}
    }
}
, function(t, e, n) {
    "use strict";
    var r = n(147);
    function i(t) {
        if ("function" != typeof t)
            throw new TypeError("executor must be a function.");
        var e;
        this.promise = new Promise((function(t) {
            e = t
        }
        ));
        var n = this;
        t((function(t) {
            n.reason || (n.reason = new r(t),
            e(n.reason))
        }
        ))
    }
    i.prototype.throwIfRequested = function() {
        if (this.reason)
            throw this.reason
    }
    ,
    i.source = function() {
        var t;
        return {
            token: new i((function(e) {
                t = e
            }
            )),
            cancel: t
        }
    }
    ,
    t.exports = i
}
, function(t, e, n) {
    "use strict";
    t.exports = function(t) {
        return function(e) {
            return t.apply(null, e)
        }
    }
}
, function(t, e) {
    var n = this;
    function r(t, e, n, r, i, o, u) {
        try {
            var a = t[o](u)
              , s = a.value
        } catch (t) {
            return void n(t)
        }
        a.done ? e(s) : Promise.resolve(s).then(r, i)
    }
    function i(t) {
        return function() {
            var e = this
              , n = arguments;
            return new Promise((function(i, o) {
                var u = t.apply(e, n);
                function a(t) {
                    r(u, i, o, a, s, "next", t)
                }
                function s(t) {
                    r(u, i, o, a, s, "throw", t)
                }
                a(void 0)
            }
            ))
        }
    }
    window.addEventListener("load", function() {
        var t = i(regeneratorRuntime.mark((function t(e) {
            var r, o;
            return regeneratorRuntime.wrap((function(t) {
                for (; ; )
                    switch (t.prev = t.next) {
                    case 0:
                        return r = function() {
                            var t = i(regeneratorRuntime.mark((function t(e) {
                                var n;
                                return regeneratorRuntime.wrap((function(t) {
                                    for (; ; )
                                        switch (t.prev = t.next) {
                                        case 0:
                                            if (!e) {
                                                t.next = 8;
                                                break
                                            }
                                            if (!(n = e.gdpr.gdprDetails())) {
                                                t.next = 8;
                                                break
                                            }
                                            return t.next = 5,
                                            Leya.setUserGdprConsent(n.getConsentValue);
                                        case 5:
                                            if (!n.vendorList) {
                                                t.next = 8;
                                                break
                                            }
                                            return t.next = 8,
                                            Leya.setGdprVendorListVersion(n.vendorList.vendorListVersion);
                                        case 8:
                                        case "end":
                                            return t.stop()
                                        }
                                }
                                ), t)
                            }
                            )));
                            return function(e) {
                                return t.apply(this, arguments)
                            }
                        }(),
                        t.next = 3,
                        Leya.getUser();
                    case 3:
                        return o = t.sent,
                        t.next = 6,
                        r(o);
                    case 6:
                        return window.setInterval(i(regeneratorRuntime.mark((function t() {
                            var e;
                            return regeneratorRuntime.wrap((function(t) {
                                for (; ; )
                                    switch (t.prev = t.next) {
                                    case 0:
                                        return t.next = 2,
                                        Leya.getUser();
                                    case 2:
                                        return e = t.sent,
                                        t.next = 5,
                                        r(e);
                                    case 5:
                                    case "end":
                                        return t.stop()
                                    }
                            }
                            ), t)
                        }
                        ))).bind(n), 250),
                        t.next = 9,
                        Leya.Events.recordPageView();
                    case 9:
                    case "end":
                        return t.stop()
                    }
            }
            ), t)
        }
        )));
        return function(e) {
            return t.apply(this, arguments)
        }
    }()),
    window.addEventListener("beforeunload", function() {
        var t = i(regeneratorRuntime.mark((function t(e) {
            return regeneratorRuntime.wrap((function(t) {
                for (; ; )
                    switch (t.prev = t.next) {
                    case 0:
                        return t.next = 2,
                        Leya.finishSession();
                    case 2:
                        delete e.returnValue;
                    case 3:
                    case "end":
                        return t.stop()
                    }
            }
            ), t)
        }
        )));
        return function(e) {
            return t.apply(this, arguments)
        }
    }())
}
, function(t, e, n) {
    "use strict";
    n.r(e);
    var r = n(1)
      , i = n(20);
    function o(t, e) {
        for (var n = 0; n < e.length; n++) {
            var r = e[n];
            r.enumerable = r.enumerable || !1,
            r.configurable = !0,
            "value"in r && (r.writable = !0),
            Object.defineProperty(t, r.key, r)
        }
    }
    function u(t, e, n) {
        return e in t ? Object.defineProperty(t, e, {
            value: n,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : t[e] = n,
        t
    }
    var a = function() {
        function t() {
            !function(t, e) {
                if (!(t instanceof e))
                    throw new TypeError("Cannot call a class as a function")
            }(this, t),
            u(this, "_id", null),
            u(this, "_bat", null),
            u(this, "_status", null),
            u(this, "_cpm", null),
            u(this, "_start", null),
            u(this, "_finish", null)
        }
        var e, n, r;
        return e = t,
        r = [{
            key: "from",
            value: function(e) {
                var n = new t;
                return n.id = e.bidder,
                n.bat = e.isAfterTimeout,
                n.status = e.status,
                n.cpm = e.cpm,
                n.start = e.start,
                n.finish = e.finish,
                n
            }
        }],
        (n = [{
            key: "id",
            get: function() {
                return this._id
            },
            set: function(t) {
                this._id = t
            }
        }, {
            key: "bat",
            get: function() {
                return this._bat
            },
            set: function(t) {
                this._bat = t
            }
        }, {
            key: "status",
            get: function() {
                return this._status
            },
            set: function(t) {
                this._status = t
            }
        }, {
            key: "cpm",
            get: function() {
                return this._cpm
            },
            set: function(t) {
                this._cpm = t
            }
        }, {
            key: "start",
            get: function() {
                return this._start
            },
            set: function(t) {
                this._start = t
            }
        }, {
            key: "finish",
            get: function() {
                return this._finish
            },
            set: function(t) {
                this._finish = t
            }
        }]) && o(e.prototype, n),
        r && o(e, r),
        t
    }();
    function s(t, e) {
        for (var n = 0; n < e.length; n++) {
            var r = e[n];
            r.enumerable = r.enumerable || !1,
            r.configurable = !0,
            "value"in r && (r.writable = !0),
            Object.defineProperty(t, r.key, r)
        }
    }
    function c(t, e, n) {
        return e in t ? Object.defineProperty(t, e, {
            value: n,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : t[e] = n,
        t
    }
    var f = function() {
        function t() {
            !function(t, e) {
                if (!(t instanceof e))
                    throw new TypeError("Cannot call a class as a function")
            }(this, t),
            c(this, "_id", null),
            c(this, "_status", null),
            c(this, "_timeout", null),
            c(this, "_start", null),
            c(this, "_finish", null),
            c(this, "_sizes", []),
            c(this, "_bidders", [])
        }
        var e, n, r;
        return e = t,
        r = [{
            key: "from",
            value: function(e) {
                var n = new t;
                n.id = e.adUnitPath || e.adUnit,
                n.status = e.status,
                n.timeout = e.timeout,
                n.start = e.start,
                n.finish = e.finish,
                n.sizes = e.adUnitSizes || [];
                var r = [];
                return Object.keys(e.bidders).forEach((function(t) {
                    return r.push(a.from(e.bidders[t]))
                }
                )),
                n.bidders = r,
                n
            }
        }],
        (n = [{
            key: "id",
            get: function() {
                return this._id
            },
            set: function(t) {
                this._id = t
            }
        }, {
            key: "status",
            get: function() {
                return this._status
            },
            set: function(t) {
                this._status = t
            }
        }, {
            key: "timeout",
            get: function() {
                return this._timeout
            },
            set: function(t) {
                this._timeout = t
            }
        }, {
            key: "start",
            get: function() {
                return this._start
            },
            set: function(t) {
                this._start = t
            }
        }, {
            key: "finish",
            get: function() {
                return this._finish
            },
            set: function(t) {
                this._finish = t
            }
        }, {
            key: "bidders",
            get: function() {
                return this._bidders
            },
            set: function(t) {
                this._bidders = t
            }
        }, {
            key: "sizes",
            get: function() {
                return this._sizes
            },
            set: function(t) {
                this._sizes = t
            }
        }]) && s(e.prototype, n),
        r && s(e, r),
        t
    }();
    function l(t, e) {
        for (var n = 0; n < e.length; n++) {
            var r = e[n];
            r.enumerable = r.enumerable || !1,
            r.configurable = !0,
            "value"in r && (r.writable = !0),
            Object.defineProperty(t, r.key, r)
        }
    }
    function h(t, e, n) {
        return e in t ? Object.defineProperty(t, e, {
            value: n,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : t[e] = n,
        t
    }
    var p = function() {
        function t() {
            !function(t, e) {
                if (!(t instanceof e))
                    throw new TypeError("Cannot call a class as a function")
            }(this, t),
            h(this, "_session", null),
            h(this, "_host", null),
            h(this, "_path", null),
            h(this, "_referrer", null),
            h(this, "_device", null),
            h(this, "_gdprc", null),
            h(this, "_tags", []),
            h(this, "_id", null),
            h(this, "_timeout", null),
            h(this, "_start", null),
            h(this, "_finish", null),
            h(this, "_adunits", [])
        }
        var e, n, r;
        return e = t,
        r = [{
            key: "from",
            value: function(e) {
                var n = new t;
                n.id = e.id,
                n.timeout = e.timeout,
                n.start = e.start,
                n.finish = e.finish,
                e.gdpr_consent && (n.gdprc = e.gdpr_consent);
                var r = [];
                return Object.keys(e.adUnits).forEach((function(t) {
                    return r.push(f.from(e.adUnits[t]))
                }
                )),
                n.adunits = r,
                n
            }
        }],
        (n = [{
            key: "session",
            get: function() {
                return this._session
            },
            set: function(t) {
                this._session = t
            }
        }, {
            key: "host",
            get: function() {
                return this._host
            },
            set: function(t) {
                this._host = t
            }
        }, {
            key: "path",
            get: function() {
                return this._path
            },
            set: function(t) {
                this._path = t
            }
        }, {
            key: "referrer",
            get: function() {
                return this._referrer
            },
            set: function(t) {
                this._referrer = t
            }
        }, {
            key: "device",
            get: function() {
                return this._device
            },
            set: function(t) {
                this._device = t
            }
        }, {
            key: "gdprc",
            get: function() {
                return this._gdprc
            },
            set: function(t) {
                this._gdprc = t
            }
        }, {
            key: "tags",
            get: function() {
                return this._tags
            },
            set: function(t) {
                this._tags = t
            }
        }, {
            key: "id",
            get: function() {
                return this._id
            },
            set: function(t) {
                this._id = t
            }
        }, {
            key: "timeout",
            get: function() {
                return this._timeout
            },
            set: function(t) {
                this._timeout = t
            }
        }, {
            key: "start",
            get: function() {
                return this._start
            },
            set: function(t) {
                this._start = t
            }
        }, {
            key: "finish",
            get: function() {
                return this._finish
            },
            set: function(t) {
                this._finish = t
            }
        }, {
            key: "adunits",
            get: function() {
                return this._adunits
            },
            set: function(t) {
                this._adunits = t
            }
        }]) && l(e.prototype, n),
        r && l(e, r),
        t
    }();
    function d(t, e) {
        for (var n = 0; n < e.length; n++) {
            var r = e[n];
            r.enumerable = r.enumerable || !1,
            r.configurable = !0,
            "value"in r && (r.writable = !0),
            Object.defineProperty(t, r.key, r)
        }
    }
    function v(t, e, n) {
        return e in t ? Object.defineProperty(t, e, {
            value: n,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : t[e] = n,
        t
    }
    var g = function() {
        function t() {
            !function(t, e) {
                if (!(t instanceof e))
                    throw new TypeError("Cannot call a class as a function")
            }(this, t),
            v(this, "_session", null),
            v(this, "_host", null),
            v(this, "_path", null),
            v(this, "_referrer", null),
            v(this, "_device", null),
            v(this, "_gdprc", null),
            v(this, "_tags", []),
            v(this, "_ad_unit", null),
            v(this, "_winner", null),
            v(this, "_cpm", null),
            v(this, "_media_type", null),
            v(this, "_size", null),
            v(this, "_auction_start", null),
            v(this, "_auction_finish", null),
            v(this, "_timeout", null),
            v(this, "_auction_id", null),
            v(this, "_status", null),
            v(this, "_bidders", [])
        }
        var e, n, r;
        return e = t,
        r = [{
            key: "from",
            value: function(e) {
                var n = new t;
                n.ad_unit = e.adUnitPath || e.adUnit,
                n.winner = e.bidder,
                n.cpm = e.cpm,
                n.media_type = e.mediaType,
                e.size.width && e.size.height && (n.size = e.size.width + "x" + e.size.height),
                n.auction_id = e.auction.id,
                n.gdprc = e.auction.gdpr_consent,
                n.auction_start = e.auction.start,
                n.auction_finish = e.auction.finish,
                n.status = e.auction.status,
                n.timeout = e.auction.timeout;
                var r = [];
                return Object.keys(e.auction.bidders).forEach((function(t) {
                    return r.push(a.from(e.auction.bidders[t]))
                }
                )),
                n.bidders = r,
                n
            }
        }],
        (n = [{
            key: "session",
            get: function() {
                return this._session
            },
            set: function(t) {
                this._session = t
            }
        }, {
            key: "host",
            get: function() {
                return this._host
            },
            set: function(t) {
                this._host = t
            }
        }, {
            key: "path",
            get: function() {
                return this._path
            },
            set: function(t) {
                this._path = t
            }
        }, {
            key: "referrer",
            get: function() {
                return this._referrer
            },
            set: function(t) {
                this._referrer = t
            }
        }, {
            key: "device",
            get: function() {
                return this._device
            },
            set: function(t) {
                this._device = t
            }
        }, {
            key: "gdprc",
            get: function() {
                return this._gdprc
            },
            set: function(t) {
                this._gdprc = t
            }
        }, {
            key: "tags",
            get: function() {
                return this._tags
            },
            set: function(t) {
                this._tags = t
            }
        }, {
            key: "ad_unit",
            get: function() {
                return this._ad_unit
            },
            set: function(t) {
                this._ad_unit = t
            }
        }, {
            key: "winner",
            get: function() {
                return this._winner
            },
            set: function(t) {
                this._winner = t
            }
        }, {
            key: "cpm",
            get: function() {
                return this._cpm
            },
            set: function(t) {
                this._cpm = t
            }
        }, {
            key: "media_type",
            get: function() {
                return this._media_type
            },
            set: function(t) {
                this._media_type = t
            }
        }, {
            key: "size",
            get: function() {
                return this._size
            },
            set: function(t) {
                this._size = t
            }
        }, {
            key: "auction_start",
            get: function() {
                return this._auction_start
            },
            set: function(t) {
                this._auction_start = t
            }
        }, {
            key: "auction_finish",
            get: function() {
                return this._auction_finish
            },
            set: function(t) {
                this._auction_finish = t
            }
        }, {
            key: "timeout",
            get: function() {
                return this._timeout
            },
            set: function(t) {
                this._timeout = t
            }
        }, {
            key: "auction_id",
            get: function() {
                return this._auction_id
            },
            set: function(t) {
                this._auction_id = t
            }
        }, {
            key: "status",
            get: function() {
                return this._status
            },
            set: function(t) {
                this._status = t
            }
        }, {
            key: "bidders",
            get: function() {
                return this._bidders
            },
            set: function(t) {
                this._bidders = t
            }
        }]) && d(e.prototype, n),
        r && d(e, r),
        t
    }();
    function y(t, e) {
        for (var n = 0; n < e.length; n++) {
            var r = e[n];
            r.enumerable = r.enumerable || !1,
            r.configurable = !0,
            "value"in r && (r.writable = !0),
            Object.defineProperty(t, r.key, r)
        }
    }
    function m(t, e, n) {
        return e in t ? Object.defineProperty(t, e, {
            value: n,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : t[e] = n,
        t
    }
    var b = function() {
        function t() {
            !function(t, e) {
                if (!(t instanceof e))
                    throw new TypeError("Cannot call a class as a function")
            }(this, t),
            m(this, "_session", null),
            m(this, "_host", null),
            m(this, "_path", null),
            m(this, "_referrer", null),
            m(this, "_device", null),
            m(this, "_gdprc", null),
            m(this, "_tags", []),
            m(this, "_ad_unit", null),
            m(this, "_cpm", null),
            m(this, "_bidder", null),
            m(this, "_bidder_start", null),
            m(this, "_bidder_finish", null),
            m(this, "_media_type", null),
            m(this, "_size", null),
            m(this, "_auction_id", null),
            m(this, "_auction_start", null),
            m(this, "_auction_finish", null),
            m(this, "_status", null),
            m(this, "_timeout", null),
            m(this, "_bidders", [])
        }
        var e, n, r;
        return e = t,
        r = [{
            key: "from",
            value: function(e) {
                var n = new t;
                n.ad_unit = e.adUnitPath || e.adUnit,
                n.cpm = e.cpm,
                n.bidder = e.bidder,
                n.bidder_start = e.start,
                n.bidder_finish = e.finish,
                n.media_type = e.mediaType,
                e.size.width && e.size.height && (n.size = e.size.width + "x" + e.size.height),
                n.auction_id = e.auction.id,
                n.gdprc = e.auction.gdpr_consent,
                n.status = e.auction.status,
                n.auction_start = e.auction.start,
                n.auction_finish = e.auction.finish,
                n.timeout = e.auction.timeout;
                var r = [];
                return Object.keys(e.auction.bidders).forEach((function(t) {
                    return r.push(a.from(e.auction.bidders[t]))
                }
                )),
                n.bidders = r,
                n
            }
        }],
        (n = [{
            key: "session",
            get: function() {
                return this._session
            },
            set: function(t) {
                this._session = t
            }
        }, {
            key: "host",
            get: function() {
                return this._host
            },
            set: function(t) {
                this._host = t
            }
        }, {
            key: "path",
            get: function() {
                return this._path
            },
            set: function(t) {
                this._path = t
            }
        }, {
            key: "referrer",
            get: function() {
                return this._referrer
            },
            set: function(t) {
                this._referrer = t
            }
        }, {
            key: "device",
            get: function() {
                return this._device
            },
            set: function(t) {
                this._device = t
            }
        }, {
            key: "gdprc",
            get: function() {
                return this._gdprc
            },
            set: function(t) {
                this._gdprc = t
            }
        }, {
            key: "tags",
            get: function() {
                return this._tags
            },
            set: function(t) {
                this._tags = t
            }
        }, {
            key: "ad_unit",
            get: function() {
                return this._ad_unit
            },
            set: function(t) {
                this._ad_unit = t
            }
        }, {
            key: "cpm",
            get: function() {
                return this._cpm
            },
            set: function(t) {
                this._cpm = t
            }
        }, {
            key: "bidder",
            get: function() {
                return this._bidder
            },
            set: function(t) {
                this._bidder = t
            }
        }, {
            key: "bidder_start",
            get: function() {
                return this._bidder_start
            },
            set: function(t) {
                this._bidder_start = t
            }
        }, {
            key: "bidder_finish",
            get: function() {
                return this._bidder_finish
            },
            set: function(t) {
                this._bidder_finish = t
            }
        }, {
            key: "media_type",
            get: function() {
                return this._media_type
            },
            set: function(t) {
                this._media_type = t
            }
        }, {
            key: "size",
            get: function() {
                return this._size
            },
            set: function(t) {
                this._size = t
            }
        }, {
            key: "auction_id",
            get: function() {
                return this._auction_id
            },
            set: function(t) {
                this._auction_id = t
            }
        }, {
            key: "auction_start",
            get: function() {
                return this._auction_start
            },
            set: function(t) {
                this._auction_start = t
            }
        }, {
            key: "auction_finish",
            get: function() {
                return this._auction_finish
            },
            set: function(t) {
                this._auction_finish = t
            }
        }, {
            key: "status",
            get: function() {
                return this._status
            },
            set: function(t) {
                this._status = t
            }
        }, {
            key: "timeout",
            get: function() {
                return this._timeout
            },
            set: function(t) {
                this._timeout = t
            }
        }, {
            key: "bidders",
            get: function() {
                return this._bidders
            },
            set: function(t) {
                this._bidders = t
            }
        }]) && y(e.prototype, n),
        r && y(e, r),
        t
    }();
    function w(t, e) {
        for (var n = 0; n < e.length; n++) {
            var r = e[n];
            r.enumerable = r.enumerable || !1,
            r.configurable = !0,
            "value"in r && (r.writable = !0),
            Object.defineProperty(t, r.key, r)
        }
    }
    var _ = function() {
        function t() {
            !function(t, e) {
                if (!(t instanceof e))
                    throw new TypeError("Cannot call a class as a function")
            }(this, t)
        }
        var e, n, r;
        return e = t,
        r = [{
            key: "sanitizeTags",
            value: function(t) {
                if (Array.isArray(t) && t.length > 0 && t.length % 2 == 0) {
                    for (var e = {}, n = 0; n < t.length; n += 2)
                        e[t[n].toLowerCase()] = t[n + 1].toLowerCase();
                    var r = {};
                    return Object.keys(e).sort().forEach((function(t) {
                        r[t] = e[t]
                    }
                    )),
                    Object.entries(r).flat()
                }
                return []
            }
        }, {
            key: "emptyArray",
            value: function(t) {
                return !(Array.isArray(t) && t.length)
            }
        }],
        (n = null) && w(e.prototype, n),
        r && w(e, r),
        t
    }()
      , x = "pv"
      , S = "s"
      , k = "pb_a"
      , O = "pb_i"
      , E = "pb_bat"
      , j = "a9_a"
      , M = "a9_i";
    function P(t, e, n, r, i, o, u) {
        try {
            var a = t[o](u)
              , s = a.value
        } catch (t) {
            return void n(t)
        }
        a.done ? e(s) : Promise.resolve(s).then(r, i)
    }
    function R(t) {
        return function() {
            var e = this
              , n = arguments;
            return new Promise((function(r, i) {
                var o = t.apply(e, n);
                function u(t) {
                    P(o, r, i, u, a, "next", t)
                }
                function a(t) {
                    P(o, r, i, u, a, "throw", t)
                }
                u(void 0)
            }
            ))
        }
    }
    function T(t, e) {
        for (var n = 0; n < e.length; n++) {
            var r = e[n];
            r.enumerable = r.enumerable || !1,
            r.configurable = !0,
            "value"in r && (r.writable = !0),
            Object.defineProperty(t, r.key, r)
        }
    }
    var A = function() {
        function t(e) {
            var n, r, i;
            !function(t, e) {
                if (!(t instanceof e))
                    throw new TypeError("Cannot call a class as a function")
            }(this, t),
            i = null,
            (r = "apiClient")in (n = this) ? Object.defineProperty(n, r, {
                value: i,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : n[r] = i,
            this.apiClient = e
        }
        var e, n, o, u, a, s;
        return e = t,
        (n = [{
            key: "handleAuctionEvent",
            value: (s = R(regeneratorRuntime.mark((function t(e) {
                var n, o, u, a, s, c;
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            return t.next = 2,
                            Leya.isSessionOpen();
                        case 2:
                            if (!t.sent) {
                                t.next = 29;
                                break
                            }
                            return t.next = 5,
                            Leya.getSession();
                        case 5:
                            return n = t.sent,
                            t.next = 8,
                            Leya.getUser();
                        case 8:
                            return o = t.sent,
                            t.next = 11,
                            Leya.getTags();
                        case 11:
                            return u = t.sent,
                            r.a.debug("Handling Prebid Auction event on session " + n.id),
                            (a = p.from(e)).session = n.id,
                            a.host = n.host,
                            a.path = n.path,
                            a.referrer = n.referrer,
                            a.device = o.device,
                            a.gdprc = o.gdpr.consented || a.gdprc,
                            a.gdprvl = o.gdpr.vendorListVersion,
                            a.tags = _.sanitizeTags(u),
                            r.a.debug("Flattening auction event: " + JSON.stringify(a)),
                            s = this.flattenAuction(a),
                            r.a.debug("Flattened auction rows: " + JSON.stringify(s)),
                            c = {
                                type: k,
                                data: s
                            },
                            t.abrupt("return", this.apiClient.sendEvent(c));
                        case 29:
                            throw r.a.error("No session"),
                            new i.a;
                        case 31:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function(t) {
                return s.apply(this, arguments)
            }
            )
        }, {
            key: "handleImpressionEvent",
            value: (a = R(regeneratorRuntime.mark((function t(e) {
                var n, o, u, a, s, c;
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            return t.next = 2,
                            Leya.isSessionOpen();
                        case 2:
                            if (!t.sent) {
                                t.next = 29;
                                break
                            }
                            return t.next = 5,
                            Leya.getSession();
                        case 5:
                            return n = t.sent,
                            t.next = 8,
                            Leya.getUser();
                        case 8:
                            return o = t.sent,
                            t.next = 11,
                            Leya.getTags();
                        case 11:
                            return u = t.sent,
                            r.a.debug("Handling Prebid Impression event on session " + n.id),
                            (a = g.from(e)).session = n.id,
                            a.host = n.host,
                            a.path = n.path,
                            a.referrer = n.referrer,
                            a.device = o.device,
                            a.gdprc = o.gdpr.consented || a.gdprc,
                            a.gdprvl = o.gdpr.vendorListVersion,
                            a.tags = _.sanitizeTags(u),
                            r.a.debug("Flattening impression event: " + JSON.stringify(a)),
                            s = this.flattenImpression(a),
                            r.a.debug("Flattened impression rows: " + JSON.stringify(s)),
                            c = {
                                type: O,
                                data: s
                            },
                            t.abrupt("return", this.apiClient.sendEvent(c));
                        case 29:
                            throw r.a.error("No session"),
                            new i.a;
                        case 31:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function(t) {
                return a.apply(this, arguments)
            }
            )
        }, {
            key: "handleBidAfterTimeoutEvent",
            value: (u = R(regeneratorRuntime.mark((function t(e) {
                var n, o, u, a, s, c;
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            return t.next = 2,
                            Leya.isSessionOpen();
                        case 2:
                            if (!t.sent) {
                                t.next = 29;
                                break
                            }
                            return t.next = 5,
                            Leya.getSession();
                        case 5:
                            return n = t.sent,
                            t.next = 8,
                            Leya.getUser();
                        case 8:
                            return o = t.sent,
                            t.next = 11,
                            Leya.getTags();
                        case 11:
                            return u = t.sent,
                            r.a.debug("Handling Prebid BidAfterTimeout event on session " + n.id),
                            (a = b.from(e)).session = n.id,
                            a.host = n.host,
                            a.path = n.path,
                            a.referrer = n.referrer,
                            a.device = o.device,
                            a.gdprc = o.gdpr.consented || a.gdprc,
                            a.gdprvl = o.gdpr.vendorListVersion,
                            a.tags = _.sanitizeTags(u),
                            r.a.debug("Flattening BidAfterTimeout event: " + JSON.stringify(a)),
                            s = this.flattenBidAfterTimeout(a),
                            r.a.debug("Flattened impression rows: " + JSON.stringify(s)),
                            c = {
                                type: E,
                                data: s
                            },
                            t.abrupt("return", this.apiClient.sendEvent(c));
                        case 29:
                            throw r.a.error("No session"),
                            new i.a;
                        case 31:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function(t) {
                return u.apply(this, arguments)
            }
            )
        }, {
            key: "flattenBidAfterTimeout",
            value: function(t) {
                var e = [];
                return t.bidders.forEach((function(n) {
                    e.push({
                        session: t.session,
                        host: t.host,
                        referrer: t.referrer,
                        path: t.path,
                        device: t.device,
                        gdprc: t.gdprc,
                        gdprvl: t.gdprvl,
                        tags: t.tags,
                        ad_unit_id: t.ad_unit,
                        bat_cpm: t.cpm,
                        bat_bidder: t.bidder,
                        bat_start: t.bidder_start,
                        bat_finish: t.bidder_finish,
                        media_type: t.media_type,
                        size: t.size,
                        auction_id: t.auction_id,
                        auction_start: t.auction_start,
                        auction_finish: t.auction_finish,
                        auction_timeout: t.timeout,
                        bidder_id: n.id,
                        bidder_bid_after_timeout: n.bat,
                        bidder_status: n.status,
                        bidder_cpm: n.cpm,
                        bidder_start: n.start,
                        bidder_finish: n.finish
                    })
                }
                )),
                e
            }
        }, {
            key: "flattenImpression",
            value: function(t) {
                var e = [];
                return t.bidders.forEach((function(n) {
                    e.push({
                        session: t.session,
                        host: t.host,
                        referrer: t.referrer,
                        path: t.path,
                        device: t.device,
                        gdprc: t.gdprc,
                        gdprvl: t.gdprvl,
                        tags: t.tags,
                        ad_unit_id: t.ad_unit,
                        winner_cpm: t.cpm,
                        winner_bidder: t.winner,
                        media_type: t.media_type,
                        size: t.size,
                        auction_id: t.auction_id,
                        auction_start: t.auction_start,
                        auction_finish: t.auction_finish,
                        auction_timeout: t.timeout,
                        bidder_id: n.id,
                        bidder_bid_after_timeout: n.bat,
                        bidder_status: n.status,
                        bidder_cpm: n.cpm,
                        bidder_start: n.start,
                        bidder_finish: n.finish
                    })
                }
                )),
                e
            }
        }, {
            key: "flattenAuction",
            value: function(t) {
                var e = [];
                return t.adunits.forEach((function(n) {
                    _.emptyArray(n.sizes) ? n.bidders.forEach((function(r) {
                        e.push({
                            session: t.session,
                            host: t.host,
                            referrer: t.referrer,
                            path: t.path,
                            device: t.device,
                            gdprc: t.gdprc,
                            gdprvl: t.gdprvl,
                            tags: t.tags,
                            auction_id: t.id,
                            auction_timeout: t.timeout,
                            auction_start: t.start,
                            auction_finish: t.finish,
                            ad_unit_id: n.id,
                            ad_unit_status: n.status,
                            ad_unit_timeout: n.timeout,
                            size: "unknown",
                            ad_unit_bid_start: n.start,
                            ad_unit_bid_finish: n.finish,
                            bidder_id: r.id,
                            bidder_bid_after_timeout: r.bat,
                            bidder_status: r.status,
                            bidder_cpm: r.cpm,
                            bidder_start: r.start,
                            bidder_finish: r.finish
                        })
                    }
                    )) : n.sizes.forEach((function(r) {
                        n.bidders.forEach((function(i) {
                            e.push({
                                session: t.session,
                                host: t.host,
                                referrer: t.referrer,
                                path: t.path,
                                device: t.device,
                                gdprc: t.gdprc,
                                gdprvl: t.gdprvl,
                                tags: t.tags,
                                auction_id: t.id,
                                auction_timeout: t.timeout,
                                auction_start: t.start,
                                auction_finish: t.finish,
                                ad_unit_id: n.id,
                                ad_unit_status: n.status,
                                ad_unit_timeout: n.timeout,
                                size: r,
                                ad_unit_bid_start: n.start,
                                ad_unit_bid_finish: n.finish,
                                bidder_id: i.id,
                                bidder_bid_after_timeout: i.bat,
                                bidder_status: i.status,
                                bidder_cpm: i.cpm,
                                bidder_start: i.start,
                                bidder_finish: i.finish
                            })
                        }
                        ))
                    }
                    ))
                }
                )),
                e
            }
        }]) && T(e.prototype, n),
        o && T(e, o),
        t
    }()
      , L = n(70);
    function F(t, e, n, r, i, o, u) {
        try {
            var a = t[o](u)
              , s = a.value
        } catch (t) {
            return void n(t)
        }
        a.done ? e(s) : Promise.resolve(s).then(r, i)
    }
    function I(t) {
        return function() {
            var e = this
              , n = arguments;
            return new Promise((function(r, i) {
                var o = t.apply(e, n);
                function u(t) {
                    F(o, r, i, u, a, "next", t)
                }
                function a(t) {
                    F(o, r, i, u, a, "throw", t)
                }
                u(void 0)
            }
            ))
        }
    }
    function N(t, e) {
        for (var n = 0; n < e.length; n++) {
            var r = e[n];
            r.enumerable = r.enumerable || !1,
            r.configurable = !0,
            "value"in r && (r.writable = !0),
            Object.defineProperty(t, r.key, r)
        }
    }
    var C = function() {
        function t(e) {
            var n, r, i;
            !function(t, e) {
                if (!(t instanceof e))
                    throw new TypeError("Cannot call a class as a function")
            }(this, t),
            i = null,
            (r = "apiClient")in (n = this) ? Object.defineProperty(n, r, {
                value: i,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : n[r] = i,
            this.apiClient = e
        }
        var e, n, o, u, a;
        return e = t,
        (n = [{
            key: "recordPageView",
            value: (a = I(regeneratorRuntime.mark((function t() {
                var e, n, o, u;
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            return t.next = 2,
                            Leya.isSessionOpen();
                        case 2:
                            if (!t.sent) {
                                t.next = 27;
                                break
                            }
                            return t.next = 5,
                            Leya.getSession();
                        case 5:
                            return e = t.sent,
                            t.next = 8,
                            Leya.getUser();
                        case 8:
                            return n = t.sent,
                            t.t0 = e.id,
                            t.t1 = e.host,
                            t.t2 = e.path,
                            t.t3 = e.referrer,
                            t.t4 = (new Date).getTime(),
                            t.t5 = n.device,
                            t.t6 = n.browserLanguage,
                            t.t7 = n.gdpr.consented,
                            t.t8 = n.gdpr.vendorListVersion,
                            t.next = 20,
                            Leya.getTags();
                        case 20:
                            return t.t9 = t.sent,
                            o = {
                                session: t.t0,
                                host: t.t1,
                                path: t.t2,
                                referrer: t.t3,
                                recorded_at: t.t4,
                                device: t.t5,
                                blan: t.t6,
                                gdprc: t.t7,
                                gdprvl: t.t8,
                                tags: t.t9
                            },
                            u = {
                                type: x,
                                data: [o]
                            },
                            r.a.debug("Recording page view event " + JSON.stringify(u)),
                            t.abrupt("return", this.apiClient.sendEvent(u));
                        case 27:
                            throw r.a.error("No session"),
                            new i.a;
                        case 29:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function() {
                return a.apply(this, arguments)
            }
            )
        }, {
            key: "recordSession",
            value: (u = I(regeneratorRuntime.mark((function t() {
                var e, n, o, u;
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            return t.next = 2,
                            Leya.getSession();
                        case 2:
                            return e = t.sent,
                            t.next = 5,
                            Leya.getUser();
                        case 5:
                            if (n = t.sent,
                            null !== e) {
                                t.next = 9;
                                break
                            }
                            throw r.a.error("No session"),
                            new i.a;
                        case 9:
                            if (null !== e.finish) {
                                t.next = 12;
                                break
                            }
                            throw r.a.error("Session is still open"),
                            new L.a;
                        case 12:
                            return t.t0 = e.id,
                            t.t1 = e.host,
                            t.t2 = e.path,
                            t.t3 = e.referrer,
                            t.t4 = e.start,
                            t.t5 = e.finish,
                            t.t6 = n.device,
                            t.t7 = n.browserLanguage,
                            t.t8 = n.gdpr.consented,
                            t.t9 = n.gdpr.vendorListVersion,
                            t.next = 24,
                            Leya.getTags();
                        case 24:
                            return t.t10 = t.sent,
                            o = {
                                session: t.t0,
                                host: t.t1,
                                path: t.t2,
                                referrer: t.t3,
                                session_start: t.t4,
                                session_finish: t.t5,
                                device: t.t6,
                                blan: t.t7,
                                gdprc: t.t8,
                                gdprvl: t.t9,
                                tags: t.t10
                            },
                            u = {
                                type: S,
                                data: [o]
                            },
                            r.a.debug("Recording session closed event " + JSON.stringify(u)),
                            t.abrupt("return", this.apiClient.sendEvent(u));
                        case 29:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function() {
                return u.apply(this, arguments)
            }
            )
        }]) && N(e.prototype, n),
        o && N(e, o),
        t
    }()
      , z = n(55)
      , D = n.n(z);
    function U(t) {
        return (U = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
            return typeof t
        }
        : function(t) {
            return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
        }
        )(t)
    }
    function B(t, e) {
        if (!(t instanceof e))
            throw new TypeError("Cannot call a class as a function")
    }
    function V(t, e) {
        return !e || "object" !== U(e) && "function" != typeof e ? function(t) {
            if (void 0 === t)
                throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return t
        }(t) : e
    }
    function W(t) {
        var e = "function" == typeof Map ? new Map : void 0;
        return (W = function(t) {
            if (null === t || (n = t,
            -1 === Function.toString.call(n).indexOf("[native code]")))
                return t;
            var n;
            if ("function" != typeof t)
                throw new TypeError("Super expression must either be null or a function");
            if (void 0 !== e) {
                if (e.has(t))
                    return e.get(t);
                e.set(t, r)
            }
            function r() {
                return q(t, arguments, J(this).constructor)
            }
            return r.prototype = Object.create(t.prototype, {
                constructor: {
                    value: r,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }),
            $(r, t)
        }
        )(t)
    }
    function G() {
        if ("undefined" == typeof Reflect || !Reflect.construct)
            return !1;
        if (Reflect.construct.sham)
            return !1;
        if ("function" == typeof Proxy)
            return !0;
        try {
            return Date.prototype.toString.call(Reflect.construct(Date, [], (function() {}
            ))),
            !0
        } catch (t) {
            return !1
        }
    }
    function q(t, e, n) {
        return (q = G() ? Reflect.construct : function(t, e, n) {
            var r = [null];
            r.push.apply(r, e);
            var i = new (Function.bind.apply(t, r));
            return n && $(i, n.prototype),
            i
        }
        ).apply(null, arguments)
    }
    function $(t, e) {
        return ($ = Object.setPrototypeOf || function(t, e) {
            return t.__proto__ = e,
            t
        }
        )(t, e)
    }
    function J(t) {
        return (J = Object.setPrototypeOf ? Object.getPrototypeOf : function(t) {
            return t.__proto__ || Object.getPrototypeOf(t)
        }
        )(t)
    }
    var K = function(t) {
        function e() {
            return B(this, e),
            V(this, J(e).apply(this, arguments))
        }
        return function(t, e) {
            if ("function" != typeof e && null !== e)
                throw new TypeError("Super expression must either be null or a function");
            t.prototype = Object.create(e && e.prototype, {
                constructor: {
                    value: t,
                    writable: !0,
                    configurable: !0
                }
            }),
            e && $(t, e)
        }(e, t),
        e
    }(W(Error));
    function H(t) {
        return (H = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
            return typeof t
        }
        : function(t) {
            return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
        }
        )(t)
    }
    function Y(t, e) {
        if (!(t instanceof e))
            throw new TypeError("Cannot call a class as a function")
    }
    function X(t, e) {
        return !e || "object" !== H(e) && "function" != typeof e ? function(t) {
            if (void 0 === t)
                throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return t
        }(t) : e
    }
    function Q(t) {
        var e = "function" == typeof Map ? new Map : void 0;
        return (Q = function(t) {
            if (null === t || (n = t,
            -1 === Function.toString.call(n).indexOf("[native code]")))
                return t;
            var n;
            if ("function" != typeof t)
                throw new TypeError("Super expression must either be null or a function");
            if (void 0 !== e) {
                if (e.has(t))
                    return e.get(t);
                e.set(t, r)
            }
            function r() {
                return tt(t, arguments, nt(this).constructor)
            }
            return r.prototype = Object.create(t.prototype, {
                constructor: {
                    value: r,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }),
            et(r, t)
        }
        )(t)
    }
    function Z() {
        if ("undefined" == typeof Reflect || !Reflect.construct)
            return !1;
        if (Reflect.construct.sham)
            return !1;
        if ("function" == typeof Proxy)
            return !0;
        try {
            return Date.prototype.toString.call(Reflect.construct(Date, [], (function() {}
            ))),
            !0
        } catch (t) {
            return !1
        }
    }
    function tt(t, e, n) {
        return (tt = Z() ? Reflect.construct : function(t, e, n) {
            var r = [null];
            r.push.apply(r, e);
            var i = new (Function.bind.apply(t, r));
            return n && et(i, n.prototype),
            i
        }
        ).apply(null, arguments)
    }
    function et(t, e) {
        return (et = Object.setPrototypeOf || function(t, e) {
            return t.__proto__ = e,
            t
        }
        )(t, e)
    }
    function nt(t) {
        return (nt = Object.setPrototypeOf ? Object.getPrototypeOf : function(t) {
            return t.__proto__ || Object.getPrototypeOf(t)
        }
        )(t)
    }
    var rt = function(t) {
        function e() {
            return Y(this, e),
            X(this, nt(e).apply(this, arguments))
        }
        return function(t, e) {
            if ("function" != typeof e && null !== e)
                throw new TypeError("Super expression must either be null or a function");
            t.prototype = Object.create(e && e.prototype, {
                constructor: {
                    value: t,
                    writable: !0,
                    configurable: !0
                }
            }),
            e && et(t, e)
        }(e, t),
        e
    }(Q(Error));
    function it(t) {
        return (it = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
            return typeof t
        }
        : function(t) {
            return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
        }
        )(t)
    }
    function ot(t, e) {
        if (!(t instanceof e))
            throw new TypeError("Cannot call a class as a function")
    }
    function ut(t, e) {
        return !e || "object" !== it(e) && "function" != typeof e ? function(t) {
            if (void 0 === t)
                throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return t
        }(t) : e
    }
    function at(t) {
        var e = "function" == typeof Map ? new Map : void 0;
        return (at = function(t) {
            if (null === t || (n = t,
            -1 === Function.toString.call(n).indexOf("[native code]")))
                return t;
            var n;
            if ("function" != typeof t)
                throw new TypeError("Super expression must either be null or a function");
            if (void 0 !== e) {
                if (e.has(t))
                    return e.get(t);
                e.set(t, r)
            }
            function r() {
                return ct(t, arguments, lt(this).constructor)
            }
            return r.prototype = Object.create(t.prototype, {
                constructor: {
                    value: r,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }),
            ft(r, t)
        }
        )(t)
    }
    function st() {
        if ("undefined" == typeof Reflect || !Reflect.construct)
            return !1;
        if (Reflect.construct.sham)
            return !1;
        if ("function" == typeof Proxy)
            return !0;
        try {
            return Date.prototype.toString.call(Reflect.construct(Date, [], (function() {}
            ))),
            !0
        } catch (t) {
            return !1
        }
    }
    function ct(t, e, n) {
        return (ct = st() ? Reflect.construct : function(t, e, n) {
            var r = [null];
            r.push.apply(r, e);
            var i = new (Function.bind.apply(t, r));
            return n && ft(i, n.prototype),
            i
        }
        ).apply(null, arguments)
    }
    function ft(t, e) {
        return (ft = Object.setPrototypeOf || function(t, e) {
            return t.__proto__ = e,
            t
        }
        )(t, e)
    }
    function lt(t) {
        return (lt = Object.setPrototypeOf ? Object.getPrototypeOf : function(t) {
            return t.__proto__ || Object.getPrototypeOf(t)
        }
        )(t)
    }
    var ht = function(t) {
        function e() {
            return ot(this, e),
            ut(this, lt(e).apply(this, arguments))
        }
        return function(t, e) {
            if ("function" != typeof e && null !== e)
                throw new TypeError("Super expression must either be null or a function");
            t.prototype = Object.create(e && e.prototype, {
                constructor: {
                    value: t,
                    writable: !0,
                    configurable: !0
                }
            }),
            e && ft(t, e)
        }(e, t),
        e
    }(at(Error));
    function pt(t, e, n, r, i, o, u) {
        try {
            var a = t[o](u)
              , s = a.value
        } catch (t) {
            return void n(t)
        }
        a.done ? e(s) : Promise.resolve(s).then(r, i)
    }
    function dt(t) {
        return function() {
            var e = this
              , n = arguments;
            return new Promise((function(r, i) {
                var o = t.apply(e, n);
                function u(t) {
                    pt(o, r, i, u, a, "next", t)
                }
                function a(t) {
                    pt(o, r, i, u, a, "throw", t)
                }
                u(void 0)
            }
            ))
        }
    }
    function vt(t, e) {
        for (var n = 0; n < e.length; n++) {
            var r = e[n];
            r.enumerable = r.enumerable || !1,
            r.configurable = !0,
            "value"in r && (r.writable = !0),
            Object.defineProperty(t, r.key, r)
        }
    }
    function gt(t, e, n) {
        return e in t ? Object.defineProperty(t, e, {
            value: n,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : t[e] = n,
        t
    }
    var yt = function() {
        function t(e) {
            !function(t, e) {
                if (!(t instanceof e))
                    throw new TypeError("Cannot call a class as a function")
            }(this, t),
            gt(this, "apiClient", null),
            gt(this, "auctions", []),
            gt(this, "lineItemsMap", {}),
            this.apiClient = e,
            setInterval(function() {
                var t = (new Date).getTime();
                this.auctions = this.auctions.filter((function(e) {
                    return t < e.time + 3e5
                }
                )) || []
            }
            .bind(this), 500)
        }
        var e, n, o, u, a, s, c;
        return e = t,
        (n = [{
            key: "init",
            value: (c = dt(regeneratorRuntime.mark((function t() {
                var e = this;
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            return t.abrupt("return", this.apiClient.getA9LineItemsMap().then((function(t) {
                                t && t.data ? (e.lineItemsMap = t.data,
                                r.a.debug("A9 Line Items map: " + JSON.stringify(e.lineItemsMap))) : r.a.warn("Retrieved empty line items map, review configuration.")
                            }
                            )).catch((function(t) {
                                throw r.a.error("API error fetching A9 Line Items Map", t),
                                new ht
                            }
                            )));
                        case 1:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function() {
                return c.apply(this, arguments)
            }
            )
        }, {
            key: "getLineItemsMap",
            value: (s = dt(regeneratorRuntime.mark((function t() {
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            return t.abrupt("return", this.lineItemsMap);
                        case 1:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function() {
                return s.apply(this, arguments)
            }
            )
        }, {
            key: "handleAuctionEvent",
            value: (a = dt(regeneratorRuntime.mark((function t(e) {
                var n, o, u, a;
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            return t.next = 2,
                            Leya.isSessionOpen();
                        case 2:
                            if (!t.sent) {
                                t.next = 21;
                                break
                            }
                            return t.next = 5,
                            Leya.getSession();
                        case 5:
                            return n = t.sent,
                            t.next = 8,
                            Leya.getUser();
                        case 8:
                            return o = t.sent,
                            t.next = 11,
                            Leya.getTags();
                        case 11:
                            return u = t.sent,
                            r.a.debug("Handling A9 Auction event on session " + n.id),
                            r.a.debug(e),
                            this.validateAndEnhanceRawAuction(e),
                            e.response.forEach((function(t) {
                                t.auction_start = e.start,
                                t.auction_finish = e.finish,
                                t.session = n.id,
                                t.host = n.host,
                                t.path = n.path,
                                t.referrer = n.referrer,
                                t.device = o.device,
                                t.gdprc = o.gdpr.consented || 3,
                                t.gdprvl = o.gdpr.vendorListVersion,
                                t.tags = _.sanitizeTags(u)
                            }
                            )),
                            this.auctions.push({
                                data: e.response,
                                time: (new Date).getTime()
                            }),
                            a = {
                                type: j,
                                data: e.response
                            },
                            t.abrupt("return", this.apiClient.sendEvent(a));
                        case 21:
                            throw r.a.error("No session"),
                            new i.a;
                        case 23:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function(t) {
                return a.apply(this, arguments)
            }
            )
        }, {
            key: "handleImpressionEvent",
            value: (u = dt(regeneratorRuntime.mark((function t(e) {
                var n, o, u, a, s, c;
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            return t.next = 2,
                            Leya.isSessionOpen();
                        case 2:
                            if (!t.sent) {
                                t.next = 30;
                                break
                            }
                            return t.next = 5,
                            Leya.getSession();
                        case 5:
                            return n = t.sent,
                            t.next = 8,
                            Leya.getUser();
                        case 8:
                            return o = t.sent,
                            t.next = 11,
                            Leya.getTags();
                        case 11:
                            return u = t.sent,
                            r.a.debug("Handling A9 Impression event on session " + n.id),
                            r.a.debug(e),
                            this.validateAndEnhanceRawImpression(e),
                            a = this.auctions.flatMap((function(t) {
                                return t.data
                            }
                            )).find((function(t) {
                                return t.amzniid === e.amzniid && t.slotName === e.slotName && t.amznp === e.amznp && t.amznbid === e.amznbid
                            }
                            )),
                            (s = {}).session = n.id,
                            s.host = n.host,
                            s.path = n.path,
                            s.referrer = n.referrer,
                            s.device = o.device,
                            s.gdprc = o.gdpr.consented || 3,
                            s.gdprvl = o.gdpr.vendorListVersion,
                            s.tags = _.sanitizeTags(u),
                            a ? (s.amzniid = a.amzniid,
                            s.amznp = a.amznp,
                            s.amznbid = a.amznbid,
                            s.cpm = a.cpm,
                            s.auction_start = a.auction_start,
                            s.auction_finish = a.auction_finish,
                            s.slotID = a.slotID,
                            s.slotName = a.slotName,
                            s.size = a.size,
                            s.amznsz = a.amznsz) : (r.a.warn("auction entry not found for impression: " + JSON.stringify(e)),
                            s.amzniid = e.amzniid,
                            s.amznp = e.amznp,
                            s.amznbid = e.amznbid,
                            s.cpm = e.cpm,
                            s.auction_start = (new Date).getTime(),
                            s.auction_finish = (new Date).getTime(),
                            s.slotID = "unknown",
                            s.slotName = e.slotName,
                            s.size = "unknown",
                            s.amznsz = "unknown"),
                            c = {
                                type: M,
                                data: s
                            },
                            t.abrupt("return", this.apiClient.sendEvent(c));
                        case 30:
                            throw r.a.error("No session"),
                            new i.a;
                        case 32:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function(t) {
                return u.apply(this, arguments)
            }
            )
        }, {
            key: "validateAndEnhanceRawImpression",
            value: function(t) {
                if (!t.amzniid)
                    throw r.a.error("missing 'amzniid'"),
                    new rt;
                if (!t.amznbid)
                    throw r.a.error("missing 'amznbid'"),
                    new rt;
                if (!t.amznp)
                    throw r.a.error("missing 'amznp'"),
                    new rt;
                if (!t.slotName)
                    throw r.a.error("missing 'slotName'"),
                    new rt;
                t.cpm = this.amznbid2cpm(t.amznbid)
            }
        }, {
            key: "validateAndEnhanceRawAuction",
            value: function(t) {
                var e = this;
                if (t.start || (t.start = (new Date).getTime()),
                t.finish || (t.finish = (new Date).getTime()),
                _.emptyArray(t.request))
                    throw r.a.error("A9 auction data missing request object"),
                    new rt;
                if (_.emptyArray(t.response))
                    throw r.a.error("A9 auction data missing response object"),
                    new rt;
                var n = t.response.find((function(t) {
                    return t.amzniid
                }
                ))
                  , i = n ? n.amzniid : D()();
                t.response = t.response.map((function(n) {
                    var r = t.request.find((function(t) {
                        return t.slotID === n.slotID
                    }
                    ));
                    return n.amzniid = n.amzniid || i,
                    n.slotName = r.slotName,
                    n.cpm = e.amznbid2cpm(n.amznbid),
                    n
                }
                )),
                delete t.request
            }
        }, {
            key: "amznbid2cpm",
            value: function(t) {
                if (0 === Object.entries(this.lineItemsMap).length && this.lineItemsMap.constructor === Object)
                    throw r.a.error("A9 Line Items Map is empty, call init() to retrieve it"),
                    new K;
                return this.lineItemsMap[t] || 0
            }
        }]) && vt(e.prototype, n),
        o && vt(e, o),
        t
    }()
      , mt = n(100)
      , bt = n.n(mt);
    function wt(t, e, n, r, i, o, u) {
        try {
            var a = t[o](u)
              , s = a.value
        } catch (t) {
            return void n(t)
        }
        a.done ? e(s) : Promise.resolve(s).then(r, i)
    }
    function _t(t) {
        return function() {
            var e = this
              , n = arguments;
            return new Promise((function(r, i) {
                var o = t.apply(e, n);
                function u(t) {
                    wt(o, r, i, u, a, "next", t)
                }
                function a(t) {
                    wt(o, r, i, u, a, "throw", t)
                }
                u(void 0)
            }
            ))
        }
    }
    function xt(t, e) {
        for (var n = 0; n < e.length; n++) {
            var r = e[n];
            r.enumerable = r.enumerable || !1,
            r.configurable = !0,
            "value"in r && (r.writable = !0),
            Object.defineProperty(t, r.key, r)
        }
    }
    function St(t, e, n) {
        return e in t ? Object.defineProperty(t, e, {
            value: n,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : t[e] = n,
        t
    }
    var kt = function() {
        function t(e, n) {
            !function(t, e) {
                if (!(t instanceof e))
                    throw new TypeError("Cannot call a class as a function")
            }(this, t),
            St(this, "events", []),
            St(this, "flushTimeoutId", null),
            St(this, "flushTimeout", null),
            St(this, "batchSize", null),
            this.flushTimeout = e,
            this.batchSize = n
        }
        var e, n, i, o, u, a, s, c;
        return e = t,
        (n = [{
            key: "scheduleFlush",
            value: (c = _t(regeneratorRuntime.mark((function t() {
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            this.flushTimeoutId = setTimeout(this.flush.bind(this), this.flushTimeout);
                        case 1:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function() {
                return c.apply(this, arguments)
            }
            )
        }, {
            key: "sendEvent",
            value: (s = _t(regeneratorRuntime.mark((function t(e) {
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            if (this.events.push(e),
                            !(this.events.length > this.batchSize)) {
                                t.next = 4;
                                break
                            }
                            return t.next = 4,
                            this.flush();
                        case 4:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function(t) {
                return s.apply(this, arguments)
            }
            )
        }, {
            key: "getA9LineItemsMap",
            value: (a = _t(regeneratorRuntime.mark((function t() {
                var e;
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            return t.next = 2,
                            Leya.getKey();
                        case 2:
                            if (!(e = t.sent)) {
                                t.next = 7;
                                break
                            }
                            return t.abrupt("return", bt.a.get("https://analytics.leya.tech/a9/line-items", {
                                headers: {
                                    "x-api-token": e
                                }
                            }));
                        case 7:
                            r.a.warn("Can't retrieve line item map without an ingestion key, use Leya.setKey() to set yours.");
                        case 8:
                        case "end":
                            return t.stop()
                        }
                }
                ), t)
            }
            ))),
            function() {
                return a.apply(this, arguments)
            }
            )
        }, {
            key: "getEventQueueSize",
            value: (u = _t(regeneratorRuntime.mark((function t() {
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            return t.abrupt("return", this.events.length);
                        case 1:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function() {
                return u.apply(this, arguments)
            }
            )
        }, {
            key: "flush",
            value: (o = _t(regeneratorRuntime.mark((function t() {
                var e, n;
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            return t.next = 2,
                            Leya.getKey();
                        case 2:
                            for (e = t.sent,
                            this.flushTimeoutId && (clearTimeout(this.flushTimeoutId),
                            this.flushTimeoutId = null),
                            t.prev = 4; this.events.length; )
                                n = this.events.splice(0, this.batchSize),
                                r.a.debug("Flushing " + n.length + " events"),
                                n.length && function() {
                                    var t = {};
                                    if (n.forEach((function(e) {
                                        t[e.type] = t[e.type] || [],
                                        t[e.type] = t[e.type].concat(e.data)
                                    }
                                    )),
                                    r.a.debug(JSON.stringify(t)),
                                    e)
                                        if (navigator.sendBeacon) {
                                            var i = new Blob([JSON.stringify(t)],{
                                                type: "text/plain; charset=UTF-8"
                                            });
                                            navigator.sendBeacon("https://analytics.leya.tech/events?xat=" + e, i)
                                        } else
                                            bt.a.post("https://analytics.leya.tech/events", JSON.stringify(t), {
                                                headers: {
                                                    "Content-Type": "application/json",
                                                    "x-api-token": e
                                                }
                                            });
                                    else
                                        r.a.warn("missing ingestion key")
                                }();
                        case 6:
                            return t.prev = 6,
                            t.next = 9,
                            this.scheduleFlush();
                        case 9:
                            return t.finish(6);
                        case 10:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this, [[4, , 6, 10]])
            }
            ))),
            function() {
                return o.apply(this, arguments)
            }
            )
        }]) && xt(e.prototype, n),
        i && xt(e, i),
        t
    }();
    !function(t) {
        var e = new kt(1e3,3)
          , n = new A(e)
          , i = new C(e)
          , o = new yt(e);
        Leya.startSession().then((function() {
            return r.a.info("Session Open")
        }
        )),
        e.scheduleFlush().then((function() {
            return r.a.info("Scheduled flush")
        }
        ));
        t.Leya = Object.assign({}, t.Leya, {
            Events: {
                Prebid: {
                    handleAuctionEvent: function(t) {
                        return n.handleAuctionEvent(t)
                    },
                    handleImpressionEvent: function(t) {
                        return n.handleImpressionEvent(t)
                    },
                    handleBidAfterTimeoutEvent: function(t) {
                        return n.handleBidAfterTimeoutEvent(t)
                    }
                },
                A9: {
                    handleAuctionEvent: function(t) {
                        return o.handleAuctionEvent(t)
                    },
                    handleImpressionEvent: function(t) {
                        return o.handleImpressionEvent(t)
                    },
                    init: function() {
                        return o.init()
                    },
                    getLineItemsMap: function() {
                        return o.getLineItemsMap()
                    }
                },
                recordPageView: function() {
                    return i.recordPageView()
                },
                recordSession: function() {
                    return i.recordSession()
                }
            }
        })
    }(window)
}
, function(t, e, n) {
    "use strict";
    function r(t, e) {
        for (var n = 0; n < e.length; n++) {
            var r = e[n];
            r.enumerable = r.enumerable || !1,
            r.configurable = !0,
            "value"in r && (r.writable = !0),
            Object.defineProperty(t, r.key, r)
        }
    }
    function i(t, e, n) {
        return e in t ? Object.defineProperty(t, e, {
            value: n,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : t[e] = n,
        t
    }
    n.r(e);
    var o = function() {
        function t() {
            !function(t, e) {
                if (!(t instanceof e))
                    throw new TypeError("Cannot call a class as a function")
            }(this, t),
            i(this, "_consented", 3),
            i(this, "_vendorListVersion", void 0)
        }
        var e, n, o;
        return e = t,
        (n = [{
            key: "gdprDetails",
            value: function() {
                var t, e, n, r, i, o, u = 0, a = 1, s = 3;
                if (window.__cmp) {
                    window.__cmp("getVendorConsents", null, (function(e) {
                        t = e
                    }
                    )),
                    window.__cmp("getVendorList", null, (function(t) {
                        e = t
                    }
                    )),
                    window.__cmp("getConsentData", null, (function(t) {
                        n = t
                    }
                    ));
                    var c = t && t.purposeConsents || []
                      , f = t && t.vendorConsents || []
                      , l = Object.keys(c).map((function(t) {
                        return c[t]
                    }
                    ))
                      , h = Object.keys(f).map((function(t) {
                        return f[t]
                    }
                    ));
                    return {
                        getConsentValue: (0 === l.length && 0 === h.length ? r = s : (i = l.every((function(t) {
                            return !1 === t
                        }
                        )),
                        o = h.every((function(t) {
                            return !1 === t
                        }
                        )),
                        r = i || o ? u : a),
                        r),
                        vendorList: e,
                        consentData: n,
                        vendorData: t
                    }
                }
                return {
                    getConsentValue: s
                }
            }
        }, {
            key: "consented",
            get: function() {
                return this._consented
            },
            set: function(t) {
                this._consented = t
            }
        }, {
            key: "vendorListVersion",
            get: function() {
                return this._vendorListVersion
            },
            set: function(t) {
                this._vendorListVersion = t
            }
        }]) && r(e.prototype, n),
        o && r(e, o),
        t
    }();
    function u(t, e, n) {
        return e in t ? Object.defineProperty(t, e, {
            value: n,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : t[e] = n,
        t
    }
    var a = function t(e) {
        !function(t, e) {
            if (!(t instanceof e))
                throw new TypeError("Cannot call a class as a function")
        }(this, t),
        u(this, "id", null),
        u(this, "host", null),
        u(this, "path", null),
        u(this, "referrer", null),
        u(this, "start", null),
        u(this, "finish", null),
        this.host = window.location.hostname,
        this.path = window.location.pathname,
        this.referrer = document.referrer,
        this.start = (new Date).getTime(),
        this.finish = null,
        this.id = e
    };
    function s(t, e, n) {
        return e in t ? Object.defineProperty(t, e, {
            value: n,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : t[e] = n,
        t
    }
    var c = function t(e, n) {
        !function(t, e) {
            if (!(t instanceof e))
                throw new TypeError("Cannot call a class as a function")
        }(this, t),
        s(this, "device", null),
        s(this, "browserLanguage", null),
        s(this, "gdpr", null),
        this.device = e,
        this.browserLanguage = n,
        this.gdpr = new o
    }
      , f = n(1)
      , l = n(55)
      , h = n.n(l)
      , p = n(70)
      , d = n(20);
    function v(t, e, n, r, i, o, u) {
        try {
            var a = t[o](u)
              , s = a.value
        } catch (t) {
            return void n(t)
        }
        a.done ? e(s) : Promise.resolve(s).then(r, i)
    }
    function g(t) {
        return function() {
            var e = this
              , n = arguments;
            return new Promise((function(r, i) {
                var o = t.apply(e, n);
                function u(t) {
                    v(o, r, i, u, a, "next", t)
                }
                function a(t) {
                    v(o, r, i, u, a, "throw", t)
                }
                u(void 0)
            }
            ))
        }
    }
    function y(t, e) {
        for (var n = 0; n < e.length; n++) {
            var r = e[n];
            r.enumerable = r.enumerable || !1,
            r.configurable = !0,
            "value"in r && (r.writable = !0),
            Object.defineProperty(t, r.key, r)
        }
    }
    function m(t, e, n) {
        return e in t ? Object.defineProperty(t, e, {
            value: n,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : t[e] = n,
        t
    }
    var b = function() {
        function t() {
            !function(t, e) {
                if (!(t instanceof e))
                    throw new TypeError("Cannot call a class as a function")
            }(this, t),
            m(this, "session", null),
            m(this, "user", null),
            m(this, "tags", null),
            m(this, "key", null),
            this.user = new c(f.b.getDeviceType(),f.b.getBrowserLanguage()),
            this.tags = []
        }
        var e, n, r, i, o, u, s, l, v, b, w, _, x, S;
        return e = t,
        (n = [{
            key: "setKey",
            value: (S = g(regeneratorRuntime.mark((function t(e) {
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            this.key = e;
                        case 1:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function(t) {
                return S.apply(this, arguments)
            }
            )
        }, {
            key: "getKey",
            value: (x = g(regeneratorRuntime.mark((function t() {
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            return t.abrupt("return", this.key);
                        case 1:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function() {
                return x.apply(this, arguments)
            }
            )
        }, {
            key: "startSession",
            value: (_ = g(regeneratorRuntime.mark((function t() {
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            if (null !== this.session) {
                                t.next = 5;
                                break
                            }
                            this.session = new a(h()()),
                            f.a.debug("Started new session " + this.session.id),
                            t.next = 10;
                            break;
                        case 5:
                            if (null !== this.session.finish) {
                                t.next = 8;
                                break
                            }
                            throw f.a.error("A session is already open, close it before starting a new one"),
                            new p.a;
                        case 8:
                            this.session = new a(h()()),
                            f.a.debug("Started new session " + this.session.id);
                        case 10:
                            return t.abrupt("return", this.getSession());
                        case 11:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function() {
                return _.apply(this, arguments)
            }
            )
        }, {
            key: "finishSession",
            value: (w = g(regeneratorRuntime.mark((function t() {
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            if (null === this.session) {
                                t.next = 10;
                                break
                            }
                            if (null !== this.session.finish) {
                                t.next = 7;
                                break
                            }
                            return f.a.debug("Ending session " + this.session.id),
                            this.session.finish = (new Date).getTime(),
                            t.abrupt("return", Leya.Events.recordSession());
                        case 7:
                            f.a.warn("Session already closed at " + this.session.finish);
                        case 8:
                            t.next = 12;
                            break;
                        case 10:
                            throw f.a.error("No session, start one first"),
                            new d.a;
                        case 12:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function() {
                return w.apply(this, arguments)
            }
            )
        }, {
            key: "setTags",
            value: (b = g(regeneratorRuntime.mark((function t(e) {
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            if (!Array.isArray(e)) {
                                t.next = 9;
                                break
                            }
                            if (e.length % 2 != 0) {
                                t.next = 6;
                                break
                            }
                            e = e.map((function(t) {
                                return t.toLowerCase()
                            }
                            )),
                            this.tags = e,
                            t.next = 7;
                            break;
                        case 6:
                            throw new Error("array argument requires pair size, two elements for each key, value pair: ['key1', 'value1', 'key2', 'value2']");
                        case 7:
                            t.next = 10;
                            break;
                        case 9:
                            throw new Error("array argument is required");
                        case 10:
                            return t.abrupt("return", this.getTags());
                        case 11:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function(t) {
                return b.apply(this, arguments)
            }
            )
        }, {
            key: "getTags",
            value: (v = g(regeneratorRuntime.mark((function t() {
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            return t.abrupt("return", this.tags);
                        case 1:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function() {
                return v.apply(this, arguments)
            }
            )
        }, {
            key: "getSession",
            value: (l = g(regeneratorRuntime.mark((function t() {
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            return t.abrupt("return", Object.assign({}, this.session));
                        case 1:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function() {
                return l.apply(this, arguments)
            }
            )
        }, {
            key: "getUser",
            value: (s = g(regeneratorRuntime.mark((function t() {
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            return t.abrupt("return", Object.assign({}, this.user));
                        case 1:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function() {
                return s.apply(this, arguments)
            }
            )
        }, {
            key: "setUserGdprConsent",
            value: (u = g(regeneratorRuntime.mark((function t(e) {
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            return this.user.gdpr.consented = e,
                            t.abrupt("return", this.getSession());
                        case 2:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function(t) {
                return u.apply(this, arguments)
            }
            )
        }, {
            key: "setGdprVendorListVersion",
            value: (o = g(regeneratorRuntime.mark((function t(e) {
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            return this.user.gdpr.vendorListVersion = e,
                            t.abrupt("return", this.getSession());
                        case 2:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function(t) {
                return o.apply(this, arguments)
            }
            )
        }, {
            key: "isSessionOpen",
            value: (i = g(regeneratorRuntime.mark((function t() {
                return regeneratorRuntime.wrap((function(t) {
                    for (; ; )
                        switch (t.prev = t.next) {
                        case 0:
                            return t.abrupt("return", null !== this.session && null === this.session.finish);
                        case 1:
                        case "end":
                            return t.stop()
                        }
                }
                ), t, this)
            }
            ))),
            function() {
                return i.apply(this, arguments)
            }
            )
        }]) && y(e.prototype, n),
        r && y(e, r),
        t
    }();
    !function(t) {
        var e = new b;
        t.Leya = Object.assign({}, t.Leya, {
            getKey: function(t) {
                return e.getKey(t)
            },
            setKey: function(t) {
                return e.setKey(t)
            },
            startSession: function() {
                return e.startSession()
            },
            finishSession: function() {
                return e.finishSession()
            },
            getSession: function() {
                return e.getSession()
            },
            getTags: function() {
                return e.getTags()
            },
            setTags: function(t) {
                return e.setTags(t)
            },
            getUser: function() {
                return e.getUser()
            },
            setUserGdprConsent: function(t) {
                return e.setUserGdprConsent(t)
            },
            setGdprVendorListVersion: function(t) {
                return e.setGdprVendorListVersion(t)
            },
            isSessionOpen: function() {
                return e.isSessionOpen()
            }
        })
    }(window)
}
]);
var streamampConfig = {
    a9Enabled: true,
    gptSingleRequestEnabled: true,
    apsPubID: '16268e26-dabe-4bf4-a28f-b8f4ee192ed3',
    bidTimeout: 1.2,
    pbjsPriceGranularity: 'high',
    hasRefreshBids: true,
    minRefreshTime: 90,
    maxRefreshTime: 120,
    hasCollapsedEmptyDivs: true,
    publisher_id: 'StreamAMP/Lipsum',
    token: 'iH2XRvTyXlZRkFlKiYhSCPOMgwLrtdejJecXtognjtbC9dBLhQoDeYxWYRvwKUfk',
    currency: {
        enabled: false,
        value: ''
    },
    namespace: 'streamamp',
    globalKeyValues: [],
    keyValues: {},
    adUnits: [{
        code: 'div-gpt-ad-1456148316198-0',
        path: '/15188745/Lipsum-Unit1',
        mediaTypes: {
            banner: {
                sizes: [[728, 90], [320, 50]]
            }
        },
        isSticky: false,
        safeFrame: false,
        outOfPage: false,
        bids: [{
            bidder: 'conversant',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935', '471 - 730', '0 - 470', ],
            params: {
                site_id: '200209'
            }
        }, {
            bidder: 'criteo',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                zoneId: 1382489,
                publisherSubId: 'Lipsum_Unit1_728x90@728x90'
            }
        }, {
            bidder: 'criteo',
            labelAny: ['471 - 730', '0 - 470'],
            params: {
                zoneId: 1382490,
                publisherSubId: 'Lipsum_Unit1_320x50@320x320'
            }
        }, {
            bidder: 'districtmDMX',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                dmxid: 138397,
                memberid: 100615
            }
        }, {
            bidder: 'districtmDMX',
            labelAny: ['471 - 730', '0 - 470'],
            params: {
                dmxid: 138395,
                memberid: 100615
            }
        }, {
            bidder: 'districtm',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                placementId: 10411881
            }
        }, {
            bidder: 'districtm',
            labelAny: ['471 - 730', '0 - 470'],
            params: {
                placementId: 10411873
            }
        }, {
            bidder: 'emx_digital',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                tagid: '40372'
            }
        }, {
            bidder: 'emx_digital',
            labelAny: ['471 - 730', '0 - 470'],
            params: {
                tagid: '67004'
            }
        }, {
            bidder: 'ix',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                siteId: '308109',
                size: [728, 90]
            }
        }, {
            bidder: 'ix',
            labelAny: ['471 - 730', '0 - 470'],
            params: {
                siteId: '308107',
                size: [320, 50]
            }
        }, {
            bidder: 'openx',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                unit: '540775856',
                delDomain: 'streamamp-d.openx.net'
            }
        }, {
            bidder: 'openx',
            labelAny: ['471 - 730', '0 - 470'],
            params: {
                unit: '540775856',
                delDomain: 'streamamp-d.openx.net'
            }
        }, {
            bidder: 'pubmatic',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                publisherId: '127864',
                adSlot: 'Lipsum_Unit1_728x90@728x90',
                uid: '643821'
            }
        }, {
            bidder: 'pubmatic',
            labelAny: ['471 - 730', '0 - 470'],
            params: {
                publisherId: '127864',
                adSlot: 'Lipsum_Unit1_320x50@320x50',
                uid: '643824'
            }
        }, {
            bidder: 'streamamp',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                placementId: 6439954
            }
        }, {
            bidder: 'streamamp',
            labelAny: ['471 - 730', '0 - 470'],
            params: {
                placementId: 6445409
            }
        }, {
            bidder: 'totaljobs',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                placementId: 16303268
            }
        }, {
            bidder: 'totaljobs',
            labelAny: ['471 - 730', '0 - 470'],
            params: {
                placementId: 16303316
            }
        }],
        breakpoints: {
            '1281 - 9999': [[728, 90]],
            '976 - 1280': [[728, 90]],
            '731 - 935': [[728, 90]],
            '471 - 730': [[320, 50]],
            '0 - 470': [[320, 50]]
        }
    }, {
        code: 'div-gpt-ad-1456148316198-1',
        path: '/15188745/Lipsum-unit2',
        mediaTypes: {
            banner: {
                sizes: [[728, 90], [320, 50]]
            }
        },
        isSticky: false,
        safeFrame: false,
        outOfPage: false,
        bids: [{
            bidder: 'conversant',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935', '471 - 730', '0 - 470', ],
            params: {
                site_id: '200209'
            }
        }, {
            bidder: 'criteo',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                zoneId: 1382489,
                publisherSubId: 'Lipsum_Unit2_728x90@728x90'
            }
        }, {
            bidder: 'criteo',
            labelAny: ['0 - 470', '471 - 730'],
            params: {
                zoneId: 1382490,
                publisherSubId: 'Lipsum_Unit2_320x50@320x50'
            }
        }, {
            bidder: 'districtmDMX',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                dmxid: 138401,
                memberid: 100615
            }
        }, {
            bidder: 'districtmDMX',
            labelAny: ['0 - 470', '471 - 730'],
            params: {
                dmxid: 138399,
                memberid: 100615
            }
        }, {
            bidder: 'districtm',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                placementId: 10411897
            }
        }, {
            bidder: 'districtm',
            labelAny: ['0 - 470', '471 - 730'],
            params: {
                placementId: 10411889
            }
        }, {
            bidder: 'emx_digital',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                tagid: '40374'
            }
        }, {
            bidder: 'emx_digital',
            labelAny: ['0 - 470', '471 - 730'],
            params: {
                tagid: '67005'
            }
        }, {
            bidder: 'ix',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                siteId: '308113',
                size: [728, 90]
            }
        }, {
            bidder: 'ix',
            labelAny: ['0 - 470', '471 - 730'],
            params: {
                siteId: '308111',
                size: [320, 50]
            }
        }, {
            bidder: 'openx',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                unit: '540775857',
                delDomain: 'streamamp-d.openx.net'
            }
        }, {
            bidder: 'openx',
            labelAny: ['0 - 470', '471 - 730'],
            params: {
                unit: '540775857',
                delDomain: 'streamamp-d.openx.net'
            }
        }, {
            bidder: 'pubmatic',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                publisherId: '127864',
                adSlot: 'Lipsum_Unit2_728x90@728x90',
                uid: '643825'
            }
        }, {
            bidder: 'pubmatic',
            labelAny: ['0 - 470', '471 - 730'],
            params: {
                publisherId: '127864',
                adSlot: 'Lipsum_Unit2_320x50@320x50',
                uid: '643828'
            }
        }, {
            bidder: 'streamamp',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                placementId: 6445422
            }
        }, {
            bidder: 'streamamp',
            labelAny: ['0 - 470', '471 - 730'],
            params: {
                placementId: 6445424
            }
        }, {
            bidder: 'totaljobs',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                placementId: 16303340
            }
        }, {
            bidder: 'totaljobs',
            labelAny: ['0 - 470', '471 - 730'],
            params: {
                placementId: 16303340
            }
        }],
        breakpoints: {
            '1281 - 9999': [[728, 90]],
            '976 - 1280': [[728, 90]],
            '731 - 935': [[728, 90]],
            '0 - 470': [[320, 50]],
            '471 - 730': [[320, 50]]
        }
    }, {
        code: 'div-gpt-ad-1474537762122-2',
        path: '/15188745/Lipsum-Unit3',
        mediaTypes: {
            banner: {
                sizes: [[160, 600], [120, 600]]
            }
        },
        isSticky: false,
        safeFrame: false,
        outOfPage: false,
        bids: [{
            bidder: 'conversant',
            labelAny: ['1281 - 9999'],
            params: {
                site_id: '200209'
            }
        }, {
            bidder: 'criteo',
            labelAny: ['1281 - 9999'],
            params: {
                zoneId: 1382493,
                publisherSubId: 'Lipsum_Unit3_160x600@160x600'
            }
        }, {
            bidder: 'districtmDMX',
            labelAny: ['1281 - 9999'],
            params: {
                dmxid: 138404,
                memberid: 100615
            }
        }, {
            bidder: 'districtm',
            labelAny: ['1281 - 9999'],
            params: {
                placementId: 10411910
            }
        }, {
            bidder: 'emx_digital',
            labelAny: ['1281 - 9999'],
            params: {
                tagid: '40376'
            }
        }, {
            bidder: 'ix',
            labelAny: ['1281 - 9999'],
            params: {
                siteId: '308116',
                size: [160, 600]
            }
        }, {
            bidder: 'openx',
            labelAny: ['1281 - 9999'],
            params: {
                unit: '540775858',
                delDomain: 'streamamp-d.openx.net'
            }
        }, {
            bidder: 'pubmatic',
            labelAny: ['1281 - 9999'],
            params: {
                publisherId: '127864',
                adSlot: 'Lipsum_Unit3_160x600@160x600',
                uid: '1096698'
            }
        }, {
            bidder: 'streamamp',
            labelAny: ['1281 - 9999'],
            params: {
                placementId: 9719396
            }
        }, {
            bidder: 'totaljobs',
            labelAny: ['1281 - 9999'],
            params: {
                placementId: 16303350
            }
        }],
        breakpoints: {
            '1281 - 9999': [[160, 600], [120, 600]]
        }
    }, {
        code: 'div-gpt-ad-1474537762122-3',
        path: '/15188745/Lipsum-Unit4',
        mediaTypes: {
            banner: {
                sizes: [[160, 600], [120, 600]]
            }
        },
        isSticky: false,
        safeFrame: false,
        outOfPage: false,
        bids: [{
            bidder: 'conversant',
            labelAny: ['1281 - 9999'],
            params: {
                site_id: '200209'
            }
        }, {
            bidder: 'criteo',
            labelAny: ['1281 - 9999'],
            params: {
                zoneId: 1382493,
                publisherSubId: 'Lipsum_Unit4_160x600@160x600'
            }
        }, {
            bidder: 'districtmDMX',
            labelAny: ['1281 - 9999'],
            params: {
                dmxid: 138406,
                memberid: 100615
            }
        }, {
            bidder: 'districtm',
            labelAny: ['1281 - 9999'],
            params: {
                placementId: 10411916
            }
        }, {
            bidder: 'emx_digital',
            labelAny: ['1281 - 9999'],
            params: {
                tagid: '40378'
            }
        }, {
            bidder: 'ix',
            labelAny: ['1281 - 9999'],
            params: {
                siteId: '308118',
                size: [160, 600]
            }
        }, {
            bidder: 'openx',
            labelAny: ['1281 - 9999'],
            params: {
                unit: '540775859',
                delDomain: 'streamamp-d.openx.net'
            }
        }, {
            bidder: 'pubmatic',
            labelAny: ['1281 - 9999'],
            params: {
                publisherId: '127864',
                adSlot: 'Lipsum_Unit4_160x600@160x600',
                uid: '1096701'
            }
        }, {
            bidder: 'streamamp',
            labelAny: ['1281 - 9999'],
            params: {
                placementId: 9719398
            }
        }, {
            bidder: 'totaljobs',
            labelAny: ['1281 - 9999'],
            params: {
                placementId: 16303351
            }
        }],
        breakpoints: {
            '1281 - 9999': [[160, 600], [120, 600]]
        }
    }, {
        code: 'Lipsum-Unit5',
        path: '/15188745/Lipsum-Unit5',
        mediaTypes: {
            banner: {
                sizes: [[728, 90], [320, 50]]
            }
        },
        isSticky: false,
        safeFrame: false,
        outOfPage: false,
        bids: [{
            bidder: 'conversant',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935', '471 - 730', '0 - 470', ],
            params: {
                site_id: '200209'
            }
        }, {
            bidder: 'criteo',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                zoneId: 1382489,
                publisherSubId: 'Lipsum_Unit5_728x90@728x90'
            }
        }, {
            bidder: 'criteo',
            labelAny: ['0 - 470', '471 - 730'],
            params: {
                zoneId: 1382490,
                publisherSubId: 'Lipsum_Unit5_320x50@320x50'
            }
        }, {
            bidder: 'districtmDMX',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                dmxid: 284978,
                memberid: 100615
            }
        }, {
            bidder: 'districtmDMX',
            labelAny: ['0 - 470', '471 - 730'],
            params: {
                dmxid: 284979,
                memberid: 100615
            }
        }, {
            bidder: 'districtm',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                placementId: 14743310
            }
        }, {
            bidder: 'districtm',
            labelAny: ['0 - 470', '471 - 730'],
            params: {
                placementId: 14743311
            }
        }, {
            bidder: 'emx_digital',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                tagid: '58226'
            }
        }, {
            bidder: 'emx_digital',
            labelAny: ['0 - 470', '471 - 730'],
            params: {
                tagid: '58227'
            }
        }, {
            bidder: 'ix',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                siteId: '335325',
                size: [728, 90]
            }
        }, {
            bidder: 'ix',
            labelAny: ['0 - 470', '471 - 730'],
            params: {
                siteId: '335326',
                size: [320, 50]
            }
        }, {
            bidder: 'openx',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                unit: '540775860',
                delDomain: 'streamamp-d.openx.net'
            }
        }, {
            bidder: 'openx',
            labelAny: ['0 - 470', '471 - 730'],
            params: {
                unit: '540775860',
                delDomain: 'streamamp-d.openx.net'
            }
        }, {
            bidder: 'pubmatic',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                publisherId: '127864',
                adSlot: 'Lipsum_Unit5_728x90@728x90',
                uid: '1809656'
            }
        }, {
            bidder: 'pubmatic',
            labelAny: ['0 - 470', '471 - 730'],
            params: {
                publisherId: '127864',
                adSlot: 'Lipsum_Unit5_320x50@320x50',
                uid: '1809657'
            }
        }, {
            bidder: 'streamamp',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                placementId: 14743207
            }
        }, {
            bidder: 'streamamp',
            labelAny: ['0 - 470', '471 - 730'],
            params: {
                placementId: 14743208
            }
        }, {
            bidder: 'totaljobs',
            labelAny: ['1281 - 9999', '976 - 1280', '731 - 935'],
            params: {
                placementId: 16303355
            }
        }, {
            bidder: 'totaljobs',
            labelAny: ['0 - 470', '471 - 730'],
            params: {
                placementId: 16303359
            }
        }],
        breakpoints: {
            '1281 - 9999': [[728, 90]],
            '976 - 1280': [[728, 90]],
            '731 - 935': [[728, 90]],
            '0 - 470': [[320, 50]],
            '471 - 730': [[320, 50]]
        }
    }],
    cmp: {
        isEnabled: true,
        config: {
            'UI Layout': 'popup',
            Language: 'en',
            'Publisher Name': 'Lipsum',
            'Publisher Logo': 'https://static.amp.services/logos/lipsum.png',
            'No Option': true,
            'Non-Consent Display Frequency': 7,
            'Publisher Purpose IDs': [1, 2, 3, 4, 5],
            'Initial Screen Body Text': 'We and our partners use technology such as cookies on our site to personalise content and ads, provide social media features, and analyse our traffic. Click below to consent to the use of this technology across the web. You can change your mind and change your consent choices at anytime by returning to this site.',
            'Initial Screen Accept Button Text': 'I Agree',
            'Initial Screen Reject Button Text': 'I do not accept',
            'Purpose Screen Body Text': 'You can set your consent preferences and determine how you want your data to be used based on the purposes below. You may set your preferences for us independently from those of third-party partners. Each purpose has a description so that you know how we and partners use your data.',
            'Vendor Screen Body Text': 'You can set consent preferences for each individual third-party company below. Expand each company list item to see what purposes they use data for to help make your choices. In some cases, companies may disclose that they use your data without asking for your consent, based on their legitimate interests. You can click on their privacy policies for more information and to opt out.',
            'Vendor Screen Accept All Button Text': 'Accept all',
            'Vendor Screen Reject All Button Text': 'Reject all'
        },
        hasCustomStyles: false,
        styles: {
            ui: {},
            link: {},
            primaryButton: {},
            primaryButtonHover: {},
            isSecondaryButtonHidden: false,
            secondaryButton: {},
            secondaryButtonHover: {},
            tableHeader: {},
            tableRow: {},
            toggleOn: {},
            toggleOff: {}
        }
    },
    breakpoints: [{
        label: '1281 - 9999',
        minWidth: 1281,
        maxWidth: 9999,
        sizesSupported: [[728, 90], [160, 600]]
    }, {
        label: '976 - 1280',
        minWidth: 976,
        maxWidth: 1280,
        sizesSupported: [[728, 90]]
    }, {
        label: '731 - 935',
        minWidth: 731,
        maxWidth: 935,
        sizesSupported: [[728, 90]]
    }, {
        label: '471 - 730',
        minWidth: 471,
        maxWidth: 730,
        sizesSupported: [[320, 50]]
    }, {
        label: '0 - 470',
        minWidth: 0,
        maxWidth: 470,
        sizesSupported: [[320, 50]]
    }]
}
var streamampUtils = {
    isStreamampDebugMode: function() {
        var url = window.location.href;
        var name = 'streamamp_debug';
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');
        var results = regex.exec(url);
        if (!results) {
            return false;
        }
        if (!results[2]) {
            return false;
        }
        return (decodeURIComponent(results[2].replace(/\+/g, ' ')).toUpperCase() === 'TRUE');
    },
    getBrowserWidth: function() {
        var width;
        var topWindow = window.top || window;
        var outerWidth = topWindow.outerWidth || 10000;
        if (topWindow.innerWidth !== undefined) {
            width = topWindow.innerWidth;
        } else if (topWindow.document.documentElement !== undefined && topWindow.document.documentElement.clientWidth !== undefined && topWindow.document.documentElement.clientWidth != 0) {
            width = topWindow.document.documentElement.clientWidth;
        } else {
            width = topWindow.document.body.clientWidth;
        }
        var minWidth = Math.min(width, outerWidth)
        streamampUtils.log('Getting browser width', minWidth);
        return minWidth;
    },
    loadScript: function(url) {
        var scriptEl = document.createElement('script');
        scriptEl.type = 'text/javascript';
        scriptEl.async = true;
        scriptEl.src = url;
        var node = document.getElementsByTagName('script')[0];
        node.parentNode.insertBefore(scriptEl, node);
    },
    normalizeKeyValue: function(keyValue) {
        if (keyValue && keyValue.keyValueType === 'variable') {
            keyValue.value = window[keyValue.value];
            if (keyValue.value === '') {
                keyValue.value = undefined;
            }
        }
        return keyValue;
    },
    styleDebugLog: function(type, arguments) {
        arguments = Array.from(arguments)
        var typeTextColor
        switch (type) {
        case 'pbjs':
            typeTextColor = '#3B88C3;';
            break;
        case 'gpt':
            typeTextColor = '#1E8E3E;';
            break;
        case 'aps':
            typeTextColor = '#FF9900;';
            break;
        default:
            typeTextColor = '';
        }
        arguments.unshift('font-family: sans-serif; font-weight: bold; color: ' + typeTextColor + '; padding: 1px 0;')
        arguments.unshift('font-family: sans-serif; font-weight: bold; color: #FFF; background: #2F0D00; padding: 1px 3px; margin: 2px 0; border-radius: 3px;')
        arguments.unshift('font-family: sans-serif; font-weight: bold; color: #2F0D00; padding: 1px 0; margin: 2px')
        arguments.unshift('%cSTREAM%cAMP' + '%c  ' + type.toUpperCase() + ': ')
        return arguments
    },
    log: function() {
        if (streamampDebugMode) {
            console.log.apply(this, streamampUtils.styleDebugLog('debug', arguments));
        }
    },
    logPbjs: function() {
        if (streamampDebugMode) {
            console.log.apply(this, streamampUtils.styleDebugLog('pbjs', arguments));
        }
    },
    logGpt: function() {
        if (streamampDebugMode) {
            console.log.apply(this, streamampUtils.styleDebugLog('gpt', arguments));
        }
    },
    logAps: function() {
        if (streamampDebugMode) {
            console.log.apply(this, streamampUtils.styleDebugLog('aps', arguments));
        }
    },
    logError: function() {
        if (streamampDebugMode) {
            console.error.apply(this, streamampUtils.styleDebugLog('error', arguments));
        } else {
            console.error.apply(this, arguments);
        }
    },
    stickyAd: function(adUnits) {
        var stickyAdUnits = adUnits.filter(function(adUnit) {
            return adUnit.isSticky === true;
        });
        if (stickyAdUnits.length === 0) {
            return;
        }
        googletag.cmd.push(function() {
            googletag.pubads().addEventListener('slotRenderEnded', function(e) {
                if (!e.isEmpty) {
                    stickyAdUnits.filter(function(adUnit) {
                        return adUnit.code === e.slot.getSlotElementId();
                    }).map(function(adUnit) {
                        streamampUtils.applyStyle(adUnit);
                    });
                }
            });
        });
    },
    applyStyle: function(adUnit) {
        var adUnitCode = adUnit.code;
        var stickyAdPosition = adUnit.stickyAdPosition;
        var adContainer = document.getElementById(adUnitCode);
        if (adContainer) {
            adContainer.style.backgroundColor = 'rgba(237, 237, 237, 0.82)';
            adContainer.style.position = 'fixed';
            adContainer.style.bottom = '0px';
            adContainer.style.padding = '4px 0 0 0';
            adContainer.style.zIndex = '9999';
            adContainer.style.width = '100%';
            adContainer.style.textAlign = 'center';
            if (stickyAdPosition === 'bl') {
                streamampUtils.log('Applying styles for sticky ad unit', {
                    code: adUnitCode,
                    position: 'bottom left'
                });
                adContainer.style.left = '0px';
            } else if (stickyAdPosition === 'br') {
                streamampUtils.log('Applying styles for sticky ad unit', {
                    code: adUnitCode,
                    position: 'bottom right'
                });
                adContainer.style.right = '0px';
            } else {
                streamampUtils.log('Applying styles for sticky ad unit', {
                    code: adUnitCode,
                    position: 'bottom center'
                });
                adContainer.style.transform = 'translate(-50%, 0%)';
                adContainer.style.left = '50%';
            }
            adContainer.style.display = '';
            var closeAdButton = document.createElement('img');
            closeAdButton.id = "close-button";
            closeAdButton.src = "data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjI0cHgiIGhlaWdodD0iMjRweCIgdmlld0JveD0iMCAwIDYxMiA2MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDYxMiA2MTI7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPGc+Cgk8Zz4KCQk8cG9seWdvbiBwb2ludHM9IjQyNC4wMzIsNDQzLjcgNDQzLjcsNDI0LjAzMiAzMjUuNjY3LDMwNiA0NDMuNywxODcuOTY3IDQyNC4wMzIsMTY4LjMgMzA2LDI4Ni4zMzMgMTg3Ljk2NywxNjguMyAxNjguMywxODcuOTY3ICAgICAyODYuMzMzLDMwNiAxNjguMyw0MjQuMDMyIDE4Ny45NjcsNDQzLjcgMzA2LDMyNS42NjcgICAiIGZpbGw9IiMwMDAwMDAiLz4KCQk8cGF0aCBkPSJNNjEyLDMwNkM2MTIsMTM3LjAwNCw0NzQuOTk1LDAsMzA2LDBDMTM3LjAwNCwwLDAsMTM3LjAwNCwwLDMwNmMwLDE2OC45OTUsMTM3LjAwNCwzMDYsMzA2LDMwNiAgICBDNDc0Ljk5NSw2MTIsNjEyLDQ3NC45OTUsNjEyLDMwNnogTTI3LjgxOCwzMDZDMjcuODE4LDE1Mi4zNiwxNTIuMzYsMjcuODE4LDMwNiwyNy44MThTNTg0LjE4MiwxNTIuMzYsNTg0LjE4MiwzMDYgICAgUzQ1OS42NCw1ODQuMTgyLDMwNiw1ODQuMTgyUzI3LjgxOCw0NTkuNjQsMjcuODE4LDMwNnoiIGZpbGw9IiMwMDAwMDAiLz4KCTwvZz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K";
            closeAdButton.style.position = "absolute";
            closeAdButton.style.top = "-12px";
            closeAdButton.style.right = "3px";
            closeAdButton.style.maxWidth = "24px";
            closeAdButton.style.maxHeight = "24px";
            closeAdButton.onclick = function() {
                adContainer.style.display = 'none';
            }
            ;
            adContainer.appendChild(closeAdButton);
            var frame = document.getElementById("google_ads_iframe_/5548363/StreamAMP_1x1_0");
            if (frame && frame.contentWindow.length) {
                document.getElementById("StreamAMP_1x1").style.backgroundColor = "";
                document.getElementById("close-button").style.display = "none";
            }
        }
    },
    polyfills: function() {
        if (!String.prototype.endsWith) {
            String.prototype.endsWith = function(search, this_len) {
                if (this_len === undefined || this_len > this.length) {
                    this_len = this.length;
                }
                return this.substring(this_len - search.length, this_len) === search;
            }
            ;
        }
        if (!Array.prototype.find) {
            Object.defineProperty(Array.prototype, 'find', {
                value: function(predicate) {
                    if (this == null) {
                        throw new TypeError('"this" is null or not defined');
                    }
                    var o = Object(this);
                    var len = o.length >>> 0;
                    if (typeof predicate !== 'function') {
                        throw new TypeError('predicate must be a function');
                    }
                    var thisArg = arguments[1];
                    var k = 0;
                    while (k < len) {
                        var kValue = o[k];
                        if (predicate.call(thisArg, kValue, k, o)) {
                            return kValue;
                        }
                        k++;
                    }
                    return undefined;
                },
                configurable: true,
                writable: true
            });
        }
        if (!Array.prototype.includes) {
            Object.defineProperty(Array.prototype, 'includes', {
                value: function(searchElement, fromIndex) {
                    if (this == null) {
                        throw new TypeError('"this" is null or not defined');
                    }
                    var o = Object(this);
                    var len = o.length >>> 0;
                    if (len === 0) {
                        return false;
                    }
                    var n = fromIndex | 0;
                    var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
                    function sameValueZero(x, y) {
                        return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
                    }
                    while (k < len) {
                        if (sameValueZero(o[k], searchElement)) {
                            return true;
                        }
                        k++;
                    }
                    return false;
                }
            });
        }
        Object.values = Object.values ? Object.values : function(obj) {
            var allowedTypes = ["[object String]", "[object Object]", "[object Array]", "[object Function]"];
            var objType = Object.prototype.toString.call(obj);
            if (obj === null || typeof obj === "undefined") {
                throw new TypeError("Cannot convert undefined or null to object");
            } else if (!~allowedTypes.indexOf(objType)) {
                return [];
            } else {
                if (Object.keys) {
                    return Object.keys(obj).map(function(key) {
                        return obj[key];
                    });
                }
                var result = [];
                for (var prop in obj) {
                    if (obj.hasOwnProperty(prop)) {
                        result.push(obj[prop]);
                    }
                }
                return result;
            }
        }
        ;
    }
};
streamampUtils.polyfills()
var streamampDebugMode = streamampUtils.isStreamampDebugMode();
var dnsUrls = {
    a9: 'https://c.amazon-adsystem.com/aax2/apstag.js',
    prebid: 'https://static.amp.services/prebidLeya1.0.3.js',
    gpt: 'https://securepubads.g.doubleclick.net/tag/js/gpt.js',
    cmp: 'https://quantcast.mgr.consensu.org/'
};

var pbjs = pbjs || {};
pbjs.que = pbjs.que || [];

var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
if (streamampConfig.gptSingleRequestEnabled) {
    googletag.cmd.push(function() {
        googletag.pubads().disableInitialLoad();
    });
}
window.AD_UNITS_TOGGLE_OFF = window.AD_UNITS_TOGGLE_OFF || [];
streamampUtils.log('AD_UNITS_TOGGLE_OFF is', window.AD_UNITS_TOGGLE_OFF)
window.AD_UNITS_TOGGLE_ON = window.AD_UNITS_TOGGLE_ON || [];
streamampUtils.log('AD_UNITS_TOGGLE_ON is ', window.AD_UNITS_TOGGLE_ON)
window.adRefreshTimer = null;
var _streamampVariables = {
    levels: window.location.pathname.split('/').filter(function(level) {
        return level !== '';
    }),
    bidTimeout: streamampConfig.bidTimeout * 1e3 || 2000,
    currentBreakpoint: streamampGetBreakpoint()
}
streamampUtils.log('URL Levels', _streamampVariables.levels)
streamampUtils.log('Setting the bid timeout', _streamampVariables.bidTimeout)
streamampAddDNSPrefetch(Object.values(dnsUrls));
streamampSetup()
function streamampSetup() {
    streamampUtils.log('Running setup()')
    pbjs.que.push(function() {
        if (streamampConfig.afterLoad && typeof streamampConfig.afterLoad === 'function') {
            streamampUtils.log('Running afterLoad event', streamampConfig.afterLoad);
            streamampConfig.afterLoad();
        }
    });
    streamampUtils.loadScript(dnsUrls.gpt);
    streamampUtils.logGpt('Initializing Ad Server, loading GoogleTag library gpt.js');
    streamampUtils.loadScript(dnsUrls.prebid);
    var prebidVersion = dnsUrls.prebid.split('/').filter(function(item) {
        return item.indexOf('prebid') != -1
    }).join('')
    streamampUtils.logPbjs('Loading', prebidVersion);
    if (streamampConfig.a9Enabled) {
        streamampUtils.logAps('APS/A9 enabled, loading apstag library apstag.js')
        !function(a9, a, p, s, t, A, g) {
            if (a[a9])
                return;
            function q(c, r) {
                a[a9]._Q.push([c, r])
            }
            a[a9] = {
                init: function() {
                    q("i", arguments)
                },
                fetchBids: function() {
                    q("f", arguments)
                },
                setDisplayBids: function() {},
                targetingKeys: function() {
                    return []
                },
                _Q: []
            };
            A = p.createElement(s);
            A.async = !0;
            A.src = t;
            g = p.getElementsByTagName(s)[0];
            g.parentNode.insertBefore(A, g)
        }("apstag", window, document, "script", "//c.amazon-adsystem.com/aax2/apstag.js");
        apstag.init({
            pubID: streamampConfig.apsPubID,
            adServer: 'googletag'
        });
    }
    if (streamampConfig.levelTargeting) {
        for (var levelIndex = 1; levelIndex < 6; levelIndex++) {
            window.streamampConfig.globalKeyValues.push({
                name: 'Level' + levelIndex,
                value: _streamampVariables.levels[levelIndex - 1] || 'none',
                keyValueType: 'static'
            });
        }
    }
    ;if (streamampConfig.toggleOffUrls) {
        streamampConfig.toggleOffUrls.forEach(function(url) {
            var level = url.level;
            var path = url.url;
            var levelsKeys = [];
            levelsKeys.push(level);
            var toggleOff = false;
            levelsKeys.forEach(function(levelKey) {
                if (_streamampVariables.levels && _streamampVariables.levels[levelKey - 1] && _streamampVariables.levels[levelKey - 1].toLowerCase() === path.toLowerCase()) {
                    toggleOff = true;
                }
            })
            if (toggleOff) {
                window.streamampConfig.adUnits.forEach(function(adUnit) {
                    adUnit.bids = []
                })
            }
        })
    }
    ;pbjs.que.push(function() {
        streamampUtils.logPbjs('Queuing enableAnalytics()')
        pbjs.enableAnalytics({
            provider: 'leya',
        });
    });
    pbjs.que.push(function() {
        pbjs.aliasBidder('appnexus', 'streamamp')
        pbjs.aliasBidder('appnexus', 'totaljobs')
    });
    pbjs.que.push(function() {
        var currencyValue = streamampConfig.currency.value;
        var currencyFlag = streamampConfig.currency.enabled;
        var currencyFileURL = 'https://static.amp.services/currency/conversion-rates.json';
        streamampUtils.logPbjs('Queuing setConfig() for consent management')
        pbjs.setConfig({
            consentManagement: {
                cmpApi: 'iab',
                timeout: 10000,
                allowAuctionWithoutConsent: true
            }
        });
        streamampUtils.logPbjs('Queuing setConfig() for filter settings')
        pbjs.setConfig({
            userSync: {
                filterSettings: {
                    iframe: {
                        bidders: '*',
                        filter: 'include'
                    }
                }
            }
        });
        streamampUtils.logPbjs('Queuing setConfig() for user ids')
        pbjs.setConfig({
            userSync: {
                userIds: [{
                    name: "pubCommonId",
                    storage: {
                        type: "cookie",
                        name: "_pubCommonId",
                        expires: 365
                    }
                }]
            }
        });
        streamampUtils.logPbjs('Queuing setConfig() for price granularity')
        pbjs.setConfig({
            priceGranularity: generatePriceGranularity(streamampConfig.pbjsPriceGranularity)
        });
        streamampUtils.logPbjs('Queuing setConfig() for bidder timeout', _streamampVariables.bidTimeout)
        pbjs.setConfig({
            bidderTimeout: _streamampVariables.bidTimeout
        });
        streamampUtils.logPbjs('Queuing setConfig() for size config (breakpoints)', streamampConfig.breakpoints.map(function(breakpoint) {
            return breakpoint.label
        }))
        pbjs.setConfig({
            sizeConfig: streamampConfig.breakpoints.map(function(breakpoint) {
                return {
                    'mediaQuery': '(min-width: ' + breakpoint.minWidth + 'px) and (max-width: ' + breakpoint.maxWidth + 'px)',
                    'sizesSupported': breakpoint.sizesSupported,
                    'labels': [breakpoint.label],
                }
            })
        });
        if (currencyFlag && currencyValue.length !== 0) {
            streamampUtils.logPbjs('Queuing setConfig() for currency. Ad server currency is', currencyValue)
            if (currencyValue === 'JPY') {
                pbjs.setConfig({
                    currency: {
                        adServerCurrency: currencyValue,
                        conversionRateFile: currencyFileURL,
                        granularityMultiplier: 100
                    }
                });
            } else {
                pbjs.setConfig({
                    currency: {
                        adServerCurrency: currencyValue,
                        conversionRateFile: currencyFileURL,
                        granularityMultiplier: 1
                    }
                });
            }
        } else {
            streamampUtils.logPbjs('Queuing setConfig() for currency. Ad server currency is USD')
            pbjs.setConfig({
                currency: {
                    adServerCurrency: 'USD',
                    conversionRateFile: currencyFileURL,
                    granularityMultiplier: 1
                }
            });
        }
    });
    if (!streamampConfig.preventInit) {
        streamampInit()
    }
}
function streamampInit() {
    streamampUtils.log('Running init()')
    if (streamampConfig.cmp.isEnabled) {
        if (streamampConfig.cmp.pathnamesToExclude && streamampConfig.cmp.pathnamesToExclude.length > 0 && streamampConfig.cmp.pathnamesToExclude.indexOf(window.location.pathname) != -1) {
            streamampUtils.log('CMP pathnames to exclude are', streamampConfig.cmp.pathnamesToExclude)
            streamampUtils.log('Preventing CMP initialization as', window.location.pathname, 'is in streamampConfig.cmp.pathnamesToExclude')
        } else {
            streamampInitializeCmp()
        }
    }
    ;if (streamampConfig.beforeInit && typeof streamampConfig.beforeInit === 'function') {
        streamampUtils.log('Running beforeInit event', streamampConfig.beforeInit);
        streamampConfig.beforeInit();
    }
    var adUnitsGPT = streamampGetAdUnitsPerBreakpoint();
    var adUnitsAPS
    if (streamampConfig.a9Enabled) {
        adUnitsAPS = streamampCreateAPSAdUnits(adUnitsGPT);
    }
    if (pbjs.adUnits && pbjs.adUnits.length) {
        var oldAdUnitCodes = pbjs.adUnits.map(function(adUnit) {
            return adUnit.code;
        });
        streamampDestroySlots(oldAdUnitCodes);
    }
    googletag.cmd.push(function() {
        var predefinedSlotIds = googletag.pubads().getSlots().map(function(slot) {
            return slot.getSlotElementId();
        });
        adUnitsGPT.forEach(function(adUnit) {
            streamampDefineAdUnitSlot(adUnit, predefinedSlotIds)
        });
        if (streamampConfig.gptSingleRequestEnabled) {
            streamampUtils.logGpt('Enabling single request (SRA)');
            googletag.pubads().enableSingleRequest();
        }
        if (streamampConfig.hasCollapedEmptyDivs) {
            streamampUtils.logGpt('Enabling collapse of empty ad divs');
            googletag.pubads().collapseEmptyDivs(true, true);
        }
        streamampUtils.logGpt('Enabling googletag service');
        googletag.enableServices();
    })
    if (streamampConfig.afterInit && typeof streamampConfig.afterInit === 'function') {
        streamampUtils.log('Running afterInit event', streamampConfig.afterInit);
        streamampConfig.afterInit();
    }
    if (streamampConfig.hasRefreshBids) {
        streamampRefresh(streamampConfig.adUnitsToRefresh)
    }
    streamampUtils.stickyAd(adUnitsGPT);
    auction(adUnitsGPT, adUnitsAPS)
}
function streamampFetchHeaderBids(adUnitsGPT, adUnitsAPS) {
    var bidders = ['prebid'];
    if (streamampConfig.a9Enabled) {
        bidders = ['a9', 'prebid'];
    }
    streamampUtils.log('Fetching header bids for bidders', bidders)
    var requestManager = {
        adserverRequestSent: false,
    };
    bidders.forEach(function(bidder) {
        requestManager[bidder] = false;
    });
    function allBiddersBack() {
        var allBiddersBack = bidders.map(function(bidder) {
            return requestManager[bidder];
        }).filter(Boolean).length === bidders.length;
        streamampUtils.log('Checking if all bidders are back', allBiddersBack)
        return allBiddersBack;
    }
    function headerBidderBack(bidder) {
        if (requestManager.adserverRequestSent === true) {
            return;
        }
        if (bidder === 'a9') {
            streamampUtils.logAps('Handling header bidder back for A9/APS')
            requestManager.a9 = true;
        } else if (bidder === 'prebid') {
            streamampUtils.logPbjs('Handling header bidder back for Prebid')
            requestManager.prebid = true;
        }
        if (allBiddersBack()) {
            sendAdServerRequest();
        }
    }
    function sendAdServerRequest() {
        if (requestManager.adserverRequestSent === true) {
            return;
        }
        requestManager.adserverRequestSent = true;
        pbjs.adserverRequestSent = true;
        requestManager.sendAdServerRequest = true;
        googletag.cmd.push(function() {
            if (streamampConfig.a9Enabled) {
                apstag.setDisplayBids();
                streamampUtils.logAps('Setting display bids')
            }
            streamampAddClientTargeting();
            pbjs.que.push(function() {
                streamampUtils.logPbjs('Queuing setTargetingForGPTAsync()')
                pbjs.setTargetingForGPTAsync();
                if (streamampConfig.gptSingleRequestEnabled) {
                    googletag.pubads().refresh(googletag.pubads().getSlots());
                } else {
                    googletag.pubads().getSlots().forEach(function(slot) {
                        googletag.display(slot.getSlotElementId());
                    });
                }
            });
            streamampUtils.logGpt('Sending ad server request')
        });
    }
    function requestBids(adUnitsGPT, adUnitsAPS, bidTimeout) {

        if (streamampConfig.a9Enabled) {
            streamampUtils.logAps('Fetching bids for', adUnitsAPS)
            var start = new Date().getTime()
            apstag.fetchBids({
                slots: adUnitsAPS,
                timeout: bidTimeout
            }, function(bids) {

                if (Leya) {
                    var a = {
                        start: start,
                        finish: new Date().getTime(),
                        request: adUnitsAPS,
                        response: bids
                    };


                    Leya.Events.A9.getLineItemsMap().then(function(lineItemsMap) {
                        if(Object.keys(lineItemsMap).length === 0 && lineItemsMap.constructor === Object){
                            Leya.Events.A9.init().then(function(){
                                Leya.Events.A9.handleAuctionEvent(a)
                            })
                        } else {
                            Leya.Events.A9.handleAuctionEvent(a)
                        }
                    })


                }

                streamampUtils.logAps('Bids received (all)', bids, '(filtered out)', bids.filter(function(bid) {
                    return bid.amzniid
                }))

                headerBidderBack('a9');
            });
        }
        pbjs.que.push(function() {
            streamampUtils.logPbjs('Queuing addAdUnits() for', adUnitsGPT)
            pbjs.addAdUnits(adUnitsGPT);
            streamampUtils.logPbjs('Queuing requestBids()')
            pbjs.requestBids({
                timeout: bidTimeout,
                bidsBackHandler: function(bidResponses) {
                    headerBidderBack('prebid');
                }
            });
        });
    }
    requestBids(adUnitsGPT, adUnitsAPS, _streamampVariables.bidTimeout);
    window.setTimeout(function() {
        sendAdServerRequest();
    }, _streamampVariables.bidTimeout);
}
function auction(adUnitsGPT, adUnitsAPS) {
    if (window.__cmp && !window.__cmp.streamampOverridden) {
        window.__cmp('getConsentData', null, function(data, success) {
            streamampUtils.log('Getting CMP Consent Data', {
                data: data,
                success: success
            })
            streamampFetchHeaderBids(adUnitsGPT, adUnitsAPS);
        });
    } else {
        streamampFetchHeaderBids(adUnitsGPT, adUnitsAPS);
    }
}
function streamampInitializeCmp() {
    streamampUtils.log('Initializing CMP')
    var elem = document.createElement('script');
    elem.src = 'https://quantcast.mgr.consensu.org/cmp.js';
    elem.async = true;
    elem.type = "text/javascript";
    var scpt = document.getElementsByTagName('script')[0];
    scpt.parentNode.insertBefore(elem, scpt);
    (function() {
        var gdprAppliesGlobally = false;
        function addFrame() {
            if (!window.frames['__cmpLocator']) {
                if (document.body) {
                    var body = document.body
                      , iframe = document.createElement('iframe');
                    iframe.style = 'display:none';
                    iframe.name = '__cmpLocator';
                    body.appendChild(iframe);
                } else {
                    setTimeout(addFrame, 5);
                }
            }
        }
        addFrame();
        function cmpMsgHandler(event) {
            var msgIsString = typeof event.data === "string";
            var json;
            if (msgIsString) {
                json = event.data.indexOf("__cmpCall") != -1 ? JSON.parse(event.data) : {};
            } else {
                json = event.data;
            }
            if (json.__cmpCall) {
                var i = json.__cmpCall;
                window.__cmp(i.command, i.parameter, function(retValue, success) {
                    var returnMsg = {
                        "__cmpReturn": {
                            "returnValue": retValue,
                            "success": success,
                            "callId": i.callId
                        }
                    };
                    event.source.postMessage(msgIsString ? JSON.stringify(returnMsg) : returnMsg, '*');
                });
            }
        }
        window.__cmp = function(c) {
            var b = arguments;
            if (!b.length) {
                return __cmp.a;
            } else if (b[0] === 'ping') {
                b[2]({
                    "gdprAppliesGlobally": gdprAppliesGlobally,
                    "cmpLoaded": false
                }, true);
            } else if (c == '__cmp')
                return false;
            else {
                if (typeof __cmp.a === 'undefined') {
                    __cmp.a = [];
                }
                __cmp.a.push([].slice.apply(b));
            }
        }
        ;
        window.__cmp.gdprAppliesGlobally = gdprAppliesGlobally;
        window.__cmp.msgHandler = cmpMsgHandler;
        if (window.addEventListener) {
            window.addEventListener('message', cmpMsgHandler, false);
        } else {
            window.attachEvent('onmessage', cmpMsgHandler);
        }
    }
    )();
    window.__cmp('init', streamampConfig.cmp.config);
    if (streamampConfig.cmp.hasCustomStyles && isNotEmptyCmp(streamampConfig.cmp.styles)) {
        var style = document.createElement('style');
        var ref = document.querySelector('script');
        var quantcastTheme = streamampConfig.cmp.styles;
        streamampUtils.log('Applying custom CMP styles', quantcastTheme)
        style.innerHTML = (isNotEmptyCmp(quantcastTheme.ui) && quantcastTheme.ui.backgroundColor ? '.qc-cmp-ui' + '{' + 'background-color:' + quantcastTheme.ui.backgroundColor + '!important;' + '}' : '') + (isNotEmptyCmp(quantcastTheme.ui) && quantcastTheme.ui.textColor ? '.qc-cmp-ui,' + '.qc-cmp-ui .qc-cmp-main-messaging,' + '.qc-cmp-ui .qc-cmp-messaging,' + '.qc-cmp-ui .qc-cmp-beta-messaging,' + '.qc-cmp-ui .qc-cmp-title,' + '.qc-cmp-ui .qc-cmp-sub-title,' + '.qc-cmp-ui .qc-cmp-purpose-info,' + '.qc-cmp-ui .qc-cmp-table,' + '.qc-cmp-ui .qc-cmp-vendor-list,' + '.qc-cmp-ui .qc-cmp-vendor-list-title' + '{' + 'color:' + quantcastTheme.ui.textColor + '!important;' + '}' : '') + (isNotEmptyCmp(quantcastTheme.link) ? '.qc-cmp-ui a,' + '.qc-cmp-ui .qc-cmp-alt-action,' + '.qc-cmp-ui .qc-cmp-link' + '{' + (quantcastTheme.link.textColor ? 'color:' + quantcastTheme.link.textColor + '!important;' : '') + (quantcastTheme.link.isUnderlined ? 'text-decoration: underline' : 'text-decoration: none' + '!important;') + '}' : '') + (isNotEmptyCmp(quantcastTheme.primaryButton) ? '.qc-cmp-ui .qc-cmp-button' + '{' + (quantcastTheme.primaryButton.backgroundColor ? 'background-color:' + quantcastTheme.primaryButton.backgroundColor + '!important;' : '') + (quantcastTheme.primaryButton.borderColor ? 'border-color:' + quantcastTheme.primaryButton.borderColor + '!important;' : '') + (quantcastTheme.primaryButton.textColor ? 'color:' + quantcastTheme.primaryButton.textColor + '!important;' : '') + 'background-image: none!important;' + '}' : '') + (isNotEmptyCmp(quantcastTheme.primaryButtonHover) ? '.qc-cmp-ui .qc-cmp-button:hover' + '{' + (quantcastTheme.primaryButtonHover.backgroundColor ? 'background-color:' + quantcastTheme.primaryButtonHover.backgroundColor + '!important;' : '') + (quantcastTheme.primaryButtonHover.borderColor ? 'border-color:' + quantcastTheme.primaryButtonHover.borderColor + '!important;' : '') + (quantcastTheme.primaryButtonHover.textColor ? 'color:' + quantcastTheme.primaryButtonHover.textColor + '!important;' : '') + 'background-image: none!important;' + '}' : '') + (isNotEmptyCmp(quantcastTheme.secondaryButton) ? '.qc-cmp-ui .qc-cmp-button.qc-cmp-secondary-button' + '{' + (quantcastTheme.secondaryButton.backgroundColor ? 'background-color:' + quantcastTheme.secondaryButton.backgroundColor + '!important;' : '') + (quantcastTheme.secondaryButton.borderColor ? 'border-color:' + quantcastTheme.secondaryButton.borderColor + '!important;' : '') + (quantcastTheme.secondaryButton.textColor ? 'color:' + quantcastTheme.secondaryButton.textColor + '!important;' : '') + 'background-image: none!important;' + '}' : '') + (isNotEmptyCmp(quantcastTheme.secondaryButtonHover) ? '.qc-cmp-ui .qc-cmp-button.qc-cmp-secondary-button:hover' + '{' + (quantcastTheme.secondaryButtonHover.backgroundColor ? 'background-color:' + quantcastTheme.secondaryButtonHover.backgroundColor + '!important;' : '') + (quantcastTheme.secondaryButtonHover.borderColor ? 'border-color:' + quantcastTheme.secondaryButtonHover.borderColor + '!important;' : '') + (quantcastTheme.secondaryButtonHover.textColor ? 'color:' + quantcastTheme.secondaryButtonHover.textColor + '!important;' : '') + 'background-image: none!important;' + '}' : '') + (quantcastTheme.isSecondaryButtonHidden ? '.qc-cmp-ui .qc-cmp-button.qc-cmp-secondary-button' + '{' + 'display: none!important;' + '}' + '.qc-cmp-ui .qc-cmp-horizontal-buttons .qc-cmp-button.qc-cmp-secondary-button,' + '.qc-cmp-ui .qc-cmp-nav-bar-buttons-container .qc-cmp-button.qc-cmp-secondary-button' + '{' + 'display: block!important;' + '}' + '@media screen and (max-width: 550px)' + '{' + '.qc-cmp-buttons.qc-cmp-primary-buttons' + '{' + 'height: 3.8rem!important;' + '}' + '}' : '') + (isNotEmptyCmp(quantcastTheme.tableHeader) ? '.qc-cmp-ui .qc-cmp-table-header,' + '.qc-cmp-ui .qc-cmp-vendor-list .qc-cmp-vendor-row-header' + '{' + (quantcastTheme.tableHeader.backgroundColor ? 'background-color:' + quantcastTheme.tableHeader.backgroundColor + '!important;' : '') + (quantcastTheme.tableHeader.textColor ? 'color:' + quantcastTheme.tableHeader.textColor + '!important;' : '') + '}' : '') + (isNotEmptyCmp(quantcastTheme.tableRow) ? '.qc-cmp-ui .qc-cmp-publisher-purposes-table .qc-cmp-table-row,' + '.qc-cmp-ui .qc-cmp-table-row.qc-cmp-vendor-row' + '{' + (quantcastTheme.tableRow.backgroundColor ? 'background-color:' + quantcastTheme.tableRow.backgroundColor + '!important;' : '') + (quantcastTheme.tableRow.textColor ? 'color:' + quantcastTheme.tableRow.textColor + '!important;' : '') + '}' : '') + '.qc-cmp-ui .qc-cmp-purpose-description,' + '.qc-cmp-ui .qc-cmp-company-cell,' + '.qc-cmp-ui .qc-cmp-vendor-info-content,' + '.qc-cmp-ui .qc-cmp-vendor-policy,' + '.qc-cmp-ui .qc-cmp-vendor-info-list' + '{' + 'color: inherit!important;' + '}' + (isNotEmptyCmp(quantcastTheme.toggleOn) ? '.qc-cmp-ui .qc-cmp-toggle.qc-cmp-toggle-on,' + '.qc-cmp-ui .qc-cmp-small-toggle.qc-cmp-toggle-on' + '{' + (quantcastTheme.toggleOn.backgroundColor ? 'background-color:' + quantcastTheme.toggleOn.backgroundColor + '!important;' : '') + (quantcastTheme.toggleOn.borderColor ? 'border-color:' + quantcastTheme.toggleOn.borderColor + '!important;' : '') + '}' : '') + (isNotEmptyCmp(quantcastTheme.toggleOff) ? '.qc-cmp-ui .qc-cmp-toggle.qc-cmp-toggle-off,' + '.qc-cmp-ui .qc-cmp-small-toggle.qc-cmp-toggle-off' + '{' + (quantcastTheme.toggleOff.backgroundColor ? 'background-color:' + quantcastTheme.toggleOff.backgroundColor + '!important;' : '') + (quantcastTheme.toggleOff.borderColor ? 'border-color:' + quantcastTheme.toggleOff.borderColor + '!important;' : '') + '}' : '') + (quantcastTheme.toggleSwitchBorderColor ? '.qc-cmp-ui .qc-cmp-toggle-switch' + '{' + 'border: 1px solid ' + quantcastTheme.toggleSwitchBorderColor + '!important;' + '}' : '') + (quantcastTheme.toggleStatusTextColor ? '.qc-cmp-ui .qc-cmp-toggle-status' + '{' + 'color:' + quantcastTheme.toggleStatusTextColor + '!important;' + '}' : '') + (quantcastTheme.dropdownArrowColor ? '.qc-cmp-ui .qc-cmp-arrow-down' + '{' + 'background:' + 'url("data:image/svg+xml;charset=utf-8,<svg xmlns=\'http://www.w3.org/2000/svg\' viewBox=\'0 0 16 16\' fill=\'none\' stroke=\'%23' + quantcastTheme.dropdownArrowColor.replace('#', '') + '\' stroke-width=\'2\' stroke-linecap=\'round\' stroke-linejoin=\'round\'><path d=\'M2 5l6 6 6-6\'/></svg>") 50% no-repeat' + '!important;' + '}' : '') + (quantcastTheme.additionalStyles ? quantcastTheme.additionalStyles : '') + '}';
        ref.parentNode.insertBefore(style, ref);
    }
}
function isNotEmptyCmp(obj) {
    return obj ? Object.getOwnPropertyNames(obj).length > 0 : false;
}
;function generatePriceGranularity(priceGranularity) {
    streamampUtils.log('Setting price granularity to', priceGranularity)
    if (priceGranularity != 'custom') {
        return priceGranularity;
    }
    return {
        'buckets': [{
            'precision': 2,
            'min': 0,
            'max': 20,
            'increment': 0.01
        }, {
            'precision': 2,
            'min': 20,
            'max': 30,
            'increment': 0.1
        }, {
            'precision': 2,
            'min': 30,
            'max': 40,
            'increment': 0.25
        }, {
            'precision': 2,
            'min': 40,
            'max': 50,
            'increment': 0.5
        }]
    };
}
function streamampConfigAdUnitSlotKeyValue(adUnitCode, googleSlot) {
    if (streamampConfig.keyValues && streamampConfig.keyValues[adUnitCode]) {
        streamampConfig.keyValues[adUnitCode].forEach(function(keyValue) {
            keyValue = streamampUtils.normalizeKeyValue(keyValue);
            if (keyValue.value !== undefined) {
                googleSlot = googleSlot.setTargeting(keyValue.name, [keyValue.value]);
            } else {
                googleSlot = googleSlot.setTargeting(keyValue.name, []);
            }
            streamampUtils.logGpt('Setting custom targeting', keyValue, 'for ad unit', adUnitCode);
        });
    }
    return googleSlot;
}
function streamampConfigSlotSafeFrame(googleSlot, adUnit) {
    streamampUtils.logGpt('Setting force safe frame for ad unit', adUnit)
    return googleSlot.setForceSafeFrame(true)
}
function streamampDefineAdUnitSlot(adUnit, predefinedSlotIds) {
    var googleSlot
    var predefinedSlotId
    if (streamampConfig.predefinedSlotOverride === true) {
        if (streamampConfig.predefinedSlotOverrideMethod === 'metoffice') {
            if (typeof unitFn !== "undefined") {
                var adUnitsToFilter = [];
                for (var key in window['metoffice']['advertising']['requiredSlots']) {
                    if (window['metoffice']['advertising']['requiredSlots'].hasOwnProperty(key)) {
                        adUnitsToFilter.push(key);
                    }
                }
                predefinedSlotId = predefinedSlotIds.find(function(slotId) {
                    return adUnitsToFilter.indexOf(adUnit.code) !== -1 && adUnit.code === slotId;
                });
            }
        }
        if (predefinedSlotId) {
            googleSlot = googletag.pubads().getSlots().find(function(slot) {
                return slot.getSlotElementId() === predefinedSlotId;
            });
            adUnit.code = googleSlot.getSlotElementId();
            adUnit.path = googleSlot.getAdUnitPath();
        } else {
            pbjs.que.push(function() {
                pbjs.removeAdUnit(adUnit.code);
            });
            return;
        }
    } else {
        if (!adUnit.outOfPage) {
            googleSlot = googletag.defineSlot(adUnit.path, adUnit.breakpoints[_streamampVariables.currentBreakpoint.label], adUnit.code)
        } else {
            googleSlot = googletag.defineOutOfPageSlot(adUnit.path, adUnit.code);
        }
    }
    googleSlot = streamampConfigAdUnitSlotKeyValue(adUnit.code, googleSlot);
    if (googleSlot && adUnit.safeFrame) {
        googleSlot = streamampConfigSlotSafeFrame(googleSlot, adUnit);
    }
    if (!predefinedSlotId && googleSlot) {
        googleSlot = googleSlot.addService(googletag.pubads());
    }
    streamampUtils.logGpt('Defining ad unit slot', {
        code: adUnit.code,
        path: adUnit.path,
        sizes: JSON.stringify(adUnit.mediaTypes.banner.sizes)
    });
    return googleSlot;
}
function streamampAddDNSPrefetch(urls) {
    if (urls && urls.length) {
        streamampUtils.log('Pre-fetching links', urls)
        var dnsPrefetchElement;
        var i;
        var node;
        for (i = 0; i < urls.length; i++) {
            dnsPrefetchElement = window.document.createElement('link');
            dnsPrefetchElement.rel = 'preconnect';
            dnsPrefetchElement.href = urls[i];
            node = window.document.getElementsByTagName('script')[0];
            node.parentNode.appendChild(dnsPrefetchElement);
        }
    }
}
function streamampShouldShowAddUnit(adUnitCode) {
    if (window.AD_UNITS_TOGGLE_ON.length) {
        var toggleOn = window.AD_UNITS_TOGGLE_ON.indexOf(adUnitCode) !== -1;
        toggleOn ? streamampUtils.log('Ad unit', adUnitCode, 'is in AD_UNITS_TOGGLE_ON and should be shown') : null;
        return toggleOn;
    } else {
        var toggleOff = window.AD_UNITS_TOGGLE_OFF.indexOf(adUnitCode) === -1;
        toggleOff ? streamampUtils.log('Ad unit', adUnitCode, 'is not in AD_UNITS_TOGGLE_OFF and should be shown') : null;
        return toggleOff;
    }
}
function streamampAddClientTargeting() {
    var key;
    var keyValue;
    var i;
    var clientConfig = window[streamampConfig.namespace + 'ClientConfig'] || {};
    if (clientConfig && clientConfig.targets) {
        for (key in clientConfig.targets) {
            if (clientConfig.targets.hasOwnProperty(key)) {
                keyValue = {
                    name: key,
                    value: clientConfig.targets[key],
                    keyValueType: 'static'
                };
                keyValue = streamampUtils.normalizeKeyValue(keyValue);
                streamampUtils.logGpt('Setting custom targeting.', keyValue);
                googletag.pubads().setTargeting(keyValue.name, [keyValue.value]);
            }
        }
    }
    if (streamampConfig.globalKeyValues && streamampConfig.globalKeyValues.length) {
        for (i = 0; i < streamampConfig.globalKeyValues.length; i++) {
            keyValue = streamampConfig.globalKeyValues[i];
            keyValue = streamampUtils.normalizeKeyValue(keyValue);
            if (keyValue.value !== undefined) {
                googletag.pubads().setTargeting(keyValue.name, [keyValue.value]);
            } else {
                googletag.pubads().setTargeting(keyValue.name, []);
            }
        }
    }
}
function streamampGetBreakpoint() {
    var browserWidth = streamampUtils.getBrowserWidth();
    var i;
    var selectedBreakpoint;
    var breakpoint;
    for (i = 0; i < streamampConfig.breakpoints.length; i++) {
        breakpoint = streamampConfig.breakpoints[i];
        if (browserWidth >= breakpoint.minWidth && browserWidth <= breakpoint.maxWidth) {
            selectedBreakpoint = breakpoint;
            break;
        }
    }
    streamampUtils.log('Getting current breakpoint', selectedBreakpoint);
    return selectedBreakpoint;
}
function streamampGetAdUnitsPerBreakpoint() {
    var selectedBreakpoint = _streamampVariables.currentBreakpoint;
    var i;
    var adUnit;
    var filteredAdUnits = [];
    if (selectedBreakpoint) {
        for (i = 0; i < streamampConfig.adUnits.length; i++) {
            adUnit = streamampConfig.adUnits[i];
            var key
            if (streamampConfig.adUnits[i].breakpoints) {
                key = Object.keys(streamampConfig.adUnits[i].breakpoints)
            } else {
                key = []
            }
            if (adUnit && streamampShouldShowAddUnit(adUnit.code) && key.indexOf(selectedBreakpoint.label) !== -1) {
                adUnit.bids = adUnit.bids.filter(function(bid) {
                    return bid.labelAny.includes(selectedBreakpoint.label)
                })
                filteredAdUnits.push(adUnit);
            }
        }
    }
    streamampUtils.log('Filtering ad units for current breakpoint', filteredAdUnits)
    return filteredAdUnits;
}
function streamampRefreshBids(selectedAdUnits) {
    streamampUtils.log(selectedAdUnits ? ('Refreshing',
    selectedAdUnits) : 'Refreshing all ad units')
    var bidTimeout = _streamampVariables.bidTimeout
    var gptSlots = streamampGetAdUnitsPerBreakpoint();
    var apstagSlots;
    if (streamampConfig.a9Enabled) {
        apstagSlots = streamampCreateAPSAdUnits(gptSlots);
        if (selectedAdUnits) {
            if (Array.isArray(selectedAdUnits)) {
                apstagSlots = apstagSlots.filter(function(apstagSlot) {
                    return selectedAdUnits.indexOf(apstagSlot.slotID) > -1;
                });
            } else if (typeof selectedAdUnits === 'string') {
                apstagSlots = apstagSlots.filter(function(apstagSlot) {
                    return apstagSlot.slotID === selectedAdUnits
                })
            }
        }
        streamampUtils.logAps('Fetching bids for', apstagSlots)
        apstag.fetchBids({
            slots: apstagSlots,
            timeout: bidTimeout
        }, function(bids) {
            streamampUtils.logAps('Bids received (all)', bids, '(filtered out)', bids.filter(function(bid) {
                return bid.amzniid
            }))
        });
    }
    googletag.cmd.push(function() {
        var gptSlots = googletag.pubads().getSlots();
        var adUnitsToRefresh = [];
        var slotIds = [];
        if (selectedAdUnits) {
            var slots = {};
            for (i = 0; i < gptSlots.length; i++) {
                slot = gptSlots[i];
                slots[slot.getSlotElementId()] = slot;
            }
            if (Array.isArray(selectedAdUnits)) {
                for (var i = 0; i < selectedAdUnits.length; i++) {
                    adUnitsToRefresh.push(slots[selectedAdUnits[i]]);
                }
                slotIds = selectedAdUnits
            } else if (typeof selectedAdUnits === 'string') {
                adUnitsToRefresh = [slots[selectedAdUnits]];
                slotIds = [selectedAdUnits];
            }
        } else {
            adUnitsToRefresh = gptSlots
            slotIds = gptSlots.map(function(gptSlot) {
                return gptSlot.getSlotElementId()
            })
        }
        pbjs.que.push(function() {
            streamampUtils.logPbjs('Queuing requestBids()')
            pbjs.requestBids({
                timeout: bidTimeout,
                adUnitCodes: slotIds,
                bidsBackHandler: function() {
                    streamampAddClientTargeting();
                    streamampUtils.logPbjs('Queuing setTargetingForGPTAsync() for', slotIds)
                    pbjs.setTargetingForGPTAsync(slotIds);
                    streamampUtils.logGpt('Sending ad server request for', adUnitsToRefresh)
                    googletag.pubads().refresh(adUnitsToRefresh);
                },
            })
            if (streamampConfig.a9Enabled) {
                streamampUtils.logAps('Setting display bids')
                apstag.setDisplayBids();
            }
        });
    })
}
function streamampRefresh(selectedAdUnits) {
    function generateRefreshTimeout() {
        var min = +streamampConfig.minRefreshTime || 60;
        var max = +streamampConfig.maxRefreshTime || 90;
        return (Math.floor(Math.random() * (max - min)) + min) * 1e3;
    }
    var refreshAds = function() {
        var refreshTimeout = generateRefreshTimeout()
        streamampUtils.log('Setting refresh', {
            selectedAdUnits: (selectedAdUnits ? selectedAdUnits : 'all'),
            refreshTimeout: refreshTimeout / 1e3 + ' seconds'
        });
        if (window.adRefreshTimer) {
            window.clearInterval(window.adRefreshTimer);
        }
        window.adRefreshTimer = setInterval(function() {
            if (streamampConfig.hasRefreshBids) {
                streamampRefreshBids(selectedAdUnits);
            }
        }, refreshTimeout);
    };
    refreshAds();
    window.onfocus = function() {
        refreshAds();
    }
    ;
    window.onblur = function() {
        streamampUtils.log('Refresh paused (interval cleared) due to window.onblur');
        window.clearInterval(window.adRefreshTimer);
        window.adRefreshTimer = null;
    }
    ;
}
;function streamampDestroySlots(adUnitCodes) {
    streamampUtils.log('Destroying ad unit slots', adUnitCodes);
    var adUnitSlots = adUnitCodes.reduce(function(slots, adUnitCode) {
        slots.push(window.gptAdSlots[adUnitCode]);
        return slots;
    }, []);
    pbjs.que.push(function() {
        streamampUtils.logPbjs('Queuing removal of', adUnitCodes, 'from pbjs.adUnits')
        pbjs.adUnits = pbjs.adUnits.filter(function(adUnit) {
            return adUnitCodes.indexOf(adUnit.code) === -1;
        });
    });
    googletag.cmd.push(function() {
        streamampUtils.logGpt('Queuing destroySlots() for', adUnitSlots)
        googletag.destroySlots(adUnitSlots);
        adUnitSlots.forEach(function(adUnitCode) {
            delete window.gptAdSlots[adUnitCode];
        });
    });
}
function streamampCreateAPSAdUnits(adUnitsGPT) {
    var label = _streamampVariables.currentBreakpoint.label
    var googleSizes = [[320, 100], [970, 90], [468, 60], [120, 600], [300, 1050]]
    function filterGoogleSize(adUnits) {
        var googleSizejson = googleSizes.map(function(googleSize) {
            return JSON.stringify(googleSize)
        })
        var filterGoogleSizes = adUnits.filter(function(adUnit) {
            return !googleSizejson.includes(JSON.stringify(adUnit))
        })
        streamampUtils.logAps('Filtering out Google sizes')
        return filterGoogleSizes
    }
    var apstagSlots = adUnitsGPT.map(function(adUnit) {
        return {
            slotID: adUnit.code,
            slotName: adUnit.path,
            sizes: filterGoogleSize(adUnit.breakpoints[label])
        }
    });
    streamampUtils.logAps('Generating apstag slots', apstagSlots)
    return apstagSlots
}
window.streamamp = {
    refreshAllBids: function() {
        streamampUtils.log('window.streamamp.refreshAllBids() was called')
        streamampRefreshBids()
    },
    refreshBids: function(selectedAdUnits) {
        if (selectedAdUnits.length > 0) {
            streamampUtils.log('window.streamamp.refreshBids() was called with', selectedAdUnits, 'ad unit(s)')
            streamampRefreshBids(selectedAdUnits)
        } else {
            streamampUtils.logError('refreshBids() must be passed an array of strings or a string of a single ad unit code')
        }
    },
    destroySlots: function(selectedAdUnits) {
        streamampDestroySlots(selectedAdUnits)
    },
    initialize: function(boolean) {
        if (boolean) {
            streamampUtils.log('window.streamamp.initialize() was called')
            streamampInit()
        }
    }
};

Leya.setKey(streamampConfig.token)
