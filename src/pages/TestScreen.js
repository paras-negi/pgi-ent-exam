import React, {Component} from 'react';
import defaultIcon from '../Icons/defaultIcon.jpg';

import {response} from '../demo/Demo';



class TestScreen extends Component{
    state={
        queData: {},
        ansObj:{},
        obj:{},
        queIndex: 0,
        min: 1,
        sec: 0,
        localQuesOpts: {}
        // checkedRadio: false,
        // isMarkedRev: false


       }

    componentDidMount(){
        this.setState({queData: response});
        // this.timerFunc(0);
    }

    timerFunc=(qid)=>{
        this.myInterval=setInterval(() => {
            if(this.state.sec > 0){
                this.setState({ sec: this.state.sec - 1 })
            } else if(this.state.sec===0){
                if(this.state.min===0){
                    clearInterval(this.myInterval)
                } else {
                    this.setState({ min: this.state.min - 1, sec: 59 })
                }
            }

            if(this.state.min===0 && this.state.sec===0){
                const newObj = {...this.state.obj}
                console.log(newObj)
                const copyInnerObj = {
                    guessedAnswer: 0,
                    markedAns: 0,
                    markedStatus: 0,
                    markedTimes: 0,
                    quesID: 0,
                    timeTaken: 0,
                    userID: 0
                }

                newObj[qid] = copyInnerObj

                this.setState({obj: newObj})
            }
        }, 1000);
    }

    addQuesOptionsToLocal = (e, qid, key, value, options) => {

        // const copyStateData = { ...this.state.obj };
        // if (qid in copyStateData) {
        //     const copyOpts = { ...copyStateData[qid] }
        //     if (key in copyOpts) {
        //         if (options.update) {
        //             copyOpts[key] = value
        //         } else {
        //             delete copyOpts[key]
        //         }
        //     } else {
        //         copyOpts[key] = value
        //     }
        //     copyStateData[qid] = copyOpts;
        // } else {
        //     if (options && options.length) { //generate f,0,0,0 add zero to other options
        //         let emptyAns = {};
        //         for (let index = 0; index < options.length; index++) {
        //             emptyAns[index] = 0
        //         }
        //         let addFirtAns = { ...emptyAns, [key]: value }
        //         copyStateData[qid] = addFirtAns
        //     } else {
        //         copyStateData[qid] = { [key]: value }
        //     }
        // }
        // this.setState({ localQuesOpts: copyStateData })
    }



    optClick=(event,qid,ans)=>{
        const {value, checked, name} = event.target;
        const copyObj={...this.state.ansObj}

        if(qid in copyObj){
          const innerCopy={...copyObj[qid]}
          if(String(ans) in innerCopy ){
            delete innerCopy[String(ans)]
          }else {
            innerCopy[ans]=String(ans)
          }
          copyObj[qid]=innerCopy;

        }else {
            copyObj[qid]={[ans]:String(ans)}
        }



        // const copyInnerObj = {...innerObj}

        // copyObj[qid] = innerObj;

        this.setState({ ansObj: copyObj });

    }

    onSaveAndNext=(queIndex)=>{
      const copyMainObj={...this.state.obj};
      const qid=this.state.queData.data[queIndex].qid;

      const convertKeyToString = obj => {
        let str = "";
        if (!Object.keys(obj).length) return str;

        Object.values(obj).forEach(v => {
          str = str + v + ",";
        });
        str = str.slice(0, -1);
        return str;
      };


      const innerObj = {
          guessedAnswer: 0,
          markedAns: convertKeyToString(this.state.ansObj[qid]),
          markedStatus:1,
          markedTimes: 0,
          quesID: qid,
          timeTaken: 8,
          userID: 244747
      }

      copyMainObj[qid]=innerObj
      this.setState({obj:copyMainObj});


    }


    nextQue=(e,queIndex)=>{
      this.onSaveAndNext(queIndex)
        const length = this.state.queData.data.length-1
        if (length === queIndex){
            this.setState({queIndex: 0})
        } else {
            this.setState({ queIndex: this.state.queIndex + 1 })
        }
    }

    // marked4Rev = (e,queIndex) =>{
    //     this.setState({ isMarkedRev: true})
    //     const length = this.state.queData.data.length - 1
    //     if(length === queIndex){
    //         this.setState({ queIndex: 0 })
    //     } else {
    //         this.setState({ queIndex: this.state.queIndex + 1,  })
    //     }

    // }


    render() {

        const {queData} = this.state
        console.log(this.state);

        const { min, sec} = this.state
        // const checked = this.state.obj.qid[markedAns]
        // console.log(ansObj)




        return(
            <section className="test-page">
                <div className= "test-page-layout">
                    <div className= "test-page-header">
                        <div className="header__element">
                            <div className="header__element header__element--left">
                                Static Mock Link for {queData.topicName}
                            </div>
                            <div className="header__element header__element--right">
                                <div className="right-content">
                                    <img alt="img" source="#" className="instruction-icon"/>
                                    <a href="#" className="instruction-link">Instructions</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="test__page-mr">
                        <div className="test__page-mr-cr">
                            <div className="test__page-mr-cr-top">
                                <div className="image-container">
                                    <img src={defaultIcon} alt="i" style={{height: "96px", width: "90px", border: "1px solid #c3c3c1"}}/>
                                </div>
                                <div className="candidate-details">
                                    <div className="candidate_name">
                                        <center>John Smith</center>
                                    </div>
                                </div>
                            </div>

                            <div className="test__page-mr-cr-main">
                                <div className="notation__area">
                                    <div className="notation__area-outer">
                                        <table className="table-1">
                                        <tbody>
                                            <tr>
                                                <td className="td-1">
                                                    <div>
                                                        <span className="answered answered-count-1">0</span>
                                                        <span className="notation-text"> Answered </span>
                                                    </div>
                                                </td>
                                                <td className="td-1">
                                                    <div>
                                                        <span className="answered answered-count-2">1</span>
                                                        <span className="notation-text"> Not Answered </span>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td className="td-1">
                                                    <div>
                                                        <span className="answered answered-count-3">19</span>
                                                        <span className="notation-text"> Not visited </span>
                                                    </div>
                                                </td>
                                                <td className="td-1">
                                                    <div>
                                                        <span className="answered answered-count-4">0</span>
                                                        <span className="notation-text"> Marked for review </span>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>

                                        </table>
                                        <table className="table-2">
                                            <tbody>
                                                <tr>
                                                    <td className="td-2">
                                                        <div>
                                                            <span className="answered answered-count-5">0</span>
                                                            <span className="notation-text"> Answered and Marked for Review (will be considered for evaluation)</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                                <div className="mock__link-container">
                                    <div className="mock__link-container-content">
                                        Static Mock Link for N...
                                    </div>
                                </div>
                                <div className="choose-question">Choose a Question</div>

                                <div className="questions-buttons">
                                {
                                    queData.data?
                                        queData.data.map((d,i)=>{
                                            return(
                                                <div key={i}  className="mark">
                                                <p className= "mark-skip">{i+1}</p>
                                                </div>
                                            )
                                        }): null
                                    // mark-green mark-voilet mark-voilet-a mark-blank
                                    // this.state.isMarkedRev? "mark-green" : "mark-skip"
                                }

                                </div>
                            </div>

                            <div className="test__page-mr-cr-btn-section">
                                <div className="btn-section">
                                    <div className="sbmt-btn-container">
                                        <button className="btn btn__primary btn__primary--finalSbmt"
                                        title="You aren't required to click on 'Submit' Button; The system will auto Submit at the end"
                                        disabled="disabled">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </div>


                        </div>

                        <div className="test__page-mr-cl">
                            <div className="test__page-mr-cl-s1">
                                <div className="group-icons">
                                        <div className="left_arrow"> </div>
                                        <div className="blue-box" title="Static Mock Link for Mains PG"></div>

                                        <div className="right_arrow"> </div>
                                </div>
                            </div>

                        <div className="test__page-mr-cl-stimer">

                                <div className="section"> Section </div>
                                <div className="timer"> Time Left: {min}:{sec} </div>

                            </div>

                            <div className="test__page-mr-cl-instr-link">
                                <div className="instr-link-section">
                                    <div className="left_arrow"></div>

                                    <div className="blue-instr-box">
                                        <div className="blue-instr-box-content" title="Static Mock Link For Neet PG Entrance Exam Instructions">
                                        Static Mock Link for... <span style={{marginLeft:"2px"}}> <img src="#" alt="i" style={{height:"1.8rem", width:"1.8rem"}}/></span>
                                        </div>
                                    </div>

                                    <div className="right_arrow"></div>
                                </div>

                            </div>

                            <div className="test__page-mr-cl-qtype">
                                <div className="question-type"> Question Type: MCQ</div>
                            </div>

                            <div className="test__page-mr-cl-main">
                            {
                                  queData.data ?
                                    response.data.map((d,i)=>{
                                        return(
                                            <div key={i} id={i} className="a" style={{ display: i === this.state.queIndex ?  "block" : "none"}}>
                                    <div className="test__page-mr-cl-main-qno">
                                        <div className="question-no"> Question No. {d.qid} </div>
                                        <img className="arrow-down" alt="i" src=""/>
                                    </div>

                                    <div className="test__page-mr-cl-main-qAnsArea">
                                        <div className="test__page-mr-cl-main-qAnsArea-1">
                                            <div className="question" dangerouslySetInnerHTML={{__html:`${d.qtext}`}}/>
                                        </div>

                                        <div className="test__page-mr-cl-main-qAnsArea-2">
                                            <div className="options">
                                            {

                                                d.options && d.options.length
                                                ? d.options.map((opt,ind)=>(

                                                    <div style={{marginBottom: "3rem"}} key={ind}>
                                                        <input type="radio" className="answer_options"

                                                                // checked={this.state.checkedRadio}
                                                            onChange={(e) => this.optClick(e,d.qid,ind+1,ind+1,{update:0})}
                                                                id={d.qid+"option"+ind}
                                                                name={"answer"+d.qid}/>
                                                        <label htmlFor={d.qid+"option"+ind} className="answer_label">
                                                        <span style={{marginLeft: "2rem"}} dangerouslySetInnerHTML={{__html:`${opt}`}}>
                                                        </span> </label> <br/>
                                                    </div>

                                                ))
                                                : null
                                            }

                                            </div>
                                        </div>
                                    </div>
                                </div> ) })  :null }
                            </div>
                            <div className="test__page-mr-cl-blank">

                            </div>

                            <div className="test__page-mr-cl-btn-section">
                                <div className="btn-section">
                                    <div>
                                        <button className="btn btn__primary btn__primary--mr" onClick={(e) => this.marked4Rev(e, this.state.queIndex)}>
                                            Mark for Review & Next
                                        </button>

                                        <button className="btn btn__primary btn__primary--clr" onClick={(e)=>this.clearRadio(e)}>
                                            Clear Response
                                        </button>
                                        <button className="btn btn__primary btn__primary--snext" onClick={(e)=>this.nextQue(e,this.state.queIndex)}>
                                            Save & next
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="footer-main">
                        Version: 2.7.0.0
                    </div>
                </div>
            </section>
        )
    }

}

export default TestScreen;
