import React from 'react';
import forwardIcon from '../Icons/forwardIcon.png';
import defaultIcon from '../Icons/defaultIcon.jpg'

function Instructions({ onRouteChange }){
  
    return(
          <section className="instr-page">
            <div className="common-page-layout">
              <div className="instr-page-header">
                &nbsp;
              </div>
              <div className="main-row">
                <div className="col-left">
                    <div className="instructions-heading">
                      <h1>Instructions</h1>
                    </div>
                    <div className= "instruction-row instruction-row-1">
                      <div className="instruction-paragraph">
                        <center> <font size = "4">Please read the instructions carefully</font></center>
                          <strong> <u> General Instructions: </u> </strong>
                            <ol>
                              <li> Total duration of mock sample questions is 20 minutes. </li>
                              <li> The clock will be set at the server. The countdown timer in the top right corner of screen will display
                              the remaining time available for you to complete the examination. When the timer reaches zero, the examination will end by itself.
                              You will not be required to end or submit your examination.
                              </li>
                              <li>The Question Palette displayed on the right side of screen will show the status of each question using one of the following symbols:<br/>
                                <table className="instruction_area" style={{paddingTop:"1.5rem"}}>
                                  <tbody>  
                                    <tr>  
                                      <td> <span className="not_visited" title="Not Visited">1</span> </td>  
                                      <td>This question has not been visited yet.</td>  
                                    </tr>  
                                    <tr>  
                                      <td> <span className="not_answered" title="Not Answered">2</span> </td>  
                                      <td>This question has been visited,but not answered.</td>  
                                    </tr>  
                                    <tr>  
                                      <td> <span className="answered" title="Answered">3</span> </td>  
                                      <td>This question has been answered and will be considered for evaluation.</td>  
                                    </tr>  
                                    <tr>  
                                      <td> <span className="review" title="Marked for Review">4</span></td>  
                                      <td>This question has been marked for review and has not been answered.</td>  
                                    </tr>  
                                    <tr>  
                                      <td><span className="review_marked_considered" title="Answered &amp; Marked for Review">5</span></td>  
                                      <td>This question has been answered and marked for review, which will be considered for evaluation.</td>  
                                    </tr>  
                                  </tbody>  
                                </table>
                              </li>
                              <li style={{paddingTop: "1.5rem"}}>
                              The Marked for Review status for a question simply indicates that you would like to look at that question again.  
                              <font color="red"> If an answer is selected for a question that is Marked for Review, the answer will be considered in the final evaluation.</font>
                              </li>
                            </ol>

                            <strong> <u> Navigating to a Question:</u> </strong>
                            <ol start="5">
                              <li>
                              To answer a question, do the following:
                              </li>
                                <ol type="i">
                                  <li>
                                  Click on the question number in the Question Palette at the right of your screen to go to that numbered question directly. 
                                  Note that using this option does NOT save your answer to the current question.
                                  </li>
                                  <li>
                                  Click on <b>Save & Next</b> to save your answer for the current question and then go to the next question.
                                  </li>
                                  <li>
                                  Click on <b>Mark for Review & Next</b> to mark your question for review, and then go to the next question.
                                  </li>
                                </ol>

                                </ol>

                            <strong> <u> Answering a Question : </u> </strong>
                            <ol start="6">
                            <li>
                                Procedure for answering a multiple choice type question:
                              </li>
                                <ol type="i">
                                  <li>
                                    To select your answer, click on the button of one of the options
                                  </li>
                                  <li>
                                  To deselect your chosen answer, click on the button of the chosen option again or 
                                  click on the Clear Response button
                                  </li>
                                  <li>
                                  To change your chosen answer, click on the button of another option
                                  </li>
                                  <li>
                                  To save your answer, you MUST click on the <b>Save & Next</b> button
                                  </li>
                                  <li>
                                  To mark the question for review, click on the <b>Mark for Review & Next</b> button. 
                                  <i>
                                  <font color="red"> If an answer is selected for a question that is Marked for Review, the answer will be considered in the final evaluation.</font>
                                  </i>
                                  </li>
                                </ol>
                              <li>
                                To change your answer to a question that has already been answered, first select that question for answering and then follow the procedure for answering of question as indicated above.
                              </li>
                              <li>
                              Note that ONLY Questions for which answers are saved after answering will be considered for evaluation. This will also include questions that have been answered and marked for review.
                              </li>
                            </ol>
                          
                        </div>
                    </div>
                  <div className="btn-section">
                      <button onClick={()=>onRouteChange('instructions2')} className="btn btn__primary btn__primary--next"> 
                      <span>
                        <strong>
                         <span>Next</span>
                         </strong>
                        <img alt="forward-icon" className="forward-icon" src={forwardIcon}/></span>
                      </button>
                  </div>
                </div>
                <div className="col-right">
                  <div className= "section-rt-main">
                    <div>
                      <center>
                        <img  className="candidate-photo" alt="i" src={defaultIcon}/>
                      </center>
                      </div>
                      <div className="candidate-name"> John Smith </div>
                  </div>
                </div>
              </div>
              <div className="footer-main"> Version : 2.4.0.0 </div>
              </div>
          </section>
    )
}

export default Instructions;