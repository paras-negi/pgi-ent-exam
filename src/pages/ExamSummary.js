import React from 'react';


function ExamSummary(){

    return(
        <section className="summary-section">
            <div className="summary-page-layout">
                <div className="summary__page-container">
                    <div className="summary__page-header">
                        <center>Exam Summary</center>
                    </div>

                    <div className="summary__page-table-container">
                        <table className="table-properties">
                            <thead style={{background: "#cee3f8", color: "#4c6989"}}>
                                <tr>
                                    <th>Section Name</th>
                                    <th>No. of Questions</th>
                                    <th>Answered</th>
                                    <th>Not Answered</th>
                                    <th>Marked for Review</th>
                                    <th>Answered &amp; Marked for Review (will be considered for evaluation)</th>
                                    <th>Not Visited</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="25%">Static Mock Link for NEET PG Jan 2020 Session</td>
                                    <td width="12%">20</td>
                                    <td width="12%">20</td>
                                    <td width="12%">0</td>
                                    <td width="12%">0</td>
                                    <td width="12%">0</td>
                                    <td width="15%">0</td>
                                </tr>
                            </tbody>      
                        </table>
                    </div>

                    <div className="summary__page-btn-section">
                        <div className="btn-section btn-section-summary">
                            <div className="button-position">
                                <button className="btn btn__primary btn__primary--summary-next"> Next </button>  
                            </div>          
                        </div>
                    </div>
                    
                </div>
                <div className="footer-main">Version: 17.03.00</div>
            </div>
        </section>
    )
}

export default ExamSummary;