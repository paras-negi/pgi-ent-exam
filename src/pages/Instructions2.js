import React from "react";
import backwardIcon from '../Icons/backwardIcon.png';
import defaultIcon from '../Icons/defaultIcon.jpg';


function Instructions2({onRouteChange}){


    return(
        <section className="instr-page">
            <div className="common-page-layout">
              <div className="instr-page-header">
              
              </div>
              <div className="main-row">
                <div className="col-left">
                    <div className="instructions-heading">
                      <h1>Other Important Instructions:</h1>
                    </div>
                    <div className= "instruction-row instruction-row-2">
                      <div className="instruction-paragraph">
                          <strong> <u> Instructions for mock test: </u> </strong>
                            <ol>
                              <li className="li-margin-bottom"> This is a Mock test. The Questions displayed are for practice purpose only. Under no circumstances should this be presumed as sample questions. </li>
                              <li className="li-margin-bottom"> Mock test is meant for creating awareness about the test delivery system. Contents are for sample only and actual content will be different on the day of examination.
                              </li>
                            </ol>
                            <p><font size = "4">Some Important Instructions are as follows:</font></p>

                              <ol>
                                <li className="li-margin-bottom">Pen, Pencil, Cell phones, pagers, calculators, Pen Drives, Tablets or any electronic devices are strictly prohibited. Violation will lead to expulsion from the examination. No arrangements have been made at the exam centres for their custody. Candidates are advised not to bring them at the exam centre.</li>
                                <li className="li-margin-bottom">Improper conduct will entail expulsion from the examination. Failure to comply with the instructions will entail expulsion/ cancellation of candidature including appropriate legal action.</li>
                                <li className="li-margin-bottom">Follow the instruction given before or after any screen during the computer based exam.</li>
                                <li className="li-margin-bottom">Use of keyboard during the exam is strictly prohibited. Wherever use of keyboard is needed a virtual keyboard will appear on screen automatically.</li>
                                <li className="li-margin-bottom">Always maintain peace, discipline and silence during the examination process.</li>
                                <li className="li-margin-bottom">NBE reserves all rights to verify identity and genuineness of each candidate at the exam centre by taking thumb impression and photograph of the candidate or by any other means.</li>
                                <li className="li-margin-bottom">Late entry and/or early exit is not permitted.</li>
                                <li className="li-margin-bottom">Please read the information bulletin carefully.</li>
                              </ol>
                            </div>
                          </div>
                          <div className="nda">
                            <div className="nda-paragraph">
                              <span style={{display:"inline-block"}}>
                                <input type="checkbox" style={{marginTop:"1px", float:"left"}}/>
                                <span className="nda-heading"><strong>NON DISCLOSURE AGREEMENT (NDA)- NEET-PG</strong></span>
                              </span> <br/>

                              <div className="nda-paragraph-list">
                              <ol style={{listStyle:"none"}}>
                                <li className="li-margin-bottom"> 
                                  NEET- PG is a proprietary examination and is conducted by National Board of Examinations. 
                                  The contents of this exam are confidential, proprietary and are owned by National Board of Examinations.
                                  NBE explicitly prohibits the candidate from publishing, reproducing or transmitting any or some contents of this test, 
                                  in whole or in part, in any form or by any means verbal or written, electronic or mechanical or for any purpose. </li>
                                <li className="li-margin-bottom">
                                  No content of this exam must be shared with friends, acquaintances or third parties including sharing through 
                                  online means or via social media. Social media includes but not limited to <strong>SMS, Whatsapp, Facebook, Twitter, 
                                  Hangouts, Blogs </strong>etc using either one’s own account or proxy account(s).
                                </li>
                                <li className="li-margin-bottom">
                                  By registering for and /or 
                                  appearing in NEET-PG the candidate explicitly agrees to the above Non Disclosure Agreement and general terms of use 
                                  for NEET- PG as contained in this Information Bulletin &amp; NEET- PG website <strong><a href="http://www.nbe.edu.in/">
                                  www.nbe.edu.in</a>
                                  </strong>.
                                </li>
                                <li className="li-margin-bottom">
                                  Violation of any act or breach of the same shall be liable for penal action and 
                                  cancellation of the candidature at the bare threshold.
                                </li>
                                <li className="li-margin-bottom">
                                  I have read and understood the instructions.
                                  All computer hardware allotted to me are in proper working condition. I declare that I am not in possession of / not 
                                  wearing / not carrying any prohibited gadget like mobile phone, bluetooth devices etc. /any prohibited material with
                                  me into the Examination Hall.I agree that in case of not adhering to the instructions, I shall be liable to be 
                                  debarred from this Test and/or to disciplinary action, which may include ban from future Tests / Examinations.
                                </li>
                              </ol>
                            </div>
                              
                            
                              </div>
                          </div>
                  <div className="btn-section">
                      <button onClick={()=>onRouteChange('instructions')} className="btn btn__primary btn__primary--prev"> 
                        <span>
                        <img alt="forward-icon" className="forward-icon" src={backwardIcon}/></span>
                          <strong>
                            <span>Previous</span>
                          </strong>
                      </button>
                      <center><button onClick={()=>onRouteChange('testpage')} className="btn btn__primary btn__primary--blue">I am ready to begin</button></center>
                  </div>
                </div>
                <div className="col-right">
                  <div className= "section-rt-main">
                    <div>
                      <center>
                        <img className="candidate-photo" alt="i" src={defaultIcon}/>
                      </center>
                      </div>
                      <div className="candidate-name"> John Smith </div>
                  </div>
                </div>
              </div>
              <div className="footer-main"> Version : 2.4.0.0 </div>
              </div>
          </section>

    )
}

export default Instructions2;