export const response = {
    "paperID": 1974,
    "topicName": "All India AIIMS PG Exam",
    "userName": "Dr. Paras Negi",
    "fullScreenMode": false,
    "time": 180,//min
    "totalMarks": 600,
    "totalQuestions": 200,
    "nightmode": 1,
    "fontsize": 100,
    "attemptStatus": "1",
    "showGuessButton": 1,
    "ShowTimer": 1,
    "showPauseButton": 1,
    "correctMarks": 4,
    "incorrectMarks": 1,
    "showInstructionsPage": 1,
    "instructionsHTML": "<p>test instructions</p>",
    "isCustomModule": 0,
    "data": [
          {
            "qid": 1,
            "noOfComponents": 0,
            "questionFilter": 1,
            "qtext": "<style type=\"text/css\">.blueText,.grayText,.greenText,.redText{display:block!important}.optionTable{margin:0 0 10px;border-radius:10px;box-shadow:1px 1px 5px 0 #ddd;background-color:#fff}.optionTableTdStyle{padding-left:0;padding-top:0;padding-bottom:0;position:relative}.optionTableGray{border:thin solid #ddd!important}.optionTableBlue{border:thin solid #479cd3!important}.optionTableGreenSolution{border:thin solid #80c244!important}.optionTableGreen{border:thin solid #80c244!important;background-color:#f0f8e9}.optionTableRed{border:thin solid #da5b59!important;background-color:#fcf4f4}.optionLabel{color:#fff;border-radius:50%;width:20px;height:20px;font-weight:700;font-size:50%;text-align:center;line-height:20px}.optionLabelGray{background-color:#474747}.optionLabelBlue{background-color:#479cd3 !important}.optionLabelGreen{background-color:#80c244 !important}.optionLabelRed{background-color:#da5b59 !important}.optionStats{position:absolute;right:5px;font-size:60%}.optionTopStats{top:5px}.optionBottomStats{bottom:2px;font-weight:700}.grayText{color:#474747}.blueText{color:#479cd3}.greenText{color:#80c244}.redText{color:#da5b59}.table-striped>tbody>tr:nth-child(odd)>td,.table-striped>tbody>tr:nth-child(odd)>th{background-color:rgba(184,218,255,.2)!important}.table-striped>tbody>tr:nth-child(even)>td,.table-striped>tbody>tr:nth-child(even)>th{background-color:#fff!important;color:#333}.table{width:100%;margin:15px 0;border:none}.borderless td,.borderless th{border-bottom:none;border-left:none;border-top:none;padding:10px}.ulClass{list-style-type:none;padding-left:0}.ulClass li{margin:10px 0}.ulClass li:before{background-color:#128adc;border-radius:50%;content:\"\";display:inline-block;margin-right:10px;margin-bottom:2px;height:6px;width:6px}.smallOption{font-size:75%}.smallOptionLabel{width:15px;height:15px;line-height:15px}.table-striped>tbody>tr:nth-child(1)>td,.table-striped>tbody>tr:nth-child(1)>th{background-color:rgba(184,218,255,.8)!important}</style><p>A patient with a deep stab wound in the middle of the forearm has impaired movement of the thumb. Examination indicates a lesion of the anterior interosseous nerve. Which of the following muscles is paralyzed?</p>",
            "previewtext": ".blueText,.grayText,.greenText",
            "marks": "+1.::.-0.33",
            "guessEnabled": true,
            "options": [
              "<p>Flexor pollicis longus and brevis</p>",
              "<p>F lexor digitorum longus and the opponent </p>",
              "<p>Flexor digitorum profundus and pronator quadratus</p>",
              "<p>Flexor digitorum profundus and superficialis</p>"
            ],
            "isMultipleOptions": false,
            "sectionName": ""
          },
          {
            "qid": 2,
            "noOfComponents": 0,
            "questionFilter": 1,
            "qtext": "<style type=\"text/css\">.blueText,.grayText,.greenText,.redText{display:block!important}.optionTable{margin:0 0 10px;border-radius:10px;box-shadow:1px 1px 5px 0 #ddd;background-color:#fff}.optionTableTdStyle{padding-left:0;padding-top:0;padding-bottom:0;position:relative}.optionTableGray{border:thin solid #ddd!important}.optionTableBlue{border:thin solid #479cd3!important}.optionTableGreenSolution{border:thin solid #80c244!important}.optionTableGreen{border:thin solid #80c244!important;background-color:#f0f8e9}.optionTableRed{border:thin solid #da5b59!important;background-color:#fcf4f4}.optionLabel{color:#fff;border-radius:50%;width:20px;height:20px;font-weight:700;font-size:50%;text-align:center;line-height:20px}.optionLabelGray{background-color:#474747}.optionLabelBlue{background-color:#479cd3 !important}.optionLabelGreen{background-color:#80c244 !important}.optionLabelRed{background-color:#da5b59 !important}.optionStats{position:absolute;right:5px;font-size:60%}.optionTopStats{top:5px}.optionBottomStats{bottom:2px;font-weight:700}.grayText{color:#474747}.blueText{color:#479cd3}.greenText{color:#80c244}.redText{color:#da5b59}.table-striped>tbody>tr:nth-child(odd)>td,.table-striped>tbody>tr:nth-child(odd)>th{background-color:rgba(184,218,255,.2)!important}.table-striped>tbody>tr:nth-child(even)>td,.table-striped>tbody>tr:nth-child(even)>th{background-color:#fff!important;color:#333}.table{width:100%;margin:15px 0;border:none}.borderless td,.borderless th{border-bottom:none;border-left:none;border-top:none;padding:10px}.ulClass{list-style-type:none;padding-left:0}.ulClass li{margin:10px 0}.ulClass li:before{background-color:#128adc;border-radius:50%;content:\"\";display:inline-block;margin-right:10px;margin-bottom:2px;height:6px;width:6px}.smallOption{font-size:75%}.smallOptionLabel{width:15px;height:15px;line-height:15px}.table-striped>tbody>tr:nth-child(1)>td,.table-striped>tbody>tr:nth-child(1)>th{background-color:rgba(184,218,255,.8)!important}</style><p>A patient with a deep stab wound in the middle of the forearm has impaired movement of the thumb. Examination indicates a lesion of the anterior interosseous nerve. Which of the following muscles is paralyzed?</p>",
            "previewtext": ".blueText,.grayText,.greenText",
            "marks": "+1.::.-0.33",
            "guessEnabled": true,
            "options": [
              "<p>Flexor pollicis lo</p>",
              "<p> opponent's thumb </ p>" ,
              "<p> quadratus</p>",
              "<p>superficialis</p>"
            ],
            "isMultipleOptions": false,
            "sectionName": ""
          },
          {
            "qid": 3,
            "noOfComponents": 0,
            "questionFilter": 1,
            "qtext": "<style type=\"text/css\">.blueText,.grayText,.greenText,.redText{display:block!important}.optionTable{margin:0 0 10px;border-radius:10px;box-shadow:1px 1px 5px 0 #ddd;background-color:#fff}.optionTableTdStyle{padding-left:0;padding-top:0;padding-bottom:0;position:relative}.optionTableGray{border:thin solid #ddd!important}.optionTableBlue{border:thin solid #479cd3!important}.optionTableGreenSolution{border:thin solid #80c244!important}.optionTableGreen{border:thin solid #80c244!important;background-color:#f0f8e9}.optionTableRed{border:thin solid #da5b59!important;background-color:#fcf4f4}.optionLabel{color:#fff;border-radius:50%;width:20px;height:20px;font-weight:700;font-size:50%;text-align:center;line-height:20px}.optionLabelGray{background-color:#474747}.optionLabelBlue{background-color:#479cd3 !important}.optionLabelGreen{background-color:#80c244 !important}.optionLabelRed{background-color:#da5b59 !important}.optionStats{position:absolute;right:5px;font-size:60%}.optionTopStats{top:5px}.optionBottomStats{bottom:2px;font-weight:700}.grayText{color:#474747}.blueText{color:#479cd3}.greenText{color:#80c244}.redText{color:#da5b59}.table-striped>tbody>tr:nth-child(odd)>td,.table-striped>tbody>tr:nth-child(odd)>th{background-color:rgba(184,218,255,.2)!important}.table-striped>tbody>tr:nth-child(even)>td,.table-striped>tbody>tr:nth-child(even)>th{background-color:#fff!important;color:#333}.table{width:100%;margin:15px 0;border:none}.borderless td,.borderless th{border-bottom:none;border-left:none;border-top:none;padding:10px}.ulClass{list-style-type:none;padding-left:0}.ulClass li{margin:10px 0}.ulClass li:before{background-color:#128adc;border-radius:50%;content:\"\";display:inline-block;margin-right:10px;margin-bottom:2px;height:6px;width:6px}.smallOption{font-size:75%}.smallOptionLabel{width:15px;height:15px;line-height:15px}.table-striped>tbody>tr:nth-child(1)>td,.table-striped>tbody>tr:nth-child(1)>th{background-color:rgba(184,218,255,.8)!important}</style><p>A patient with a deep stab wound in the middle of the forearm has impaired movement of the thumb. Examination indicates a lesion of the anterior interosseous nerve. Which of the following muscles is paralyzed?</p>",
            "previewtext": ".blueText,.grayText,.greenText",
            "marks": "+1.::.-0.33",
            "guessEnabled": true,
            "options": [
              "<p>Flexor pollicis</p>",
              "<p> Flex opponent's thumb </ p>" ,
              "<p>Flexor digitorum pronator quadratus</p>",
              "<p>Flexor digitorumd superficialis</p>"
            ],
            "isMultipleOptions": false,
            "sectionName": ""
          }
    ]
  }